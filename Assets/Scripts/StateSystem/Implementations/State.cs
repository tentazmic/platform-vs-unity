﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using StateSystem.Transitions;

namespace StateSystem.Implementations
{
	/// <summary>
	/// A simple implementation of the IState interface
	/// </summary>
	public abstract class State : IState
	{
		protected State(int[] transitionIds, IDictionary<int, ITransition> specialTransitions = null)
		{
			TransitionIDs = transitionIds;
			SpecialTransitions = new ReadOnlyDictionary<int, ITransition>(specialTransitions ?? new Dictionary<int, ITransition>());
		}
		
		#region IState

		public StateEscapeType EscapeType { get; protected set; } = StateEscapeType.None;
		public virtual bool EscapeStackExecution => true;

		public virtual bool AreTransitionsEnabled => true;

		public int[] TransitionIDs { get; }
		public IReadOnlyDictionary<int, ITransition> SpecialTransitions { get; }

		public abstract bool ReadyForEntry(bool isTransitioningFromAState);

		public abstract void Enter();

		public abstract void Update();
		
		public virtual void Exit() { }
		
		#endregion // IState
	}
}