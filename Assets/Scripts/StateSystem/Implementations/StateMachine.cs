﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using StateSystem.Transitions;

namespace StateSystem.Implementations
{
	/// <summary>
	/// The default implementation of the IStateMachine interface
	/// </summary>
	public class StateMachine : IStateMachine
	{
		Action<IStateMachine> _enterEvent;

		public StateMachine(IDictionary<int, IState> states)
		{
			DefaultState = states.First().Value;
			States = new ReadOnlyDictionary<int, IState>(states);
			
			TransitionStates = new Dictionary<int, IState>();
			Transition(States.First().Value);
		}
		
		#region IStateMachine

		public event Action<IStateMachine> OnEnter
		{
			add => _enterEvent += value;
			remove => _enterEvent -= value;
		}

		public IState DefaultState { get; }
		public IState Current { get; private set; }
		public IReadOnlyDictionary<int, IState> States { get; }
		public IDictionary<int, IState> TransitionStates { get; }

		public void StateUpdate()
		{
			Current?.Update();
			this.ProcessStateEscape();
		}

		public bool FindNextState(out IState next) => this.DFindNextState(out next);

		public void Transition(IState next)
		{
			this.DTransition(next);
			Current = next;
			_enterEvent?.Invoke(this);
		}

		public void Clear() => this.DClear();

		#endregion // IStateMachine
	}
}