﻿using System.Collections.Generic;

namespace StateSystem.Implementations
{
	/// <summary>
	/// A basic implementation of the IStateMachineStack interface
	/// </summary>
	public class StateMachineStack : IStateMachineStack
	{
		readonly List<IStateMachine> _stateMachines;
		readonly IStateMachineStackConfig _stackConfig;

		public IReadOnlyCollection<IStateMachine> StateMachines => _stateMachines.AsReadOnly();
		
		public StateMachineStack(IStateMachineStackConfig stackConfig = null, params IStateMachine[] machines)
		{
			_stateMachines = new List<IStateMachine>();
			_stackConfig = stackConfig;
			foreach (var machine in machines)
				_stateMachines.Add(machine);
		}
		
		public void ExecuteMachine()
		{
			foreach (var machine in _stateMachines)
			{
				machine.StateUpdate();

				if (!(machine.Current == null || machine.Current.EscapeStackExecution))
					break;
			}
		}

		public void Add(IStateMachine machine)
		{
			_stateMachines.Add(machine);
			_stackConfig?.OrderList(_stateMachines);
		}

		public void Remove(IStateMachine machine)
		{
			_stateMachines.Remove(machine);
			_stackConfig?.OrderList(_stateMachines);
		}

		public void Clear() => _stateMachines.Clear();
	}
}