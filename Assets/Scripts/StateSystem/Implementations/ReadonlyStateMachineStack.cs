﻿using System.Collections.Generic;

namespace StateSystem.Implementations
{
	public class ReadonlyStateMachineStack
	{
		readonly IStateMachineStack _source;

		public ReadonlyStateMachineStack(IStateMachineStack source)
		{
			_source = source;
		}

		public IReadOnlyCollection<IStateMachine> StateMachines => _source.StateMachines;
	}
}