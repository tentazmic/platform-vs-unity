﻿using System.Linq;
using StateSystem.Transitions;

namespace StateSystem
{
	public static class DImpIStateMachine
	{
		public static void ProcessStateEscape(this IStateMachine machine)
		{
			switch (machine.Current?.EscapeType)
			{
				case StateEscapeType.Default:
					machine.Clear();
					break;
					
				case StateEscapeType.Next:
					var pair = machine.TransitionStates.Count == 0 ? machine.States.First() : machine.TransitionStates.First();
					machine.Transition(pair.Value);
					break;

				case StateEscapeType.Find:
				{
					machine.FindNextState(out var next);
					machine.Transition(next);
				}
					break;
				
				case StateEscapeType.None:
				default:
				{
					if (machine.FindNextState(out var next))
						machine.Transition(next);
				}
					break;

			}
		}
		
		public static void DOnStateEscape(this IStateMachine machine, StateEscapeType escapeType)
		{
			switch (escapeType)
			{
				case StateEscapeType.Next:
					var transitionStates = machine.TransitionStates;
					var idStatePair = transitionStates.Count == 0 ? machine.States.First() : transitionStates.First();
					machine.Transition(idStatePair.Value);
					break;
				case StateEscapeType.Find:
					machine.FindNextState(out var next);
					machine.Transition(next);
					break;
				case StateEscapeType.Default:
					machine.Clear();
					break;
				case StateEscapeType.None:
					break;
			}
		}

		/// <summary>
		/// Finds the next valid state to transition from the current one
		/// </summary>
		/// <param name="machine"></param>
		/// <param name="next"></param>
		/// <returns>Have we found a state to transition to</returns>
		public static bool DFindNextState(this IStateMachine machine, out IState next)
		{
			var current = machine.Current;
			if (!(current is {AreTransitionsEnabled: true}))
			{
				next = machine.DefaultState;
				return false;
			}

			var transitionStates = machine.TransitionStates;
			var transitions = current.SpecialTransitions;
			foreach (var stateEntry in transitionStates)
			{
				var id = stateEntry.Key;
				var state = stateEntry.Value;
				
				// TODO : There should be a way to structure this in a way that avoids repetition
				if (transitions.ContainsKey(id))
				{
					var transition = transitions[id];

					if (transition.Evaluate(current))
					{
						if (transition.OverridesStatesCheck)
						{
							next = state;
							return true;
						}
					}
					else
						continue;
				}

				if (!state.ReadyForEntry(true))
					continue;
				// TODO END

				next = state;
				return true;
			}

			next = machine.DefaultState;
			return false;
		}

		public static void DTransition(this IStateMachine machine, IState next)
		{
			machine.Current?.Exit();

			if (next == null)
			{
				
				return;
			}

			var ids = next.TransitionIDs;
			
			machine.TransitionStates.Clear();
			foreach (var id in ids)
			{
				if (!machine.States.ContainsKey(id)) 
					continue;
				machine.TransitionStates.Add(id, machine.States[id]);
			}

			next.Enter();
		}

		public static void DClear(this IStateMachine machine) 
			=> machine.Transition(machine.States.First().Value);
	}
}