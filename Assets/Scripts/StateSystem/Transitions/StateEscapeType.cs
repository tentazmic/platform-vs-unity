﻿namespace StateSystem.Transitions
{
	[System.Serializable]
	public enum StateEscapeType
	{
		/// No Escape
		None,

		/// Escape to the default state of the state machine
		Default,

		/// Escape to the default state of the current states transitions
		Next,

		/// Escape, then test the entry conditions of all transition states
		Find
	}

	public static class EStateEscapeType
	{
		public static bool NoEscape(this StateEscapeType type) => type == StateEscapeType.None;
	}
}