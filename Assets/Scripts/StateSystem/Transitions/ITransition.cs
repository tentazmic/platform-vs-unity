﻿namespace StateSystem.Transitions
{
	/// <summary>
	/// Template for an extra condition to transition between two states
	/// </summary>
	public interface ITransition
	{
		bool OverridesStatesCheck { get; }
		
		bool Evaluate(IState current);
	}
}