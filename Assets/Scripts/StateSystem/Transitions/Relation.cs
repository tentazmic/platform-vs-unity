﻿using UnityEngine;
using static UnityEngine.Mathf;

namespace StateSystem.Transitions
{
	/// Defines basic mathematical logic
	[System.Serializable]
	public enum Relation
	{
		Equals,
		NotEquals,
		GreaterThan,
		LessThan,
		GreaterThanOrEqual,
		LessThanOrEqual
	}

	public static class ERelation
	{
		public static bool Evaluate(this Relation relation, int a, int b) =>
			relation switch
			{
				Relation.GreaterThan => a > b,
				Relation.GreaterThanOrEqual => a >= b,
				Relation.LessThan => a < b,
				Relation.LessThanOrEqual => a <= b,
				Relation.Equals => a == b,
				Relation.NotEquals => a != b,
				_ => false
			};
		
		public static bool Evaluate(this Relation relation, float a, float b) =>
			relation switch
			{
				Relation.GreaterThan => a > b,
				Relation.GreaterThanOrEqual => a >= b,
				Relation.LessThan => a < b,
				Relation.LessThanOrEqual => a <= b,
				Relation.Equals => Approximately(a, b),
				Relation.NotEquals => !Approximately(a, b),
				_ => false
			};
	}
}