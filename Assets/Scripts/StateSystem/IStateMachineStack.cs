﻿using System.Collections.Generic;

namespace StateSystem
{
	/// <summary>
	/// Defines methods for a State Machine Stack
	/// A state machine stack runs multiple state machines sequentially
	/// </summary>
	public interface IStateMachineStack
	{
		IReadOnlyCollection<IStateMachine> StateMachines { get; }
		
		void ExecuteMachine();
		
		void Add(IStateMachine machine);
		void Remove(IStateMachine machine);
		void Clear();
	}
}