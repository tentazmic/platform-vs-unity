﻿using System;
using System.Collections.Generic;

namespace StateSystem
{
	public interface IStateMachine
	{
		event Action<IStateMachine> OnEnter;
		
		IState DefaultState { get; }

		IState Current { get; }

		IReadOnlyDictionary<int, IState> States { get; }
		IDictionary<int, IState> TransitionStates { get; }

		void StateUpdate();

		bool FindNextState(out IState next);

		void Transition(IState next);

		void Clear();
	}
}