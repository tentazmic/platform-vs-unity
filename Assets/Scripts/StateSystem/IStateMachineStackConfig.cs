﻿using System.Collections.Generic;

namespace StateSystem
{
	public interface IStateMachineStackConfig
	{
		void OrderList(IList<IStateMachine> stack);
	}
}