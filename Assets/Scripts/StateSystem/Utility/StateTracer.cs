﻿using System.Collections.Generic;
using System.Linq;

namespace StateSystem.Utility
{
	/// <summary>
	/// Tracks the entered states of a state machine
	/// </summary>
	public static class StateTracer
	{
		static readonly Dictionary<IStateMachine, List<string>> MachineToTraceDictionary = new Dictionary<IStateMachine, List<string>>();


		/// <summary>
		/// Cache's a state machine to trace
		/// </summary>
		/// <param name="machine"></param>
		/// <returns>Was the machine cached?</returns>
		public static bool BeginStateMachineTrace(IStateMachine machine)
		{
			if (MachineToTraceDictionary.ContainsKey(machine))
				return false;

			MachineToTraceDictionary.Add(machine, new List<string>());
			machine.OnEnter += OnStateEnter;
			
			OnStateEnter(machine);

			return true;
		}

		/// <summary>
		/// Ends the tracing, removes the state from the cache, and returns the used states
		/// </summary>
		/// <param name="machine"></param>
		/// <returns>The states the machine had used since it began</returns>
		public static string FinishStateTrace(IStateMachine machine)
		{
			return !MachineToTraceDictionary.ContainsKey(machine) 
				? "" 
				: MachineToTraceDictionary[machine].Aggregate(((s, s1) => s + " " + s1));
		}

		static void OnStateEnter(IStateMachine machine)
		{
			if (!MachineToTraceDictionary.ContainsKey(machine))
			{
				machine.OnEnter -= OnStateEnter;
				return;
			}
			
			MachineToTraceDictionary[machine].Add(machine.Current?.GetType().Name);
		}
	}
}