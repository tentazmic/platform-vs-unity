﻿using System.Collections.Generic;
using System.Linq;
using StateSystem.Transitions;

namespace StateSystem.Utility
{
	public static class StateMachineEx
	{
		public static readonly KeyValuePair<int, IState> KNullState = new KeyValuePair<int, IState>(-1, null);  
		
		public static bool FindAnyStateToEnter(this IStateMachine machine, out IState foundState)
		{
			foreach (var entry in machine.States)
			{
				var state = entry.Value;
				if (state == null)
					continue;
				
				if (!state.ReadyForEntry(machine.Current != null))
					continue;
				
				foundState = state;
				return true;
			}

			foundState = null;
			return false;
		}
		
		public static IState OnStateEscapeWithNullAsDefault(this IStateMachine machine, StateEscapeType escapeType)
		{
			switch (escapeType)
			{
				case StateEscapeType.Next:
					var transitionStates = machine.TransitionStates;
					return transitionStates.Count == 0 ? null : transitionStates.First().Value;
				
				case StateEscapeType.Find:
					machine.FindNextState(out var next);
					return next;

				case StateEscapeType.Default:
				default:
					return machine.States.First().Value;
			}
		}
	}
}