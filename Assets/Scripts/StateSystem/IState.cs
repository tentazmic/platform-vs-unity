﻿using System.Collections.Generic;
using StateSystem.Transitions;

namespace StateSystem
{
	/// <summary>
	/// Interface for States. Should act as a System in an ECS model (no held state)
	/// </summary>
	public interface IState
	{
		StateEscapeType EscapeType { get; }
		
		/// If this state's machine is being used in a stack, should the lower states be called after this one?
		bool EscapeStackExecution { get; }
		
		bool AreTransitionsEnabled { get; }
		
		int[] TransitionIDs { get; }
		IReadOnlyDictionary<int, ITransition> SpecialTransitions { get; }

		bool ReadyForEntry(bool isTransitioningFromAState);

		void Enter();
		void Update();
		void Exit();
	}
}