﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PlatformVS.Core.Input.Motions;
using PlatformVS.Core.Input.Motions.Charge;
using PlatformVS.Core.Input.Units;
using UnityEngine;

namespace Tests.EditModeTests
{
	public class MotionIdentification
	{
		static readonly IChargeCache KNullChargeCache = new NullChargeCache(); 
		
		// MotionUnit GetMotionUnit(IEnumerable<Vector2> trail, bool canCharge = true)
		// {
		// 	var stick = new InputStick();
		// 	var unit = new MotionUnit(stick, canCharge);
		// 	foreach (var vector2 in trail)
		// 	{
		// 		stick.Value = vector2;
		// 		unit.Tick();
		// 	}
		// 	
		// 	return unit;
		// }
		
		[Test]
		public void T00_QCClockwise()
		{
			var trail = new[] {Vector2.up, new Vector2(-1, 1), Vector2.left};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Up, MotionDirection.UpForward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T01_QCAntiClockwise()
		{
			var trail = new[] {Vector2.up, new Vector2(1, 1), Vector2.right};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Down, MotionDirection.DownForward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T02_DSUp()
		{
			var trail = new[] {Vector2.up, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.up };
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Forward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T03_DSBack()
		{
			var trail = new[] {Vector2.left, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero,  Vector2.left};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Forward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T04_DragonClockwise()
		{
			var trail = new[] {Vector2.right, new Vector2(1, -1), Vector2.one};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.UpForward, MotionDirection.DownForward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T05_DragonAntiClockwise()
		{
			var trail = new[] {Vector2.right, Vector2.one, new Vector2(1, -1)};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.DownForward, MotionDirection.UpForward, MotionDirection.Forward}, directions);
		}
		
		
		[Test]
		public void T06_DSBad()
		{
			var trail = new[] {Vector2.left, Vector2.zero, Vector2.down};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Down, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T07_DSBad()
		{
			var trail = new[] {Vector2.left, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero,  Vector2.up};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreNotEqual(new [] {MotionDirection.Forward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T08_DSBad()
		{
			var trail = new[] {Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreNotEqual(new [] {MotionDirection.Forward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T09_DragonClockwiseGaps()
		{
			var trail = new[] {Vector2.right, Vector2.right, Vector2.right, Vector2.right, Vector2.right, new Vector2(1, -1), Vector2.one};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.UpForward, MotionDirection.DownForward, MotionDirection.Forward}, directions);
		}
		
		[Test]
		public void T20_SimpleDown()
		{
			var trail = new[] {Vector2.down, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			Debug.Log("A: " + angle + " X Mult: " + xMult);
			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.Forward}, directions);
		}
		
		
		[Test]
		public void T21_SimpleDown()
		{
			var trail = new[] {new Vector2(1, -1), Vector2.down, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero};
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(KNullChargeCache, angle, xMult, trail).ToArray();
			var directions = steps.Select(step => step.direction);
			Debug.Log(directions.Aggregate("", (s, direction) => s + " " + direction));
			
			Assert.AreEqual(new [] {MotionDirection.DownForward, MotionDirection.Forward}, directions);
		}

		const string KQuarterCircle = "QuarterCircle";
		const string KChargeQuarterCircle = "ChargeQuarterCircle";
		const string KDoubleSnap = "DoubleSnap";
		const string KCharge = "ChargeStraight";
		const string KDragon = "Dragon";

		[Test]
		public void T30_VerifyQC()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Down}, 
				new MotionStep {direction = MotionDirection.DownForward},
				new MotionStep {direction = MotionDirection.Forward}
			};

			if (!MotionLibrary.GetMotions(steps, out var motion))
				Assert.Fail();
			Assert.AreEqual(KQuarterCircle, motion[0].Definition.name);
		}
		
		[Test]
		public void T31_VerifyQCAntiClockwise()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Up}, 
				new MotionStep {direction = MotionDirection.UpForward},
				new MotionStep {direction = MotionDirection.Forward}
			};

			if (!MotionLibrary.GetMotions(steps, out var motion))
				Assert.Fail();
			Assert.AreEqual(KQuarterCircle, motion[0].Definition.name);
		}
		
		[Test]
		public void T32_NotDoubleSnap()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Up}, 
				new MotionStep {direction = MotionDirection.UpForward},
				new MotionStep {direction = MotionDirection.Forward}
			};

			if (!MotionLibrary.GetMotions(steps, out var motion))
				Assert.Fail();
			Assert.AreNotEqual(KDoubleSnap, motion[0].Definition.name);
		}
		
		[Test]
		public void T33_Charge()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Backward, chargeTime = 30}, 
				new MotionStep {direction = MotionDirection.Forward}
			};

			if (!MotionLibrary.GetMotions(steps, out var motion))
				Assert.Fail();
			Assert.AreEqual(KCharge, motion[0].Definition.name);
		}

		[Test]
		public void T34_QC()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.DownForward},
				new MotionStep {direction = MotionDirection.Forward}
			};
			
			if (!MotionLibrary.GetMotions(steps, out var motions))
				Assert.Fail();
			
			Assert.True(motions.Any(motion => motion.Definition.name == KQuarterCircle));
		}
		
		[Test]
		public void T35_Dragon()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.DownForward},
				new MotionStep {direction = MotionDirection.UpForward},
				new MotionStep {direction = MotionDirection.Forward}
			};
			
			if (!MotionLibrary.GetMotions(steps, out var motions))
				Assert.Fail();
			
			Assert.True(motions.Any(motion => motion.Definition.name == KDragon));
		}
		
		[Test]
		public void T36_DQC()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Down, chargeTime = 40},
				new MotionStep {direction = MotionDirection.DownForward},
				new MotionStep {direction = MotionDirection.Forward}
			};
			
			if (!MotionLibrary.GetMotions(steps, out var motions))
				Assert.Fail();
			
			Assert.True(motions.Aggregate(false, (b, motion) => b || motion.Definition.name == KChargeQuarterCircle || motion.Definition.name == KQuarterCircle));
		}
		
		[Test]
		public void T37_BadDragon()
		{
			MotionLibrary.OnLoad();
			var steps = new[]
			{
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.Down},
				new MotionStep {direction = MotionDirection.DownForward},
				new MotionStep {direction = MotionDirection.Forward}
			};
			
			if (!MotionLibrary.GetMotions(steps, out var motions))
				Assert.Fail();
			
			Assert.False(motions.Any(motion => motion.Definition.name == KDragon));
		}

		[Test]
		public void T50_Charge()
		{
			const int chargeTime = 30;
			var trail = new List<Vector2> {Vector2.up, Vector2.zero};

			for (var i = 0; i < chargeTime; i++)
				trail.Add(Vector2.down);


			var charge = new ChargeCache();
			foreach (var input in trail)
			{
				charge.AddCharge(input);
				charge.Tick();
			}
			
			var (angle, xMult) = MotionUnit.GetTransformDataFromVector(trail[0]);

			var steps = MotionUnit.FindMotionSteps(charge, angle, xMult, trail.ToArray()).ToArray();
			Debug.Log(steps.Aggregate("", (s, step) => $"{s} {step};"));
			
			Assert.AreEqual(new []
			{
				new MotionStep {direction = MotionDirection.Backward, chargeTime = chargeTime},
				new MotionStep {direction = MotionDirection.Forward}
			}, steps);
		}
	}
}