﻿using UniBoost.Input.Implementations;
using PlatformVS.Core.Input;
using PlatformVS.Core.Input.Motions.Charge;
using PlatformVS.Core.Input.Units;
namespace Tests.EditModeTests
{
	public class TestInput : IInput
	{
		public bool IsActive { get; set; }
		public InputStick Control { get; }
		public IChargeCache ChargeCache { get; }
		public MotionUnit Motion { get; }
		public InputButton Modifier { get; }
		public InputButton Jump { get; }
		public InputButton Light { get; }
		public InputButton Medium { get; }
		public InputButton Heavy { get; }
		public InputButton Super { get; }
		public InputButton Grab { get; }
		public InputButton Shield { get; }

		public TestInput()
		{
			Control = new InputStick();
			ChargeCache = new ChargeCache();
			Motion = new MotionUnit(Control, ChargeCache);
			Modifier = new InputButton();
			Jump = new InputButton();
			Light = new InputButton();
			Medium = new InputButton();
			Heavy = new InputButton();
			Super = new InputButton();
			Grab = new InputButton();
			Shield = new InputButton();
		}
	}
}