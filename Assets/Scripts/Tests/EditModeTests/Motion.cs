﻿using NUnit.Framework;
using PlatformVS.Core.VSEntity;
using UnityEngine;
using static PlatformVS.Core.Input.Motions.EMotionDirection;
using static PlatformVS.Core.Input.Motions.MotionDirection;

namespace Tests.EditModeTests
{
	public class Motion
	{
		[Test]
		public void T00_Forward()
		{
			var vector = Vector2.right;
			Assert.AreEqual(Forward, GetDirectionFromVector(vector));
		}
		
		[Test]
		public void T01_Back()
		{
			var vector = Vector2.left;
			Assert.AreEqual(Backward, GetDirectionFromVector(vector));
		}
		
		[Test]
		public void T04_FRightUForward()
		{
			var vector = Vector2.one;
			Assert.AreEqual(UpForward, GetDirectionFromVector(vector));
		}
		
		[Test]
		public void T05_DForward()
		{
			var vector = new Vector2(1, -0.94f);
			Assert.AreEqual(DownForward, GetDirectionFromVector(vector));
		}
		
		[Test]
		public void T06_FRightDBack()
		{
			var vector = new Vector2(-0.5f, -0.94f);
			Assert.AreEqual(DownBackward, GetDirectionFromVector(vector));
		}
	}
}