﻿using SysType = System.Type;

namespace UnityType.Extensions
{
	public static class TypeEx
	{
		/// <summary>
		/// Checks whether the type is derivative of a generic class without specifying its type parameter.
		/// </summary>
		/// <param name="typeToCheck">The type to check.</param>
		/// <param name="generic">The generic class without type parameter.</param>
		/// <returns>True if the type is subclass of the generic class.</returns>
		/// <example><code>
		/// class Base&lt;T> { }
		/// class IntDerivative : Base&lt;int> { }
		/// class StringDerivative : Base&lt;string> { }
		///
		/// bool intIsSubclass = typeof(IntDerivative).IsSubclassOfRawGeneric(typeof(Base&lt;>)); // true
		/// bool stringIsSubclass = typeof(StringDerivative).IsSubclassOfRawGeneric(typeof(Base&lt;>)); // true
		/// </code></example>
		public static bool IsSubclassOfRawGeneric(this SysType typeToCheck, SysType generic)
		{
			while (typeToCheck != null && typeToCheck != typeof(object))
			{
				UniType cur = typeToCheck.IsGenericType ? typeToCheck.GetGenericTypeDefinition() : typeToCheck;

				if (generic == cur)
					return true;

				typeToCheck = typeToCheck.BaseType;
			}

			return false;
		}
		
		/// <summary>
		/// Checks whether the type inherits from the base type.
		/// </summary>
		/// <param name="typeToCheck">The type to check.</param>
		/// <param name="baseType">
		/// The base type to check inheritance from. It can be a generic type without the type parameter.
		/// </param>
		/// <returns>Whether <paramref name="typeToCheck"/>> inherits <paramref name="baseType"/>.</returns>
		/// <example><code>
		/// class Base&lt;T> { }
		/// class IntDerivative : Base&lt;int> { }
		///
		/// bool isAssignableWithTypeParam = typeof(typeof(Base&lt;int>).IsAssignableFrom(IntDerivative)); // true
		/// bool isAssignableWithoutTypeParam = typeof(typeof(Base&lt;>)).IsAssignableFrom(IntDerivative); // false
		/// bool inherits = typeof(IntDerivative).Inherits(typeof(Base&lt;>)); // true
		/// </code></example>
		public static bool InheritsFrom(this SysType typeToCheck, SysType baseType)
		{
			bool subClassOfRawGeneric = false;
			if (baseType.IsGenericType)
				subClassOfRawGeneric = typeToCheck.IsSubclassOfRawGeneric(baseType);

			return baseType.IsAssignableFrom(typeToCheck) || subClassOfRawGeneric;
		}
	}
}