﻿using System.Collections.Generic;

namespace UnityType
{
	public class TypeComparer : IEqualityComparer<UniType>
	{
		public bool Equals(UniType x, UniType y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null)) return false;
			if (ReferenceEquals(y, null)) return false;
			if (x.GetType() != y.GetType()) return false;
			return x.guidAssignmentFailed == y.guidAssignmentFailed && x.guid == y.guid;
		}

		public int GetHashCode(UniType obj)
		{
			unchecked
			{
				return (obj.guidAssignmentFailed.GetHashCode() * 397) ^ (obj.guid != null ? obj.guid.GetHashCode() : 0);
			}
		}
	}
}