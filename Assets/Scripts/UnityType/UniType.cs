﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace UnityType
{
    /// <summary>
    /// Reference to a class definition <see cref="System.Type"/> that can be serialized
    /// </summary>
    [Serializable]
    public sealed class UniType : ISerializationCallbackReceiver
    {
        public const string Null = "(None)";
        public const string NameOfTypeNameField = nameof(typeNameAndAssembly);

        public bool guidAssignmentFailed;
        public string guid;

        [SerializeField] string typeNameAndAssembly;

        Type _referencedType;

        public Type ReferencedType
        {
            get => _referencedType;
            set
            {
                MakeSureTypeHasName(value);
                _referencedType = value;
                typeNameAndAssembly = GetTypeNameAndAssembly(value);
                SetClassGuidIfExists(value);
            }
        }
        
        public UniType() { }

        public UniType(string assemblyQualifiedTypeName) =>
            ReferencedType = string.IsNullOrEmpty(assemblyQualifiedTypeName)
                ? null
                : Type.GetType(assemblyQualifiedTypeName);

        public UniType(Type type) => ReferencedType = type;


        void ISerializationCallbackReceiver.OnBeforeSerialize() { }

        void ISerializationCallbackReceiver.OnAfterDeserialize() =>
            _referencedType = string.IsNullOrEmpty(typeNameAndAssembly)
                ? null
                : TryGetTypeFromSerializedFields();

        public override string ToString() 
            => !(ReferencedType == null || ReferencedType.FullName == null)
                ? ReferencedType.FullName
                : Null;

        Type TryGetTypeFromSerializedFields()
        {
            var type = Type.GetType(typeNameAndAssembly);
            if (type == null && typeNameAndAssembly != Null)
                Debug.LogWarning($"'{typeNameAndAssembly}' was referenced but class type was not found");

            return type;
        }

        void SetClassGuidIfExists(Type type)
        {
            try
            {
                guid = GetClassGuid(type);
            }
            catch (UnityException)
            {
                guidAssignmentFailed = true;
                guid = string.Empty;
            }
        }

        public static implicit operator Type(UniType uniType) => uniType?.ReferencedType;
        public static implicit operator UniType(Type type) => new UniType(type);

        public static string GetClassGuid(Type type)
        {
            if (type == null || type.FullName == null)
                return string.Empty;

            #if UNITY_EDITOR
            var guids = AssetDatabase.FindAssets(type.Name);

            foreach (var guid in guids)
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(guid);
                var asset = AssetDatabase.LoadAssetAtPath<MonoScript>(assetPath);

                if (asset == null) continue;
                if (asset.GetClass() == type) return guid;
            }
            #endif
            
            return string.Empty;
        }

        public static void MakeSureTypeHasName(Type type)
        {
            if (type == null) return;
            if (type.FullName == null)
                throw new ArgumentException($"'{type}' does not have a full name", nameof(type));
        }

        public static string GetTypeNameAndAssembly(Type type)
        {
            MakeSureTypeHasName(type);
            return type == null
                ? string.Empty
                : type.FullName + ", " + type.Assembly.GetName().Name;
        }
    }
}
