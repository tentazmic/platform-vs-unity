﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace UnityType.Editor.Util
{
	public static class TypeCollector
	{
		public static IEnumerable<Assembly> GetAllAssemblies() => AppDomain.CurrentDomain.GetAssemblies();

		public static IEnumerable<Assembly> GetAssembliesTypeHasAccessTo(Type type)
		{
			Assembly typeAssembly;

			try
			{
				typeAssembly = type == null ? Assembly.Load("Assembly-CSharp") : type.Assembly;
			}
			catch (FileNotFoundException)
			{
				throw new FileNotFoundException("Assembly-CSharp.dll was not found. Please create any " +
				                                "script in the Assets folder so that the assembly is generated.");
			}

			var assemblies = new List<Assembly> {typeAssembly};
			
			assemblies.AddRange(
				typeAssembly.GetReferencedAssemblies().Select(Assembly.Load)
				);

			return assemblies;
		}

		public static IEnumerable<Type> GetFilteredTypesFromAssemblies(
			IEnumerable<Assembly> assemblies,
			TypeOptionsAttribute options)
		{
			static IEnumerable<Type> GetVisibleTypes(Assembly assembly)
			{
				try
				{
					return assembly.GetTypes().Where(type => type.IsVisible);
				}
				catch (ReflectionTypeLoadException e)
				{
					Debug.LogError($"Types could not be abstracted from assembly {assembly}: {e.Message}");
					return new Type[0];
				}	
			}

			var types = new List<Type>();

			foreach (var assembly in assemblies)
			{
				types.AddRange(
					GetVisibleTypes(assembly)
						.Where(type => options == null || options.MatchesRequirements(type))
					);
			}

			return types;
		}
	}
}