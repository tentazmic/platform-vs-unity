﻿using System;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using UnityType.Editor.Util;

namespace UnityType.Editor.Dropdown
{
	public class DropdownWindow : EditorWindow
	{
		const string KVisualTreeAsset = "Assets/Scripts/UnityType/Editor/UIElements/dropdown.uxml";
		const string KDropdownItemUxml = "Assets/Scripts/UnityType/Editor/UIElements/dropdown-item.uxml";
		const string KStyles = "Assets/Scripts/UnityType/Editor/UIElements/dropdown.uss";
		
		const float DistanceToRightBorderOffset = 8f;

		const float KSearchFieldHeight = 20f;
		const float KMinHeight = 100f + KSearchFieldHeight;
		const float KMaxHeight = 400f + KSearchFieldHeight;

		static VisualTreeAsset _dropdownItem;

		ToolbarSearchField _searchField;
		ListView _listView;

		public static void Create(Action<Type> onSelect, TypeOptionsAttribute options)
		{
			CreateInstance<DropdownWindow>().OnCreate(onSelect, options);
			if (_dropdownItem == null)
				_dropdownItem = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(KDropdownItemUxml);
		}

		void OnCreate(Action<Type> onSelect, TypeOptionsAttribute options)
		{
			wantsMouseMove = true;

			var root = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(KVisualTreeAsset).CloneTree();
			root.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>(KStyles));

			_listView = root.Q<ListView>("list");
			PopulateListView(options, onSelect);

			_searchField = root.Q<ToolbarSearchField>();
			_searchField.AddManipulator(new SearchManipulator(() =>
			{
				PopulateListView(options, onSelect, type =>
				{
					var typeName = type.Name;
					var searchString = _searchField.value;
					if (typeName.Length < searchString.Length)
						return false;
					
					var length = Mathf.Min(typeName.Length, searchString.Length);
					for (var i = 0; i < length; i++)
					{
						if (searchString[i] != typeName[i])
							return false;
					}

					return true;
				});
			}));

			rootVisualElement.Add(root);

			var windowPos = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);

			var distanceToRightBorder = Screen.currentResolution.width + windowPos.x - DistanceToRightBorderOffset;
			var windowSize = new Vector2(450f, CalculateWindowHeight());

			var buttonRect = new Rect(windowPos, new Vector2(distanceToRightBorder, 10));
			ShowAsDropDown(buttonRect, windowSize);
			// Show();
		}
		
		void PopulateListView(TypeOptionsAttribute options, Action<Type> onSelect, Func<Type, bool> filter = null)
		{
			// var assemblies = TypeCollector.GetAssembliesTypeHasAccessTo(null);
			var assemblies = TypeCollector.GetAllAssemblies();
			var items = TypeCollector.GetFilteredTypesFromAssemblies(assemblies, options)
				.Where(filter ?? (type => true)).ToList();
			items.Sort((type, type1) => string.Compare(type.FullName, type1.FullName, StringComparison.Ordinal));
			
			_listView.itemHeight = 20;
			_listView.selectionType = SelectionType.Single;
			_listView.itemsSource = items;

			_listView.makeItem = () => _dropdownItem.CloneTree().contentContainer;
			_listView.bindItem = (element, i) =>
			{
				if (i >= items.Count || i < 0)
					return;

				element.Q<Label>().text = items[i].FullName;
			};
			_listView.onItemsChosen += objs =>
			{
				onSelect((Type) _listView.selectedItem);
				Close();
			};
		}

		float CalculateWindowHeight()
		{
			var count = _listView.itemsSource.Count;
			var itemHeight = _listView.itemHeight;

			var prospectiveHeight = count * itemHeight + KSearchFieldHeight;
			return Mathf.Clamp(prospectiveHeight, KMinHeight, KMaxHeight);
		}

		class SearchManipulator : Manipulator
		{
			readonly Action _callback;

			public SearchManipulator(Action callback)
			{
				_callback = callback;
			}
			
			protected override void RegisterCallbacksOnTarget()
			{
				target.RegisterCallback<KeyDownEvent>(OnKeyDownEvent);
			}

			protected override void UnregisterCallbacksFromTarget()
			{
				target.UnregisterCallback<KeyDownEvent>(OnKeyDownEvent);
			}

			void OnKeyDownEvent(KeyboardEventBase<KeyDownEvent> evt)
				=> _callback?.Invoke();
		}
	}
}