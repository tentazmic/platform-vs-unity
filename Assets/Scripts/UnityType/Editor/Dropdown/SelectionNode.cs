﻿using System;
using System.Collections.Generic;
using UnityEditor.Build.Content;
using UnityEngine;

namespace UnityType.Editor.Dropdown
{
	/// <summary>
	/// A node in the selection tree. It may be a folder or an item that represents <see cref="System.Type"/>
	/// </summary>
	internal class SelectionNode
	{
		public readonly List<SelectionNode> ChildNodes = new List<SelectionNode>();
		public readonly Type Type;

		public bool Selected;

		readonly string _name;
		readonly SelectionNode _parentNode;

		bool _expanded;

		public Rect Rect { get; private set; }

		public bool IsExpaned
		{
			get => IsFolder && _expanded;
			set => _expanded = value;
		}

		bool IsFolder => ChildNodes.Count != 0;
		bool IsHoveredOver => Rect.Contains(Event.current.mousePosition);
		bool IsRoot => _parentNode == null;

		public SelectionNode(string name, SelectionNode parentNode, Type type)
		{
			_name = name;
			_parentNode = parentNode;
			Type = type;
		}

		SelectionNode()
		{
			_name = string.Empty;
			_parentNode = null;
			Type = null;
		}

		public SelectionNode FindChild(string name)
		{
			for (var i = ChildNodes.Count - 1; i >= 0; i--)
			{
				if (ChildNodes[i]._name == name)
					return ChildNodes[i];
			}

			return null;
		}
		
		public static SelectionNode CreateRoot() => new SelectionNode();
	}
}