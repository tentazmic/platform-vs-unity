﻿using System;
using System.Collections.Generic;

namespace UnityType.Editor.Dropdown
{
	internal class SelectionTree
	{
		readonly List<SelectionNode> _searchNodeTree = new List<SelectionNode>();
		readonly SelectionNode _root;
		readonly SelectionNode _noneElement;
		readonly Action<Type> _onTypeSelected;

		SelectionNode _selectedNode;
		
	}
}