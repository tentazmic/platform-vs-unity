﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityType.Editor.Dropdown;

namespace UnityType.Editor
{
	[CustomPropertyDrawer(typeof(UniType))]
	[CustomPropertyDrawer(typeof(TypeOptionsAttribute), true)]
	public class UniTypePropertyDrawer : PropertyDrawer
	{
		const string KVisualTreeAssetFileName = "Assets/Scripts/UnityType/Editor/UIElements/unitype-drawer.uxml";
		const string KStyleSheet = "Assets/Scripts/UnityType/Editor/UIElements/unitype-drawer.uss";

		TypeOptionsAttribute _optionsAttribute;

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			return CreateGUI(property, attribute);
		}

		public VisualElement CreateGUI(SerializedProperty property, Attribute usedAttribute, bool drawLabel = true)
		{
			var typeNameProperty = property.FindPropertyRelative(UniType.NameOfTypeNameField);
			_optionsAttribute = (TypeOptionsAttribute) usedAttribute;
			
			var container = new VisualElement();
			var drawer = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(KVisualTreeAssetFileName).CloneTree(property.propertyPath);
			container.styleSheets.Add(AssetDatabase.LoadAssetAtPath<StyleSheet>(KStyleSheet));

			var label = drawer.Q<Label>();
			
			if (drawLabel)
				label.text = property.displayName;
			else
				label.RemoveFromHierarchy();
			
			var button  = drawer.Q<Button>();
			button.clicked += () => DropdownWindow.Create(type =>
			{
				var typeName = UniType.GetTypeNameAndAssembly(type);
				typeNameProperty.stringValue = typeName;
				UpdateButtonText(button, typeName);
				
				property.serializedObject.ApplyModifiedProperties();
			}, _optionsAttribute);

			UpdateButtonText(button, typeNameProperty.stringValue);
			
			container.Add(drawer);
			
			return container;
		}

		void UpdateButtonText(Button button, string typeName)
			=> button.text = Type.GetType(typeName) == null
								? UniType.Null
								: GetDisplayName(Type.GetType(typeName)?.FullName);

		string GetDisplayName(string typeName) 
			=> (_optionsAttribute ?? new TypeOptionsAttribute()).ShortName ? typeName.Split('.').Last() : typeName;
	}
}