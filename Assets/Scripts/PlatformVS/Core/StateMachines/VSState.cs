﻿using System.Collections.Generic;
using PlatformVS.Core.Input;
using StateSystem.Implementations;
using StateSystem.Transitions;

namespace PlatformVS.Core.StateMachines
{
	public abstract class VSState : State
	{
		protected readonly IInput Input;

		protected VSState(IInput input, int[] transitionIds, IDictionary<int, ITransition> specialTransitions = null) :
			base(transitionIds, specialTransitions)
		{
			Input = input;
		}

		public int FramesElapsed { get; private set; }

		public override void Enter()
		{
			FramesElapsed = 0;
			EscapeType = StateEscapeType.None;
		}

		public override void Update()
		{
			FramesElapsed++;
		}
	}
}