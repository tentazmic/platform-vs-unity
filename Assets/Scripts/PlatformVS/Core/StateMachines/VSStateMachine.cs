﻿using System;
using System.Collections.Generic;
using System.Linq;
using StateSystem;
using StateSystem.Utility;

namespace PlatformVS.Core.StateMachines
{
	public abstract class VSStateMachine : IStateMachine
	{
		Action<IStateMachine> _enterEvent;

		protected void SetupStates()
		{
			Transition(States.First().Value);
		}
		
		#region IStateMachine
		
		public event Action<IStateMachine> OnEnter
		{
			add => _enterEvent += value;
			remove => _enterEvent -= value;
		}
		
		public abstract IState DefaultState { get; }
		public IState Current { get; private set; }
		
		public abstract IReadOnlyDictionary<int, IState> States { get; }
		public IDictionary<int, IState> TransitionStates { get; } = new Dictionary<int, IState>();
		
		public virtual void StateUpdate()
		{
			Current?.Update();
			this.ProcessStateEscape();
		}

		public virtual bool FindNextState(out IState next) => Current == null ? this.FindAnyStateToEnter(out next) : this.DFindNextState(out next);

		public virtual void Transition(IState next)
		{
			this.DTransition(next);
			Current = next;
			_enterEvent?.Invoke(this);
		}

		public virtual void Clear() => this.DClear();
		
		#endregion // IStateMachine
	}
}