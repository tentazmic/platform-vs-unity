﻿using System;
using System.Collections.Generic;
using PlatformVS.Core.VSEntity;
using StateSystem;
using UnityEngine;
using UnityType;

namespace PlatformVS.Core.StateMachines
{
	[CreateAssetMenu(fileName = "New State Machine Config", menuName = "State Machines/Stack Config")]
	public class StateMachineStackConfig : ScriptableObject, IStateMachineStackConfig, ISerializationCallbackReceiver
	{
		[SerializeField, Inherits(typeof(VSStateMachine), ShortName = true)]
		UniType[] machineOrder = new UniType[5];

		Type[] _typeOrder;

		public void OrderList(IList<IStateMachine> stack)
		{
			var stackCopy = new List<IStateMachine>(stack);
			stack.Clear();
			for (var i = _typeOrder.Length - 1; i >= 0; i--)
			{
				var mach = stackCopy.Find(machine => machine?.GetType() == _typeOrder[i]);
				if (mach != null)
					stack.Add(mach);
			}
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize() { }

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			_typeOrder = new Type[machineOrder.Length];
			for (var i = 0; i < _typeOrder.Length; i++)
				_typeOrder[i] = machineOrder[i];
		}
	}
}