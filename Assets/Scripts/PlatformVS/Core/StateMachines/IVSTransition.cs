﻿using PlatformVS.Core.Input;
using StateSystem;
using StateSystem.Transitions;

namespace PlatformVS.Core.StateMachines
{
	public interface IVSTransition : ITransition
	{
		bool Evaluate(IState current, IInput input);
	}
}