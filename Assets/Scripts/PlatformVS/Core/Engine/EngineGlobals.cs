﻿namespace PlatformVS.Core.Engine
{
	// [UnityScriptableSingleton(new[] {UnitySingletonType.LoadFromResources, UnitySingletonType.CreateNew},
	// 	nameof(EngineGlobals))]
	//  : UnityScriptableSingleton<EngineGlobals>
	public static class EngineGlobals
	{
		public const int FrameRate = 60;
		public const float FrameDuration = 1f / FrameRate;
	}
}