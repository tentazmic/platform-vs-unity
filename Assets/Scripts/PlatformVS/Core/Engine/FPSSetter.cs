﻿using System;
using UnityEngine;

namespace PlatformVS.Core.Engine
{
	[AddComponentMenu("PlatformVS/Engine/FPS Setter")]
	public class FPSSetter : MonoBehaviour
	{
		void Awake()
		{
			Application.targetFrameRate = EngineGlobals.FrameRate;
			Time.captureFramerate = EngineGlobals.FrameRate;
		}
	}
}