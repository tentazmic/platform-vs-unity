﻿using PlatformVS.Core.Engine;
using UnityEngine;

namespace PlatformVS.Core.PropertyContainers
{
	[System.Serializable]
	public sealed class SteadyMotion
	{
		public float maxSpeed = 5;
		public int framesToMax = 10;
		
		[HideInInspector]
		public float acceleration;

		public void Calculate() => acceleration = maxSpeed / (framesToMax * EngineGlobals.FrameDuration);
	}
}