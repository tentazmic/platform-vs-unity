﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlatformVS.Core.Engine;
using UnityEngine;

using static UnityEngine.Mathf;

namespace PlatformVS.Core.PropertyContainers
{
	[Serializable]
	public struct BurstMotion
	{
		// TODO Needs to be fixed with an editor
		const float KTimeDistanceThreshold = 0.01f;
		
		[SerializeField] float startSpeed;
		[SerializeField] int frameDuration;

		[SerializeField] AnimationCurve velocityCurve;

		[SerializeField] float effectiveTraction;

		[Space] 
		
		// Calculated values
		[SerializeField] AnimationCurve accelerationCurve;
		[SerializeField] float distance;
		[SerializeField] float endSpeed;

		public float StartSpeed => startSpeed;
		public float EndSpeed => endSpeed;
		public int FrameDuration => frameDuration;
		public float Distance => distance;
		public float EffectiveTraction => effectiveTraction;

		public AnimationCurve AccelerationCurve => accelerationCurve;

		public BurstMotion(float distance, int frameDuration, float startSpeed = 0, float endSpeed = 0, AnimationCurve velocityCurve = null)
		{
			endSpeed = Clamp(endSpeed, 0, startSpeed);
			
			this.distance = distance;
			this.frameDuration = frameDuration;
			if (Approximately(startSpeed, 0))
			{
				var duration = (float) frameDuration / EngineGlobals.FrameRate;
				startSpeed = (distance / duration) + endSpeed;
			}

			this.startSpeed = startSpeed;
			this.endSpeed = endSpeed;
			
			this.velocityCurve = velocityCurve ?? AnimationCurve.Linear(0, 1f, 1f, endSpeed / startSpeed);
			
			effectiveTraction = 0;
			accelerationCurve = AnimationCurve.Constant(0, (float)frameDuration / EngineGlobals.FrameRate, 0);
		}

		public float GetAccelerationFactor(int frame)
		{
			var time = (float) frame / EngineGlobals.FrameRate;
			return accelerationCurve.Evaluate(time);
		}

		public float GetAccelerationFactor(float time)
			=> accelerationCurve.Evaluate(time) * 0.5f;

		public void Calculate()
		{
			if (velocityCurve.length < 2)
				return;
			
			const float keyFrameSeconds = 1f / EngineGlobals.FrameRate;
			const float adjustmentMultiplier = 1f / (keyFrameSeconds * (EngineGlobals.FrameRate - 1));
			
			var keyframes = new List<Keyframe>();

			Keyframe first = velocityCurve.keys.First(), last = velocityCurve.keys.Last();

			distance = 0;
			var traction = Approximately(0, effectiveTraction) ? 1f : effectiveTraction;
			var duration = (float) frameDuration / EngineGlobals.FrameRate;

			for (var time = first.time; time < last.time; time += keyFrameSeconds)
			{
				var	bTime = Clamp(time + keyFrameSeconds, first.time, last.time);

				var a = velocityCurve.Evaluate(time);
				var b = velocityCurve.Evaluate(bTime);

				distance += a * startSpeed;

				if (!Approximately(time, 0) && Abs(a - b) < KTimeDistanceThreshold) continue;

				var acceleration = (b - a) * startSpeed / (keyFrameSeconds);
				// Debug.Log(time);

				if (keyframes.Count > 0)
				{
					var prev = keyframes.Last();
					if (Approximately(prev.value, acceleration))
						continue;
				}
				
				var frame = new Keyframe(time * adjustmentMultiplier * duration, acceleration)
					{weightedMode = WeightedMode.None};
				keyframes.Add(frame);
			}

			distance *= keyFrameSeconds;

			if (!Approximately(last.time * duration, keyframes.Last().time))
				keyframes.Add(
					new Keyframe(last.time * duration, keyframes.First().value) 
						{weightedMode = WeightedMode.None}
					);

			accelerationCurve = new AnimationCurve(keyframes.ToArray());

			endSpeed = velocityCurve.keys.Last().value * startSpeed;
		}
	}
}