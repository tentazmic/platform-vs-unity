﻿using UnityEngine;

namespace PlatformVS.Core.PropertyContainers
{
	[System.Serializable]
	public sealed class DashMotion
	{
		[Min(0)] public float startSpeed = 1;
		
		[Min(1)] public int frameDuration = 10;
		
		[Min(0)] public float frictionMultiplier = 1;
	}
}