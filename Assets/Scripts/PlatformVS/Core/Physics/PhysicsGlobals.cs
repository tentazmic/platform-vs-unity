﻿using PlatformVS.Core.Engine;
using UniBoost;
using UniBoost.Singletons;
using UnityEngine;

namespace PlatformVS.Core.Physics
{
	[UnityScriptableSingleton(new [] {UnitySingletonType.LoadFromResources, UnitySingletonType.CreateNew}, nameof(PhysicsGlobals))]
	public class PhysicsGlobals : UnityScriptableSingleton<PhysicsGlobals>
	{
		[SerializeField, Min(0.001f)]
		float minMoveSpeed = 0.001f, 
			groundCheckDistance = 0.1f;

		[SerializeField, Min(1)] 
		float crouchTractionMultiplier = 2f, 
			backwardsTractionMultiplier = 1.6f;

		[SerializeField, Min(3)] int permeationRayCount = 5;
		[SerializeField, Min(1)] float permeationRayArraySpread = 2f;

		public static float MinMoveSpeed => Instance.minMoveSpeed;
		public static float GroundCheckDistance => Instance.groundCheckDistance;

		public static float CrouchTractionMultiplier => Instance.crouchTractionMultiplier;
		public static float BackwardsTractionMultiplier => Instance.backwardsTractionMultiplier;

		public static int PermeationRayCount => Instance.permeationRayCount;
		public static float PermeationRayArraySpread => Instance.permeationRayArraySpread;
	}
}