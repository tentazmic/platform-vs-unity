﻿using UniBoost;
using UniBoost.Singletons;
using UnityEngine;

namespace PlatformVS.Core.Physics
{
	[UnityScriptableSingleton(new [] {UnitySingletonType.LoadFromResources, UnitySingletonType.CreateNew}, nameof(LayerGlobals))]
	public class LayerGlobals : UnityScriptableSingleton<LayerGlobals>
	{
		[SerializeField] string permeateCast = "PermeateCast";
		
		[SerializeField] string character = "Character";
		[SerializeField] string permeateCharacter = "PhasingCharacter";

		[SerializeField] string ledge = "Ledge";

		[SerializeField] string water = "Water";

		[SerializeField] string waterScanner = "WaterScanner";

		public static LayerMask PermeateCast => LayerMask.NameToLayer(Instance.permeateCast);

		public static int Character => LayerMask.NameToLayer(Instance.character);
		public static int CharacterPermeate => LayerMask.NameToLayer(Instance.permeateCharacter);

		public static int Ledge => LayerMask.NameToLayer(Instance.ledge);

		public static LayerMask Fluid => 1 << LayerMask.NameToLayer(Instance.water);
		public static LayerMask FluidScanner => 1 << LayerMask.NameToLayer(Instance.waterScanner);
	}
}