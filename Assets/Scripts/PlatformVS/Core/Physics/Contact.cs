﻿using PlatformVS.Core.Environment;
using UnityEngine;

namespace PlatformVS.Core.Physics
{
	/// Keeps information on the conditions of an object we are in contact with
	public readonly struct Contact
	{
		public readonly ITerrain Terrain;
		public readonly Collider Collider;
		public readonly CollisionPhase Phase;
		public readonly Vector2 Normal;
		public readonly float NormalDot;

		public Contact(ITerrain terrain, Collider collider, Vector2 normal, CollisionPhase phase, float normalDot)
		{
			Terrain = terrain;
			Collider = collider;
			Normal = normal;
			Phase = phase;
			NormalDot = normalDot;
		}
		public static implicit operator bool(Contact contact) => contact.Collider;
	}
}