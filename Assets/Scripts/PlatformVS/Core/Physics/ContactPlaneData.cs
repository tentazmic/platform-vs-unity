﻿using UnityEngine;

namespace PlatformVS.Core.Physics
{
	public struct ContactPlaneData
	{
		public Collider Collider;
		public bool Phaseable;

		public static implicit operator bool(ContactPlaneData data) => data.Collider;

		public static bool operator ==(ContactPlaneData data1, ContactPlaneData data2)
			=> data1.Collider == data2.Collider;

		public static bool operator !=(ContactPlaneData data1, ContactPlaneData data2) => !(data1 == data2);
		
		public bool Equals(ContactPlaneData other)
		{
			return Equals(Collider, other.Collider) && Phaseable == other.Phaseable;
		}

		public override bool Equals(object obj)
		{
			return obj is ContactPlaneData other && Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Collider != null ? Collider.GetHashCode() : 0) * 397) ^ Phaseable.GetHashCode();
			}
		}
	}
}