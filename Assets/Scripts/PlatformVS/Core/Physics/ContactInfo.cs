﻿using System;
using System.Collections.Generic;
using UniBoost.Extensions;
using UnityEngine;

namespace PlatformVS.Core.Physics
{
	/// Information on the contacts in the four directions
	public class ContactInfo
	{
		Action _onEnteredBelow, _onExitedBelow;
		Action _onEnteredAbove, _onExitedAbove;
		Action _onEnteredLeft, _onExitedLeft;
		Action _onEnteredRight, _onExitedRight;

		Contact _belowContact, _aboveContact;
		Contact _leftContact, _rightContact;
		
		#region Actions
		
		public event Action OnEnteredBelow
		{
			add => _onEnteredBelow += value;
			remove => _onEnteredBelow -= value;
		}
		public event Action OnExitedBelow
		{
			add => _onExitedBelow += value;
			remove => _onExitedBelow -= value;
		}
		
		public event Action OnEnteredAbove
		{
			add => _onEnteredAbove += value;
			remove => _onEnteredAbove -= value;
		}
		public event Action OnExitedAbove
		{
			add => _onExitedAbove += value;
			remove => _onExitedAbove -= value;
		}
		
		public event Action OnEnteredLeft
		{
			add => _onEnteredLeft += value;
			remove => _onEnteredLeft -= value;
		}
		public event Action OnExitedLeft
		{
			add => _onExitedLeft += value;
			remove => _onExitedLeft -= value;
		}
		
		public event Action OnEnteredRight
		{
			add => _onEnteredRight += value;
			remove => _onEnteredRight -= value;
		}
		public event Action OnExitedRight
		{
			add => _onExitedRight += value;
			remove => _onExitedRight -= value;
		}
		
		#endregion // Actions

		public bool NoContacts => !(Below || Above || Left || Right);
		
		public CollisionPhase BelowPhase { get; private set; }  = CollisionPhase.None;
		public Contact Below
		{
			get => _belowContact;
			set
			{
				var prev = _belowContact;
				_belowContact = value;

				BelowPhase = ECollisionPhase.Evaluate(_belowContact, prev);

				switch (BelowPhase)
				{
					case CollisionPhase.Entry:
						_onEnteredBelow?.Invoke();
						break;
					case CollisionPhase.Exit:
						_onExitedBelow?.Invoke();
						break;
				}
			}
		}

		public CollisionPhase AbovePhase { get; private set; }  = CollisionPhase.None;
		public Contact Above
		{
			get => _aboveContact;
			set
			{
				var prev = _aboveContact;
				_aboveContact = value;

				AbovePhase = ECollisionPhase.Evaluate(_aboveContact, prev);

				switch (AbovePhase)
				{
					case CollisionPhase.Entry:
						_onEnteredAbove?.Invoke();
						break;
					case CollisionPhase.Exit:
						_onExitedAbove?.Invoke();
						break;
				}
			}
		}

		public CollisionPhase LeftPhase { get; private set; }  = CollisionPhase.None;
		public Contact Left
		{
			get => _leftContact;
			set
			{
				var prev = _leftContact;
				_leftContact = value;

				LeftPhase = ECollisionPhase.Evaluate(_leftContact, prev);

				switch (LeftPhase)
				{
					case CollisionPhase.Entry:
						_onEnteredLeft?.Invoke();
						break;
					case CollisionPhase.Exit:
						_onExitedLeft?.Invoke();
						break;
				}
			}
		}

		public CollisionPhase RightPhase { get; private set; }  = CollisionPhase.None;
		public Contact Right
		{
			get => _rightContact;
			set
			{
				var prev = _rightContact;
				_rightContact = value;

				RightPhase = ECollisionPhase.Evaluate(_rightContact, prev);

				switch (RightPhase)
				{
					case CollisionPhase.Entry:
						_onEnteredRight?.Invoke();
						break;
					case CollisionPhase.Exit:
						_onExitedRight?.Invoke();
						break;
				}
			}
		}

		public bool InContactWith(Collider collider) 
			=> _belowContact.Collider == collider || _aboveContact.Collider == collider || _leftContact.Collider == collider || _rightContact.Collider == collider;

		public bool AreContactsInDirection(Vector2 direction)
		{
			var verticalSign = direction.y.AdvSign();
			var horizontalSign = direction.x.AdvSign();

			var isInContactVertical = verticalSign switch { -1 => Below, 1 => Above, _ => true };

			var isInContactHorizontal = horizontalSign switch { -1 => Left, 1 => Right, _ => true };

			return isInContactVertical && isInContactHorizontal;
		}

		public IEnumerable<Direction> GetContactDirections()
		{
			var directions = new List<Direction>();
			
			if (Below) directions.Add(Direction.Down);
			if (Above) directions.Add(Direction.Up);
			if (Left) directions.Add(Direction.Left);
			if (Right) directions.Add(Direction.Right);

			return directions.ToArray();
		}

		public Contact GetContactInDirection(Vector2 direction)
			=> GetContactInDirection(EDirection.FromVector2(direction));

		public Contact GetContactInDirection(Direction direction) 
			=> direction switch
				{
					Direction.Up => Above,
					Direction.Down => Below,
					Direction.Left => Left,
					Direction.Right => Right,
					_ => new Contact()
				};
	}
}