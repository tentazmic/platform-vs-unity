﻿using UnityEngine;

namespace PlatformVS.Core.Physics
{
	public enum Direction
	{
		None, Up, Down, Left, Right
	}

	public static class EDirection
	{
		public static Direction FromVector2(Vector2 vector2)
		{
			if (vector2 == Vector2.zero)
				return Direction.None;
			
			var angle = Vector2.SignedAngle(vector2, Vector2.up);
			var absAngle = Mathf.Abs(angle);
			
			if (absAngle < 45)
				return Direction.Up;
			if (absAngle > 135)
				return Direction.Down;

			return angle > 0 ? Direction.Right : Direction.Left;
		}
	}
}