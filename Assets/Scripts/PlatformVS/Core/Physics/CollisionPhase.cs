﻿namespace PlatformVS.Core.Physics
{
	/// <summary>
	/// Phases of a collision
	/// </summary>
	public enum CollisionPhase
	{
		None = 0,
		Entry = 1,
		Stay = 2,
		Exit = 3
	}

	public static class ECollisionPhase
	{
		public static bool InTransitionalPhase(this CollisionPhase phase) 
			=> phase == CollisionPhase.Entry || phase == CollisionPhase.Exit;

		public static bool InContact(this CollisionPhase phase)
			=> phase == CollisionPhase.Entry || phase == CollisionPhase.Stay;
		
		public static void NextPhase(this ref CollisionPhase phase)
			=> phase = (CollisionPhase) (((int)phase + 1) % 4);

		public static CollisionPhase Evaluate(bool inContactCurrent, bool inContactPrevious)
		{
			if (!(inContactCurrent || inContactPrevious)) return CollisionPhase.None;
			if (inContactCurrent && !inContactPrevious) return CollisionPhase.Entry;
			// inContactPrevious must be true
			return inContactCurrent ? CollisionPhase.Stay : CollisionPhase.Exit;
		}
	}
}