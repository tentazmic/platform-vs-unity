﻿using System.Collections.Generic;
using UniBoost.Singletons;
using UnityEngine;

namespace PlatformVS.Core.Environment
{
	[UnitySingleton(UnitySingletonType.CreateNew)]
	public class EnvironmentCache : UnitySingleton<EnvironmentCache>
	{
		readonly Dictionary<Collider, IFluid> _fluidMap = new Dictionary<Collider, IFluid>();
		readonly Dictionary<Collider, ITerrain> _terrainMap = new Dictionary<Collider, ITerrain>();

		public static IFluid GetFluidFromCollider(Collider collider)
		{
			if (!collider)
				return EnvironmentGlobals.DefaultFluid;
			
			if (Instance._fluidMap.ContainsKey(collider))
				return Instance._fluidMap[collider];

			var success = collider.TryGetComponent(out IFluid fluid);
			
			if (success)
				Instance._fluidMap.Add(collider, fluid);
			else
				fluid = EnvironmentGlobals.DefaultFluid;

			return fluid;
		}

		public static ITerrain GetTerrainFromCollider(Collider collider)
		{
			if (!collider)
				return EnvironmentGlobals.EmptyTerrain;
			
			if (Instance._terrainMap.ContainsKey(collider))
				return Instance._terrainMap[collider];

			var success = collider.TryGetComponent(out ITerrain terrain);
			
			if (!success)
				terrain = EnvironmentGlobals.EmptyTerrain;
			
			Instance._terrainMap.Add(collider, terrain);

			return terrain;
		}
	}
}