﻿using PlatformVS.Core.Engine;
using UniBoost;
using UniBoost.Singletons;
using UnityEngine;

namespace PlatformVS.Core.Environment
{
	[UnityScriptableSingleton(new [] {UnitySingletonType.LoadFromResources, UnitySingletonType.CreateNew}, nameof(EnvironmentGlobals))]
	public class EnvironmentGlobals : UnityScriptableSingleton<EnvironmentGlobals>
	{
		[SerializeField] Fluid fluid = new Fluid();
		
		[SerializeField, Tooltip("The settings of the terrain to use if the collider lacks an ITerrain")]
		Terrain emptyTerrain = new Terrain();

		public static ITerrain EmptyTerrain => Instance.emptyTerrain;
		public static IFluid DefaultFluid => Instance.fluid;
	}
}