﻿using UniBoost.Physics;
using UnityEngine;

namespace PlatformVS.Core.Environment
{
	[AddComponentMenu("PlatformVS/Environment/Fluid Trigger Cache")]
	public class FluidTriggerCache : GTriggerCache<IFluid>
	{
		void Start()
		{
			Current = EnvironmentGlobals.DefaultFluid;
			Updater = OnUpdate;			
		}

		static IFluid OnUpdate(Collider arg) => EnvironmentCache.GetFluidFromCollider(arg);
	}
}