﻿using UnityEngine;

namespace PlatformVS.Core.Environment
{
	/// <summary>
	/// Template for Ledges
	/// </summary>
	public interface ILedge
	{
		LedgeDirection Direction { get; }
		Collider DetectionCollider { get; }

		/// <summary>
		/// Gets the closest point on the ledge from the current position as the grabbers anchor
		/// </summary>
		/// <param name="grabberPosition">The position to 'reach' from</param>
		/// <returns>The closest ledge anchor</returns>
		Transform GetLedgeAnchor(Vector3 grabberPosition);

		void ReleaseLedgeAnchor(Transform anchor);

		Vector3 GetGetupPosition(Vector3 hangPoint);
	}
}