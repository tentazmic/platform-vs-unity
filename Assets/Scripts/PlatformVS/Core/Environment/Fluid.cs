﻿using UnityEngine;

namespace PlatformVS.Core.Environment
{
	[System.Serializable]
	public class Fluid : IFluid
	{
		[SerializeField] float massDensity = 1;
		[SerializeField] float elasticity = 1;
		[SerializeField] float surfaceTension = 1;

		public float MassDensity => massDensity;
		public float Elasticity => elasticity;
		public float SurfaceTension => surfaceTension;

		public Vector2 GetVelocityAtPosition(Vector3 position) => Vector2.zero;
	}
}