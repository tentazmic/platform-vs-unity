﻿namespace PlatformVS.Core.Environment
{
	/// <summary>
	/// Values for what way you must face when hanging from a ledge
	/// </summary>
	public enum LedgeDirection
	{
		Left = -1,
		Either = 0,
		Right = 1
	}
}