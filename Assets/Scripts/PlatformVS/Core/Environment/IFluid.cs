﻿using UnityEngine;

namespace PlatformVS.Core.Environment
{
	/// Template for all fluids, both liquid and gas
	public interface IFluid
	{
		/// How hard it is to move through through the fluid
		float MassDensity { get; }
		
		/// How much the fluid can be displaced
		float Elasticity { get; }
		
		/// How hard it is to break into the fluid
		float SurfaceTension { get; }

		/// Gets the direction and magnitude the fluid flows in at the provided position
		Vector2 GetVelocityAtPosition(Vector3 position);
	}
}