﻿using UnityEngine;

namespace PlatformVS.Core.VSEntity
{
	public enum Facing
	{
		Left = -1, Right = 1
	}

	public static class EFacing
	{
		public static Facing Evaluate(float value)
		{
			if (Mathf.Approximately(value, 0))
				value = 1f;
			return value > 0 ? Facing.Right : Facing.Left;
		}
	}
}