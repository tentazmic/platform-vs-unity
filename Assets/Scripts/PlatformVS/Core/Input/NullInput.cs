﻿using InputAssist;
using UniBoost.Input.Implementations;
using PlatformVS.Core.Input.Motions;
using PlatformVS.Core.Input.Motions.Charge;
using PlatformVS.Core.Input.Units;
using UnityEngine;

namespace PlatformVS.Core.Input
{
	public class NullInput : IInput
	{
		public bool IsActive
		{
			get => false;
			set { }
		}

		public InputStick Control
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public IChargeCache ChargeCache 
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public MotionUnit Motion
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public InputButton Modifier
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return new InputButton();
			}
		}

		public InputButton Jump
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public InputButton Light
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}
		
		public InputButton Medium
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}
		
		public InputButton Heavy
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}
		
		public InputButton Super
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public InputButton Grab
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}

		public InputButton Shield
		{
			get
			{
				Debug.LogWarning("Trying to access a null input");
				return null;
			}
		}
	}
}