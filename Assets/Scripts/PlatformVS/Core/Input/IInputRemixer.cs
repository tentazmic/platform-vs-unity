﻿using System.Runtime.CompilerServices;
using PlatformVS.Core.Input;

namespace PlatformVS.Core.Input
{
	/// <summary>
	/// Alters the source input
	/// </summary>
	public interface IInputRemixer : IInput
	{
		IInput Source { get; set; }

		void UpdateInput();
		void Reset();
	}
}