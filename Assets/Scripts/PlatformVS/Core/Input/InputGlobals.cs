﻿using System.Collections.Generic;
using PlatformVS.Core.Input.Motions;
using UniBoost.Extensions;
using UniBoost.Singletons;
using UnityEngine;

namespace PlatformVS.Core.Input
{
	[UnityScriptableSingleton(new [] {UnitySingletonType.LoadFromResources, UnitySingletonType.CreateNew}, nameof(InputGlobals))]
	public class InputGlobals : UnityScriptableSingleton<InputGlobals>
	{
		[SerializeField, Min(3)] int successiveDownTimeoutWindow = 7;
		[SerializeField, Range(0.2f, 1)] float inputStickMagnitudeDifferenceForSnapThreshold = 0.8f;
		
		[SerializeField, Range(0.1f, 1)] float walkActuationThreshold = 0.1f;
		
		[SerializeField, Range(-1, -0.2f)] float crouchActuationThreshold = -0.4f;
		[SerializeField, Range(0.5f, 1)] float crawlActuationMax = 0.7f;
		[SerializeField, Range(0, 1)] float turnAroundDuringProneActuationThreshold = 0.7f;

		[SerializeField, Range(0.6f, 1)] float dashActuationThreshold = 0.9f;

		[SerializeField, Range(0.5f, 1)] float platformPermeateActuationThreshold = 0.5f;

		[SerializeField, Range(0, 0.7f)] float alteredFallActuationThreshold = 0.4f;

		[SerializeField, Range(0.5f, 1)] float motionActuationThreshold = 0.9f;
		
		[SerializeField, Min(20)] int motionBufferSize = 60;
		[SerializeField, Min(10)] int motionTimeout = 40;

		[SerializeField, Min(4)] int chargeSlots = 8;
		[SerializeField, Min(0.1f)] float chargeMultiplier = 1;
		[SerializeField, HideInInspector] Vector2[] chargeStorageDirections;
		[SerializeField, Min(20)] int chargeCountdownMax = 60;
		
		[SerializeField, Min(1)] int neutralControlStickForMotionTimeout = 5;

		[SerializeField] MotionDefinition airTurnAroundMotion = null;
		[SerializeField] MotionDefinition superJumpMotion = null;


		public static float InputStickMagnitudeDifferenceForSnapThreshold => Instance.inputStickMagnitudeDifferenceForSnapThreshold;

		/// How far does the Control Stick's x-value have to go for a walk to be registered 
		public static float WalkActuationThreshold => Instance.walkActuationThreshold;

		/// How far does the Control Stick's y-value have to go for a crouch to be registered
		public static float CrouchActuationThreshold => Instance.crouchActuationThreshold;

		/// What is the maximum x-value for crawling
		public static float CrawlActuationMax => Instance.crawlActuationMax;

		/// How far does the Control Stick's x-value have to go in the opposite of the facing direction for a turnaround to be triggered
		public static float TurnAroundDuringProneActuationThreshold => Instance.turnAroundDuringProneActuationThreshold;

		/// How far does the Control Stick have to go to register a dash 
		public static float DashActuationThreshold => Instance.dashActuationThreshold;

		/// How far does the Control Stick's magnitude have to go in order to drop through a platform
		public static float PlatformPermeateActuationThreshold => Instance.platformPermeateActuationThreshold;

		/// How far does the Control Stick's y-value have to go to start altering fall speeds
		public static float AlteredFallActuationThreshold => Instance.alteredFallActuationThreshold;

		public static float MotionActuationThreshold => Instance.motionActuationThreshold;
		
		public static int MotionBufferSize => Instance.motionBufferSize;

		public static int MotionTimeout => Instance.motionTimeout;

		public static IEnumerable<Vector2> ChargeStorageDirections => Instance.chargeStorageDirections;

		public static int ChargeCountdownMax => Instance.chargeCountdownMax;

		public static float ChargeMultiplier => Instance.chargeMultiplier;

		public static int NeutralControlStickForMotionTimeout => Instance.neutralControlStickForMotionTimeout;

		public static MotionDefinition AirTurnAroundMotion => Instance.airTurnAroundMotion;

		public static MotionDefinition SuperJumpMotion => Instance.superJumpMotion;

		void OnValidate()
		{
			var theta = Mathf.PI * 2 * (1f / chargeSlots);
			chargeStorageDirections = new Vector2[chargeSlots];
			var current = Vector2.up;
			for (var i = 0; i < chargeSlots; i++)
			{
				chargeStorageDirections[i] = current;

				current = current.RotateVector(theta);
			}
		}
	}
}