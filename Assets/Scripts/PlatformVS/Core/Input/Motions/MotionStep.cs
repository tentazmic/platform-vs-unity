﻿using UnityEngine;

namespace PlatformVS.Core.Input.Motions
{
	[System.Serializable]
	public class MotionStep
	{
		public MotionDirection direction;
		[Min(0)] public int chargeTime;
		public bool isOptional;
		public int time = -1;

		public override bool Equals(object obj) =>
			obj switch
			{
				MotionStep motion => direction == motion.direction 
				                     && chargeTime <= motion.chargeTime 
				                     && (isOptional || !motion.isOptional) 
				                     && (time < 0 || time < motion.time),
				_ => false
			};

		protected bool Equals(MotionStep other)
		{
			return direction == other.direction && chargeTime == other.chargeTime && isOptional == other.isOptional && time == other.time;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (int) direction;
				hashCode = (hashCode * 397) ^ chargeTime;
				hashCode = (hashCode * 397) ^ isOptional.GetHashCode();
				hashCode = (hashCode * 397) ^ time;
				return hashCode;
			}
		}

		public override string ToString() => $"{direction}:{chargeTime}, Is {(isOptional? "" : "Not ")}Optional, Time {time}";
	}
}