﻿using UnityEngine;

namespace PlatformVS.Core.Input.Motions
{
	public struct Motion
	{
		public MotionDefinition Definition;
		public bool IsClockwise;
		public Vector2 Direction;

		public override bool Equals(object obj) =>
			obj switch
			{
				Motion motion => motion.Definition == Definition,
				_ => false
			};
	}
}