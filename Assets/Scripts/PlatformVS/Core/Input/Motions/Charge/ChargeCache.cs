﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PlatformVS.Core.Input.Motions.Charge
{
	public class ChargeCache : IChargeCache
	{
		const float KOverChargeCompensationMultiplier = 0.41818f;

		readonly ChargeDirectionInfo[] _chargeDirectionInfos;
		
		public ChargeCache()
		{
			_chargeDirectionInfos = InputGlobals.ChargeStorageDirections
				.Select(vector2 => new ChargeDirectionInfo(vector2)).ToArray();
		}

		public IReadOnlyCollection<ChargeDirectionInfo> ChargeInfo => _chargeDirectionInfos;

		public void Tick()
		{
			for (var i = 0; i < _chargeDirectionInfos.Length; i++)
			{
				var charge = _chargeDirectionInfos[i];

				if (!charge.JustAdded)
				{
					charge.Countdown--;
					if (charge.Countdown < 1)
					{
						charge.Charge = 0;
						charge.Countdown = 1;
					}
				}
				else
				{
					charge.JustAdded = false;
				}
				
				_chargeDirectionInfos[i] = charge;
			}
		}

		public void AddCharge(Vector2 input)
		{
			if (input == Vector2.zero)
				return;
			
			for (var i = 0; i < _chargeDirectionInfos.Length; i++)
			{
				var charge = _chargeDirectionInfos[i];
				var dot = Vector2.Dot(charge.Direction, input);
				if (dot < 0)
					continue;

				charge.Charge += dot;
				if (charge.Countdown < InputGlobals.ChargeCountdownMax)
					charge.Countdown++;
				charge.JustAdded = true;
				_chargeDirectionInfos[i] = charge;
			}
		}

		public float GetCharge(Vector2 direction)
		{
			var totalCharge = 0f;
			for (var i = 0; i < _chargeDirectionInfos.Length; i++)
			{
				var dot = Vector2.Dot(_chargeDirectionInfos[i].Direction, direction);
				if (dot < 0)
					continue;

				totalCharge += _chargeDirectionInfos[i].Charge;
			}

			return totalCharge * InputGlobals.ChargeMultiplier * KOverChargeCompensationMultiplier;
		}

		int GetClosestDirection(Vector2 vector)
		{
			var closest = 0;
			var closestDot = -2f;
			for (var i = 0; i < _chargeDirectionInfos.Length; i++)
			{
				
				var dot = Vector2.Dot(_chargeDirectionInfos[i].Direction, vector);
				if (dot < closestDot)
					continue;

				closestDot = dot;
				closest = i;
			}

			return closest;
		}
	}
}