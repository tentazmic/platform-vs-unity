﻿using System;
using System.Collections;
using PlatformVS.Core.VSEntity;
using UnityEngine;

namespace PlatformVS.Core.Input
{
	public class InputCreator : ScriptableObject
	{
		public virtual IInput CreateInput(MonoBehaviour owner) { return null; }
	}
}