﻿using PlatformVS.Core.Input;

namespace PlatformVS.Core.Input
{
	public interface IUseInput
	{
		IInput Input { get; }
	}
}