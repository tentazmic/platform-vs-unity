﻿using PlatformVS.Core.Input;
using PlatformVS.Core.VSEntity;
using UnityEngine;

namespace PlatformVS.Input.Creators
{
	[CreateAssetMenu(menuName = "Input/Create/Standard")]
	public class CreateStandardInput : InputCreator
	{
		public override IInput CreateInput(MonoBehaviour owner) => new StandardInput(owner.StartCoroutine);
	}
}