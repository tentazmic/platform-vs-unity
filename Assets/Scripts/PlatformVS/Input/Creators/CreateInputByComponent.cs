﻿using PlatformVS.Core.Input;
using PlatformVS.Core.Input.Units;
using PlatformVS.Core.VSEntity;
using UnityEngine;

namespace PlatformVS.Input.Creators
{
	[CreateAssetMenu(menuName = "Input/Create/By Component")]
	public class CreateInputByComponent : InputCreator
	{
		public override IInput CreateInput(MonoBehaviour owner) => owner.GetComponent<IInputComponent>();
	}
}