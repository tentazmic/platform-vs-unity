﻿using InputAssist;
using UniBoost.Input.Implementations;
using PlatformVS.Core.Input;
using PlatformVS.Core.Input.Motions;
using PlatformVS.Core.Input.Motions.Charge;
using PlatformVS.Core.Input.Units;
using PlatformVS.Core.VSEntity;
using UnityEngine;

namespace PlatformVS.Input
{
	[DisallowMultipleComponent]
	public class InputSimulator : MonoBehaviour, IInputComponent
	{
		[SerializeField] InputStick control;
		[SerializeField] MotionUnit motion;
		
		[SerializeField] InputButton modifier;
		[SerializeField] InputButton jump;
		[SerializeField] InputButton light;
		[SerializeField] InputButton medium;
		[SerializeField] InputButton heavy;
		[SerializeField] InputButton super;
		[SerializeField] InputButton grab;
		[SerializeField] InputButton shield;
		
		public bool IsActive
		{
			get => gameObject.activeInHierarchy;
			set => gameObject.SetActive(value);
		}

		public InputStick Control => control;
		public IChargeCache ChargeCache { get; private set;  }
		public MotionUnit Motion => motion;
		public InputButton Modifier => modifier;
		public InputButton Jump => jump;
		public InputButton Light => light;
		public InputButton Medium => medium;
		public InputButton Heavy => heavy;
		public InputButton Super => super;
		public InputButton Grab => grab;
		public InputButton Shield => shield;
		
		void Awake()
		{
			ChargeCache = new ChargeCache();
			motion = new MotionUnit(control, ChargeCache);
		}
	}
}