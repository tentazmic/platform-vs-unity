﻿using System;
using System.Collections;
using UniBoost.Input.Implementations;
using PlatformVS.Core.Input;
using PlatformVS.Core.Input.Motions;
using PlatformVS.Core.Input.Motions.Charge;
using PlatformVS.Core.Input.Units;
using PlatformVS.Input.Collections;
using UniBoost.Input;
using UnityEngine;
using UnityEngine.InputSystem;
using static UniBoost.Input.ProcessInput;

namespace PlatformVS.Input
{
	public class StandardInput : IInput, Controls.IKeyboardActions
	{
		readonly Func<IEnumerator, Coroutine> _startCoroutine;

		Controls.KeyboardActions _actions;

		bool _isActive;

		public StandardInput(Func<IEnumerator, Coroutine> startCoroutine)
		{
			_startCoroutine = startCoroutine;
			
			_actions = new Controls.KeyboardActions(new Controls());
			_actions.SetCallbacks(this);
			
			Control = new InputStick();
			ChargeCache = new ChargeCache();
			Motion = new MotionUnit(Control, ChargeCache);
			Modifier = new InputButton();
			Jump = new InputButton();
			Light = new InputButton();
			Medium = new InputButton();
			Heavy = new InputButton();
			Super = new InputButton();
			Grab = new InputButton();
			Shield = new InputButton();

			IsActive = true;
			
			UniBoost.UpdatePump.OnUpdate += Control.Tick;
			UniBoost.UpdatePump.OnUpdate += Motion.Tick;
		}

		~StandardInput()
		{
			UniBoost.UpdatePump.OnUpdate -= Control.Tick;
			UniBoost.UpdatePump.OnUpdate -= Motion.Tick;
		}
		
		#region IInput

		public bool IsActive
		{
			get => _isActive;
			set
			{
				_isActive = value;
				if (_isActive)
					_actions.Enable();
				else
					_actions.Disable();
			}
		}

		public InputStick Control { get; }
		public IChargeCache ChargeCache { get; }
		public MotionUnit Motion { get; }
		public InputButton Modifier { get; }
		public InputButton Jump { get; }
		public InputButton Light { get; }
		public InputButton Medium { get; }
		public InputButton Heavy { get; }
		public InputButton Super { get; }
		public InputButton Grab { get; }
		public InputButton Shield { get; }

		#endregion // IInput
		
		#region IKeyboardActions
		
		public void OnControl(InputAction.CallbackContext context)
		{
			if (!Process2DAxis(context, Control))
				return;
			
			if (Control.State.IsInTransitionalState())
				_startCoroutine(ShuffleAction(Control));
		}

		public void OnModifier(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Modifier))
				_startCoroutine(ShuffleAction(Modifier));
		}

		public void OnJump(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Jump))
				_startCoroutine(ShuffleAction(Jump));
		}

		public void OnGrab(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Grab))
				_startCoroutine(ShuffleAction(Grab));
		}

		public void OnShield(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Shield))
				_startCoroutine(ShuffleAction(Shield));
		}

		public void OnLight(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Light))
				_startCoroutine(ShuffleAction(Light));
		}

		public void OnMedium(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Medium))
				_startCoroutine(ShuffleAction(Medium));
		}

		public void OnHeavy(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Heavy))
				_startCoroutine(ShuffleAction(Heavy));
		}

		public void OnSuper(InputAction.CallbackContext context)
		{
			if (ProcessButton(context, Super))
				_startCoroutine(ShuffleAction(Super));
		}

		#endregion // IKeyboardActions
	}
}