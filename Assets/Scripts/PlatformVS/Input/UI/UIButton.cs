﻿using System;
using System.CodeDom;
using UniBoost.Input.Implementations;
using UnityEngine;
using UnityEngine.UI;

namespace PlatformVS.Input.UI
{
	public class UIButton : MonoBehaviour
	{
		[SerializeField] Color releasedColour = Color.grey;
		[SerializeField] Color heldColour = Color.yellow;

		[SerializeField] Image buttonImage;
		[SerializeField] Image splashEffect;
		[SerializeField, Min(5)] int splashEffectTimeout = 10;

		[SerializeField] Sprite downSprite;
		[SerializeField] Sprite upSprite;

		InputButton _button;

		Sprite _splashDefaultSprite;
		int _splashEffectCountdown;

		public InputButton Button
		{
			set
			{
				_button = value;
				enabled = _button != null;
			}
		}

		void Awake()
		{
			enabled = false;
			_splashDefaultSprite = splashEffect.sprite;
		}

		void Update()
		{
			buttonImage.color = _button.HasInput ? heldColour : releasedColour;

			if (_button.Down)
			{
				splashEffect.sprite = downSprite;
				_splashEffectCountdown = splashEffectTimeout;
			}
			else if (_button.Up)
			{
				splashEffect.sprite = upSprite;
				_splashEffectCountdown = splashEffectTimeout;
			}

			_splashEffectCountdown--;
			if (_splashEffectCountdown == 0)
				splashEffect.sprite = _splashDefaultSprite;
		}
	}
}