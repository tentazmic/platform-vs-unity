﻿using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor;

namespace UniBoost.Editor
{
	[CustomEditor(typeof(Object), true, isFallback = true)]
	public class UIEditorStandIn : UnityEditor.Editor
	{
		public override VisualElement CreateInspectorGUI()
		{
			var container = new VisualElement();
			UIElementsHelper.FillDefaultInspector(container, serializedObject, false);
			return container;
		}
	}
}