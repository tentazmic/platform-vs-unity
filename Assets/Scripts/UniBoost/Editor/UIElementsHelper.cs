﻿using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;

namespace UniBoost.Editor
{
	public static class UIElementsHelper
	{
		public static void FillDefaultInspector(VisualElement container, SerializedObject serializedObject, bool hideScript)
		{
			var property = serializedObject.GetIterator();
			if (!property.NextVisible(true)) return;
			
			do
			{
				if (property.propertyPath == "m_Script" && hideScript)
				{
					continue;
				}

				var field = new PropertyField(property) {name = "PropertyField:" + property.propertyPath};

				if (property.propertyPath == "m_Script" && serializedObject.targetObject != null)
				{
					field.SetEnabled(false);
				}
 
				container.Add(field);
			} while (property.NextVisible(false));
		}
	}
}