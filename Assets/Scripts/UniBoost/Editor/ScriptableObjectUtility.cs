﻿using System.IO;
using UnityEngine;
using UnityEditor;

namespace UniBoost
{
	public static class ScriptableObjectUtility
	{
		/// <summary>
		///	This makes it easy to create, name and place unique new ScriptableObject asset files.
		/// </summary>
		public static void CreateAsset<T>() where T : ScriptableObject
		{
			var path = AssetDatabase.GetAssetPath(Selection.activeObject);
			if (path == "") 
				path = "Assets";
			else if (Path.GetExtension(path) != "") 
				path = path.Replace (Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
 
			path += "/New " + typeof(T);
			
			CreateAssetAtPath<T>(path);
			EditorUtility.FocusProjectWindow();
		}


		public static void CreateAssetAtPath<T>(string path) where T : ScriptableObject
		{
			var asset = ScriptableObject.CreateInstance<T>();
			
			var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + ".asset");
 
			AssetDatabase.CreateAsset(asset, assetPathAndName);
 
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}
}