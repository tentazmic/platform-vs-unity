﻿using System;

namespace UniBoost.Input
{
	[Serializable]
	public enum ControlState { Released, Down, Held, Up }

	public static class EControlState
	{
		public static void ShuffleState(this ref ControlState state)
			=> state = (ControlState) (((int)state + 1) % 4);

		public static bool IsInTransitionalState(this ControlState state)
			=> state == ControlState.Down || state == ControlState.Up;

		public static bool IsActivated(this ref ControlState state)
			=> state == ControlState.Down || state == ControlState.Held;
	}
}