﻿using static UnityEngine.Mathf;
using UniBoost.Extensions;

namespace InputAssist.Utilities
{
	public static class MathEx
	{
		public static float RemoveDeadZone(this float value, float deadZone)
		{
			var s = Sign(value);
			var abs = Abs(value);
			abs -= deadZone;
			abs = Abs(abs);
			return abs.Remap(0, deadZone, 0, 1) * s;
		}
		
		/// Remaps the value from one range to another
		// public static float Remap(this float value, float fromMin, float fromMax, float toMin, float toMax)
		// 	=> (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;
	}
}
