﻿using System;
using UnityEngine;

namespace UniBoost.Input.Implementations
{
	[Serializable]
	public class InputButton : IInputUnit
	{
		[SerializeField] protected ControlState state;

		public ControlState State
		{
			get => state;
			set
			{
				state = value;
				TimeOfLastUpdate = Time.time;
			}
		}

		public float TimeOfLastUpdate { get; protected set; }
		
		public bool Down => this.DDown();
		public bool Held => this.DHeld();
		public bool Up => this.DUp();
		public bool Released => this.DReleased();

		public bool HasInput => this.DHasInput();

		public virtual void Tick() { }

		public virtual void ShuffleState() => state.ShuffleState();
	
		public virtual void Reset() => this.DReset();
		
		public override string ToString() => $"{nameof(InputButton)} State: {State.ToString()}";

		public static implicit operator bool(InputButton control) => control.HasInput;
	}
}