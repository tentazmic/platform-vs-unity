﻿using System;
using UnityEngine;

namespace UniBoost.Input.Implementations
{
	[Serializable]
	public class InputGeneric<T> : InputButton, IInputUnit<T>
	{
		[SerializeField] protected T value;
		
		public virtual T Value
		{
			get => value;
			set => this.value = value;
		}

		public override void ShuffleState()
		{
			base.ShuffleState();
			if (state == ControlState.Released)
				value = default;
		}

		public override void Reset()
		{
			base.Reset();
			value = default;
		}

		public override string ToString() => base.ToString() + "; Value: " + value;

		public static implicit operator T(InputGeneric<T> inputGeneric) => inputGeneric.value;
	}
}