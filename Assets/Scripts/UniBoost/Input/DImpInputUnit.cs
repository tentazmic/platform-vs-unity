﻿using UnityEngine;

namespace UniBoost.Input
{
	public static class DImpInputUnit
	{
		public static bool DDown(this IInputUnit unit)
			=> unit.State == ControlState.Down;

		public static bool DHeld(this IInputUnit unit)
			=> unit.Down || unit.State == ControlState.Held;

		public static bool DUp(this IInputUnit unit)
			=> unit.State == ControlState.Up;

		public static bool DReleased(this IInputUnit unit)
			=> unit.Up || unit.State == ControlState.Released;

		public static bool DHasInput(this IInputUnit unit)
			=> unit.Down || unit.Held;

		public static void DReset(this IInputUnit unit) => unit.State = ControlState.Released;
	}
}