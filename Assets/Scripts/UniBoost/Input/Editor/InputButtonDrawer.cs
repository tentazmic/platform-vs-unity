﻿using UniBoost.Input.Implementations;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace UniBoost.Input.Editor
{
	// [CustomPropertyDrawer(typeof(InputButton))]
	public class InputButtonDrawer : PropertyDrawer
	{
		const string MainVisualTree = "Assets/Scripts/UniBoost/Input/Editor/UIElements/Control/ui_button.uxml";
		const string MainStyleSheet = "Assets/Scripts/UniBoost/Input/Editor/UIElements/Control/ss_button.uss";

		SerializedProperty _stateProperty;

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(MainVisualTree).CloneTree(property.propertyPath);
			// var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(MainStyleSheet);
			//
			// visualTree.styleSheets.Add(styleSheet);

			_stateProperty = property.FindPropertyRelative("atom.state");

			visualTree.Q<Label>("display-name").text = property.displayName;

			var button = visualTree.Q<Button>();
			button.text = StateToButtonText();
			button.clicked += () =>
			{
				var pressed = !GetPressed();
				_stateProperty.enumValueIndex = ButtonToStateIndex(pressed);
				button.text = StateToButtonText();
				property.serializedObject.ApplyModifiedProperties();
			};
			
			var enumField = visualTree.Q<EnumField>();
			enumField.RegisterCallback<FocusOutEvent>(evt =>
			{
				button.text = StateToButtonText();
			});

			var container = new VisualElement();
			container.Add(visualTree);
			return container;
		}

		bool GetPressed() => _stateProperty.enumValueIndex == 0 || _stateProperty.enumValueIndex == 1;

		string StateToButtonText()
		{
			var index = _stateProperty.enumValueIndex;
			return index == 0 || index == 1 ? "Pressed" : "Released";
		}

		int ButtonToStateIndex(bool pressed) => pressed ? 0 : 2;
	}
}