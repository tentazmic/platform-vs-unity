﻿namespace UniBoost.Input
{
	public interface IInputUnit
	{
		ControlState State { get; set; }
		float TimeOfLastUpdate { get; }
		
		bool Down { get; }
		bool Held { get; }
		bool Up { get; }
		bool Released { get; }
		
		bool HasInput { get; }

		void Tick();
		void ShuffleState();
		void Reset();
	}

	public interface IInputUnit<T> : IInputUnit
	{
		T Value { get; set; }
	}
}