﻿using System.Collections;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace UniBoost.Input
{
	public static class ProcessInput
	{
		/// <summary>
		/// Processes a binary input. Designed for use when the Action Type is set to 'Button'
		/// and 'Press' Interaction set to 'Release Only' is set 
		/// </summary>
		/// <param name="context">The callback context from the new InputSystem</param>
		/// <param name="inputUnit">The Stream to alter</param>
		/// <returns>Should we shuffle the state</returns>
		public static bool ProcessButton(CallbackContext context, IInputUnit inputUnit)
		{
			if (context.started)
				inputUnit.State = ControlState.Down;

			if (context.performed)
				inputUnit.State = ControlState.Up;

			return !context.canceled;
		}
		
		/// <summary>
		/// Processes the context as a 1D axis
		/// </summary>
		/// <param name="context">The callback context from the new InputSystem</param>
		/// <param name="inputUnit">The Stream to alter</param>
		public static void Process1DAxis(CallbackContext context, IInputUnit<float> inputUnit)
		{
			if (Time.time <= inputUnit.TimeOfLastUpdate)
				return;

			if (context.canceled)
			{
				inputUnit.State = ControlState.Up;
				return;
			}

			inputUnit.Value = context.ReadValue<float>();
			
			if (context.started)
				inputUnit.State = ControlState.Down;

			if (context.performed)
				inputUnit.State = ControlState.Held;
		}
		
		/// <summary>
		/// Processes the context as a 2D axis
		/// </summary>
		/// <param name="context">The callback context from the new InputSystem</param>
		/// <param name="inputUnit">The Stream to alter</param>
		public static bool Process2DAxis(CallbackContext context, IInputUnit<Vector2> inputUnit)
		{
			if (Time.time <= inputUnit.TimeOfLastUpdate)
				return false;

			var value = context.ReadValue<Vector2>();

			var canceled = context.canceled || value == Vector2.zero;

			if (canceled)
			{
				inputUnit.State = ControlState.Up;
				inputUnit.Value = Vector2.zero;
				return true;
			}

			inputUnit.Value = value;
			
			if (context.started)
				inputUnit.State = ControlState.Down;
			else if (context.performed)
				inputUnit.State = ControlState.Held;

			return true;
		}

		/// <summary>
		/// Processes a binary input as a 1D axis
		/// </summary>
		/// <param name="context">The callback context from the new InputSystem</param>
		/// <param name="inputUnit">The Stream to alter</param>
		/// <param name="positiveDirection">The value to add when the button is down</param>
		public static void ProcessButtonAsAxis(CallbackContext context, IInputUnit<float> inputUnit, float positiveDirection)
		{
			if (!(context.started && context.performed))
				return;
			
			if (context.started)
			{
				// If the current state is Down or Held the player is still committed to the input
				// so no need to reset
				if (!inputUnit.Held)
					inputUnit.State = ControlState.Down;
				inputUnit.Value += positiveDirection;
			}

			if (!context.performed) return;
			inputUnit.Value -= positiveDirection;
			inputUnit.State = ControlState.Up;
		}

		/// <summary>
		/// Shuffles the provided action on this frame and the next frame
		/// </summary>
		/// <param name="inputUnit">The action to shuffle</param>
		/// <returns></returns>
		public static IEnumerator ShuffleAction(IInputUnit inputUnit)
		{
			yield return new WaitForEndOfFrame();
			inputUnit.ShuffleState();
		}
	}
}