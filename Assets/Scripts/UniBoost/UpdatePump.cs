﻿/*	
	http://wiki.unity3d.com/index.php/UpdatePump
*/
using System;
using UniBoost.Singletons;

namespace UniBoost
{
	/// <summary>
	/// A way of executing Unity's update messages on non-MonoBehaviour objects.
	/// </summary>
	[UnitySingleton(UnitySingletonType.CreateNew)]
	public class UpdatePump : UnitySingleton<UpdatePump>
	{
		Action _onUpdate;
		Action _onFixedUpdate;
		Action _onLateUpdate;
		
		public static event Action OnUpdate
		{
			add => Instance._onUpdate += value;
			remove
			{
				if (InstanceExists) Instance._onUpdate -= value;
			}
		}

		public static event Action OnFixedUpdate
		{
			add => Instance._onFixedUpdate += value;
			remove
			{
				if (InstanceExists) Instance._onFixedUpdate -= value;
			}
		}
		
		public static event Action OnLateUpdate
		{
			add => Instance._onLateUpdate += value;
			remove
			{
				if (InstanceExists) Instance._onLateUpdate -= value;
			}
		}

		void Update() => _onUpdate?.Invoke();
		void FixedUpdate() => _onFixedUpdate?.Invoke();
		void LateUpdate() => _onLateUpdate?.Invoke();

		void OnDestroy()
		{
			_onUpdate = null;
			_onFixedUpdate = null;
			_onLateUpdate = null;
		}
	}
}