﻿// http://wiki.unity3d.com/index.php?title=FramesPerSecond

using System.Collections;
using UnityEngine;

namespace UniBoost.Utilities
{
	/// <summary>
	/// Calculates the FPS over each Update
	/// </summary>
	[AddComponentMenu( "Utilities/FPS Display")]
	public class FPSDisplay : MonoBehaviour
	{
		const int KTargetFPS = 60;
		const int KCriticalFPS = 10;
		// It is also fairly accurate at very low FPS counts (<10).
		// We do this not by simply counting frames per interval, but
		// by accumulating FPS for each frame. This way we end up with
		// the overall FPS even if the interval renders something like
		// 5.5 frames.
		
		[SerializeField] bool updateColor = true;
		[SerializeField] float frequency = 0.5f;
		[SerializeField] int decimalPlaces = 1;

		float _framesAccumulatedOverInterval;
		int _framesDrawnOverInterval;
		// Color _textColour = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
		string _displayText = "";

		Color _textColour;

		readonly (Color moreFrames, Color targetRange, Color belowRange, Color criticalLow) _textColours =
			(Color.cyan, Color.green, Color.yellow, Color.red);
		
		void Start()
		{
		    StartCoroutine( FPS() );
		}
	 
		void Update()
		{
		    _framesAccumulatedOverInterval += Time.timeScale/ Time.deltaTime;
		    ++_framesDrawnOverInterval;
		}
	 
		IEnumerator FPS()
		{
			// Infinite loop executed every "frequency" seconds
			while( true )
			{
				// Update the FPS
			    var fps = _framesAccumulatedOverInterval / _framesDrawnOverInterval;
			    _displayText = "FPS " + fps.ToString("n" + decimalPlaces);
	 
				//Update the color
				// _textColour = (fps >= 30) ? Color.green : ((fps > 10) ? Color.red : Color.yellow);
				if (Mathf.Approximately(fps, KTargetFPS))
					_textColour = _textColours.targetRange;
				else if (fps > KTargetFPS)
					_textColour = _textColours.moreFrames;
				else if (fps < KCriticalFPS)
					_textColour = _textColours.criticalLow;
				else
					_textColour = _textColours.belowRange;	
				
	 
		        _framesAccumulatedOverInterval = 0.0F;
		        _framesDrawnOverInterval = 0;
	 
				yield return new WaitForSeconds(frequency);
			}
			// ReSharper disable once IteratorNeverReturns
		}
	 
		void OnGUI()
		{
			GUI.color = updateColor ? _textColour : Color.white;
			
			int w = Screen.width, h = Screen.height;
			var rect = new Rect(w - 60, h - 20, w, 20);

			GUI.Label(rect, _displayText);
		}
	}
}