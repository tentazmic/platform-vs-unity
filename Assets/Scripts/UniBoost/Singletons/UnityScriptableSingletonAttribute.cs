﻿using System;

namespace UniBoost.Singletons
{
	[AttributeUsage(AttributeTargets.Class)]
	public class UnityScriptableSingletonAttribute : Attribute
	{
		public readonly UnitySingletonType[] SingletonTypePriority;
		public readonly string ResourcesLoadPath;
		public readonly bool AllowSetInstance;

		public UnityScriptableSingletonAttribute(UnitySingletonType singletonType, string resourcesLoadPath = "",
			bool allowSetInstance = false)
		{
			SingletonTypePriority = new[] {singletonType};
			ResourcesLoadPath = resourcesLoadPath;
			AllowSetInstance = allowSetInstance;
		}


		public UnityScriptableSingletonAttribute(UnitySingletonType[] singletonTypes, string resourcesLoadPath = "",
			bool allowSetInstance = false)
		{
			SingletonTypePriority = singletonTypes;
			ResourcesLoadPath = resourcesLoadPath;
			AllowSetInstance = allowSetInstance;
		}
	}
}