﻿/*
	Author: Jon Kenkel (nonathaj)
	Date: 2/9/16
	
	http://wiki.unity3d.com/index.php/Secure_UnitySingleton
*/

using System;
using UnityEngine;
using static UnityEngine.Debug;

namespace UniBoost.Singletons
{
	/// <summary>
	/// Class for holding singleton component instances in Unity
	/// </summary>
	/// <example>
	/// [UnitySingleton(UnitySingletonType.LoadFromResources, false, "test")]
	/// public class MyClass : UnitySingleton&lt;MyClass&gt; { }
	/// </example>
	/// <example>
	/// [UnitySingleton(UnitySingletonType.CreateNew)]
	/// public class MyOtherClass : UnitySingleton&lt;MyOtherClass&gt; { }
	/// </example>
	/// <typeparam name="T">The type of the singleton</typeparam>
	public abstract class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
	{
		static T _instance = null;

		static bool _shuttingDown = false;
		static object _lock = new object();

		protected static T Instance
		{
			get
			{
				if (_shuttingDown)
				{
					LogWarning($"[Singleton] Instance '{typeof(T)}' already destroyed. Returning null");
					return null;
				}

				lock (_lock)
				{
					if (!InstanceExists)
						Generate();

					return _instance;
				}
			}
			set
			{
				var type = typeof(T);
				var attribute = GetAttribute();
				
				if (attribute == null)
				{
					LogError($"Cannot find UnitySingleton attribute on {type.Name}");
					return;
				}

				if (attribute.AllowSetInstance)
					lock (_lock)
					{
						_instance = value;
					}
				else
					LogError($"{type.Name} is not allowed to set instances. Please set the allowSetInstance flag to true to enable this feature");
			}
		}
		
		public static bool InstanceExists => _instance != null;
		
		#region Statics
 
		/// <summary>
		/// Destroy the current static instance of this singleton
		/// </summary>
		/// <param name="destroyGameObject">Should we destroy the GameObject of the instance too?</param>
		public static void DestroyInstance(bool destroyGameObject = true)
		{
			if (!InstanceExists) return;

			lock (_lock)
			{
				if (destroyGameObject)
					Destroy(_instance.gameObject);
				else
					Destroy(_instance);

				_instance = null;
			}
		}

		static void Generate()
		{
			var attribute = GetAttribute();
			
			if (attribute == null)
			{
				LogError("Cannot find UnitySingleton attribute on " + typeof(T).Name);
				return;
			}

			for (var i = 0; i < attribute.SingletonTypePriority.Length; i++)
			{
				if (TryGenerateInstance(
					attribute.SingletonTypePriority[i], 
					attribute.DestroyOnLoad, 
					attribute.ResourcesLoadPath, 
					i == attribute.SingletonTypePriority.Length - 1))
					break;
			}
		}

		static UnitySingletonAttribute GetAttribute() => (UnitySingletonAttribute) Attribute.GetCustomAttribute(typeof(T), typeof(UnitySingletonAttribute));

		/// <summary>
		/// Attempts to generate a singleton with the given parameters
		/// </summary>
		/// <param name="type"></param>
		/// <param name="resourcesLoadPath"></param>
		/// <param name="destroyOnLoad"></param>
		/// <param name="warn"></param>
		/// <returns></returns>
		static bool TryGenerateInstance(UnitySingletonType type, bool destroyOnLoad, string resourcesLoadPath, bool warn)
		{
			var typeName = typeof(T).Name;
			switch (type)
			{
				case UnitySingletonType.ExistsInScene:
					_instance = FindObjectOfType<T>();
					if (_instance != null) break;
					
					if(warn)
						LogError($"Cannot find an object with a {typeName}. Please add one to the scene.");
					return false;
				
				case UnitySingletonType.LoadFromResources when string.IsNullOrEmpty(resourcesLoadPath):
					if(warn)
						LogError("UnitySingletonAttribute.resourcesLoadPath is not a valid Resources location in " + typeName);
					return false;
					
				case UnitySingletonType.LoadFromResources:
					var pref = Resources.Load<T>(resourcesLoadPath);
					if (pref == null)
					{
						if(warn)
							LogError($"Failed to load prefab with {typeName} component attached to it from folder Resources/{resourcesLoadPath}. Please add a prefab with the component to that location, or update the location.");
						return false;
					}
					
					_instance = Instantiate(pref);
					if (_instance != null) break;
					
					if (warn)
						LogError($"Failed to create instance of prefab {pref} with component {typeName}. Please check your memory constraints");
					return false;
				
				case UnitySingletonType.CreateNew:
				default:
					var go = new GameObject(typeName + " Singleton");
					if (go == null)
					{
						if (warn)
							LogError($"Failed to create GameObject for instance of {typeName}. Please check your memory constraints.");
						return false;
					}
					_instance = go.AddComponent<T>();
					if (_instance != null) break;
					
					if (warn)
						LogError($"Failed to add component of {typeName} to new GameObject. Please check your memory constraints.");
					Destroy(go);
					return false;
			}
	 
			if (!destroyOnLoad)
				DontDestroyOnLoad(_instance.gameObject);
	 
			return true;
		}
		
		#endregion // Statics

		/// <summary>
		/// Called when this object is created.  Children should call this base method when overriding.
		/// </summary>
		protected virtual void Awake()
		{
			if (InstanceExists && _instance != this)
				Destroy(gameObject);
		}

		void OnApplicationQuit() => _shuttingDown = true;
		void OnDestroy() => _shuttingDown = true;
	}
}