﻿/*
	Original:
		Author: Jon Kenkel (nonathaj)
		Date: 2/9/16
	
	http://wiki.unity3d.com/index.php/Secure_UnitySingleton
*/

using System;

namespace UniBoost.Singletons
{
	[AttributeUsage(AttributeTargets.Class)]
	public class UnitySingletonAttribute : Attribute
	{
		public readonly UnitySingletonType[] SingletonTypePriority;
		public readonly bool DestroyOnLoad;
		public readonly string ResourcesLoadPath;
		public readonly bool AllowSetInstance;
 
		public UnitySingletonAttribute(UnitySingletonType singletonCreateType, bool destroyInstanceOnLevelLoad = true, string resourcesPath = "", bool allowSet = false)
		{
			SingletonTypePriority = new [] { singletonCreateType };
			DestroyOnLoad = destroyInstanceOnLevelLoad;
			ResourcesLoadPath = resourcesPath;
			AllowSetInstance = allowSet;
		}
 
		public UnitySingletonAttribute(UnitySingletonType[] singletonCreateTypePriority, bool destroyInstanceOnLevelLoad = true, string resourcesPath = "", bool allowSet = false)
		{
			SingletonTypePriority = singletonCreateTypePriority;
			DestroyOnLoad = destroyInstanceOnLevelLoad;
			ResourcesLoadPath = resourcesPath;
			AllowSetInstance = allowSet;
		}
	}
	
	/// <summary>
	/// What kind of singleton is this and how should it be generated?
	/// </summary>
	public enum UnitySingletonType
	{
		/// Already exists in the scene, just look for it
		ExistsInScene,                  
		/// Load from the Resources folder, at the given path
		LoadFromResources,            
		/// Create a new GameObject and create this singleton on it when needed
		CreateNew
	}
}