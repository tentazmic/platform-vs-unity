﻿using System;
using UnityEngine;
using static UnityEngine.Debug;

namespace UniBoost.Singletons
{
	/// <summary>
	/// Class for singleton ScriptableObject instance in Unity
	/// </summary>
	/// <example>
	/// [UnityScriptableSingleton(UnitySingletonType.LoadFromResources, false, "test")]
	/// public class MyScriptable : UnitySingleton&lt;MyScriptable&gt; { }
	/// </example>
	/// <typeparam name="T">The type of the singleton</typeparam>
	public class UnityScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
	{
		static T _instance;

		static bool _shuttingDown;
		static object _lock = new object();

		protected static T Instance
		{
			get
			{
				if (_shuttingDown)
				{
					LogWarning($"[Singleton] Instance '{typeof(T)}' already destroyed. Returning null");
					return null;
				}

				lock (_lock)
				{
					if (!InstanceExists)
						Generate();

					return _instance;
				}
			}
			set
			{
				var type = typeof(T);
				var attribute =
					(UnityScriptableSingletonAttribute) Attribute.GetCustomAttribute(type,
						typeof(UnityScriptableSingletonAttribute));
				
				if (attribute == null)
				{
					LogError($"Cannot find UnitySingleton attribute on {type.Name}");
					return;
				}

				if (attribute.AllowSetInstance)
					lock (_lock)
					{
						_instance = value;
					}
				else
					LogError($"{type.Name} is not allowed to set instances. Please set the allowSetInstance flag to true to enable this feature");
			}
		}

		public static bool InstanceExists => _instance != null;

		#region Statics
		
		/// <summary>
		/// Destroy the current static instance of this singleton
		/// </summary>
		public static void DestroyInstance()
		{
			if (!InstanceExists) return;

			lock (_lock)
			{
				Destroy(_instance);

				_instance = null;
			}
		}
		
		static void Generate()
		{
			var type = typeof(T);
			var attribute =
				(UnityScriptableSingletonAttribute) 
				Attribute.GetCustomAttribute(type, typeof(UnityScriptableSingletonAttribute));

			if (attribute == null)
			{
				LogError("Cannot find UnityScriptableSingleton attribute on " + typeof(T).Name);
				return;
			}

			for (var i = 0; i < attribute.SingletonTypePriority.Length; i++)
			{
				if (TryGenerateInstance(
					attribute.SingletonTypePriority[i],
					attribute.ResourcesLoadPath,
					i == attribute.SingletonTypePriority.Length - 1)) 
					break;
			}
		}

		/// <summary>
		/// Attempts to generate a singleton with the given parameters
		/// </summary>
		/// <param name="type"></param>
		/// <param name="resourcesLoadPath"></param>
		/// <param name="destroyOnLoad"></param>
		/// <param name="warn"></param>
		/// <returns></returns>
		static bool TryGenerateInstance(UnitySingletonType type, string resourcesLoadPath, bool warn)
		{
			var typeName = typeof(T).Name;

			switch (type)
			{
				case UnitySingletonType.LoadFromResources when string.IsNullOrEmpty(resourcesLoadPath):
					if (warn)
						LogError("UnityScriptableSingletonAttribute.resourcesLoadPath is not a valid Resources location in " + typeName);
					return false;
				
				case UnitySingletonType.LoadFromResources:
					_instance = Resources.Load<T>(resourcesLoadPath);
					if (_instance == null)
					{
						if(warn)
							LogError($"Failed to load ScriptableObject with {typeName} component attached to it from folder Resources/{resourcesLoadPath}. Please add a prefab with the component to that location, or update the location.");
						return false;
					}
					break;
				
				case UnitySingletonType.ExistsInScene:
				case UnitySingletonType.CreateNew:
				default:
					_instance = CreateInstance<T>();
					if (_instance == null)
					{
						if (warn)
							LogError($"Failed to create ScriptableObject of instance{typeName}. Please check your memory constraints.");
						return false;
					}
					break;
			}

			return true;
		}

		#endregion // Statics

		void OnDestroy() => _shuttingDown = true;
	}
}