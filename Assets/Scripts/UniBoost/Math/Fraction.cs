﻿namespace UniBoost.Math
{
	public struct Fraction
	{
		public int Denominator;
		public int Numerator;
		
		public int Floor => Numerator / Denominator;

		public Fraction(int numerator, int denominator = 1)
		{
			Numerator = numerator;
			Denominator = denominator;
			
			Simplify();
		}

		public Fraction(string fraction)
		{
			var pieces = fraction.Split('/');
			
			Numerator = int.Parse(pieces[0]);
			Denominator = int.Parse(pieces[1]);
			
			Simplify();
		}

		void Simplify()
		{
			if (Denominator < 0)
			{
				Numerator = -Numerator;
				Denominator = -Denominator;
			}

			int gcd = GreatestCommonDivisor(Numerator, Denominator);
			Numerator /= gcd;
			Denominator /= gcd;
		}

		public static Fraction operator +(Fraction a, Fraction b)
		{
			var denominator = a.Denominator * b.Denominator;
			var numerator = (a.Numerator * b.Denominator) + (b.Numerator * a.Denominator);
			return new Fraction(numerator, denominator);
		}

		public static Fraction operator -(Fraction a) => new Fraction(-a.Numerator, a.Denominator);
		public static Fraction operator -(Fraction a, Fraction b) => a + -b;

		public static Fraction operator *(Fraction a, Fraction b)
			=> new Fraction(a.Numerator * b.Numerator, a.Denominator * b.Denominator);

		public static Fraction operator /(Fraction a, Fraction b)
			=> a * new Fraction(b.Denominator, b.Numerator);

		public static implicit operator float(Fraction f) => (float) f.Numerator / f.Denominator;

		public override string ToString()
			=> Numerator + "/" + Denominator;

		/// <summary>
		/// Finds the greatest common divisor
		/// Pulled from:
		/// 	https://stackoverflow.com/questions/18541832/c-sharp-find-the-greatest-common-divisor
		/// 	https://github.com/drewnoakes/metadata-extractor-dotnet/blob/master/MetadataExtractor/Rational.cs#L295
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		static int GreatestCommonDivisor(uint a, uint b)
		{
			while (a != 0 && b != 0)
			{
				if (a > b)
					a %= b;
				else
					b %= a;
			}

			return (int)(a | b);
		}
		
		/// <summary>
		/// Finds the greatest common divisor
		/// Pulled from:
		/// 	https://stackoverflow.com/questions/18541832/c-sharp-find-the-greatest-common-divisor
		/// 	https://github.com/drewnoakes/metadata-extractor-dotnet/blob/master/MetadataExtractor/Rational.cs#L295
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		static int GreatestCommonDivisor(int a, int b)
		{
			if (a < 0) a = -a;
			if (b < 0) a = -b;
			
			while (a != 0 && b != 0)
			{
				if (a > b)
					a %= b;
				else
					b %= a;
			}

			return a | b;
		}
	}
}