﻿using System.Collections;
using UnityEngine;
using static UnityEngine.Mathf;

namespace UniBoost.Math
{
	public struct Matrix2x2
	{
		// [ (0, 0)  (0, 1) ]
		// [ (1, 0)  (1, 1) ]
		float[,] data;

		public Matrix2x2(float theta = 0)
		{
			data = new float[2, 2];
			SetRotation(theta);
		}

		public Matrix2x2(float[,] matrix)
		{
			data = new float[2, 2];
			for (var i = 0; i < 2 && i < data.GetLength(0); i++)
				for (var j = 0; j < 2 && j < data.GetLength(1); j++)
					data[i, j] = matrix[i, j];
		}

		public Matrix2x2(Matrix2x2 from) : this(from.data) { }

		public float this[int i, int j]
		{
			get => data[i, j];
			set => data[i, j] = value;
		}

		public void SetRotation(float theta)
		{
			data[0, 0] = Cos(theta);
			data[0, 1] = -Sin(theta);
			data[1, 0] = Sin(theta);
			data[1, 1] = Cos(theta);
		}

		// The expression is for the x calc to use minus, but we already negate the value
		// when setting the rotation
		public static Vector2 operator * (Matrix2x2 mat, Vector2 v2) => new Vector2(
				v2.x * mat[0, 0] + v2.y * mat[0, 1],
				v2.x * mat[1, 0] + v2.y * mat[1, 1]
				);
	}
}