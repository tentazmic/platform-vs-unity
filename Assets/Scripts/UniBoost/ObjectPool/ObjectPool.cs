﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UniBoost.ObjectPool
{
	public class ObjectPool<T> : IObjectPooler<T>
	{
		readonly Func<T> _dwellerProducer;
		
		readonly Queue<T> _sleepingDwellers;
		readonly List<T> _activeDwellers;
		readonly bool _canExpand;

		public int Count { get; private set; }
		public int DwellersInUse => _activeDwellers.Count;

		public ObjectPool(Func<T> dwellerProducer, int initialSize, bool canExpand = false)
		{
			_dwellerProducer = dwellerProducer;
			Count = 0;
			_canExpand = canExpand;
			
			_sleepingDwellers = new Queue<T>(initialSize);
			_activeDwellers = new List<T>(initialSize);
			
			Warm(initialSize);
		}

		void Warm(int capacity)
		{
			for (var i = 0; i < capacity; i++)
				Create();
		}

		T Create()
		{
			var dweller = _dwellerProducer();
			_sleepingDwellers.Enqueue(dweller);
			Count++;
			return dweller;
		}

		public bool PullDweller(out T dweller)
		{
			var empty = _sleepingDwellers.Count == 0;
			if (empty && !_canExpand)
			{
				dweller = default;
				return false;
			}
			
			dweller = empty ? Create() : _sleepingDwellers.Dequeue();
			_activeDwellers.Add(dweller);
			return true;
		}

		public void ReturnDweller(T dweller)
		{
			if (_sleepingDwellers.Contains(dweller) || !_activeDwellers.Contains(dweller))
				return;
			
			if (!_activeDwellers.Remove(dweller)) Count++;
			_sleepingDwellers.Enqueue(dweller);
		}
	}
}