﻿namespace UniBoost.ObjectPool
{
	public interface IObjectPooler<T>
	{
		int Count { get; }
		int DwellersInUse { get; }
		
		bool PullDweller(out T dweller);
		void ReturnDweller(T dweller);
	}
}