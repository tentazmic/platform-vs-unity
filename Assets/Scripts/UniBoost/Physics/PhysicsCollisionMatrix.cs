﻿using System.Collections.Generic;
using UnityEngine;

namespace UniBoost.Physics
{
	public static class PhysicsCollisionMatrix
	{
		static Dictionary<int, int> _masksByLayer;
 
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public static void Init()
		{
			_masksByLayer = new Dictionary<int, int>();
			for (var i = 0; i < 32; i++)
			{
				var mask = 0;
				for (var j = 0; j < 32; j++)
				{
					if(!UnityEngine.Physics.GetIgnoreLayerCollision(i, j))
						mask |= 1 << j;
				}
				_masksByLayer.Add(i, mask);
			}
		}
 
		public static int GetMaskFromLayer(int layer) => _masksByLayer[layer];
	}
}