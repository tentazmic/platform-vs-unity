﻿using System.Collections.Generic;
using UnityEngine;

namespace UniBoost.Physics
{
	[RequireComponent(typeof(MeshCollider))]
	public class ColliderStripper : MonoBehaviour
	{
		[SerializeField] float maxAngle = 45;

		void Start()
		{
			var cos = Mathf.Cos(maxAngle);
			var col = GetComponent<MeshCollider>();

			var sharedMesh = col.sharedMesh;
			var vertices = sharedMesh.vertices;
			var triangles = new List<int>(sharedMesh.triangles);

			for (var i = triangles.Count - 1; i >= 0; i -= 3)
			{
				var point1 = transform.TransformPoint(vertices[triangles[i - 2]]);
				var point2 = transform.TransformPoint(vertices[triangles[i - 1]]);
				var point3 = transform.TransformPoint(vertices[triangles[i]]);
				var faceNormal = Vector3.Cross(point3 - point2, point1 - point2).normalized;

				if (!(Vector3.Dot(Vector3.up, faceNormal) <= cos)) continue;
				triangles.RemoveAt(i);
				triangles.RemoveAt(i - 1);
				triangles.RemoveAt(i - 2);
			}

			col.sharedMesh = new Mesh {vertices = vertices, triangles = triangles.ToArray()};
			
			Destroy(this);
			
		}
	}
}