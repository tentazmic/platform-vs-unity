﻿using UniBoost.Singletons;
using UnityEngine;

namespace UniBoost.Physics
{
	[UnitySingleton(UnitySingletonType.CreateNew, false)]
	public class ManualPhysicsSimulator : UnitySingleton<ManualPhysicsSimulator>
	{
		protected override void Awake()
		{
			UnityEngine.Physics.autoSimulation = false;
		}

		/// <summary>
		/// Get's called at the end of the Update cycle.
		/// Defined in project settings under "Script Execution Order".
		/// </summary>
		void Update()
		{
			var safeDeltaTime = Time.deltaTime < Time.maximumDeltaTime ? Time.deltaTime : Time.maximumDeltaTime;

			Physics2D.Simulate(safeDeltaTime);
		}
	}
}