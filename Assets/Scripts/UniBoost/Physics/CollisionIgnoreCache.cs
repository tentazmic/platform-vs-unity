﻿using System.Collections.Generic;
using System.Linq;
using UniBoost.Singletons;
using UnityEngine;
using static UnityEngine.Physics;

namespace UniBoost.Physics
{
	[UnitySingleton(UnitySingletonType.CreateNew)]
	public class CollisionIgnoreCache : UnitySingleton<CollisionIgnoreCache>
	{
		readonly struct ColliderPair
		{
			public readonly Collider Collider1;
			public readonly Collider Collider2;

			public ColliderPair(Collider collider1, Collider collider2)
			{
				Collider1 = collider1;
				Collider2 = collider2;
			}

			public static bool operator ==(ColliderPair pair1, ColliderPair pair2)
				=> pair1.Collider1 == pair2.Collider1 && pair1.Collider2 == pair2.Collider2
				   || pair1.Collider1 == pair2.Collider2 && pair1.Collider2 == pair2.Collider1;

			public static bool operator !=(ColliderPair pair1, ColliderPair pair2)
				=> !(pair1 == pair2);
			
			public bool Equals(ColliderPair other)
			{
				return Equals(Collider1, other.Collider1) && Equals(Collider2, other.Collider2);
			}

			public override bool Equals(object obj)
			{
				return obj is ColliderPair other && Equals(other);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					return ((Collider1 != null ? Collider1.GetHashCode() : 0) * 397) ^ (Collider2 != null ? Collider2.GetHashCode() : 0);
				}
			}
		}
		
		readonly Dictionary<ColliderPair, int> _pairDictionary = new Dictionary<ColliderPair, int>();
		

		public static void AddColliderPair(Collider collider1, Collider collider2, int frameTimeout = 1,
			bool permanent = false)
		{
			var pair = new ColliderPair(collider1, collider2);
			if (permanent) frameTimeout = -1;
			if (Instance._pairDictionary.ContainsKey(pair))
				return;
			
			Instance._pairDictionary.Add(pair, frameTimeout);
			IgnoreCollision(pair.Collider1, pair.Collider2);
		}

		public static void RemoveColliderPair(Collider collider1, Collider collider2) 
			=> DeleteEntry(new ColliderPair(collider1, collider2));

		public static void RemoveCollider(Collider col)
		{
			var keys = Instance._pairDictionary
				.Where(pair => col == pair.Key.Collider1 || col == pair.Key.Collider2)
				.Select(pair => pair.Key);
			foreach (var key in keys)
				DeleteEntry(key);
		}

		public static bool ContainsPair(Collider collider1, Collider collider2) => Instance._pairDictionary.ContainsKey(new ColliderPair(collider1, collider2));

		void Update()
		{
			var toDelete = new List<ColliderPair>();
			
			foreach (var key in _pairDictionary.Keys.ToList())
			{
				// IgnoreCollision(key.Collider1, key.Collider2);

				var framesLeft = _pairDictionary[key];
				
				if (framesLeft < 0)
					continue;

				framesLeft--;
				_pairDictionary[key] = framesLeft;
				if (framesLeft <= 0)
					toDelete.Add(key);
			}

			foreach (var pair in toDelete)
				DeleteEntry(pair);
		}

		static void DeleteEntry(ColliderPair pair)
		{
			IgnoreCollision(pair.Collider1, pair.Collider2, false);
			Instance._pairDictionary.Remove(pair);
		}
	}
}