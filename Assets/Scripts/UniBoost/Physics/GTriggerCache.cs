﻿using System;
using System.Linq;
using UnityEngine;

namespace UniBoost.Physics
{
	public abstract class GTriggerCache<T> : TriggerCache
	{
		Action<T> _onCurrentUpdated;
		protected Func<Collider, T> Updater;

		Collider _currentCollider;
		public T Current { get; protected set; }

		public event Action<T> OnCurrentUpdated
		{
			add => _onCurrentUpdated += value;
			remove => _onCurrentUpdated -= value;
		}
		
		protected override void OnTriggerEnter(Collider other)
		{
			base.OnTriggerEnter(other);
			
			if (other == _currentCollider)
				return;
			
			_currentCollider = other;
			Current = Updater(_currentCollider);
			_onCurrentUpdated?.Invoke(Current);
		}

		protected override void OnTriggerExit(Collider other)
		{
			base.OnTriggerExit(other);
			
			if (other != _currentCollider)
				return;

			_currentCollider = OverlappedColliders.Count > 0 ? OverlappedColliders.Last() : null;
			Current = Updater(_currentCollider);
			_onCurrentUpdated?.Invoke(Current);
		}
	}
}