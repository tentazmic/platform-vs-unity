﻿using System;
using System.Collections.Generic;
using UniBoost.Extensions;
using UnityEngine;

namespace UniBoost.Physics
{
	[AddComponentMenu("Utilities/Trigger Cache")]
	[RequireComponent(typeof(Rigidbody), typeof(Collider))]
	public class TriggerCache : MonoBehaviour
	{
		Action<Collider> _onTriggerEnter, _onTriggerStay, _onTriggerExit;

		public List<Collider> OverlappedColliders { get; } = new List<Collider>();

		public event Action<Collider> OnTriggerEnterEvent
		{
			add => _onTriggerEnter += value;
			remove => _onTriggerEnter -= value;
		}
		public event Action<Collider> OnTriggerStayEvent
		{
			add => _onTriggerStay += value;
			remove => _onTriggerStay -= value;
		}
		public event Action<Collider> OnTriggerExitEvent
		{
			add => _onTriggerExit += value;
			remove => _onTriggerExit -= value;
		}
		
		void Reset() => Start();
		void Start()
		{
			gameObject.GetOrAddComponent<Collider>().isTrigger = true;
			gameObject.GetComponent<Rigidbody>().useGravity = false;
		}

		public void ClearCache() => OverlappedColliders.Clear();

		protected virtual void OnTriggerEnter(Collider other)
		{
			OverlappedColliders.Add(other);
			_onTriggerEnter?.Invoke(other);
		}

		protected virtual void OnTriggerStay(Collider other) => _onTriggerStay?.Invoke(other);

		protected virtual void OnTriggerExit(Collider other)
		{
			OverlappedColliders.Remove(other);
			_onTriggerExit?.Invoke(other);
		}
	}
}