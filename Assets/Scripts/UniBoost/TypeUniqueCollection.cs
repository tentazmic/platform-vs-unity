﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UniBoost
{
	public class TypeUniqueCollection<T> : IEnumerable<T> where T : class
	{
		readonly IDictionary<Type, T> _entryMap = new Dictionary<Type, T>();

		public void Add(T component)
		{
			if (_entryMap.ContainsKey(component.GetType()))
				return;

			_entryMap.Add(component.GetType(), component);
		}

		public void AddAll(params T[] components)
		{
			foreach (var component in components)
				Add(component);
		}

		public bool Get<U>(out U component) where U : T
		{
			if (!_entryMap.TryGetValue(typeof(U), out var comp))
			{
				component = default;
				return false;
			}

			component = (U) comp;
			return true;
		}

		#region IEnumerable

		public IEnumerator<T> GetEnumerator()
			=> _entryMap.Values.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
		
		#endregion
	}
}