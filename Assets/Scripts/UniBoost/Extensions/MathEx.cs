﻿using UnityEngine;
using static UnityEngine.Mathf;

namespace UniBoost.Extensions
{
	public static class MathEx
	{
		/// <summary>
		/// Is the value within the exclusive range?
		/// </summary>
		/// <param name="value"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool InsideRangeExclusive(this float value, float left, float right)
			=> value > left && value < right;

		/// <summary>
		/// Is the value within the inclusive range?
		/// </summary>
		/// <param name="value"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool InsideRangInclusive(this float value, float left, float right)
			=> value >= left && value <= right;

		public static bool InsideRange(this float value, float left, float right, bool leftExclusive = true,
			bool rightExclusive = true) =>
			(leftExclusive ? value > left : value >= left) && (rightExclusive ? value < right : value <= right);

		/// <summary>
		/// Is the value outside of the exclusive range?	
		/// </summary>
		/// <param name="value"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool OutsideOfRange(this float value, float left, float right)
			=> value < left || value > right;
		
		/// <summary>
		/// Is the value outside of the inclusive range?	
		/// </summary>
		/// <param name="value"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool OutsideOfRange(this int value, int left, int right)
			=> value < left || value > right;
		
		/// <summary>
		/// Remaps the value from one range to another
		/// </summary>
		/// <param name="value"></param>
		/// <param name="fromMin"></param>
		/// <param name="fromMax"></param>
		/// <param name="toMin"></param>
		/// <param name="toMax"></param>
		/// <returns></returns>
		public static float Remap(this float value, float fromMin, float fromMax, float toMin, float toMax)
			=> (value - fromMin) / (fromMax - fromMin) * (toMax - toMin) + toMin;

		/// <summary>
		/// Remaps the value from one range to another
		/// </summary>
		/// <param name="value"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <returns></returns>
		public static float Remap(this float value, Vector2 from, Vector2 to)
			=> value.Remap(from.x, from.y, to.x, to.y);

		public static int AdvSign(this float value)
			=> Approximately(value, 0) ? 0 : (value > 0 ? 1 : -1);
		
		/// Returns the sign values of the vector, if the value is zero it will return zero
		public static Vector2Int Sign(this Vector2 vector2)
			=> new Vector2Int(vector2.x.AdvSign(), vector2.y.AdvSign());

		public static Vector2 Abs(this Vector2 vector2)
			=> new Vector2(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y));

		public static Vector2 GetNormal(this Vector2 vector2, bool getAnticlockwise = false)
			=> new Vector2(vector2.y, vector2.x) * (getAnticlockwise ? new Vector2(-1, 1) : new Vector2(1, -1));
		
		public static Vector2 RotateVector(this Vector2 vector, float theta)
		{
			var x = vector.x * Cos(theta) + vector.y * Sin(theta);
			var y = -vector.x * Sin(theta) + vector.y * Cos(theta);
			return new Vector2(x, y);
		}

		public static Vector3 AccelerateTo(this Vector3 vec, Vector3 target, Vector3 acceleration)
		{
			var signs = new Vector3(
				(int)Mathf.Abs(vec.x - target.x),
				(int)Mathf.Abs(vec.y - target.y),
				(int)Mathf.Abs(vec.z - target.z)
			);

			acceleration.Scale(signs);
			var result = vec + (acceleration * Time.deltaTime);
			var signs2 = new Vector3(
				(int)Mathf.Abs(result.x - target.x),
				(int)Mathf.Abs(result.y - target.y),
				(int)Mathf.Abs(result.z - target.z)
			);
			if (!Approximately(signs.x, signs2.x)) result.x = 0.0f;
			if (!Approximately(signs.y, signs2.y)) result.y = 0.0f;
			if (!Approximately(signs.z, signs2.z)) result.z = 0.0f;

			return result;
		}

		public static bool IsZero(this float f) => Approximately(f, 0);

		public static bool IsClockwiseTo(this Vector2 v1, Vector2 v2)
        {
			var b = v1.y * v2.x > v1.x * v2.y;
			return !b;
        }
	}
}