﻿using System.Collections.Generic;
using UnityEngine;

namespace Fysics
{
	public enum EColPhase { None, Enter, Stay, Exit }

	public static class Extension
	{
		public static void TickPhase(this ref EColPhase phase, bool inContact)
		{
			switch (phase)
			{
				case EColPhase.None:
				case EColPhase.Exit:
					phase = inContact ? EColPhase.Enter : EColPhase.None;
					break;
				case EColPhase.Enter:
				case EColPhase.Stay:
					phase = inContact ? EColPhase.Stay : EColPhase.Exit;
					break;
			}
		}
	}

	public class Contact
	{
		public Dictionary<Body, Vector2> bodies = new();
		public Vector2 normal;
		public Vector2 friction;
		public EColPhase phase = EColPhase.None;

		public bool IsActive => bodies.Count != 0;

		public void AddBody(Body body, Vector2 contactNormal)
		{
			if (!bodies.ContainsKey(body))
				bodies.Add(body, contactNormal);
		}
	}
}