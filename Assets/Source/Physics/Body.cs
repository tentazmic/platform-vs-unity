using System;
using System.Collections.Generic;
using UnityEngine;
using UniBoost.Math;

namespace Fysics
{
	public class Body : MonoBehaviour
	{
		public const int kVerts = 4;
		public const uint kInfiniteMassThreshold = 100000;
		public const float kRigidityMax = 100;
		public const float kShockAbsorptionTotalMax = 20;

		[SerializeField] Transform _origin;
		[SerializeField, Range(0, 360)] float rotation = 0;
		[Range(1, kInfiniteMassThreshold + 1)] public uint mass = 10;
		[SerializeField, Range(0, 10)] float groundFriction = 2;
		[Range(0, 1)] public float airFriction = 0.2f; // Acts as the drag coefficient

		public Vector2 velocity;
		public Vector2 acceleration;
		public Vector2 environmentAcceleration;

		public Primitive[] primitives = new[] { new Primitive() };
		public PrimitiveLink[] links = new PrimitiveLink[0];
		public int edt_Selected = -1;

		[SerializeField] Vector2 extents = new(1.4f, 2f);
		// What proportion of the box is in flexible
		// 1 is a completely flexible box, 0 is a completely inflexible
		[SerializeField, Range(0, 1)] float flexion = 0.1f;
		// How quickly the body restores any deformation
		[Range(0.1f, 1)] public float restorationFactor = 0.4f;
		// How much the box prevents itself from deforming
		[SerializeField, Range(0.1f, kRigidityMax)] public float rigidity = 10;
		// How much of the velocity is absorbed upon collisioin
		[SerializeField, Range(0, 100)] float shockAbsorption = 2;

		// When off, the individual points can deform independently
		// When on, when one point deforms the two adjacent points deform
		//          with them
		public bool deformAsOne = false;

		// Ghosting and Diving
		// Truth be told these should be the same thing
		// Rather than something being Ghostable it should have a Ghost Density
		// Each Body has its own Dive Density, if your Dive Density is lower than
		// their Ghost Density you can dive through them
		public bool isEnveloped = false;
		public int divingTime = -1;
		public bool ghostable = true;
		public bool ghosting = false;

		[NonSerialized]
		public Body preemptiveDiveBody;
		[NonSerialized] public int preemptiveTick;

		[NonSerialized] public Vector2[] FullBox = new Vector2[4];
		[NonSerialized] public Vector2[] RigidBox = new Vector2[4];
		[NonSerialized] public Vector2[] FlexBox = new Vector2[4];

		// Won't be needing information on what point the collision happens with
		//[NonSerialized] public List<Contact> contacts = new ();
		[NonSerialized] public bool inContact = false;
		[NonSerialized] public Contact[] contacts = new[] { new Contact(), new Contact(), new Contact(), new Contact() };

		[NonSerialized] public FluidBox containingFluid;

		[NonSerialized] public float groundFrictionMultiplier = 1;

		// Experienced Forces

		public Vector2 Position => _origin.position;
		public Vector2 FeetPos => transform.position;

		public bool HasInfiniteMass => mass > kInfiniteMassThreshold;
		public bool IsInFlexible => Mathf.Approximately(0, flexion);
		public float Rigidity => IsInFlexible ? kRigidityMax : rigidity;
		public float ShockAbsorption => shockAbsorption;

		public float GroundFriction => groundFriction * groundFrictionMultiplier;

		public bool DiveIntention
		{
			get => divingTime != -1;
			set
			{
				if (value)
					divingTime = divingTime == -1 ? 0 : divingTime;
				else
					divingTime = -1;
			}
		}

		public Body PreemptiveDive
		{
			get => preemptiveTick >= 0 ? preemptiveDiveBody : null;
			set
			{
				preemptiveDiveBody = value;
				if (value) preemptiveTick = 20;
			}
		}

		public Contact TopLeft => contacts[0];
		public Contact TopRight => contacts[1];
		public Contact BottomLeft => contacts[3];
		public Contact BottomRight => contacts[2];

		Matrix2x2 _matRotation = new();

		public float Rotation
		{
			get => rotation;
			set
			{
				rotation = value;
				_matRotation.SetRotation(rotation * Mathf.Deg2Rad);
			}
		}

		public Matrix2x2 RotationMatrix => new Matrix2x2(_matRotation);

		//public void ApplyMotion(Vector2 motion) => transform.position += (Vector3)motion;

		private void Reset()
		{
			_origin = transform;
		}

		public void SetPosition(Vector2 position)
		{
			var offset = _origin.position - transform.position;
			transform.position = (Vector3)position + offset;
		}

		public Vector2 GetClosestPointOnSurface(Vector2 position)
		{
			var dist = float.MaxValue;
			var pos = Vector2.zero;
			for (var i = 0; i < kVerts; i++)
			{
				var j = (i + 1) % kVerts;
				var line1 = FlexBox[i] + Position;
				var line2 = FlexBox[j] + Position;
				var point = FysicsManager.ClosestPointOnLine(position, line1, line2);

				var d = (position - point).sqrMagnitude;
				if (d < dist)
				{
					dist = d;
					pos = point;
				}
			}
			return pos;
		}

		#region Should Be in the Fysics Manager

		public void CalculateBoxes()
		{
			var f = 1f - flexion;
			for (int i = 0; i < FullBox.Length; i++)
			{
				Vector2 diff;
				if (i == 0) diff = new Vector2(-extents.x, extents.y);
				else if (i == 1) diff = extents;
				else if (i == 2) diff = new Vector2(extents.x, -extents.y);
				else diff = -extents;

				FullBox[i] = diff;
				FlexBox[i] = diff;
				RigidBox[i] = diff * f;
			}
		}

		public void ResetState()
		{
			inContact = false;
			isEnveloped = false;
			for (int i = 0; i < FullBox.Length; i++)
			{
				FlexBox[i] = FullBox[i];
				contacts[i].bodies = new();
				contacts[i].normal = Vector2.zero;
			}
		}

		public void ResetMultipliers()
		{
			groundFrictionMultiplier = 1f;
		}

		public Contact[] GetContactsInDirection(Vector2 direction, float factor = 0.1f)
		{
			if (direction == Vector2.zero) return new Contact[0];

			direction = _matRotation * direction;
			List<Contact> contactsInDir = new List<Contact>();
			foreach (var c in contacts)
			{
				if (Vector2.Dot(c.normal, direction) >= factor)
					contactsInDir.Add(c);
			}
			return contactsInDir.ToArray();
		}

		#endregion

		private void Awake()
		{
			if (!_origin)
				_origin = transform;

			_matRotation = new(rotation * Mathf.Deg2Rad);
		}

		void Start()
		{
			FindObjectOfType<FysicsManager>()?.AddBody(this);
		}

		void OnDestroy()
		{
			FindObjectOfType<FysicsManager>()?.RemoveBody(this);
		}

		[NonSerialized] public bool DrawAllGizmoBoxes = true;

        private void OnDrawGizmosSelected()
        {
			DrawAllGizmoBoxes = false;
        }

        private void OnDrawGizmos()
		{
			for (var i = 0; i < primitives.Length; i++)
			{
				BodyEditor.DrawPrimitive(this, i);

				if (!DrawAllGizmoBoxes && edt_Selected != i)
					continue;

				var p = primitives[i];
				Vector3 min = Vector3.one * float.MaxValue,
				        max = Vector3.one * float.MinValue;
				var sum = Vector3.zero;
				for (var j = 0; j < Primitive.kVerts; j++)
                {
					var fb = p.FullBox[j] + Position;
					sum += (Vector3) fb;
					if (fb.x < min.x) min.x = fb.x;
					if (fb.y < min.y) min.y = fb.y;
					if (fb.x > max.x) max.x = fb.x;
					if (fb.y > max.y) max.y = fb.y;
				}

				var center = sum / 4;
                Gizmos.color = new Color(0.1f, 0.1f, 0.1f, 0.05f);
                Gizmos.DrawCube(center,
                    new Vector3(Mathf.Abs(max.x - min.x), Mathf.Abs(max.y - min.y), 1f));
			}

			DrawAllGizmoBoxes = true;
		}
	}
}
