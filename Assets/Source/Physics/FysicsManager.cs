﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static UnityEngine.Mathf;
using UniBoost.Extensions;
using UniBoost.Math;

namespace Fysics
{
	// TODO When a body moves into another without a constant force pushing
	// them together they separate even though they shouldn't, prevents Clinging
	// on walls
	// Need a way to fix/get around this

	public class FysicsManager : MonoBehaviour
	{
		[SerializeField] bool _acceptAnyway = false;
		[SerializeField] List<Body> _selection = new();
		public List<FluidBox> _fluids = new();

		List<BData> _bodies = new();

		[SerializeField] Vector2 gravity = new Vector2(0, -9.81f);
		[SerializeField] float massWeighting = 3.0f;
		[SerializeField, Range(0, 1)] float defaultFrictionFactor = 0.4f;

		public bool useRegularTick = true;
		[SerializeField] bool _applyMotion = false;
		[SerializeField] bool _velocityAsMotion = false; 

		[SerializeField] bool _drawFrameStart = true;
		[SerializeField] bool _drawProjected = true;
		[SerializeField] bool _drawContact = true;
		[SerializeField] bool _drawFlex = true;
		[SerializeField] bool _drawResolution = true;

		[NonSerialized] public bool stepFlag = false;

		bool Draw => _drawFrameStart || _drawResolution || _drawContact || _drawFlex || _drawResolution;

		bool _blockBodyAdditions = false;

		public Body GetBody(int index) => _bodies[index].body;

		#region Public

		public List<Body> CircleOverlap(Vector2 position, float radius, params Body[] skippables)
		{
			var bodies = _bodies.Select(bData => bData.body).ToArray();
			return CircleOverlap(position, radius, bodies, skippables);
		}

		public static List<Body> CircleOverlap(Vector2 position, float radius, Body[] bodies, params Body[] skippables)
		{
			// Should add this data to a queue so that it can be done in place
			// with the physics step but going lazy for now
			var overlaps = new List<Body>();
			var skippables_ = new HashSet<Body>(skippables);
			foreach (var body in bodies)
			{
				if (skippables_.Contains(body))
					continue;

				for (int i = 0; i < Body.kVerts; i++)
				{
					var j = (i + 1) % Body.kVerts;
					var line1 = body.FlexBox[i] + body.Position;
					var line2 = body.FlexBox[j] + body.Position;
					var dist = DistanceToLine(position, line1, line2);

					if (dist > radius)
						continue;

					overlaps.Add(body);
					break;
				}
			}

			return overlaps;
		}

		static bool BoxOverlap(Vector2[] boxA, Vector2[] boxB)
		{
			void Check(in Vector2 vertex, in Plane plane, ref bool active)
			{
				var endFactor = GetBoundOverlapFactor(plane, vertex);
				active &= endFactor < 0.001f;
			}

			var pointsA = new[] { true, true, true, true };
			var pointsB = new[] { true, true, true, true };

			for (int edge = 0; edge < Body.kVerts; edge++)
			{
				var edgeEnd = (edge + 1) % Body.kVerts;
				var planeA = CalcPlaneData(boxA[edge], boxA[edgeEnd]);
				var planeB = CalcPlaneData(boxB[edge], boxB[edgeEnd]);
				for (var vert = 0; vert < Body.kVerts; vert++)
				{
					Check(in boxA[vert], in planeB, ref pointsA[vert]);
					Check(in boxB[vert], in planeA, ref pointsB[vert]);
				}
			}

			for (int i = 0; i < 4; i++)
				if (pointsA[i] || pointsB[i])
					return true;
			return false;
		}

		public List<Body> BoxOverlaps(Vector2[] box, params Body[] skippables)
		{
			var bodies = _bodies.Select(bData => bData.body).ToArray();
			return BoxOverlaps(box, bodies, skippables);
		}

		public static List<Body> BoxOverlaps(Vector2[] box, Body[] bodies, params Body[] skippables)
		{
			// Should add this data to a queue so that it can be done in place
			// with the physics step but going lazy for now
			var overlaps = new List<Body>();
			var skippables_ = new HashSet<Body>(skippables);
			foreach (var body in bodies)
			{
				if (skippables_.Contains(body))
					continue;

				var otherBox = body.FlexBox.Select(vert => vert + body.Position).ToArray();
				if (BoxOverlap(box, otherBox))
					overlaps.Add(body);
			}

			return overlaps;
		}

		public void AddGhostContact(Body self, Body other)
		{
			for (var i = 0; i < _bodies.Count; i++)
			{
				if (_bodies[i].body != other)
					continue;

				AddGhostContact(self, i);
				return;
			}
		}

		public void AddGhostContact(Body self, int other)
		{
			if (!_bodies[other].body.ghostable)
				return;

			BData A, B;
			for (var i = 0; i < _bodies.Count; i++)
			{
				if (_bodies[i].body == self)
				{
					if (i < other)
					{
						A = _bodies[i];
						B = _bodies[other];
					}
					else
					{
						A = _bodies[other];
						B = _bodies[i];
					}

					if (!_cache.GhostContacts.ContainsKey((A, B)))
						_cache.GhostContacts.Add((A, B), 0);
					return;
				}
			}
		}

		#endregion

		class DebugData
		{
			public Vector3 frameStart;
			public Vector3 projected;
			public Vector3 postContact;
			public Vector3[] postFlexPhaseBox = new Vector3[Body.kVerts];
			public Vector3 postResolution;
		}

		readonly FysicsCache _cache = new();
		Dictionary<Body, DebugData> _debugData = new();

		public void AddBody(Body body)
		{
			if (_blockBodyAdditions) return;
			if (_bodies.Where(data => data.body == body).Count() != 0)
			{
				Debug.LogWarning($"Tried to add body {body.name} twice");
				return;
			}

			body.CalculateBoxes();
			var data = new BData();
			data.body = body;
			_bodies.Add(data);
			_debugData.Add(body, new DebugData());
		}

		public void RemoveBody(Body body)
		{
			if (_blockBodyAdditions) return;
			var i = _bodies.FindIndex(data => data.body == body);
			if (i == -1)
				return;
			
			_bodies.RemoveAt(i);
			_debugData.Remove(body);
		}

		public void AddFluid(FluidBox box)
		{
			if (_fluids.Contains(box))
			{
				Debug.LogWarning($"Tried to add body {box.name} twice");
				return;
			}

			_fluids.Add(box);
		}

		public FluidBox FindContainingFluid(Body body)
		{
			var pos = (Vector2)body.Position;
			foreach (var fluid in _fluids)
			{
				var fluidPos = (Vector2)fluid.transform.position;
				Vector2 min = fluidPos - fluid.extents, max = fluidPos + fluid.extents;
				if (pos.x > min.x && pos.y > min.y && pos.x < max.x && pos.y < max.y)
					return fluid;
			}
			return null;
		}

		public void RemoveFluid(FluidBox box)
		{
			if (!_fluids.Contains(box))
				return;

			_fluids.Remove(box);
		}

		#region Data
		// In the proper implementation, group up the infinite and non-infinity mass bodies
		// Perform the non-infinite to infinite resolution first
		// Then the non-infinite to non-infinite
		// Of course, no infinte to infinite

		public struct Plane
		{
			public Vector2 pos1, pos2;
			public Vector2 normal;
			public float zero;

			public Plane Inverted() => new Plane { pos1 = pos1, pos2 = pos2, normal = -normal, zero = -zero };

			public override string ToString() => $"{normal}";
		}

		public class Point
		{
			public bool active = true;
			public Plane plane;
			public int iOther;
			public float distanceToEdge = float.MaxValue;
			public float factor = float.PositiveInfinity;

			public override string ToString() => (active ? "T" : "F") + $",{factor} {plane}";
		}

		public class CrossoverFactor
		{
			public Body other;
			public float factor = 1f; // 0 - 1, a proportion of the motion
		}

		public class BData
		{
			public Body body;
			public Vector2 motion;
			public Vector2 endPosition;
			public Vector2[] BeginBox = new Vector2[Body.kVerts];
			public Vector2[] EndBox = new Vector2[Body.kVerts];

			public Vector2 adjustmentVector = Vector2.zero;
			public float adjustmentMagnitude = 0;
			public float crossOverFactor = 0;
			public Vector2 shockAbsorptionVector = Vector2.zero;
			public float shockAbsorptionMagnitude = 0;

			public Vector2[] flexAdjustments = new Vector2[Body.kVerts];

			// Carry Over
			public Vector2 prevVelocity;
			public bool prevGhosting;

			public void Reset()
			{
				adjustmentVector = Vector2.zero;
				adjustmentMagnitude = 0;
				crossOverFactor = 0;
				shockAbsorptionVector = Vector2.zero;
				shockAbsorptionMagnitude = 0;
				for (var i = 0; i < Body.kVerts; i++)
					flexAdjustments[i] = Vector2.zero;
			}

			public void RecalculateEndPositions()
			{
				var mat = body.RotationMatrix;
				var pos = (Vector2)body.Position;
				endPosition = pos + motion;
				for (var i = 0; i < Body.kVerts; i++)
				{
					var vert = mat * body.FlexBox[i];
					BeginBox[i] = pos + vert;
					EndBox[i] = BeginBox[i] + motion;
				}
			}

			public void ApplyFlexAdjustment(int index, Vector2 adj)
			{
				var flex = flexAdjustments[index];
				var sign = flex.Sign();
				if (!Approximately(flex.x, 0) && SameX(flex, adj)) flex.x = sign.x * Max(Abs(adj.x), Abs(flex.x));
				else flex.x += adj.x;

				if (!Approximately(flex.y, 0) && SameY(flex, adj)) flex.y = sign.y * Max(Abs(adj.y), Abs(flex.y));
				else flex.y += adj.y;

				flexAdjustments[index] = flex;
			}

			public override string ToString() => body ? body.name : "NULL";
		}

		public enum EnvelopFlag { A, B, Both }

		public class BetweenCache
		{
			public bool activeOnPreviousFrame;
			public int iAPlane;
			public int iBPlane;
			public HashSet<int> indexesA = new();
			public HashSet<int> indexesB = new();
		}

		public class FysicsCache
		{
			public List<BData> bodies = new ();

			// Crossovers and Oversteps don't have their own tuple list
			// as they will be resolved when they are found
			public HashSet<(BData, BData)> regular = new();
			public Dictionary<(BData, BData), (bool, bool)> regularContacts = new();
			public Dictionary<(BData, BData), EnvelopFlag> envelops = new();

			public Dictionary<(BData, BData), BetweenCache> betweens = new();
			public Dictionary<(BData, BData), BetweenCache> prevBetweens = new();

			public HashSet<(BData, BData)> prevContacts = new();

			//public Dictionary<BData, List<BData>> GhostContacts = new();
			public Dictionary<(BData, BData), int> GhostContacts = new();
			public HashSet<(BData, BData)> FlexIgnores = new();

			public void NextFrame()
			{
				bodies.Clear();
				regular.Clear();
				prevContacts = regularContacts.Keys.ToHashSet();
				regularContacts = new();
				envelops.Clear();
				prevBetweens = betweens;
				betweens = new();
				FlexIgnores.Clear();
			}
		}

		public static bool IsStatic(Body body) => body.velocity.sqrMagnitude < 0.1f;

		public static float ProjectAlongVector(Vector2 vector, Vector2 point) => Vector2.Dot(vector, point);

		public static bool SameX(Vector2 a, Vector2 b) => Mathf.Approximately(Mathf.Sign(a.x), Mathf.Sign(b.x));
		public static bool SameY(Vector2 a, Vector2 b) => Mathf.Approximately(Mathf.Sign(a.y), Mathf.Sign(b.y));

		void PrepareBody(BData data)
		{
			var body = data.body;
			if (body.divingTime >= 0) body.divingTime++;
			if (body.preemptiveTick >= 0) body.preemptiveTick--;

			body.ResetState();
			if (_velocityAsMotion)
				data.motion = body.velocity;
			else
			{
				data.prevVelocity = body.velocity;
				data.motion = body.velocity * Time.deltaTime;
			}

			data.Reset();
			data.RecalculateEndPositions();
		}

		#endregion

		#region Funcs

		public static Vector2 ClosestPointOnLine(Vector2 position, Vector2 line1, Vector2 line2)
		{
			var pLine1 = position - line1;
			var vector = line2 - line1;

			var dot = Vector2.Dot(pLine1, vector);
			var t = dot / vector.sqrMagnitude;
			t = Clamp01(t);

			return line1 + vector * t;
		}

		public static Plane CalcPlaneData(Vector2 pos1, Vector2 pos2)
		{
			Vector2 plane = pos1 - pos2;
			Plane data = new Plane
			{
				pos1 = pos1,
				pos2 = pos2,
				normal = plane.GetNormal().normalized,
			};
			data.zero = ProjectAlongVector(data.normal, pos1);
			return data;
		}

		public static (Plane left, Plane right) CalcPlaneEnds(Plane plane)
		{
			var left = CalcPlaneData(plane.pos1, plane.pos1 + plane.normal);
			var right = CalcPlaneData(plane.pos2, plane.pos2 - plane.normal);
			return (left, right);
		}

		public static float DistanceToLine(Vector2 point, Vector2 line1, Vector2 line2)
		{
			var A = point - line1;
			var line = line2 - line1;
			var dot = Vector2.Dot(A, line);
			var sqLen = line.sqrMagnitude;
			var param = -1.0f;
			if (sqLen != 0) param = dot / sqLen;

			Vector2 linePoint;
			if (param < 0)
			{
				linePoint = line1;
			}
			else if (param > 1)
			{
				linePoint = line2;
			}
			else
			{
				linePoint = line1 + param * line;
			}

			return (point - linePoint).magnitude;
		}

		/// <summary>
		/// An Overlap less than zero means the point has crossed the boundary
		/// </summary>
		/// <param name="plane"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static float GetOverlapFactor(Plane plane, Vector2 point)
		{
			var fct = ProjectAlongVector(plane.normal, point);
			return fct - plane.zero;
		}

		public static float GetBoundOverlapFactor(Plane plane, Vector2 point)
		{
			var bounds = CalcPlaneEnds(plane);
			if (!(IsBehindPlane(bounds.left, point) || IsBehindPlane(bounds.right, point))) return float.MaxValue;

			return GetOverlapFactor(plane, point);
		}

		public static bool IsBehindPlane(Plane plane, Vector2 point)
		{
			var x = point - plane.pos1;
			var y = point - plane.pos2;
			var z = x + y;
			var dot = Vector2.Dot(plane.normal, z.normalized);
			return dot < 0;
		}

		// https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
		public static bool FindCrossoverPoint(Vector2 posA, Vector2 motionA, Vector2 posB, Vector2 motionB, out Vector2 overlapPoint)
		{
			float divisor = -motionB.x * motionA.y + motionA.x * motionB.y;
			var bToA = posA - posB;
			float s = (motionA.x * bToA.y - motionA.y * bToA.x) / divisor;
			float t = (motionB.x * bToA.y - motionB.y * bToA.x) / divisor;

			if (s >= 0f && s <= 1f && t >= 0f && t <= 1f)
			{
				overlapPoint = posA + t * motionA;
				return true;
			}

			overlapPoint = Vector2.zero;
			return false;
		}

		public static float GetRayDistanceThroughFlex(Body body, Vector2 bodyPos, Vector2 origin, Vector2 motion)
		{
			float entryDist = float.MaxValue, exitDist = float.MinValue;
			bool change = false;
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
			{
				var bOrigin = body.FlexBox[i] + bodyPos;
				var bMotion = body.FlexBox[j] - body.FlexBox[i];
				if (!FindCrossoverPoint(origin, motion, bOrigin, bMotion, out var overlapPoint)) continue;

				change = true;
				var dist = (origin - overlapPoint).sqrMagnitude;
				entryDist = Mathf.Min(dist, entryDist);
				exitDist = Mathf.Max(dist, exitDist);
			}

			if (!change) return -1f;

			// The exit should always be bigger than the entry
			return Sqrt(exitDist) - Sqrt(entryDist);
		}

		public static bool IsPointWithinFlex(Body body, Vector2 bodyPos, Vector2 point)
		{
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
			{
				var plane = CalcPlaneData(bodyPos + body.FlexBox[i], bodyPos + body.FlexBox[j]);
				if (!IsBehindPlane(plane, point))
					return false;
			}
			return true;
		}

		public static void FindFlexOverlaps(BData A, BData B, Point[] pointsA, Point[] pointsB)
		{
			void Check(BData body, int iVert, int iEdge, in Plane plane, Point[] points)
			{
				var end = body.EndBox[iVert];
				var endFactor = GetBoundOverlapFactor(plane, end);
				//if (!(startFactor == float.MaxValue || endFactor == float.MaxValue) && Mathf.Sign(startFactor) > 0 && Mathf.Sign(endFactor) < 0)
				//	Debug.Log($"{body.name} {iAgainst}:{index} S {startFactor} E {endFactor}");
				points[iVert].active &= endFactor < 0.001f;
				if (!points[iVert].active) return;

				var dist = DistanceToLine(body.endPosition, plane.pos1, plane.pos2);
				if (dist > points[iVert].distanceToEdge || (Approximately(dist, points[iVert].distanceToEdge) && -endFactor > points[iVert].factor)) return;

				points[iVert].active = true;
				points[iVert].plane = plane;
				points[iVert].iOther = iEdge;
				points[iVert].factor = -endFactor;
				points[iVert].distanceToEdge = dist;
			}

			// If these bodies have an overstep, we need a check that ignores thee edges with an
			// opposing dot product to the overstep edge

			for (int iEdge = 0, jEdge = 1; iEdge < Body.kVerts; iEdge++, jEdge = (jEdge + 1) % Body.kVerts)
			{
				//Vector2 A1 = A.FlexBox[iAgainst] + endA, A2 = A.FlexBox[jAgainst] + endA;
				var planeA = CalcPlaneData(A.EndBox[iEdge], A.EndBox[jEdge]);
				//Vector2 B1 = B.FlexBox[iEdge] + endB, B2 = B.FlexBox[jEdge] + endB;
				var planeB = CalcPlaneData(B.EndBox[iEdge], B.EndBox[jEdge]);

				for (var iVert = 0; iVert < Body.kVerts; iVert++)
				{
					Check(A, iVert, iEdge, planeB, pointsA);
					Check(B, iVert, iEdge, planeA, pointsB);
				}
			}
		}

		public static void FindClosestFlexEdge(BData A, BData B, Point[] pointsA, Point[] pointsB)
		{
			void Check(BData body, int index, in Plane plane, Point[] points)
			{
				var point = body.EndBox[index];
				var factor = GetOverlapFactor(plane, point);
				//var planeCentre = (plane.pos1 + plane.pos2) * 0.5f;
				//var dist = Vector2.Distance(pos, planeCentre);
				var dist = DistanceToLine(body.endPosition, plane.pos1, plane.pos2);
				factor = Abs(factor);
				if (dist < points[index].distanceToEdge || (Approximately(dist, points[index].distanceToEdge) && factor < points[index].factor))
				{
					points[index].active = true;
					points[index].factor = factor;
					points[index].plane = plane;
					points[index].distanceToEdge = dist;
				}
			}

			var first = true;
			for (int iEdge = 0, jEdge = 1; iEdge < Body.kVerts; iEdge++, jEdge = (jEdge + 1) % Body.kVerts)
			{
				var planeA = CalcPlaneData(A.EndBox[iEdge], A.EndBox[jEdge]);
				var planeB = CalcPlaneData(B.EndBox[iEdge], B.EndBox[jEdge]);

				for (var iVert = 0; iVert < Body.kVerts; iVert++)
				{
					if (first)
						pointsA[iVert].active = pointsB[iVert].active = false;

					if (A.body.contacts[iVert].bodies.Count != 0 && A.body.contacts[iVert].bodies.ContainsKey(B.body))
						Check(A, iVert, planeB, pointsA);
					if (B.body.contacts[iVert].bodies.Count != 0 && B.body.contacts[iVert].bodies.ContainsKey(A.body))
						Check(B, iVert, planeA, pointsB);
				}
				first = false;
			}
		}

		//public static void FindClosestOverstepFlexEdge(BData A, BData B, Point[] pointsA, Point[] pointsB)
		//{
		//	void Check(Body body, Vector2 pos, int index, int iAgainst, in Plane plane, Point[] points)
		//	{
		//		var pointStart = pos + body.FlexBox[index];
		//		var motion = body.motion;
		//		if (!FindCrossoverPoint(pointStart, motion, plane.pos1, plane.pos2 - plane.pos1, out var overlapPoint)) return;

		//		var factor = (overlapPoint - pointStart).magnitude / motion.magnitude;
		//		if (factor < points[index].factor)
		//		{
		//			Debug.Log($"{body.name} {index}:{iAgainst} {factor} {plane.normal} {pointStart} {overlapPoint}");
		//			points[index].active = true;
		//			points[index].factor = factor;
		//			points[index].plane = plane;
		//		}
		//	}

		//	var posA = (Vector2)A.transform.position;
		//	var posB = (Vector2)B.transform.position;

		//	for (int iAgainst = 0, jAgainst = 1; iAgainst < Body.kVerts; iAgainst++, jAgainst = (jAgainst + 1) % Body.kVerts)
		//	{
		//		Vector2 A1 = A.FlexBox[iAgainst] + posA, A2 = A.FlexBox[jAgainst] + posA;
		//		var planeA = CalcPlaneData(A.Begin, A2);
		//		Vector2 B1 = B.FlexBox[iAgainst] + posB, B2 = B.FlexBox[jAgainst] + posB;
		//		var planeB = CalcPlaneData(B1, B2);

		//		for (var iCheck = 0; iCheck < Body.kVerts; iCheck++)
		//		{
		//			Check(A, posA, iCheck, iAgainst, planeB, pointsA);
		//			Check(B, posB, iCheck, iAgainst, planeA, pointsB);
		//		}
		//	}
		//}

		public static Plane[] GetOpposingEdges(BData body, Vector2 motion)
		{
			var planes = new List<Plane>();
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				var plane = CalcPlaneData(body.EndBox[i], body.EndBox[j]);
				if (Vector2.Dot(plane.normal, motion) < 0)
					planes.Add(plane);
			}
			return planes.ToArray();
		}

		public static (Plane, int)[] GetOpposingEdgesWithIndexes(BData body, Vector2 motion)
		{
			List<(Plane, int)> planes = new();
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				var plane = CalcPlaneData(body.EndBox[i], body.EndBox[j]);
				if (Vector2.Dot(plane.normal, motion) < 0)
					planes.Add((plane, i));
			}
			return planes.ToArray();
		}

		public static Vector2[] GetForwardPoints(BData body)
		{
			List<Vector2> flexPoints = new List<Vector2>();
			var motion = body.motion;
			motion.Normalize();
			foreach (var point in body.body.FlexBox)
			{
				if (Vector2.Dot(point.normalized, motion) >= -0.4f)
					flexPoints.Add(point + body.body.Position);
			}

			return flexPoints.ToArray();
		}

		public static Vector2[] GetInterpolatedForwardPoints(BData body)
		{
			var flexPoints = GetForwardPoints(body);
			var points = new List<Vector2>();
			if (true)
			{
				// Along lines
				const float kResolution = 1f / 4f;
				for (var i = 0; i < flexPoints.Length - 1; i++)
					for (float t = 0; t <= 1f; t += kResolution)
						points.Add(Vector2.Lerp(flexPoints[i], flexPoints[i + 1], t));
			}
			else
			{
				// Between ends
				var first = flexPoints[0];
				var last = flexPoints.Last();
				const float kResolution = 1.0f / 8f;
				for (float t = 0; t <= 1f; t += kResolution)
					points.Add(Vector2.Lerp(first, last, t));
			}

			return points.ToArray();
		}

		public static Vector2[] GetInterpolatedPoints(BData body, bool withMotion = false)
		{
			var flexPoints = withMotion ? body.EndBox : body.BeginBox;
			var points = new List<Vector2>();
			const float kResolution = 1f / 4f;
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				Vector2 a = flexPoints[i], b = flexPoints[j];
				for (float t = 0; t <= 1f; t += kResolution)
					points.Add(Vector2.Lerp(a, b, t));
			}

			return points.ToArray();
		}

		public static void FindClosestEdgeToMidpoint(BData A, BData B, out Point pointA, out Point pointB)
		{
			void Check(Vector2 pos, int otherEdge, Plane plane, Point point)
			{
				var dist = DistanceToLine(pos, plane.pos1, plane.pos2);
				if (dist < point.distanceToEdge)
				{
					point.distanceToEdge = dist;
					point.plane = plane;
					point.iOther = otherEdge;
				}
			}

			pointA = new Point();
			pointB = new Point();

			var posA = A.endPosition;
			var posB = B.endPosition;

			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				//Vector2 A1 = A.FlexBox[i] + posA, A2 = A.FlexBox[j] + posA;
				var planeA = CalcPlaneData(A.EndBox[i], A.EndBox[j]);
				//Vector2 B1 = B.FlexBox[i] + posB, B2 = B.FlexBox[j] + posB;
				var planeB = CalcPlaneData(B.EndBox[i], B.EndBox[j]);

				Check(posA, i, planeB, pointA);
				Check(posB, i, planeA, pointB);
			}
		}

		public static void FindClosestEdgeToMidpoint(BData me, BData other, out Point point)
		{
			void Check(Vector2 pos, int otherEdge, Plane plane, Point point)
			{
				var dist = DistanceToLine(pos, plane.pos1, plane.pos2);
				if (dist < point.distanceToEdge)
				{
					point.distanceToEdge = dist;
					point.plane = plane;
					point.iOther = otherEdge;
				}
			}

			point = new Point();

			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				var planeB = CalcPlaneData(other.EndBox[i], other.EndBox[j]);

				Check(me.endPosition, i, planeB, point);
			}
		}

		public static float CalculateRatioForA(Body A, Body B, float massWeighting)
		{
			float ratMassA, ratRigidA;
			if (A.HasInfiniteMass == B.HasInfiniteMass) // Neither have infinite mass
			{
				// If the mass difference is above this, treat the larger one as if it has infinite mass
				const int kMassDifferenceThreshold = 1400;
				var diff = Abs(A.mass - B.mass);
				ratMassA = diff > kMassDifferenceThreshold
					? (float)B.mass / (float)(A.mass + B.mass)
					: (A.HasInfiniteMass ? 0 : 1);
			}
			else // Only one has infinite mass
				ratMassA = A.HasInfiniteMass ? 0 : 1;

			// Having infinite mass also means the object is immovable
			if (A.HasInfiniteMass == B.HasInfiniteMass)
				ratRigidA = A.Rigidity / (A.Rigidity + B.Rigidity);
			else
			{
				ratRigidA = A.HasInfiniteMass ? 0 : 1;
			}

			//if (A.IsInFlexible == B.IsInFlexible)
			//else
			//	ratRigidA = A.IsInFlexible ? 1 : 0;

			var ratA = (ratRigidA + ratMassA * massWeighting) / (1.0f + massWeighting);
			return ratA;
		}

		public static Vector2 GetHalfVectorThroughBody(Vector2 startPos, Vector2 direction, BData other)
		{
			// Should make this other.body.extents.x + other.body.extents.y
			var magnitude = 100f;
			var closest = Vector2.zero;
			var farthest = Vector2.zero;
			float vClose = float.PositiveInfinity, vFar = float.NegativeInfinity;
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
			{
				if (!FindCrossoverPoint(startPos, direction * magnitude, other.EndBox[i], other.EndBox[j] - other.EndBox[i], out var overlapPoint))
					continue;

				var dist = (overlapPoint - startPos).sqrMagnitude;
				if (dist < vClose)
				{
					vClose = dist;
					closest = overlapPoint;
				}
				if (dist > vFar)
				{
					vFar = dist;
					farthest = overlapPoint;
				}
			}

			if (float.IsInfinity(vClose) || float.IsInfinity(vFar))
				return Vector2.zero;

			return (farthest - closest) / 2;
		}

		#endregion

		#region Crossover

		public enum CrossoverType {  None, Crossover, Overstep, Between, Envelop }

		public struct CrossoverCache
		{
			public CrossoverType type;
			public BData other;
			public float motionRatio;
		}

		public static bool CheckForCrossover(BData A, BData B, out (float a, float b) ratios)
		{
			(Vector2 point1, Vector2 point2) GetBounds(BData body)
			{
				var vec = body.motion.GetNormal().normalized;
				var plane = CalcPlaneData(Vector2.zero, vec);
				Vector2 point1 = Vector2.zero, point2 = Vector2.zero;
				float fct1 = float.MaxValue, fct2 = float.MinValue;
				for (var i = 0; i < Body.kVerts; i++)
				{
					var factor = GetOverlapFactor(plane, body.body.FlexBox[i]);
					if (factor < fct1)
					{
						fct1 = factor;
						point1 = body.BeginBox[i];
					}
					if (factor > fct2)
					{
						fct2 = factor;
						point2 = body.BeginBox[i];
					}
				}

				return (point1, point2);
			}

			var boundsA = GetBounds(A);
			var boundsB = GetBounds(B);

			float A1B1 = Vector2.Distance(boundsA.point1, boundsB.point1);
			float A1B2 = Vector2.Distance(boundsA.point1, boundsB.point2);
			float A2B1 = Vector2.Distance(boundsA.point2, boundsB.point1);
			float A2B2 = Vector2.Distance(boundsA.point2, boundsB.point2);
			(Vector2 a, Vector2 b) closestPair, furthestPair;
			if (A1B1 < A1B2 && A1B1 < A2B1 && A1B1 < A2B2)
			{
				closestPair.a = boundsA.point1;
				closestPair.b = boundsB.point1;
				furthestPair.a = boundsA.point2;
				furthestPair.b = boundsB.point2;
			}
			else if (A1B2 < A2B1 && A1B2 < A2B2)
			{
				closestPair.a = boundsA.point1;
				closestPair.b = boundsB.point2;
				furthestPair.a = boundsA.point2;
				furthestPair.b = boundsB.point1;
			}
			else if (A2B1 < A2B2)
			{
				closestPair.a = boundsA.point2;
				closestPair.b = boundsB.point1;
				furthestPair.a = boundsA.point1;
				furthestPair.b = boundsB.point2;
			}
			else
			{
				closestPair.a = boundsA.point2;
				closestPair.b = boundsB.point2;
				furthestPair.a = boundsA.point1;
				furthestPair.b = boundsB.point1;
			}

			ratios = (0f, 0f);
			if (!FindCrossoverPoint(closestPair.a, A.motion, closestPair.b, B.motion, out Vector2 overlapPoint))
			{
				return false;
			}

			const float kIntersectionFactor = 1.4f;
			ratios.a = (closestPair.a - overlapPoint).magnitude / A.motion.magnitude * kIntersectionFactor;
			ratios.b = (closestPair.b - overlapPoint).magnitude / B.motion.magnitude * kIntersectionFactor;
			return true;
		}

		public static bool CheckForOverstep(BData bodyM, BData bodyS, out float ratio)
		{
			var points = GetInterpolatedPoints(bodyM);
			var edgeCrossFlags = new byte[points.Length];
			var posM = bodyM.body.Position;

			// These distances are for how far you need to travel for the furthest and
			// closest points (to the plane) to sit on the plane
			var closeDistance = float.PositiveInfinity;
			var farDistance = float.NegativeInfinity;
			var midPointDistance = float.PositiveInfinity;
			var anyOversteps = false;
			ratio = 0;
			for (int i = 0, j = 1; i < Body.kVerts; i++, j = (++j) % Body.kVerts)
			{
				var planeStart = bodyS.EndBox[i];
				var planeMotion = bodyS.EndBox[j] - planeStart;

				if (FindCrossoverPoint(posM, bodyM.motion, planeStart, planeMotion, out var overlapMid))
				{
					var dist = Vector2.Distance(posM, overlapMid);
					if (dist < midPointDistance)
						midPointDistance = dist;
				}

				for (int k = 0; k < points.Length; k++)
				{
					if (IsPointWithinFlex(bodyS.body, bodyS.endPosition, points[k] + bodyM.motion))
						continue;
					if (!FindCrossoverPoint(points[k], bodyM.motion, planeStart, planeMotion, out Vector2 overlap))
						continue;

					anyOversteps |=  ++edgeCrossFlags[k] > 1;

					var dist = Vector2.Distance(points[k], overlap);
					if (dist < closeDistance)
						closeDistance = dist;
					if (dist > farDistance)
						farDistance = dist;
				}
			}

			if (!anyOversteps || float.IsInfinity(closeDistance)) return false;

			var mag = bodyM.motion.magnitude;
			var farDist = float.IsFinite(midPointDistance) ? midPointDistance : farDistance;
			var t = Clamp(mag - farDist, 0, 10f) / 10f;

			ratio = Lerp(closeDistance, farDist, t) / mag; // * kIntersectionFactor;
			return true;
		}

		public static CrossoverType CheckForBetweenAndEnvelop(BData A, BData B, out EnvelopFlag enveloped, out BetweenCache cache)
		{
			bool envelopedA = true, envelopedB = true;
			enveloped = EnvelopFlag.Both;
			cache = new BetweenCache();
			var edgeCrossesA = new HashSet<int>();
			var edgeCrossesB = new HashSet<int>();
			byte[] betweenChecksA = new byte[4], betweenChecksB = new byte[4];
			int overstepEdges = 0;

			for (int iEdge = 0, jEdge = 1; iEdge < Body.kVerts; iEdge++, jEdge = ++jEdge % Body.kVerts)
			{
				var planeA = CalcPlaneData(A.EndBox[iEdge], A.EndBox[jEdge]);
				var planeB = CalcPlaneData(B.EndBox[iEdge], B.EndBox[jEdge]);

				envelopedA &= IsBehindPlane(planeB, A.body.Position);
				envelopedB &= IsBehindPlane(planeA, B.body.Position);

				for (var iVert = 0; iVert < Body.kVerts; iVert++)
				{
					if (FindCrossoverPoint(A.endPosition, A.EndBox[iVert] - A.endPosition, planeB.pos1, planeB.pos2 - planeB.pos1, out var overlapPoint))
					{
						edgeCrossesA.Add(iVert);
						if (++betweenChecksA[iVert] > 1)
						{
							cache.indexesA.Add(iVert);
							overstepEdges++;
						}
					}

					if (FindCrossoverPoint(B.endPosition, B.EndBox[iVert] - B.endPosition, planeA.pos1, planeA.pos2 - planeA.pos1, out overlapPoint))
					{
						edgeCrossesB.Add(iVert);
						if (++betweenChecksB[iVert] > 1)
						{
							cache.indexesB.Add(iVert);
							overstepEdges++;
						}
					}
				}
			}

			//if ((Approximately(normalSumA.x, 0) && Approximately(normalSumA.y, 0)) || (Approximately(normalSumB.x, 0) && Approximately(normalSumB.y, 0)))
			if (overstepEdges > 2 || edgeCrossesA.Count == Body.kVerts || edgeCrossesB.Count == Body.kVerts)
				return CrossoverType.Between;

			if (envelopedA || envelopedB)
			{
				if (!(envelopedA && envelopedB)) enveloped = envelopedA ? EnvelopFlag.A : EnvelopFlag.B;
				return CrossoverType.Envelop;
			}

			return CrossoverType.None;
		}

		public static CrossoverType CrossoverPass(BData A, BData B, out CrossoverCache crossA, out CrossoverCache crossB, out EnvelopFlag enveloped, out BetweenCache cache)
		{
			enveloped = EnvelopFlag.Both;
			cache = new BetweenCache();
			if (A.body.HasInfiniteMass && B.body.HasInfiniteMass)
			{
				crossA = crossB = new CrossoverCache();
				return CrossoverType.None;
			}

			crossA = new CrossoverCache { other = B };
			crossB = new CrossoverCache { other = A };

			var staticA = IsStatic(A.body);
			var staticB = IsStatic(B.body);

			if (! (staticA || staticB))
			{
				if (CheckForCrossover(A, B, out (float a, float b) ratios))
				{
					crossA.motionRatio = ratios.a;
					crossB.motionRatio = ratios.b;
					return CrossoverType.Crossover;
				}
			}

			var type = CheckForBetweenAndEnvelop(A, B, out enveloped, out cache);
			if (type != CrossoverType.None || (staticA && staticB))
				return type;

			// staticA || staticB
			BData bodyS, bodyM;
			if (staticA)
			{
				bodyS = A;
				bodyM = B;
			}
			else
			{
				bodyS = B;
				bodyM = A;
			}

			if (!CheckForOverstep(bodyM, bodyS, out float ratio))
				return CrossoverType.None;

			if (staticA)
				crossB.motionRatio = ratio;
			else
				crossA.motionRatio = ratio;
			return CrossoverType.Overstep;
		}

		public void CrossoverPhase(bool doPrep = true)
		{
			var crossovers = new Dictionary<BData, CrossoverCache>();
			var secondPass = new Dictionary<BData, HashSet<BData>>();
			// Crossovers and oversteps need to be checked to see if
			// there aren't any with a smaller ratio

			void AddCrossover(BData body, CrossoverCache cross)
			{
				if (!crossovers.ContainsKey(body))
				{
					crossovers.Add(body, cross);
					if (secondPass.ContainsKey(body))
						secondPass.Remove(body);
					return;
				}

				var current = crossovers[body];
				if (cross.motionRatio >= current.motionRatio)
					return;

				crossovers[body] = cross;
				if (crossovers[current.other].other != body)
					return;

				crossovers.Remove(current.other);
				if (secondPass.ContainsKey(current.other))
					secondPass[current.other].Add(body);
				else
				{
					var l = new HashSet<BData>();
					l.Add(body);
					secondPass.Add(current.other, l);
				}
			}

			bool TryAddBetweenAndEnvelop(CrossoverType type, (BData, BData) key, EnvelopFlag flag, BetweenCache betweenCache)
			{
				if (type == CrossoverType.Envelop)
				{
					_cache.envelops.Add(key, flag);
					return true;
				}

				if (type == CrossoverType.Between)
				{
					var previous = _cache.prevBetweens.ContainsKey(key);
					if (previous)
					{
						var cache = _cache.prevBetweens[key];
						cache.activeOnPreviousFrame = true;
						_cache.betweens.Add(key, cache);
					}
					else
						_cache.betweens.Add(key, betweenCache);
					return true;
				}

				return false;
			}

			bool firstRun = true;
			for (var i = 0; i < _bodies.Count - 1; i++)
			{
				BData bodyI = _bodies[i];
				if (doPrep && firstRun)
					PrepareBody(_bodies[i]);

				for (var j = i + 1; j < _bodies.Count; j++)
				{
					BData bodyJ = _bodies[j];
					if (doPrep && firstRun)
						PrepareBody(_bodies[j]);
					if (bodyI.body.HasInfiniteMass && bodyJ.body.HasInfiniteMass)
						continue;

					var key = (bodyI, bodyJ);
					if (bodyI.body.ghosting || bodyJ.body.ghosting)
					{
						if (!_cache.GhostContacts.ContainsKey(key))
							_cache.GhostContacts.Add(key, 0);
						continue;
					}

					var type = CrossoverPass(bodyI, bodyJ, out var crossI, out var crossJ, out var envelopFlag, out var betweenCache);

					if (bodyI.body.PreemptiveDive == bodyJ.body || bodyJ.body.PreemptiveDive == bodyI.body)
					{
						if (!_cache.GhostContacts.ContainsKey(key))
							_cache.GhostContacts.Add(key, 0);
					}
					if (type == CrossoverType.None)
					{
						_cache.regular.Add(key);
						continue;
					}


					if (_cache.GhostContacts.ContainsKey(key)) continue;
					if (TryAddBetweenAndEnvelop(type, key, envelopFlag, betweenCache)) continue;

					AddCrossover(bodyI, crossI);
					AddCrossover(bodyJ, crossJ);
				}
				firstRun = false;
			}

			foreach (var (body, toSkip) in secondPass)
			{
				var crosses = new List<(BData, CrossoverCache me, CrossoverCache other)>();
				foreach (var other in _cache.bodies)
				{
					if (toSkip.Contains(other) || body == other) continue;
					if (_cache.envelops.ContainsKey((body, other)) || _cache.envelops.ContainsKey((other, body))
						|| _cache.betweens.ContainsKey((body, other)) || _cache.betweens.ContainsKey((other, body)))
						continue;

					var type = CrossoverPass(body, other, out var crossMe, out var crossOther, out var envelopFlag, out var betweenCache);

					// If we detect a crossover or overstep this time just take the smallest ratio'
					if (type == CrossoverType.None)
						continue;

					if (crosses.Count == 0)
					{
						crosses.Add((other, crossMe, crossOther));
						continue;
					}

					for (var i = 0; i < crosses.Count; i++)
					{
						if (crossMe.motionRatio < crosses[i].me.motionRatio)
						{
							crosses.Insert(i, (other, crossMe, crossOther));
							break;
						}
					}
				}

				// These should be order in ascending order by crossMe.motionRatio
				foreach (var (other, crossMe, crossOther) in crosses)
				{
					if (crossovers.ContainsKey(other))
					{
						var currOther = crossovers[other];
						if (crossOther.motionRatio < currOther.motionRatio) // Only replace the current one if it is less
							continue;

						crossovers.Remove(currOther.other);
						crossovers.Remove(other);
					}

					if (crossovers.ContainsKey(body)) crossovers[body] = crossMe;
					else crossovers.Add(body, crossMe);
					crossovers.Add(other, crossOther);
					break;
				}
			}

			// Resolve the crossovers and oversteps
			foreach (var (body, crossCache) in crossovers)
			{
				if (float.IsInfinity(crossCache.motionRatio))
				{
					Debug.LogError($"Cross Cache for {body.body.name} yielded infinity motion Ratio");
					continue;
				}

				body.motion *= crossCache.motionRatio;
				body.RecalculateEndPositions();

				if (!_cache.regular.Contains((crossCache.other, body)))
					_cache.regular.Add((body, crossCache.other));
			}
		}

		#endregion

		#region Contact

		public static (bool, bool) ContactAdjustmentCalculation(Point point, BData A, BData B, float massWeighting)
		{
			// The MORE massive you are, the LESS you move
			// The MORE rigid you are,   the MORE you move
			// Unless scaled the this will move both bodies so they are touching each other
			// Needs to be scaled by a value (something to do with velocity?)
			float totalRigidity = A.body.rigidity + B.body.rigidity;

			var dotA = -Vector2.Dot(point.plane.normal, A.body.velocity);
			var dotB = -Vector2.Dot(-point.plane.normal, B.body.velocity);
			var approachFactor = dotA + dotB;
			approachFactor = Abs(approachFactor);

			var magAdjustA = Min(point.factor, Abs(dotA) * Time.deltaTime);
			var magAdjustB = Min(point.factor, Abs(dotB) * Time.deltaTime);

			bool ResolveClipping(BData me, BData other, Plane plane, ref float adjustment)
			{
				if (me.body.rigidity > approachFactor) return false;

				var startPos = (plane.pos1 + plane.pos2) / 2 + plane.normal;
				var vec = GetHalfVectorThroughBody(startPos, -plane.normal, other);
				var ratio = me.body.rigidity / totalRigidity;
				var stepFactor = ratio * adjustment;
				stepFactor = Min(stepFactor, vec.magnitude);
				adjustment = Max(0, adjustment - stepFactor);
				//adjustment = Clamp(dotA * Time.deltaTime - stepFactor, 0, adjustment);
				return true;
			}

			var clipA = ResolveClipping(A, B, point.plane, ref magAdjustA);
			var clipB = ResolveClipping(B, A, point.plane.Inverted(), ref magAdjustB);

			// ShockAbsorption
			A.shockAbsorptionVector += point.plane.normal;
			B.shockAbsorptionVector -= point.plane.normal;
			if (clipA)
			{
				A.shockAbsorptionMagnitude += B.body.ShockAbsorption;
			}
			else
			{
				A.shockAbsorptionMagnitude += Abs(dotA) * 2;
			}
			if (clipB)
			{
				B.shockAbsorptionMagnitude += A.body.ShockAbsorption;
			}
			else
			{
				B.shockAbsorptionMagnitude += Abs(dotB) * 2;
			}

			if (!(clipA || clipB))
			{
				magAdjustA = magAdjustB = point.factor / 2;
				if (A.body.HasInfiniteMass)
				{
					magAdjustA = 0;
					clipB = true;
				}
				else if (B.body.HasInfiniteMass)
				{
					magAdjustB = 0;
					clipA = true;
				}
			}
			else if (!clipA)
			{
				magAdjustA = 0;
			}
			else if (!clipB)
			{
				magAdjustB = 0;
			}
			else
			{
				var r = approachFactor * Time.deltaTime / 2;
				magAdjustA = Max(0, magAdjustA - r);
				magAdjustB = Max(0, magAdjustB - r);
			}

			// This shouldn't override but combine
			void Adjust(BData data, Vector2 normal, Vector2 perp, float adjustmemt)
			{
				data.adjustmentMagnitude = 1;

				var nrm = Vector2.Dot(normal, data.adjustmentVector);
				var prp = Vector2.Dot(perp, data.adjustmentVector);

				nrm += adjustmemt;
				data.adjustmentVector = nrm * normal + prp * perp;
			}

			var perp = point.plane.normal.GetNormal();
			Adjust(A, point.plane.normal, perp, magAdjustA);
			Adjust(B, -point.plane.normal, perp, magAdjustB);

			//A.adjustmentVector += point.plane.normal;
			//A.adjustmentMagnitude = magAdjustA;
			//B.adjustmentVector -= point.plane.normal;
			//B.adjustmentMagnitude = magAdjustB;
			return (clipA, clipB);
		}

		// Overstep and Crossover
		public static bool ContactPassRegular(BData A, BData B, float massWeighting, bool ghost, out (bool, bool) clipping)
		{
			void AggregateResults(int index, Point aggregate, Point point, Body me, Body other)
			{
				if (!point.active) return;

				if (point.factor < aggregate.factor)
				{
					aggregate.factor = point.factor;
					aggregate.plane = point.plane;
				}

				if (ghost)
					return;

				me.contacts[index].AddBody(other, point.plane.normal);
				me.contacts[index].normal += point.plane.normal;
				if (other.deformAsOne)
				{
					var next = (point.iOther + 1) % Body.kVerts;
					other.contacts[point.iOther].normal -= point.plane.normal;
					other.contacts[point.iOther].AddBody(me, -point.plane.normal);
					other.contacts[next].normal -= point.plane.normal;
					other.contacts[next].AddBody(me, -point.plane.normal);
				}
			}

			// If we find no collision we should remove this pair from consideration
			var pointsA = new[] { new Point(), new Point(), new Point(), new Point() };
			var pointsB = new[] { new Point(), new Point(), new Point(), new Point() };
			FindFlexOverlaps(A, B, pointsA, pointsB);

			// Aggregate results
			// Check if there are any overlaps
			// Update the body of its collisions
			var anyOverlaps = false;
			Point aggA = new Point(), aggB = new Point();
			for (int i = 0; i < Body.kVerts; i++)
			{
				anyOverlaps |= pointsA[i].active || pointsB[i].active;
				AggregateResults(i, aggA, pointsA[i], A.body, B.body);
				AggregateResults(i, aggB, pointsB[i], B.body, A.body);
			}

			clipping = (false, false);
			if (!anyOverlaps)
				return false;
			if (ghost)
				return true; // Exit early to not touch the adjustment stuff

			bool t = float.IsFinite(aggB.factor);
			if (float.IsInfinity(aggA.factor) || (t && aggB.factor > aggA.factor))
			{
				aggA.factor = aggB.factor;
				aggA.plane = aggB.plane.Inverted();
			}

			clipping = ContactAdjustmentCalculation(aggA, A, B, massWeighting);
			return true;
		}

		public static void ContactPassBetween(BData A, BData B, float massWeighting)
		{
			void GetCrossFactor(BData body, BData other, Point point)
			{
				var planes = GetOpposingEdges(other, body.motion);
				var points = GetForwardPoints(body);
				var sqrFactor = float.MinValue;
				point.factor = 0;

				foreach (var plane in planes)
					foreach (var pos in points)
					{
						if (!FindCrossoverPoint(pos, body.motion, plane.pos1, plane.pos2 - plane.pos1, out var overlapPoint))
							continue;

						var f = (overlapPoint - pos).sqrMagnitude;
						if (f <= sqrFactor) continue;

						sqrFactor = f;
						point.factor = Sqrt(f);
						point.plane = plane;
					}
			}

			var staticA = IsStatic(A.body);
			var staticB = IsStatic(B.body);
			Point pointA, pointB;

			// If both are static push away from the closest edge
			// If one or more isn't push back along their motions
			if (staticA && staticB)
			{
				FindClosestEdgeToMidpoint(A, B, out pointA, out pointB);

				if (pointB.distanceToEdge < pointA.distanceToEdge)
				{
					pointA.distanceToEdge = pointB.distanceToEdge;
					pointA.plane = pointB.plane.Inverted();
					//pointA.awayNormal = -pointB.awayNormal;
				}

				pointA.factor = pointA.distanceToEdge;
				ContactAdjustmentCalculation(pointA, A, B, massWeighting);
				return;
			}

			pointA = new Point();
			pointB = new Point();
			pointA.factor = 0;
			pointB.factor = 0;
			if (!staticA) GetCrossFactor(A, B, pointA);
			if (!staticB) GetCrossFactor(B, A, pointB);

			if (pointB.factor < pointA.factor)
			{
				pointA.factor = pointB.factor;
				pointA.plane = pointB.plane.Inverted();
				//pointA.awayNormal = -pointB.awayNormal;
			}

			ContactAdjustmentCalculation(pointA, A, B, massWeighting);
		}

		public static void ContactPassEnvelop(BData A, BData B, EnvelopFlag flag)
		{
			FindClosestEdgeToMidpoint(A, B, out var pointA, out var pointB);

			var rigidityTotal = A.body.Rigidity + B.body.Rigidity;
			var escapeFactor = rigidityTotal / 50f;
			float ratRigidA, ratMassA;
			if (A.body.HasInfiniteMass)
			{
				ratMassA = 0;
				ratRigidA = 0;
			}
			else if (B.body.HasInfiniteMass)
			{
				ratMassA = 1;
				ratRigidA = 1;
			}
			else
			{
				float massTotal = A.body.mass + B.body.mass;
				ratRigidA = A.body.Rigidity / rigidityTotal;
				ratMassA = (float)B.body.mass / massTotal;
			}

			var onA = flag == EnvelopFlag.A;
			var onB = flag == EnvelopFlag.B;
			if (flag == EnvelopFlag.Both) onA = onB = true;

			void Resolve(BData data, Point point, float ratRigid, float ratMass)
			{
				if (data.body.DiveIntention)
				{
					var target = ClosestPointOnLine(data.body.Position, point.plane.pos1, point.plane.pos2);
					var direction = (target - data.body.Position).normalized;
					var accel = 4f;
					if (data.body.divingTime > 45)
					{
						float t = data.body.divingTime - 45;
						accel += t * t;
						accel = Mathf.Min(accel, 30f);
					}
					Debug.Log(data.body);
					return;
				}

				var adjustment = data.adjustmentVector * data.adjustmentMagnitude;
				adjustment += point.plane.normal * ((ratRigid + ratMass) / 2.0f);
				data.adjustmentVector = adjustment.normalized;
				data.adjustmentMagnitude = adjustment.magnitude;
			}

			if (onA) Resolve(A, pointA, ratRigidA, ratMassA);
			if (onB) Resolve(B, pointB, 1.0f - ratRigidA, 1.0f - ratMassA);

			//if (onA)
			//{
			//	if (A.body.Diving)
   //             {

				
			//	var adjustment = A.adjustmentVector * A.adjustmentMagnitude;
			//	adjustment += pointA.plane.normal * ((ratRigidA + ratMassA) / 2.0f);
			//	A.adjustmentVector = adjustment.normalized;
			//	A.adjustmentMagnitude = adjustment.magnitude;
			//}
			//if (onB)
			//{
			//	var adjustment = B.adjustmentVector * B.adjustmentMagnitude;
			//	adjustment += pointB.plane.normal * (1.0f -(ratRigidA + ratMassA) / 2.0f);
			//	B.adjustmentMagnitude = adjustment.magnitude;
			//	B.adjustmentVector = adjustment.normalized;
			//}
		}

		public static void ContactPassGhosts(BData A, BData B, int time)
		{
			// Check that a large proportion of one body is in another
			// if so proceed with the Dive Resolution

			// Construct the intersection between the two bodies
			var a_enveloped = new[] { true, true, true, true };
			var b_enveloped = new[] { true, true, true, true };
			var interceptPoints = new List<Vector2>();

			for (var iEdge = 0; iEdge < Body.kVerts; iEdge++)
			{
				var j = (iEdge + 1) % Body.kVerts;
				var a_start = A.EndBox[iEdge];
				var a_line = A.EndBox[j] - a_start;

				var a_plane = CalcPlaneData(A.EndBox[iEdge], A.EndBox[j]);
				var b_plane = CalcPlaneData(B.EndBox[iEdge], B.EndBox[j]);

				for (var iPoint = 0; iPoint < Body.kVerts; iPoint++)
				{
					j = (iPoint + 1) % Body.kVerts;
					var b_start = B.EndBox[iPoint];
					var b_line = B.EndBox[j] - b_start;

					a_enveloped[iPoint] &= IsBehindPlane(b_plane, A.EndBox[iPoint]);
					b_enveloped[iPoint] &= IsBehindPlane(a_plane, B.EndBox[iPoint]);

					if (FindCrossoverPoint(a_start, a_line, b_start, b_line, out var overlapPoint))
					{
						interceptPoints.Add(overlapPoint);
					}
				}
			}

			for (var i = 0; i < Body.kVerts; i++)
			{
				if (a_enveloped[i])
				{
					interceptPoints.Add(A.EndBox[i]);
				}
				if (b_enveloped[i])
				{
					interceptPoints.Add(B.EndBox[i]);
				}
			}

			float TriangleArea(Vector2 a, Vector2 b, Vector2 c)
			{
				var perp = DistanceToLine(a, b, c);
				return 0.5f * Vector2.Distance(b, c) * perp;
			}

			float SquareArea(Vector2 a, Vector2 b, Vector2 c, Vector2 d)
			{
				return TriangleArea(a, b, c) + TriangleArea(a, c, d);
			}

			float intersection = 0;
			if (interceptPoints.Count == 3)
			{
				intersection = TriangleArea(interceptPoints[0], interceptPoints[1], interceptPoints[2]);
			}
			else if (interceptPoints.Count == 4)
			{
				intersection = SquareArea(interceptPoints[0], interceptPoints[1], interceptPoints[2], interceptPoints[3]);
			}
			else if (interceptPoints.Count >= 5)
			{
				var mid = new Vector2();
				foreach (var point in interceptPoints)
					mid += point;

				mid /= interceptPoints.Count;

				for (var i = 0; i < interceptPoints.Count - 1; i++)
					intersection += TriangleArea(mid, interceptPoints[i], interceptPoints[i + 1]);
			}

			if (Approximately(intersection, 0))
				return;

			float a_area = SquareArea(A.EndBox[0], A.EndBox[1], A.EndBox[2], A.EndBox[3]);
			float b_area = SquareArea(B.EndBox[0], B.EndBox[1], B.EndBox[2], B.EndBox[3]);
			a_area = intersection / a_area;
			b_area = intersection / b_area;

			var dist = Abs(a_area - b_area);
			float ratA = 0, ratB = 0;
			Vector2 a_dir, b_dir;

			if (dist < 0.2f) // Both are influenced
			{
				ratA = a_area / (a_area + b_area);
				ratB = 1.0f - ratA;
				FindClosestEdgeToMidpoint(A, B, out var pointA, out var pointB);

				a_dir = pointA.plane.normal;
				b_dir = pointB.plane.normal;
			}
			else if (a_area > b_area)
			{
				ratA = a_area / (a_area + b_area);
				FindClosestEdgeToMidpoint(A, B, out var point);
				a_dir = point.plane.normal;
				b_dir = Vector2.zero;
			}
			else
			{
				ratB = b_area / (a_area + b_area);
				FindClosestEdgeToMidpoint(B, A, out var point);
				a_dir = Vector2.zero;
				b_dir = point.plane.normal;
			}


			float factor = (float)(time * time) / 50.0f;
			A.body.environmentAcceleration += a_dir * ratA * factor;
			B.body.environmentAcceleration += b_dir * ratB * factor;
		}

		public void ContactPhase()
		{
			foreach (var (A, B) in _cache.regular)
			{
				var ghosting = _cache.GhostContacts.ContainsKey((A, B));
				var success = ContactPassRegular(A, B, massWeighting, ghosting, out var clipping);

				if (success)
				{
					if (ghosting)
						if (!_cache.GhostContacts.ContainsKey((A, B)))
							_cache.GhostContacts.Add((A, B), 0);
					else
						_cache.regularContacts.Add((A, B), clipping);
				}
				else if (ghosting)
				{
					if (_cache.GhostContacts.ContainsKey((A, B)))
						_cache.GhostContacts.Remove((A, B));
				}
			}

			foreach (var ((A, B), cache) in _cache.betweens)
			{
				if (_cache.GhostContacts.ContainsKey((A, B))) continue;
				ContactPassBetween(A, B, massWeighting);
			}

			foreach (var ((A, B), flag) in _cache.envelops)
			{
				if (_cache.GhostContacts.ContainsKey((A, B))) continue;
				ContactPassEnvelop(A, B, flag);
			}

			var keys = new List<(BData, BData)>(_cache.GhostContacts.Keys);
			foreach (var key in keys)
			{
				_cache.GhostContacts[key]++;
				ContactPassGhosts(key.Item1, key.Item2, _cache.GhostContacts[key]);
			}

			foreach (var body in _cache.bodies)
			{
				var bef = body.motion.x;
				body.motion += body.adjustmentVector * body.adjustmentMagnitude;
				body.RecalculateEndPositions();
			}
		}

		#endregion

		#region Flex
		public class FlexAdjustment
		{
			public int iVert;
			public int iOther;
			public Vector2 direction;
			public float factor = float.MaxValue;
			public bool closerToCurrent;
			public bool isA;
		}

		public static float FlexRatioForA(Body A, Body B)
		{
			if (A.IsInFlexible != B.IsInFlexible)
				return A.IsInFlexible ? 0 : 1;

			var rigidityTotal = A.Rigidity + B.Rigidity;
			return A.Rigidity / rigidityTotal;
		}

		public static void FlexPassRegular(BData A, BData B, (bool A, bool B) clipping)
		{
			void Resolve(BData self, int index, in Point point, float ratio)
			{
				//self.FlexBox[index] += point.plane.normal * (point.factor * ratio);
				self.ApplyFlexAdjustment(index, point.plane.normal * (point.factor * ratio));
			}

			var pointsA = new[] { new Point(), new Point(), new Point(), new Point() };
			var pointsB = new[] { new Point(), new Point(), new Point(), new Point() };
			FindClosestFlexEdge(A, B, pointsA, pointsB);

			//float ratA;
			//if (A.body.IsInFlexible == B.body.IsInFlexible)
			//{
			//	var rigidityTotal = A.body.Rigidity + B.body.Rigidity;
			//	ratA = A.body.Rigidity / rigidityTotal;
			//}
			//else ratA = A.body.IsInFlexible ? 0 : 1;
			float ratA;
			if (clipping.A && clipping.B)
				ratA = FlexRatioForA(A.body, B.body);
			else if (clipping.A)
				ratA = 1.0f;
			else if (clipping.B)
				ratA = 0;
			else
			{
				//ratA = 0.5f;
				//Debug.Log($"{A} {B} Both clippings are false");
				return;
			}
			var ratB = 1.0f - ratA;
			for (var i = 0; i < Body.kVerts; i++)
			{
				if (clipping.A && pointsA[i].active) Resolve(A, i, in pointsA[i], ratA);
				if (clipping.B && pointsB[i].active) Resolve(B, i, in pointsB[i], ratB);
			}
		}

		public static void FlexPassBetweens(BData A, BData B, BetweenCache cache)
		{
			bool EmplaceAdjustment(FlexAdjustment[] adjustments, float factor, int index, int iOther, Plane plane, bool closerToCurrent, bool isA)
			{
				for (var i = 0; i < 2; i++)
				{
					if (factor > adjustments[i].factor) continue;

					adjustments[i].iVert = index;
					adjustments[i].iOther = iOther;
					adjustments[i].direction = plane.normal;
					adjustments[i].closerToCurrent = closerToCurrent;
					adjustments[i].isA = isA;
					adjustments[i].factor = factor;
					return true;
				}
				return false;
			}

			bool StaticFlexAdjustment(BData me, int index, FlexAdjustment[] adjustments, bool isA, in Point point)
			{
				var pointPos = me.EndBox[index];
				var factor = GetOverlapFactor(point.plane, pointPos);
				if (factor > 0.001f) return false;
				factor = -factor;

				var dist1 = Vector2.Distance(point.plane.pos1, pointPos);
				var dist2 = Vector2.Distance(point.plane.pos2, pointPos);
				var closerToCurr = dist1 / (dist1 + dist2) <= 0.5f;

				return EmplaceAdjustment(adjustments, factor, index, point.iOther, point.plane, closerToCurr, isA);
			}

			int Work(BData me, BData other, FlexAdjustment[] adjustments, bool isA)
			{
				var isStatic = IsStatic(me.body);
				int iPlane = 0;

				if (isStatic)
				{
					FindClosestEdgeToMidpoint(me, other, out var point);

					for (var i = 0; i < Body.kVerts; i++)
						if (StaticFlexAdjustment(me, i, adjustments, isA, in point))
							iPlane = i;
				}
				else
				{
					var safetyVec = me.body.FullBox[1].magnitude * 2f * me.motion.normalized;
					var planes = GetOpposingEdgesWithIndexes(other, safetyVec);
					var points = GetForwardPoints(me);
					var motion = safetyVec + me.motion;
					var planeData = new Dictionary<(Plane, int), int>();
					foreach (var point in points)
					{
						var crossoverPos = point - safetyVec;
						foreach (var p in planes)
						{
							if (!FindCrossoverPoint(crossoverPos, motion, p.Item1.pos1, p.Item1.pos2 - p.Item1.pos1, out var x))
								continue;

							if (planeData.ContainsKey(p))
								planeData[p] = planeData[p] + 1;
							else
								planeData.Add(p, 1);
						}
					}

					if (planeData.Count == 0) return iPlane;

					var pData = planeData.ToList();
					pData.Sort((KeyValuePair<(Plane, int), int> x, KeyValuePair<(Plane, int), int> y) => x.Value - y.Value);
					var (plane, index) = pData.First().Key;
					var diff = (plane.pos2 - plane.pos1) * 4f;
					var pPos1 = plane.pos1 - diff;
					var pPos2 = plane.pos2 + diff;
					var pMotion = pPos2 - pPos1; ;

					for (var i = 0; i < Body.kVerts; i++)
					{
						var pointPos = me.BeginBox[i];
						var crossoverPos = pointPos - safetyVec;

						if (!FindCrossoverPoint(crossoverPos, motion, pPos1, pMotion, out var overlapPoint))
							continue;

						var factor = -GetOverlapFactor(plane, pointPos + me.motion);
						var dist1 = Vector2.Distance(plane.pos1, overlapPoint);
						var dist2 = Vector2.Distance(plane.pos2, overlapPoint);
						var closerToCurrent = dist1 / (dist1 + dist2) <= 0.5f;
						if (EmplaceAdjustment(adjustments, factor, i, index, plane, closerToCurrent, isA))
							iPlane = i;
					}
				}

				return iPlane;
			}

			var smallests = new[] { new FlexAdjustment(), new FlexAdjustment() };
			if (!cache.activeOnPreviousFrame)
			{
				void AAdAdjustment(Dictionary<int, FlexAdjustment> adjustments, Vector2 pos, Vector2 point, int iVert, int iEdge, Plane plane, bool isA)
				{
					var dot = Vector2.Dot(point - pos, plane.normal);
					if (dot > 0.1f)
						return;

					var factor = GetOverlapFactor(plane, point);
					if (factor >= 0) return;

					factor = -factor;
					var dist1 = Vector2.Distance(plane.pos1, point);
					var dist2 = Vector2.Distance(plane.pos2, point);
					var adjustment = new FlexAdjustment
					{
						iVert = iVert,
						iOther = iEdge,
						direction = plane.normal,
						factor = factor,
						closerToCurrent = dist1 >= dist2,
						isA = isA
					};
					if (adjustments.ContainsKey(iVert))
					{
						if (adjustments[iVert].factor > factor)
							adjustments[iVert] = adjustment;
					}
					else
					{
						adjustments.Add(iVert, adjustment);
					}
				}

				var adjustmentsA = new Dictionary<int, FlexAdjustment>();
				var adjustmentsB = new Dictionary<int, FlexAdjustment>();

				for (int iEdge = 0, jEdge = 1; iEdge < Body.kVerts; iEdge++, jEdge = (++jEdge) % Body.kVerts)
				{
					var planeA = CalcPlaneData(A.EndBox[iEdge], A.EndBox[jEdge]);
					var planeB = CalcPlaneData(B.EndBox[iEdge], B.EndBox[jEdge]);

					for (var iVert = 0; iVert < Body.kVerts; iVert++)
					{
						if (cache.indexesA.Contains(iVert))
							AAdAdjustment(adjustmentsA, A.endPosition, A.EndBox[iVert], iVert, iEdge, planeB, true);
						if (cache.indexesB.Contains(iVert))
							AAdAdjustment(adjustmentsB, A.endPosition, B.EndBox[iVert], iVert, iEdge, planeA, false);
					}
				}

				cache.iAPlane = 80;
				cache.iBPlane = 80;
				var smallestA = float.MaxValue;
				var smallestB = float.MaxValue;

				foreach (var (i, adjustment) in adjustmentsA)
				{
					if (adjustment.factor < smallestA)
					{
						smallestA = adjustment.factor;
						cache.iAPlane = i;
					}
					if (adjustment.factor < smallests[0].factor)
						smallests[0] = adjustment;
					else if (adjustment.factor < smallests[1].factor)
						smallests[1] = adjustment;
				}

				foreach (var (i, adjustment) in adjustmentsB)
				{
					if (adjustment.factor < smallestB)
					{
						smallestB = adjustment.factor;
						cache.iBPlane = i;
					}
					adjustment.direction = -adjustment.direction;
					if (adjustment.factor < smallests[0].factor)
						smallests[0] = adjustment;
					else if (adjustment.factor < smallests[1].factor)
						smallests[1] = adjustment;
				}

				//            cache.iAPlane = Work(A, B, adjustmentsA, true);
				//cache.iBPlane = Work(B, A, adjustmentsB, false);

				//void FindSmallest(FlexAdjustment[] adjustments)
				//for (var i = 0; i < 2; i++)
				//{
				//	if (adjustmentsA[i].factor < smallests[0].factor)
				//		smallests[0] = adjustmentsA[i];
				//	else if (adjustmentsA[i].factor < smallests[1].factor)
				//		smallests[1] = adjustmentsA[i];

				//	if (adjustmentsB[i].factor < smallests[0].factor)
				//	{
				//		smallests[0] = adjustmentsB[i];
				//		smallests[0].direction = -smallests[0].direction;
				//	}
				//	else if (adjustmentsB[i].factor < smallests[1].factor)
				//	{
				//		smallests[1] = adjustmentsB[i];
				//		smallests[1].direction = -smallests[1].direction;
				//	}
				//}
			}
			else
			{
				Plane planeA, planeB;
				var useA = cache.iAPlane < Body.kVerts;
				var useB = cache.iBPlane < Body.kVerts;
				if (useA) planeA = CalcPlaneData(A.EndBox[cache.iAPlane], A.EndBox[(cache.iAPlane + 1) % Body.kVerts]);
				else planeA = new Plane();
				if (useB) planeB = CalcPlaneData(B.EndBox[cache.iBPlane], B.EndBox[(cache.iBPlane + 1) % Body.kVerts]);
				else planeB = new Plane();

				void Calc(BData body, bool isA, int index, Plane plane, int iOther)
				{
					//var point = pos + body.FlexBox[index];
					var point = body.EndBox[index];

					var factor = -GetOverlapFactor(plane, point);
					if (factor < 0) return;
					var dist1 = Vector2.Distance(plane.pos1, point);
					var dist2 = Vector2.Distance(plane.pos2, point);
					var closerToCurrent = dist1 / (dist1 + dist2) <= 0.5f;
					EmplaceAdjustment(smallests, factor, index, iOther, isA ? plane : plane.Inverted(), closerToCurrent, isA);
				}

				for (var i = 0; i < Body.kVerts; i++)
				{
					if (useB)
						Calc(A, true, i, planeB, cache.iBPlane);
					if (useA)
						Calc(B, false, i, planeA, cache.iAPlane);
				}
			}

			//float ratA;
			//if (A.body.IsInFlexible == B.body.IsInFlexible)
			//{
			//	var rigidityTotal = A.body.Rigidity + B.body.Rigidity;
			//	ratA = A.body.Rigidity / rigidityTotal;
			//}
			//else ratA = A.body.IsInFlexible ? 0 : 1;
			var ratA = FlexRatioForA(A.body, B.body);

			// At this stage we know what points need to be adjusted, what edge they need
			// to respect and the ratio of the movement
			// Go again and find the factor to be used
			//ratA = 1;

			foreach (var adj in smallests)
			{
				BData me, other;
				Vector2 dir;
				float ratio;
				if (adj.isA)
				{
					me = A;
					other = B;
					dir = adj.direction;
					ratio = ratA;
				}
				else
				{
					me = B;
					other = A;
					dir = -adj.direction;
					ratio = 1.0f - ratA;
				}

				var factor = adj.factor * ratio;
				var otherFactor = adj.factor * (1 - ratio);

				me.body.contacts[adj.iVert].AddBody(other.body, dir);
				me.body.contacts[adj.iVert].normal += dir;

				if (other.body.deformAsOne)
				{
					var next = (adj.iOther + 1) % Body.kVerts;

					me.ApplyFlexAdjustment(adj.iVert, dir * factor);
					other.ApplyFlexAdjustment(adj.iOther, -dir * otherFactor);
					other.ApplyFlexAdjustment(next, -dir * otherFactor);
					other.body.contacts[adj.iOther].AddBody(me.body, -dir);
					other.body.contacts[adj.iOther].normal -= dir;
					other.body.contacts[next].AddBody(me.body, -dir);
					other.body.contacts[next].normal -= dir;
				}
				else
				{
					var iOther = adj.closerToCurrent ? adj.iOther : (adj.iOther + 1) % Body.kVerts;
					me.ApplyFlexAdjustment(adj.iVert, dir * factor);
					other.ApplyFlexAdjustment(iOther, -dir * otherFactor);
					other.body.contacts[iOther].AddBody(me.body, -dir);
					other.body.contacts[iOther].normal -= dir;
				}
			}
		}

		void FlexResolve(BData body)
		{
			var reverseMat = new Matrix2x2(-body.body.Rotation);
			for (var i = 0; i < Body.kVerts; i++)
			{
				body.body.FlexBox[i] += reverseMat * body.flexAdjustments[i];
			}
			body.RecalculateEndPositions();
		}

		public void FlexPhase(bool resolve = false)
		{
			foreach (var ((A, B), clipping) in _cache.regularContacts)
			{
				if (_cache.GhostContacts.ContainsKey((A, B))) continue;
				FlexPassRegular(A, B, clipping);
			}

			foreach (var ((A, B), cache) in _cache.betweens)
			{
				if (_cache.GhostContacts.ContainsKey((A, B))) continue;
				FlexPassBetweens(A, B, cache);
			}

			if (!resolve) return;

			// Only here for debugging
			foreach (var body in _cache.bodies)
				FlexResolve(body);
		}

		#endregion

		#region Resolution

		public void ResolutionPass(BData data)
		{
			var body = data.body;
			data.prevGhosting = body.ghosting;

			// Shock absorption occurs by the combination of the compression the point and the shock absorption
			// Shock absorption reduces the velocity to 0
			// Collision reflection occurs when a flex point cannot continue to compress in the
			// direction it wants to
			// The velocity is reduced a bit and then reflected along the collision plane
			(float, Vector2) CalcRigidAdjustment(int curr, int prev, int next)
			{
				var planeP = CalcPlaneData(body.RigidBox[prev], body.RigidBox[curr]);
				var planeN = CalcPlaneData(body.RigidBox[curr], body.RigidBox[next]);

				var factorP = GetOverlapFactor(planeP, body.FlexBox[curr]);
				var factorN = GetOverlapFactor(planeN, body.FlexBox[curr]);

				// Both the actors should be negative, so this means we get the largest motion
				//return factorN < factorP ? (factorN, planeN.normal) : (factorP, planeP.normal);
				if (factorN > 0)
				{
					factorN = 0;
					planeN.normal = Vector2.zero;
				}
				if (factorP > 0)
				{
					factorP = 0;
					planeP.normal = Vector2.zero;
				}
				return (factorN + factorP, (planeP.normal + planeN.normal).normalized);
			}

			var nrmAccel = body.acceleration.normalized;

			var adjustmentMagnitude = 0f;
			var adjustmentVector = Vector2.zero;
			var friction = Vector2.zero;
			var frictionTargetVelocity = Vector2.zero;
			int frictionBodies = 0;
			var flexion = Vector2.zero;

			for (
				int curr = 0, prev = Body.kVerts - 1, next = 1; curr < Body.kVerts;
				curr++, prev = ++prev % Body.kVerts, next = ++next % Body.kVerts)
			{
				if (!body.contacts[curr].IsActive)
					continue;

				body.inContact = true;

				// Spring Back Calcs
				// We do a check for each flex point that goes into its inner bounds
				// of the rigid box
				// If any do, we move the rigid box to resolve the overlap (adding it to
				// the motion)
				// Then recalculate our velocity using the motion
				{
					// We could use Body.flexion, but in the future the distance between the rigid box and the
					// full box will not be uniform
					var divisor = body.IsInFlexible ? 1.0f : (body.RigidBox[curr] - body.FullBox[curr]).magnitude;
					var flexDisplacement = body.FlexBox[curr] - body.FullBox[curr];
					var compression = flexDisplacement / divisor;
					var perpToCompression = compression.GetNormal().normalized.Abs();

					var frict = Vector2.zero;

					foreach (var (other, contactNormal) in body.contacts[curr].bodies)
					{
						// If we're travelling away from the body, don't apply friction
						if (Vector2.Dot(nrmAccel, contactNormal) > 0.1f)
							continue;

						var nrm = contactNormal.GetNormal().Abs();
						frict += (perpToCompression + nrm).normalized * other.GroundFriction;
						var otherVel = _bodies.First(data => data.body == other).prevVelocity;
						var target = Vector2.Dot(perpToCompression, otherVel) * perpToCompression;
						frictionTargetVelocity += target;

						// Friction calculations for when calcing ground friction and velocity at a point is possible
						/* 
						 * frict += perpToCompression * other.GroundFrictionAtPoint(contactPoint)
						 * 
						 * var otherVel = _bodies.First(data => data.body == other).prevVelocity + other.VelocityOnPoint(contactPoint)
						 * var target = Vector2.Dot(perpToCompression, otherVel) * perpToCompression;
						 * frictionTargetVelocity += target;
						 */
					}

					if (frict != Vector2.zero)
					{
						var nrm = body.contacts[curr].normal.GetNormal().Abs();
						frict += (perpToCompression + nrm).normalized * body.GroundFriction;
						frictionBodies += body.contacts[curr].bodies.Count;
						friction += frict;
						body.contacts[curr].friction = frict;
					}

					flexion += flexDisplacement;
				}

				var rToPrev = (body.RigidBox[prev] - body.RigidBox[curr]).normalized;
				var rToNext = (body.RigidBox[next] - body.RigidBox[curr]).normalized;

				var flexToRigid = (body.FlexBox[curr] - body.RigidBox[curr]).normalized;
				var dotPrev = Vector2.Dot(rToPrev, flexToRigid);
				var dotNext = Vector2.Dot(rToNext, flexToRigid);
				if (dotPrev > 0 || dotNext > 0)
				{
					// These are meant to be negated but we just multiply them together instead
					// In truth the magnitude calculation is unnecessary as the adjustment values
					// are only used to reflect velocity
					// UPDATE Don''t use the magnitude at all because that leads to inconsitent
					// results. If a two bodies were to approach a wall at the same speed but one
					// was slightly further than the other they would have different adjustment
					// magnitudes and therefore different reflections
					var (magnitude, vector) = CalcRigidAdjustment(curr, prev, next);
					adjustmentVector -= vector;
					adjustmentMagnitude = Min(adjustmentMagnitude, magnitude);
				}
			}

			if (frictionBodies == 0) frictionBodies = 1;
			friction *= Time.deltaTime / frictionBodies;
			frictionTargetVelocity /= frictionBodies;
			adjustmentVector.Normalize();

			body.acceleration += body.environmentAcceleration;

			if (_applyMotion)
			{
				if (body.inContact)
				{
					var shockAbsorption = data.shockAbsorptionVector * (data.shockAbsorptionMagnitude + body.ShockAbsorption);

					body.velocity.x = MoveTowards(body.velocity.x, frictionTargetVelocity.x, Abs(shockAbsorption.x));
					body.velocity.y = MoveTowards(body.velocity.y, frictionTargetVelocity.y, Abs(shockAbsorption.y));

					var dirFlexion = flexion.normalized;
					var vel = Vector2.Dot(dirFlexion, data.prevVelocity);
					if (vel > 0)
					{
						Debug.Log("flexion");
						data.motion += flexion * body.restorationFactor;
					}

					if (!Approximately(adjustmentMagnitude, 0))
					{
						var xPlane = adjustmentVector.GetNormal();
						var velFactorY = Vector2.Dot(adjustmentVector, body.velocity);
						var accelFactor = Vector2.Dot(adjustmentVector, body.acceleration);
						var velFactorX = Vector2.Dot(xPlane, body.velocity);
						if (velFactorY < 0)
						{
							velFactorY = -velFactorY;
							accelFactor = -accelFactor;
						}
						Debug.Log($"{adjustmentVector} {velFactorY} {accelFactor}");
						body.velocity = Max(velFactorY, accelFactor) * adjustmentVector + velFactorX * xPlane;
					}
				}

				body.transform.position += (Vector3)data.motion;
				body.velocity += body.acceleration * Time.deltaTime;
			}

			body.environmentAcceleration = gravity;
			body.acceleration = Vector2.zero;
			if (!body.isEnveloped) body.divingTime = -1;
			if (!_applyMotion) return;

			body.velocity.x = MoveTowards(body.velocity.x, frictionTargetVelocity.x, friction.x);
			body.velocity.y = MoveTowards(body.velocity.y, frictionTargetVelocity.y, friction.y);

			body.containingFluid = FindContainingFluid(body);
			if (body.containingFluid)
			{
				Vector2 fluidVelocityAtPoint = Vector2.zero; // TODO To be calculated

				var density = body.containingFluid.Data.density;
				var flowVelocity = (fluidVelocityAtPoint - body.velocity).Abs();
				var dragCoefficient = body.airFriction;
				var area = 1f;
				var force = 0.5f * density * area * dragCoefficient * Time.deltaTime * flowVelocity * flowVelocity;

				body.velocity.x = MoveTowards(body.velocity.x, fluidVelocityAtPoint.x, force.x);
				body.velocity.y = MoveTowards(body.velocity.y, fluidVelocityAtPoint.y, force.y);
			}
		}

		public void ResolutionPhase(bool resolve = true)
		{
			foreach (var body in _cache.bodies)
			{
				if (resolve)
					FlexResolve(body);
				for (var i = 0; i < Body.kVerts; i++)
				{
					body.body.contacts[i].normal.Normalize();
					body.body.contacts[i].phase.TickPhase(body.body.contacts[i].bodies.Count != 0);
				}

				ResolutionPass(body);
			}
		}

		#endregion

		public void DebugStep()
		{
			foreach (var data in _bodies)
			{
				PrepareBody(data);
				_cache.bodies.Add(data);
				_debugData[data.body].frameStart = data.body.Position;
				_debugData[data.body].projected = data.endPosition;
			}

			CrossoverPhase(false);
			ContactPhase();

			foreach (var body in _cache.bodies)
			{
				var data = _debugData[body.body];
				data.postContact = body.endPosition;
			}

			FlexPhase(true);

			foreach (var body in _cache.bodies)
			{
				var data = _debugData[body.body];
				for (var i = 0; i < Body.kVerts; i++)
					data.postFlexPhaseBox[i] = (Vector3)body.EndBox[i];
			}

			ResolutionPhase(false);

			foreach (var body in _cache.bodies)
			{
				var data = _debugData[body.body];
				data.postResolution = data.frameStart + (Vector3)body.motion;
			}
		}

		public void RegularStep()
		{
			CrossoverPhase();
			ContactPhase();
			FlexPhase(); // Resolutions done in here
		}

		private void Awake()
		{
			_blockBodyAdditions = !_acceptAnyway && _bodies.Count != 0;
			if (_blockBodyAdditions || _acceptAnyway)
				foreach (var body in _selection)
				{
					var data = new BData();
					data.body = body;
					_bodies.Add(data);
					_debugData.Add(body, new DebugData());
					body.containingFluid = FindContainingFluid(body);
				}
		}
		private void Start()
		{
			if (!useRegularTick)
				Step();
		}

		public void Step()
		{
			_cache.NextFrame();
			stepFlag = true;
			if (Draw)
				DebugStep();
			else
				RegularStep();
		}

		private void Update()
		{
			if (useRegularTick) Step();
			stepFlag = false;
		}

		public static void DrawFull(Body body, Vector3 pos, Matrix2x2 mat, int from, int to)
		{
			Gizmos.DrawLine(pos + (Vector3)(mat * body.FullBox[from]), pos + (Vector3)(mat * body.FullBox[to]));
		}

		public static void DrawFlex(Body body, Vector3 pos, Matrix2x2 mat, int from, int to)
		{
			Gizmos.DrawLine(pos + (Vector3)(mat * body.FlexBox[from]), pos + (Vector3)(mat * body.FlexBox[to]));
		}

		public static void DrawRigid(Body body, Vector3 pos, Matrix2x2 mat, int from, int to)
		{
			Gizmos.DrawLine(pos + (Vector3)(mat * body.RigidBox[from]), pos + (Vector3)(mat * body.RigidBox[to]));
		}


		private void OnGUI()
		{
			var rctButton = new Rect(Screen.width - 100f, 10f, 90f, 20f);
			if (!Application.isPlaying || useRegularTick || !GUI.Button(rctButton, "Step")) return;

			Step();
		}

		private void OnDrawGizmos()
		{
			if (!Application.isPlaying) return;

			// FrameStart
			if (_drawFrameStart)
				foreach (var (body, data) in  _debugData)
				{
					var mat = body.RotationMatrix;
					for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
					{
						Gizmos.color = Color.black;
						DrawFull(body, data.frameStart, mat, i, j);
						Gizmos.color = Color.grey;
						DrawRigid(body, data.frameStart, mat, i, j);
					}
				}

			if (_drawProjected)
				foreach (var (body, data) in _debugData)
				{
					var mat = body.RotationMatrix;
					for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
					{
						Gizmos.color = Color.blue;
						DrawFull(body, data.projected, mat, i, j);
					}
				}

			if (_drawContact)
				foreach (var (body, data) in _debugData)
				{
					var mat = body.RotationMatrix;
					for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
					{
						Gizmos.color = Color.yellow;
						DrawFull(body, data.postContact, mat, i, j);
					}
				}

			if (_drawFlex)
				foreach (var (body, data) in _debugData)
				{
					var mat = body.RotationMatrix;
					for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
					{
						Gizmos.color = Color.green;
						DrawFlex(body, data.postContact, mat, i, j);
					}
				}

			if (_drawResolution)
				foreach (var (body, data) in _debugData)
				{
					var mat = body.RotationMatrix;
					for (int i = 0, j = 1; i < Body.kVerts; i++, j = ++j % Body.kVerts)
					{
						Gizmos.color = new Color(1, 0, 1);
						Gizmos.DrawSphere(data.postResolution, 0.05f);
						if (!body.IsInFlexible)
							DrawRigid(body, data.postResolution, mat, i, j);
					}
				}
		}

		public List<int> CastBody(Body body, Vector2 direction, float distance)
		{
			var points = new List<Vector2>();
			const int kResolution = 2;
			const float kStep = 1f / (kResolution + 1);
			for (int i = 0; i < Body.kVerts; i++)
			{
				Vector2 a = body.FlexBox[i], b = body.FlexBox[(i + 1) % Body.kVerts];

				var dotA = Vector2.Dot(direction, a);
				var dotB = Vector2.Dot(direction, b);
				if (dotA <= 0 || dotB <= 0)
					continue;

				a += body.Position;
				b += body.Position;

				points.Add(a);
				for (var t = kStep; t < 1f; t += kStep)
					points.Add(Vector2.Lerp(a, b, t));
				points.Add(b);
			}

			var contacts = new List<int>();
			for (var j = 0; j < _cache.bodies.Count; j++)
			{
				var other = _cache.bodies[j].body;
				if (body == other) continue;

				foreach (var p in points)
				{
					for (var i = 0; i < Body.kVerts; i++)
					{
						var curr = other.FlexBox[i] + other.Position;
						var next = other.FlexBox[(i + 1) % Body.kVerts] + other.Position;

						if (!FindCrossoverPoint(p, direction, curr, next - curr, out var overlapPoint))
							continue;

						var dist = Vector2.Distance(overlapPoint, p);
						if (dist > distance) continue;

						contacts.Add(j);
						goto __nextBody;
					}
				}
			__nextBody:;
			}

			return contacts;
		}
	}
}
