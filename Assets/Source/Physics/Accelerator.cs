using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fysics
{
    [RequireComponent(typeof(Body))]
    public class Accelerator : MonoBehaviour
    {
        [SerializeField] Vector2 acceleration;

        FysicsManager manager;
        Body body;

        private void Awake()
        {
            manager = FindObjectOfType<FysicsManager>();
            body = GetComponent<Body>();
        }

        private void Update()
        {
            if (manager.useRegularTick || manager.stepFlag)
            {
                Debug.Log(2);
                body.acceleration += acceleration;
            }
        }
    }
}
