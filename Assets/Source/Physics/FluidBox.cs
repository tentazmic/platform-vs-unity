﻿using System.Collections;
using UnityEngine;

namespace Fysics
{
    public class FluidBox : MonoBehaviour
    {
        public Vector2 extents = new Vector2(1.0f, 1.0f);
        [SerializeField] Fluid fluid;

        public Fluid Data => fluid;

        void Start()
        {
            FindObjectOfType<FysicsManager>()?.AddFluid(this);
        }

        void OnDestroy()
        {
            FindObjectOfType<FysicsManager>()?.RemoveFluid(this);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(0, 1, 1, 0.1f);
            Gizmos.DrawCube(transform.position, new Vector3(extents.x * 2, extents.y * 2, 1.0f));
        }
    }
}