using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Fysics
{
    public class TriggerTester : MonoBehaviour
    {
        [SerializeField, Min(0.1f)] float _size = 1;

        FysicsManager _manager;
        List<Body> _overlaps = new();

        private void Start()
        {
            _manager = FindObjectOfType<FysicsManager>();
        }

        void Update()
        {
            _overlaps = _manager.CircleOverlap(transform.position, _size);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _size);
        }

        private void OnGUI()
        {
            var rect = new Rect(4f, 10f, 100f, 20f);

            foreach (var body in _overlaps)
            {
                GUI.Label(rect, body.name);
                rect.y += 22f;
            }
        }
    }
}
