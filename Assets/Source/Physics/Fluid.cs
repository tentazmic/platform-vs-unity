﻿using System;
using UnityEngine;

namespace Fysics
{
    [Serializable]
    public class Fluid
    {
        public float density = 5f;
    }
}