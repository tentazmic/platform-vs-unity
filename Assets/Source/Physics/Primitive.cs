﻿using System;
using System.Collections;
using UnityEngine;

namespace Fysics
{
	// The distance field can be between 0 and 4. The number before the decimal tells
	// what edge this applies to and the number after is the lerp distance into the edge.
	// The friction value applies until the distance of the next PrimitiveFriction
	// in the friction def array or the end
	[Serializable]
	public struct PrimitiveFriction
	{
		public float Distance;
		public float Friction;
	}

	// Defines a link between two primitives and the edges they are connected on
	[Serializable]
	public struct PrimitiveLink
	{
		public int iA, iB;
		public int edgeA, edgeB;
	}

	[Serializable]
	public class Primitive
	{
		public const int kVerts = 4;

		public Vector2[] FullBox = new[] { new Vector2(-1, 1), new Vector2(1, 1), new Vector2(1, -1), new Vector2(-1, -1) };
		public Vector2[] RigidBox = new[] { new Vector2(-0.5f, 0.5f), new Vector2(0.5f, 0.5f), new Vector2(0.5f, -0.5f), new Vector2(-0.5f, -0.5f) };
		public bool[] FreeEdges = new[] { true, true, true, true };
		[NonSerialized]
		public Vector2[] Box = new Vector2[4];

		public PrimitiveFriction[] FrictionDef = new[] { new PrimitiveFriction { Distance = 0, Friction = 2 } };

		// How quickly the primitive restores any deformation
		[SerializeField, Range(0.1f, 1)] float _restorationFactor = 0.4f;
		// How much the primitive prevents itself from deforming
		[SerializeField, Range(0.1f, Body.kRigidityMax)] float _rigidity = 10;
		// How much momentum this primitive absorbs
		[SerializeField, Range(0, 100)] public float _shockAbsorption = 2;

		[NonSerialized] public bool InContact = false;
		[NonSerialized] public Contact[] contacts = new[] { new Contact(), new Contact(), new Contact(), new Contact() };

		public float RestorationFactor => _restorationFactor;
		public float Rigidity => _rigidity;
		public float ShockAbsorption => _shockAbsorption;
	}
}