using UnityEngine;
using UnityEditor;

namespace Fysics
{
    [CustomEditor(typeof(FysicsManager))]
    public class FysicsManagerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var myTarget = (FysicsManager)target;

            if (Application.isPlaying && !myTarget.useRegularTick && GUILayout.Button("Step")) myTarget.Step();
        }
    }
}
