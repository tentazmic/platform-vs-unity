﻿using UniBoost.Input;
using UnityEngine;

namespace Input
{
	[System.Serializable]
	public struct InputFrame
	{
		public Vector2 control;
		public ControlState stStick;
		public bool snap;
		public int snapCount;
		public int framesSinceSnap;
		public ControlState jump;
		public ControlState light;
		public ControlState medium;
		public ControlState heavy;
		public ControlState grab;
		public ControlState guard;

		public bool EquivalentPrimaryAttackButtons(in InputFrame other)
		{
			return light == other.light && medium == other.medium && heavy == other.heavy;
		}
	}
}