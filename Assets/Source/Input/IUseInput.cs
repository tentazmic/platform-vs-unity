﻿namespace Input
{
	public interface IUseInput
	{
		IInput Input { get; }
	}
}