﻿using System.Runtime.CompilerServices;

namespace Input
{
	/// <summary>
	/// Alters the source input
	/// </summary>
	public interface IInputRemixer : IInput
	{
		IInput Source { get; set; }

		void UpdateInput();
		void Reset();
	}
}