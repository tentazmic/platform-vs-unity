﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniBoost.Extensions;
using UnityEngine;
using static UnityEngine.Mathf;
using Input.Motions.Charge;
using Input.Motions;
using Motion = Input.Motions.Motion;

namespace Input.Units
{
	[Serializable]
	public class MotionUnit
	{
		const int kMotionTimeout = 40;
		const int kNeutralControlStickForMotionTimeout = 5;
		const int kMotionBufferSize = 60;
		const float kMotionActuationThreshold = 0.9f;

		readonly InputStick _stick;
		readonly IChargeCache _chargeCache;
		
		readonly Vector2[] _inputBuffer;
		readonly int _bufferSize;

		static readonly Vector2 ForwardVector = Vector2.right;

		List<Motion> _motionsIdentified;
		List<int> _motionTimeouts = new List<int>(1);

		public Motion[] CurrentMotions => _motionsIdentified.ToArray();

		public MotionUnit(InputStick stick, IChargeCache chargeCache)
		{
			_motionsIdentified = new List<Motion>(1);
			_stick = stick;
			_chargeCache = chargeCache;
			_bufferSize = kMotionBufferSize;
			_inputBuffer = new Vector2[_bufferSize];
		}

		public void Tick()
		{
			_chargeCache.Tick();

			UpdateMotionTrail();
			FindNewMotions();
		}

		void UpdateMotionTrail()
		{
			var input = _stick.Value;
			if (input.magnitude < kMotionActuationThreshold) input = Vector2.zero;
			
			_chargeCache.AddCharge(input);

			for (var i = _bufferSize - 1; i > 0; i--)
				_inputBuffer[i] = _inputBuffer[i - 1];

			_inputBuffer[0] = input;

			for (var i = _motionTimeouts.Count - 1; i > -1; i--)
			{
				var timeout = _motionTimeouts[i];
				timeout--;

				if (timeout > 0)
				{
					_motionTimeouts[i] = timeout;
					continue;
				}
					
				_motionsIdentified.RemoveAt(i);
				_motionTimeouts.RemoveAt(i);
			}
		}

		void FindNewMotions()
		{
			// Find reorientation function
			var current = _stick.Value;
			if (current == Vector2.zero)
				return;
			
			var (angle, xMult) = GetTransformDataFromVector(current);

			var foundDirections = FindMotionSteps(_chargeCache, angle, xMult, _inputBuffer);
			
			// if (foundDirections.Count == 3)
			// 	Debug.Log(foundDirections.Aggregate("", (s, step) => $"{s} {step}"));

			if (!MotionLibrary.GetMotions(foundDirections.ToArray(), out var motions))
				return;
			
			var motionTimeout = kMotionTimeout;
			foreach (var t in motions)
			{
				var motion = t;
				motion.Direction = _inputBuffer[0];

				if (_motionsIdentified.Contains(motion))
					continue;
				
				// if (motion.Definition.name == "DoubleSnap")
				// 	Debug.Log(_inputBuffer.Select((vector2, i) => (vector2, i)).Aggregate("", (s, tuple) => $"{s} {tuple.i}: {tuple.vector2};"));
				
				_motionsIdentified.Add(motion);
				_motionTimeouts.Add(motionTimeout);
			}
		}

		public static List<MotionStep> FindMotionSteps(IChargeCache chargeCache, float angle, int xMultiplier, Vector2[] trail)
		{
			var foundDirections = new List<MotionStep>();
			var last = MotionDirection.Forward;
			var resetCountDown = kNeutralControlStickForMotionTimeout;
			var first = true;
			
			for (var i = trail.Length - 1; i >= 0; i--)
			{
				var input = trail[i];
				if (input == Vector2.zero)
				{
					resetCountDown--;
					continue;
				}

				input.x *= xMultiplier;

				var vector = input.RotateVector(angle);
				var direction = EMotionDirection.GetDirectionFromVector(vector);

				if (!first && resetCountDown > 0)
				{
					if (last == direction)
					{
						foundDirections.Last().time = i;
						continue;
					}
				}

				var step = new MotionStep {direction = direction, chargeTime = (int) chargeCache.GetCharge(input), time = i};
				foundDirections.Add(step);
				resetCountDown = kNeutralControlStickForMotionTimeout;
				last = direction;

				if (first)
					first = false;
			}

			return foundDirections;
		}

		public static (float, int) GetTransformDataFromVector(Vector2 vector)
		{
			var flipX = vector.x < 0 ? -1 : 1;
			vector.x *= flipX;
			var angle = Vector2.SignedAngle(ForwardVector, vector);
			// if (vector.y < 0) angle *= -1;
				// angle += 180f;
			return (angle * Deg2Rad * flipX, flipX);
		}

		public bool HasMotion(MotionDefinition motionDefinition) => CurrentMotions.Any(motion => motion.Definition = motionDefinition);
	}
}