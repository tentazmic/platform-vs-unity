﻿using System;
using UniBoost.Input.Implementations;
using UnityEngine;
using static UnityEngine.Mathf;

namespace Input.Units
{
	[Serializable]
	public class InputStick : InputGeneric<Vector2>
	{
		public bool snap;

		public override Vector2 Value
		{
			get => value;
			set
			{
				var previousValue = this.value;
				var diff = Mathf.Abs(value.magnitude - previousValue.magnitude);
				snap = diff >= 0.8f;

				this.value = value;

				TimeOfLastUpdate = Time.time;
			}
		}
		
		public float X => Value.x;
		public float Y => Value.y;
		
		public Vector2 Abs => new Vector2(Abs(Value.x), Abs(Value.y));

		public Vector2Int Sign
		{
			get
			{
				var x = (int) (Approximately(Value.x, 0) ? 0 : Sign(Value.x));
				var y = (int) (Approximately(Value.y, 0) ? 0 : Sign(Value.y));
				return new Vector2Int(x, y);
			}
		}
		
		public override void Tick()
		{
			base.Tick();
			if (snap) snap = !snap;
		}
	}
}