﻿using System.Linq;
using TMPro;
using Input;
using Input.Motions.Charge;
using UnityEngine;

namespace Input.UI
{
	[AddComponentMenu("Platform VS/Input/Input UI")]
	public class InputUI : MonoBehaviour
	{
		[SerializeField] TMP_Text textDisplayPrefab;
		[SerializeField] RectTransform controlStick;
		[SerializeField] RectTransform controlStickBackground;
		[SerializeField] float controlStickMoveSize = 100f;

		[SerializeField] TMP_Text motionDisplay;

		[SerializeField] UIButton modifierButton;
		[SerializeField] UIButton jumpButton;
		[SerializeField] UIButton grabButton;
		[SerializeField] UIButton shieldButton;

		IInput _input;

		TMP_Text[] _chargeDisplays;
		IChargeCache _chargeCache;

		string _motionDisplayDefault;

		public void SetInput(IInput input)
		{
			_input = input;
			if (_input == null)
			{
				enabled = false;
				_chargeCache = null;
				foreach (var display in _chargeDisplays)
					Destroy(display.gameObject);

				modifierButton.Button = jumpButton.Button = grabButton.Button = shieldButton.Button = null;
			}
			else
			{
				enabled = true;
				_chargeCache = _input.ChargeCache;
				var chargeCount = _chargeCache.ChargeInfo.Count;
				_chargeDisplays = new TMP_Text[chargeCount];

				var i = 0;
				foreach (var info in _chargeCache.ChargeInfo)
				{
					var display = Instantiate(textDisplayPrefab, controlStickBackground);
					display.GetComponent<RectTransform>()
						.anchoredPosition = info.Direction * (controlStickMoveSize * 1.3f);
					_chargeDisplays[i] = display;
					i++;
				}

				_motionDisplayDefault = motionDisplay.text;

				if (modifierButton)
					modifierButton.Button = _input.Modifier;
			
				if (jumpButton)
					jumpButton.Button = _input.Jump;

				if (grabButton)
					grabButton.Button = _input.Grab;

				if (shieldButton)
					shieldButton.Button = _input.Shield;
			}
		}
		
		void Awake()
		{
			enabled = false;
		}

		void Update()
		{
			controlStick.anchoredPosition = _input.Control.Value * controlStickMoveSize;

			motionDisplay.text = _input.Motion.CurrentMotions.Length == 0 
				? _motionDisplayDefault 
				: _input.Motion.CurrentMotions.Aggregate("", (s, motion) => s + " " + motion.Definition.name) + " " + _input.Motion.CurrentMotions.Length;

			var i = 0;
			foreach (var info in _chargeCache.ChargeInfo)
			{
				_chargeDisplays[i].text = $"{info.Charge:#0.###} : {info.Countdown}";
				i++;
			}
		}
	}
}