﻿using UniBoost.Input.Implementations;
using Input.Units;
using Input.Motions.Charge;

namespace Input
{
    /// <summary>
    /// Defines the core controls of the game's input
    /// </summary>
    public interface IInput
    {
        bool IsActive { get; set; }
        
        InputStick Control { get; }
        
        IChargeCache ChargeCache { get; }
        MotionUnit Motion { get; }
        
        InputButton Modifier { get; }
        InputButton Jump { get; } 
        InputButton Light { get; } 
        InputButton Medium { get; } 
        InputButton Heavy { get; } 
        InputButton Super { get; } 
        
        InputButton Grab { get; }
        InputButton Shield { get; }
    }
}
