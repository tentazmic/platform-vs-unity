﻿using UnityEngine;

namespace Input.Motions
{
	[CreateAssetMenu(menuName = "Input/Motion", order = 0)]
	public class MotionDefinition : ScriptableObject
	{
		// The array must end with Forward
		// The default order should be the anticlockwise motion
		[SerializeField] MotionStep[] baseSteps;
		[SerializeField] int mandatoryStepCount;

		public bool AreStepsValid(MotionStep[] motionSteps, out bool clockwise)
		{	
			// Debug.Log("");
			// Debug.Log($"{name}, Steps {baseSteps.Length}, Mandatory {mandatoryStepCount}");
			clockwise = false;

			if (mandatoryStepCount > motionSteps.Length)
				return false;

			var valid = true;
			int i = baseSteps.Length - 1, j = motionSteps.Length - 1;
			
			for (; i > -1 && j > -1; i--)
			{
				var baseStep = baseSteps[i];
				var motionStep = motionSteps[j];
				if (clockwise)
					motionStep.direction.FlipVertical();
				
				// Debug.Log($"Base {baseStep} : Motion {motionStep}\n      {(clockwise ? "" : "Anti-")}Clockwise, i {i}, j {j}");

				if (baseStep.direction == motionStep.direction &&
				    baseStep.chargeTime <= motionStep.chargeTime && (baseStep.time < 0 || baseStep.time >= motionStep.time))
				{
					j--;
					continue;
				}
				
				if (baseStep.isOptional)
					continue;

				if (motionStep.direction.IsPureHorizontal() || clockwise)
				{
					// We've already attempted to flip the rotation
					// So, if we're here it's not valid
					valid = false;
					break;
				}
				
				// Flip orientation and try again
				clockwise = true;
				i++;
			}
			
			// Debug.Log($"{(valid ? "Good" : "Bad")}");
			
			return valid;
		}
	}
}