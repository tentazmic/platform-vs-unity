﻿using System.Collections.Generic;
using UnityEngine;

namespace Input.Motions.Charge
{
	public class NullChargeCache : IChargeCache
	{
		public IReadOnlyCollection<ChargeDirectionInfo> ChargeInfo => new ChargeDirectionInfo[0];
		public void Tick() { }

		public void AddCharge(Vector2 input) { }

		public float GetCharge(Vector2 direction) => 0;
	}
}