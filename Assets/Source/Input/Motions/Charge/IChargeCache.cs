﻿using System.Collections.Generic;
using UnityEngine;

namespace Input.Motions.Charge
{
	public interface IChargeCache
	{
		IReadOnlyCollection<ChargeDirectionInfo> ChargeInfo { get; }
		
		void Tick();
		
		void AddCharge(Vector2 input);

		float GetCharge(Vector2 direction);
	}

	public struct ChargeDirectionInfo
	{
		public readonly Vector2 Direction;
		public float Charge;
		public int Countdown;
		public bool JustAdded;

		public ChargeDirectionInfo(Vector2 direction)
		{
			Direction = direction;
			Charge = 0;
			Countdown = 0;
			JustAdded = false;
		}
	}
}