﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Input.Motions
{
	public static class MotionLibrary
	{
		static MotionDefinition[] _allMotionDefinitions;

		public static bool GetMotions(MotionStep[] steps, out Motion[] motions)
		{
			var list = new List<Motion>();
			// Find step time diffs
			foreach (var motionDefinition in _allMotionDefinitions)
			{
				if (!motionDefinition.AreStepsValid(steps, out var clockwise))
					continue;

				list.Add(new Motion {Definition = motionDefinition, IsClockwise = clockwise});
			}

			motions = list.ToArray();
			return list.Count > 0;
		}

		[RuntimeInitializeOnLoadMethod]
		public static void OnLoad()
		{
			_allMotionDefinitions = AssetDatabase
				.FindAssets("t:" + nameof(MotionDefinition))
				.Select(AssetDatabase.GUIDToAssetPath)
				.Select(AssetDatabase.LoadAssetAtPath<MotionDefinition>)
				.ToArray();
		}
	}
}