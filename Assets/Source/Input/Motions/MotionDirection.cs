﻿using UnityEngine;
using static Input.Motions.MotionDirection;

namespace Input.Motions
{
	public enum MotionDirection
	{
		Forward, Backward, Up, Down,
		UpForward, UpBackward,
		DownForward, DownBackward
	}

	public static class EMotionDirection
	{
		public static bool IsPureHorizontal(this MotionDirection direction)
			=> direction == Forward || direction == Backward;
		
		public static void FlipVertical(this ref MotionDirection direction) =>
			direction = direction switch
			{
				Up => Down,
				UpForward => DownForward,
				UpBackward => DownBackward,
				Down => Up,
				DownForward => UpForward,
				DownBackward => UpBackward,
				_ => direction
			};

		/// <summary>
		/// Gets the MotionDirection from the vector, assumes that the forward direction is (1, 0)
		/// </summary>
		/// <param name="vector"></param>
		/// <returns></returns>
		public static MotionDirection GetDirectionFromVector(Vector2 vector)
		{
			var angle = Vector2.SignedAngle(vector, Vector2.up);
			var absAngle = Mathf.Abs(angle);
			
			if (absAngle < 22.5f)
				return Up;
			if (absAngle > 157.5f)
				return Down;

			// Forward Checks
			if (angle > 0)
			{
				if (angle <= 67.5f)
					return UpForward;
				if (angle <= 115.5f)
					return Forward;
				if (angle <= 157.5f)
					return DownForward;
			}

			
			if (absAngle <= 67.5f)
				return UpBackward;
			
			return absAngle <= 115.5f ? Backward : DownBackward;
		}
	}	
}