using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Fysics;
using Input;
using StateSystem;

namespace Actor
{
	public class FighterContext
	{
		// These should be passed into the TryEntry function as const
		public ActorData data;
		public FighterStats stats;
		public Body body;
		public InputFrame input;
		public FysicsManager manager;
	}

	public struct FighterFlags
	{
		public bool NoClearCling;
		public bool NoClearLedgeGrab;
	}

	[RequireComponent(typeof(ActorData))]
	public class Fighter : MonoBehaviour
	{
		static int sFighterIndex = 0;

		[SerializeField] Body _body;

		public StateMachine<FighterContext, FighterFlags> Machine { get; private set; }

		// Used for offsetting the debug info
		int _fighterIndex = 0;
		ActorData _data;
		FighterStats _stats;

		FighterContext _context;
		StandardInput _input;

		FysicsManager manager;

		public FighterContext Context => _context;

		private void Awake()
		{
			_fighterIndex = sFighterIndex++;
			_data = GetComponent<ActorData>();
			_stats = GetComponent<FighterStats>();
			_input = new StandardInput(StartCoroutine);
			_context = new FighterContext { data = _data, stats = _stats, body = _body };
			States.Init();

			var states = new List<IState<FighterContext, FighterFlags>>
			{
				States.Skid, States.WallPush, States.PreDash, States.DashPivot, States.GroundDash, States.Turnaround,
				States.Slide, States.RunOut, States.Dive, States.LedgeJump, States.LedgeGetup, States.LedgeGrab, States.LedgeHang, States.ClingTurnaround, States.Scale, States.Cling,
				States.Run, States.Stand, States.PreemptiveDive, States.GroundJump, States.AirJump, States.Airborne
			};

			var tree = StateMachine<FighterContext, FighterFlags>.CreateTree(Clauses.GetAll(), states.ToArray());
			Machine = new StateMachine<FighterContext, FighterFlags>(States.Airborne, tree);
			Machine.OnStateChanged += () => { _data.stFramesSinceEntry = 0; };
			Debug.Log(Machine.TreeRoot.Print(0, _context));
		}

		private void Start()
		{
			manager = FindObjectOfType<FysicsManager>();
			_context.manager = manager;
		}

		ulong frame = 0;

		private void Update()
		{
			/* NOTE
			 * The states are seeing many instances where CanEnter does a physics calculation
			 * Then a similar physics calculation is done in Enter which is wasteful
			 * 
			 * In the proper implementation the frame step will look like this:
			 * StateMachine Branch Evaluation and CanEnter Physics Queries
			 * Physics Step
			 * State Update
			 * StateMachine Leaf Evaluation, State Entry
			 * 
			 * Branch Evaluation is the step where the StateMachine finds what leaf we are on
			 * The states on that leaf then submit physics queries to be evaluated in the physics
			 * step, the results of which are cached
			 * 
			 * State Update
			 * Leaf Evaluation then runs CanEnter on all the states in the leaf we found earlier,
			 * if a valid leaf is found we enter it
			 */

			frame++;
			var framesSinceSnap = _context.input.framesSinceSnap;
			if (_input.Control.snap)
				framesSinceSnap = 0;

			var iFrame = new InputFrame
			{
				//control = control,
				//stStick = UniBoost.Input.ControlState.Held,
				control = _input.Control.Value * (_input.Modifier ? 0.5f : 1.0f),
				stStick = _input.Control.State,
				snap = _input.Control.snap,
				framesSinceSnap = framesSinceSnap,
				jump = _input.Jump.State,
				light = _input.Light.State,
				medium = _input.Medium.State,
				heavy = _input.Heavy.State,
				guard = _input.Shield.State,
				grab = _input.Grab.State
			};
			_context.input = iFrame;

			var normal = Vector2.zero;
			foreach (var contact in _body.contacts)
				normal += contact.normal;

			_context.data.towardContact = -normal.normalized;

			_data.grabbableBodies = manager.CircleOverlap(_body.Position + _stats.GrabOffset, _stats.GrabRadius, _body);

			_data.stFramesSinceEntry++;
			_body.ResetMultipliers();
			Machine.Update(_context);

			//if (_body.ghosting && !_body.anyOverlaps)
			//	_body.ghosting = false;
			_body.ghosting &= !(_body.ghosting && !_body.isEnveloped);

			if (iFrame.control.magnitude > 0.95f && !_body.inContact)
			{
				_body.DiveIntention = true;
				//_body.ghosting = true;
				var contacts = manager.CastBody(_body, iFrame.control, 3f);
				var allContacts = _body.contacts.SelectMany(contact => contact.bodies.Keys).Distinct();
				foreach (var contact in contacts)
				{
					if (allContacts.Contains(manager.GetBody(contact)))
						continue;
					manager.AddGhostContact(_body, contact);
				}
			}
		}

		private void OnDrawGizmos()
		{
			if (!_stats) _stats = GetComponent<FighterStats>();
			if (!_data) _data = GetComponent<ActorData>();
			Gizmos.DrawWireSphere(_body.Position + _stats.GrabOffset, _stats.GrabRadius);

			if (_body && Application.isPlaying)
				for (int i = 0; i < Body.kVerts; i++)
				{
					int j = (i + 1) % Body.kVerts;
					FysicsManager.DrawFull(_body, _body.Position + _input.Control.Value * _stats.DiveLookAheadDist, _body.RotationMatrix, i, j);
				}

			if (_data.grabBody)
			{
				var line1 = _data.grabBody.FlexBox[_data.grabEdge];
				var line2 = _data.grabBody.FlexBox[(_data.grabEdge + 1) % Fysics.Body.kVerts];
				var grabAxleLocal = (line2 - line1) * _data.grabPointAlongEdge + line1;
				var grabAxleCentre = grabAxleLocal + _data.grabBody.Position;
				Gizmos.DrawSphere(grabAxleCentre, .3f);
			}
		}

		private void OnGUI()
		{
			const float kWidth = 220;
			const float kBuffer = 10;
			var rect = new Rect(kBuffer + _fighterIndex * (kWidth + kBuffer), 4, kWidth, 20);

			var strings = new[]
			{
				name,
				Machine.Current.ActiveState(_context).ToString(),
				$"Vel {_body.velocity}"
			};

			foreach (var s in strings)
			{
				GUI.Label(rect, s);
				rect.y += 12;
			}

			rect.x += kWidth + 10.0f;
			rect.y = 4;
			foreach (var body in _data.grabbableBodies)
			{
				GUI.Label(rect, body.name);
				rect.y += 12;
			}
		}
	}
}
