﻿using System.Collections.Generic;
using UnityEngine;
using Fysics;
using State = StateSystem.IState<Actor.FighterContext, Actor.FighterFlags>;
using Machine = StateSystem.StateMachine<Actor.FighterContext, Actor.FighterFlags>;

namespace Actor
{
	public class FighterAnimation : MonoBehaviour
	{
		[SerializeField] Animator _animator;
		[SerializeField] Fighter _fighter;

		readonly int _anm_Stand = Animator.StringToHash("idle");
		readonly int _anm_Aerial = Animator.StringToHash("aerial");
		readonly int _anm_GroundLocomotion = Animator.StringToHash("groundlocomotion");
		readonly int _anm_GroundLocoRatio = Animator.StringToHash("groundloco_ratio");

		readonly int _anm_JumpSquat = Animator.StringToHash("jumpsquat");
		readonly int _anm_Cling = Animator.StringToHash("crouch");
		readonly int _anm_Scale = Animator.StringToHash("crawl");

		readonly int _anm_Dash = Animator.StringToHash("initial_dash");
		readonly int _anm_Sprint = Animator.StringToHash("sprint");
		readonly int _anm_Skid = Animator.StringToHash("skid");
		readonly int _anm_Slide = Animator.StringToHash("slide");

		delegate int GetID();
		Dictionary<State, GetID> _stateAnimationMap;

		bool _inGroundLoco = false;
		Transform _xyzModel;

		private void Start()
		{
			_fighter.Machine.OnStateChanged += OnStateChanged;
			_xyzModel = _animator.transform;

			_stateAnimationMap = new Dictionary<State, GetID>
			{
				{ States.Stand, () => _anm_Stand },
				{ States.Airborne, () => _anm_Aerial },
				{ States.Run, () => { _inGroundLoco = true; return _anm_GroundLocomotion; } },
				{ States.WallPush, () => _anm_JumpSquat },
				{ States.RunOut, () => { _inGroundLoco = true; return _anm_GroundLocomotion; } },
				{ States.PreDash, () => _anm_Dash },
				{ States.GroundDash, () => _anm_Dash },
				{ States.DashPivot, () => _anm_Dash },
				{ States.Skid, () => _anm_Skid },
				{ States.Slide, () => _anm_Slide },
				{ States.Cling, () => _anm_Cling },
				{ States.Scale, () => _anm_Scale }
			};
		}

		private void Update()
		{
			_xyzModel.right = _fighter.Context.data.facing switch
			{
				EFacing.Left => Vector3.right,
				EFacing.Right => Vector3.left,
				_ => _xyzModel.right
			};

			if (_inGroundLoco)
			{
				var vel = _fighter.Context.body.velocity.magnitude;
				var f = Mathf.Clamp(vel, 4.0f, 11.5f) - 4.0f;
				f /= (11.5f - 4.0f);
				_animator.SetFloat(_anm_GroundLocoRatio, f);
			}
		}

		void OnStateChanged()
		{
			_inGroundLoco = false;
			var current = _fighter.Machine.Current.ActiveState(_fighter.Context);
			if (_stateAnimationMap.ContainsKey(current))
			{
				//Debug.Log(current.ActiveState(_fighter.Context).ToString());
				_animator.Play(_stateAnimationMap[current](), 0, 0);
			}
		}
	}
}