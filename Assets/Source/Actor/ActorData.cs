﻿using System.Collections.Generic;
using UnityEngine;
using UniBoost.Extensions;

namespace Actor
{
	public enum EFacing { Left, Right }
	// Shortcut refers to holding the jump button and inputting the stick
	public enum EDashFlag { Normal, Shortcut, NoBack, Auto }
	public enum EClingFlag { None, Crouch, WallHug }

	public class ActorData : MonoBehaviour
	{
		public Vector2 upVector = Vector2.up;
		public EFacing facing = EFacing.Right;

		public Vector2 launchVector = Vector2.zero;
		public Vector2 towardContact = Vector2.zero;

		//public bool MidDash = false;
		public EDashFlag DashingFlag = EDashFlag.Normal;
		public bool DashPivot_LeaveAfterPivot = false;

		public List<int> clingContacts = new();
		public Vector2 clingAngle = Vector2.zero;
		public EClingFlag ClingFlag = EClingFlag.None;

		public List<Fysics.Body> grabbableBodies = new();

		public int grabEdge = -1;
		public Fysics.Body grabBody;
		public float grabPointAlongEdge;

		public Vector2 getupStartOffset;
		public List<Vector2> getupJumpVectors;

		public bool diveGrab = false;
		public Vector2 diveThrustDir = Vector2.zero;

		public int stFramesSinceEntry = 0;

		public Vector2 ForwardVector => upVector.GetNormal(facing == EFacing.Left);
		public Vector2 ForwardContactVector => towardContact.GetNormal(facing == EFacing.Right);

		public Vector2 GetGrabPos()
		{
			if (!grabBody) return Vector2.zero;

			var j = (grabEdge + 1) % Fysics.Body.kVerts;
			var relPos = Vector2.Lerp(grabBody.FlexBox[grabEdge], grabBody.FlexBox[j], grabPointAlongEdge);
			return relPos + grabBody.Position;
		}
	}
}