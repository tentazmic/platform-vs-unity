﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniBoost.Input;
using UniBoost.Extensions;
using StateSystem;
using static UnityEngine.Mathf;
using Fysics;
using UniBoost.Extensions;
using UniBoost.Math;
using _Single = StateSystem.StSingle<Actor.FighterContext, Actor.FighterFlags>;
using _Clause = StateSystem.StateClause<Actor.FighterContext>;
using _Output = StateSystem.StateOutput<Actor.FighterContext>;

namespace Actor
{
	public static class Clauses
	{
		public static readonly _Clause Button = new(nameof(Button), 8, c =>
		{
			if (c.input.guard == ControlState.Down)
				return ESCButton.Guard;
			if (c.input.grab == ControlState.Down)
				return ESCButton.Grab;
			if (c.input.heavy == ControlState.Down)
				return ESCButton.Heavy;
			if (c.input.medium == ControlState.Down)
				return ESCButton.Medium;
			if (c.input.light == ControlState.Down)
				return ESCButton.Light;
			if (c.input.jump == ControlState.Down)
				return ESCButton.Jump;

			return ESCButton.None;
		});

		public static readonly _Clause InContact = new(nameof(InContact), 2, c => c.body.inContact ? ESCBool.True : ESCBool.False);

		public static readonly _Clause Grounded = new(nameof(Grounded), 2, c =>
		{
			var dot = Vector2.Dot(c.data.towardContact, c.data.upVector);
			return dot < -0.5f ? ESCBool.True : ESCBool.False;
		});

		public static readonly _Clause Stick = new(nameof(Stick), 4, c =>
		{
			if (c.input.control == Vector2.zero)
				return ESCStickInput.None;
			if (c.input.control.sqrMagnitude < (0.5f * 0.5f))
				return ESCStickInput.Low;
			if (c.input.stStick == ControlState.Down && c.input.control.sqrMagnitude > (0.9f * 0.9f))
				return ESCStickInput.Snap;

			return ESCStickInput.Full;
		});

		public static readonly _Clause[] All = { Button, Grounded, Stick };

		public static _Clause[] GetAll()
		{
			var all = new[] { Button, Stick, InContact, Grounded };
			all[3].ParentRequirements.Add(InContact, ESCBool.True);
			return all;
		}
	}

	// NOTE C++ Implementation
	// All the states are stored in a global array.
	// The sttate struct looks like so:
	// State:
	//   DEBUG(name)
	//   StateTraits traits
	//   Dict<Clause, Value> conditions
	//   FuncPtr CanEnter
	//   FuncPtr Enter
	//   FuncPtr Update
	//   FuncPtr Exit
	//
	// All states are defined in a single file, following this syntax:
	// STATE (name) {
	//   TRAITS (space separated list)
	//   CONDITIONS:
	//     (Clause), (value)
	//     (clause), (value)
	//   fn CanEnter
	//   fn Enter
	//   fn Update
	//   fn Exit
	// }

	// Any of the fields can be omitted, its value will be filled in with the default/null value
	// We can use the ppprocess for this

	public static class States
	{
		public static _Single Stand = new(
			nameof(Stand),
			delegate(FighterContext c, ref FighterFlags flags)
			{
				var control = Abs(Vector2.Dot(c.data.towardContact.GetNormal(), c.input.control));
				return control < 0.1f;
			},
			c => c.body.acceleration = Vector2.zero,
			delegate { },
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Grounded, ESCBool.True }
			});

		public static _Single Airborne = new(
			nameof(Airborne),
			delegate { return true; },
			_ => { },
			delegate { },
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.False }
			},
			FStateTraits.Airborne);

		// Unlike Smash Bros, you naturally progression from walk
		// speed to run speed over time
		// A dash will just take you to your run instantly
		// Thus, dash attacks need to check the forward velocity is
		// above a value
		// This state will cover the progression from the a walk
		// to a run, didn't have a more appropriate name
		public static _Single Run = new(
			nameof(Run),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				// Don't have a clause for stick any nor do we allow multiple clauses
				if (c.input.stStick == ControlState.Released || c.input.stStick == ControlState.Up) return false;
				var dot = Vector2.Dot(c.data.upVector, c.input.control);
				return Abs(dot) < 0.2f;
			},
			_ => { },
			delegate (FighterContext c, ref _Output _)
			{
				var clockwise = c.data.towardContact.IsClockwiseTo(c.input.control);
				var accelDir = c.data.towardContact.GetNormal(clockwise);
				var vel = Vector2.Dot(c.body.velocity, accelDir);
				var a = (GroundLocomotionAcceleration(vel) + AidFromFriction(c.body, accelDir, c.body.BottomLeft, c.body.BottomRight)) * accelDir * c.stats.RunSpeed;
				c.body.acceleration += a;
			},
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Grounded, ESCBool.True },
				//{ Clauses.Stick, ESCStickInput.PerpToContact }
			});

		public static _Single WallPush = new(
			nameof(WallPush),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.input.stStick != ControlState.Held) return false;
				var right = c.data.upVector.GetNormal();
				var control = Vector2.Dot(right, c.input.control);
				if (Abs(control) < 0.7f)
					return false;

				if (control > 0)
				{
					return Vector2.Dot(c.body.TopRight.normal, c.input.control) < -0.7f
							&& Vector2.Dot(c.body.BottomRight.normal, c.input.control) < -0.7f;
				}

				return Vector2.Dot(c.body.TopLeft.normal, c.input.control) < -0.7f
						&& Vector2.Dot(c.body.BottomLeft.normal, c.input.control) < -0.7f;
			},
			delegate { },
			delegate(FighterContext c, ref _Output _) { },
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Grounded, ESCBool.True },
				{ Clauses.Stick, ESCStickInput.Full }
			}
			);

		// Run out
		// When travelling at high speed with a released control stick
		// you'll take long stides to slow yourself down
		public static _Single RunOut = new(
			nameof(RunOut),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var contactForward = c.data.towardContact.GetNormal(c.data.facing == EFacing.Right);
				var velDot = Vector2.Dot(contactForward, c.body.velocity);
				//Debug.Log($"{c.data.contactVector} {contactForward} {velDot}");
				return velDot > c.stats.RunOutSpeedMin;
			},
			c => { c.data.DashingFlag = EDashFlag.NoBack; },
			delegate(FighterContext c, ref _Output _) { c.body.groundFrictionMultiplier *= c.stats.RunOutFrictionMult; },
			delegate (FighterContext c, in FighterFlags f) { c.data.DashingFlag = EDashFlag.Normal; },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Grounded, ESCBool.True },
				{ Clauses.Stick, ESCStickInput.None }
			});

		static bool DashCheck(Vector2 input, Vector2 contact, EDashFlag dashFlag, ControlState button)
		{
			var ftrPerpToContact = Abs(Vector2.Dot(input, contact));
			if (ftrPerpToContact >= 0.2f)
				return false;

			if (dashFlag == EDashFlag.Shortcut && !button.IsActivated())
				return false;
			else if (dashFlag != EDashFlag.Shortcut && button != ControlState.Down)
				return false;

			return true;
		}

		static Dictionary<_Clause, byte> sDashClauses = new Dictionary<_Clause, byte>
		{
			{ Clauses.Grounded, ESCBool.True },
			{ Clauses.Stick, ESCStickInput.Full }
		};
		/// The startup for a forward ground dash
		public static _Single PreDash = new(
			nameof(PreDash),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.data.DashingFlag == EDashFlag.Shortcut)
					return false;
				if (c.data.DashingFlag == EDashFlag.Normal && c.input.framesSinceSnap >= 8)
					return false;
				return DashCheck(c.input.control, c.data.towardContact, c.data.DashingFlag, c.input.jump);
			},
			c =>
			{
				var l = c.data.towardContact.GetNormal();
				if (Vector2.Dot(l, c.input.control) < 0) l = -l;
				c.data.launchVector = l;
			},
			delegate (FighterContext c, ref _Output output)
			{
				output = c.data.stFramesSinceEntry < c.stats.PreGroundDashFrames
					? EStateOutput.Hold
					: EStateOutput.TriggerExit;
				c.data.DashingFlag = c.data.stFramesSinceEntry == c.stats.PreGroundDashFrames
					? EDashFlag.Auto
					: EDashFlag.Normal;
			},
			delegate(FighterContext c, in FighterFlags f) { c.data.DashingFlag = EDashFlag.Normal; },
			sDashClauses,
			FStateTraits.DashInterruptible
			);

		public static _Single GroundDash = new(
			nameof(GroundDash),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.data.DashingFlag == EDashFlag.Auto) return true;
				if (c.data.DashingFlag == EDashFlag.NoBack) return false;
				if (Clauses.Stick.Evaluate(c) != ESCStickInput.Full) return false;

				var contact = Abs(Vector2.Dot(c.input.control, c.data.towardContact));
				var back = Vector2.Dot(c.body.velocity, c.input.control);
				if (contact >= 0.2f || back >= 0)
					return false;

				if (c.data.DashingFlag == EDashFlag.Shortcut && !c.input.jump.IsActivated())
					return false;
				if (c.data.DashingFlag != EDashFlag.Shortcut && c.input.jump != ControlState.Down)
					return false;

				return true;
			},
			c =>
			{
				var vel = ApplyJump(c.data.launchVector, c.stats.GroundDashPower, c.body.velocity);
				c.body.velocity = vel;
				c.data.DashingFlag = EDashFlag.Shortcut;
				//c.data.MidDash = true;
			},
			delegate (FighterContext c, ref _Output output)
			{
				if (c.data.stFramesSinceEntry >= c.stats.GroundDashDuration)
					output = EStateOutput.TriggerExit;
				else
				{
					output = EStateOutput.Hold; // Want a conditional hold so it holds so long Grounded is trues
					output.requiredTransitionTraits = FStateTraits.DashInterruptible | FStateTraits.Airborne;
					//output.clause = Clauses.Button;
					//output.clauseCheckValue = ESCButton.None;
				}
			},
			delegate (FighterContext c, in FighterFlags f) { c.data.DashingFlag = EDashFlag.Normal; },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.True },
				//{ Clauses.Stick, ESCStickInput.Full }
			});

		// Length: The duration of the pivot, after which the ground dash happens
		// Pivot Frame: The frame the character turns around, is less than or equal to Length
		// If you release the stick prior to Length being reached, you will not dash
		// You are locked in the dash pivot until Pivot Frame has been reached at which point you
		// return to standing
		// This gives you a Perfect Pivot
		// A short Length gives you a faster Dash Dance
		// A high Pivot Frame gives you an easier Perfect Pivot

		public static _Single DashPivot = new(
			nameof(DashPivot),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.data.DashingFlag == EDashFlag.NoBack) return false;

				if (!DashCheck(c.input.control, c.data.towardContact, c.data.DashingFlag, c.input.jump))
					return false;

				var back = Vector2.Dot(c.data.ForwardContactVector, c.input.control);
				return back < 0;
			},
			c =>
			{
				var l = c.data.towardContact.GetNormal();
				if (Vector2.Dot(l, c.input.control) < 0) l = -l;
				c.data.launchVector = l;
				//c.data.MidDash = true;
				c.data.DashPivot_LeaveAfterPivot = false;
			},
			delegate (FighterContext c, ref _Output output)
			{
				if (c.data.stFramesSinceEntry == c.stats.GroundDashPivotLength)
				{
					output = EStateOutput.TriggerExit;
					c.data.DashingFlag = EDashFlag.Auto;
					return;
				}

				c.body.groundFrictionMultiplier *= c.stats.GroundDashPivotFrictionMult;
				var combined = c.input.control + c.data.launchVector;
				var hold = Vector2.Dot(combined, c.data.launchVector) > 1.6f;

				c.data.DashPivot_LeaveAfterPivot |= !hold;

				if (c.data.stFramesSinceEntry < c.stats.GroundDashPivotFrame)
				{

					output = EStateOutput.Hold;
					output.requiredTransitionTraits = FStateTraits.DashInterruptible;
					return;
				}

				if (c.data.stFramesSinceEntry == c.stats.GroundDashPivotFrame)
				{
					c.data.facing = c.data.facing == EFacing.Left ? EFacing.Right : EFacing.Left;
					if (c.data.DashPivot_LeaveAfterPivot)
					{
						output = EStateOutput.TriggerExit;
						return;
					}
				}

				output = hold ? EStateOutput.Hold : EStateOutput.TriggerExit;
			},
			delegate (FighterContext c, in FighterFlags f)
			{
				c.data.DashingFlag = EDashFlag.Normal;
				//c.data.MidDash = false;
			},
			sDashClauses,
			FStateTraits.DashInterruptible
			);

		public static _Single Turnaround = new(
			nameof(Turnaround),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				// I feel as though we'll only want this logic for grounded turnarounds
				var forward = c.data.upVector.GetNormal(c.data.facing == EFacing.Left);
				var dot = Vector2.Dot(forward, c.input.control);
				return dot < -0.3f;
			},
			c => { c.data.facing = c.data.facing == EFacing.Left ? EFacing.Right : EFacing.Left; },
			delegate { },
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None }
			},
			FStateTraits.Turnaround
			);

		public static _Single Skid = new(
			nameof(Skid),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (!c.input.stStick.IsActivated()) return false;
				var contactPerp = c.data.towardContact.GetNormal();
				if (Vector2.Dot(c.body.velocity, contactPerp) < 0) contactPerp = -contactPerp;
				var fctVelocity = Vector2.Dot(c.body.velocity, contactPerp);
				if (fctVelocity < 5.4f) return false;

				var fctControlPerpToNormal = Vector2.Dot(c.data.towardContact, c.input.control);
				var fctControlAgainstVel = Vector2.Dot(contactPerp, c.input.control);
				return Abs(fctControlPerpToNormal) < 0.2f && fctControlAgainstVel < 0;
			},
			c => { c.data.DashingFlag = EDashFlag.NoBack; },
			delegate (FighterContext c, ref _Output output)
			{
				c.body.groundFrictionMultiplier *= c.stats.SkidFrictionMult;
				// If our speed along the surface is below a threshold we escape the state
				// If we've released the stick but our speed is above the threshold we stay
				var perpToContact = c.data.towardContact.GetNormal();
				var shouldHoldState = Abs(Vector2.Dot(perpToContact, c.body.velocity)) > 3;
				if (!shouldHoldState) return;

				if (Vector2.Dot(c.body.velocity, perpToContact) < 0) perpToContact = -perpToContact;
				var releasedStick = Vector2.Dot(perpToContact, c.input.control) < 0.6f;
				if (releasedStick)
					output = EStateOutput.LocalHold;
			},
			delegate (FighterContext c, in FighterFlags f) { c.data.DashingFlag = EDashFlag.Normal; },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.True },
				{ Clauses.Button, ESCButton.None }
			});

		public static _Single Slide = new(
			nameof(Slide),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var vel = Abs(Vector2.Dot(c.data.towardContact.GetNormal(), c.body.velocity));
				var contact = Vector2.Dot(c.input.control, -c.data.towardContact);
				return vel > 9 && contact < -0.8f;
			},
			delegate { },
			delegate (FighterContext c, ref _Output output)
			{
				c.body.groundFrictionMultiplier *= c.stats.SlideFrictionMult;
				var vel = Abs(Vector2.Dot(c.data.towardContact.GetNormal(), c.body.velocity));
				var contact = Vector2.Dot(c.input.control, -c.data.towardContact);
				if (vel < 1 || contact > 0.2f)
					output = EStateOutput.TriggerExit;
				else
					output = EStateOutput.LocalHold;
			},
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Grounded, ESCBool.True },
				{ Clauses.Stick, ESCStickInput.Full }
			});

		static EClingFlag FindClingType(Fysics.Body body, Vector2 direction)
		{
			var tl = -Vector2.Dot(direction, body.TopLeft.normal);
			var tr = -Vector2.Dot(direction, body.TopRight.normal);
			return tl > 0 || tr > 0 ? EClingFlag.WallHug : EClingFlag.Crouch;
		}

		static void ClingExit(FighterContext c, in FighterFlags f)
		{
			if (f.NoClearCling) return;

			c.data.ClingFlag = EClingFlag.None;
			c.data.clingContacts.Clear();
			c.data.clingAngle = Vector2.zero;
		}

		public static _Single Cling = new(
			nameof(Cling),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var fctToContact = Vector2.Dot(c.input.control, c.data.towardContact);
				var success = c.data.ClingFlag != EClingFlag.None || fctToContact > 0.8f;
				flags.NoClearCling = success;
				return success;
			},
			delegate(FighterContext c)
			{
				c.data.ClingFlag = FindClingType(c.body, c.input.control);
				c.data.clingAngle = -c.data.towardContact;
				c.data.clingContacts.Clear();
				for (int i = 0; i < c.body.contacts.Length; i++)
					if (Vector2.Dot(c.body.contacts[i].normal, c.data.clingAngle) > 0.5f)
						c.data.clingContacts.Add(i);
			},
			delegate(FighterContext c, ref _Output output)
			{
				c.body.groundFrictionMultiplier *= c.stats.ClingFrictionMult;
			},
			ClingExit,
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.InContact, ESCBool.True }
			});

		public static _Single Scale = new(
			nameof(Scale),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.data.ClingFlag == EClingFlag.None) return false;
				var fctAwayFromContact = Vector2.Dot(c.input.control, c.data.towardContact);
				var fctPerpToContact = Abs(Vector2.Dot(c.input.control, c.data.ForwardContactVector));
				var success = fctAwayFromContact < 0.2f && fctPerpToContact > 0.7f;
				flags.NoClearCling = success;
				return success;
			},
			delegate(FighterContext c) { c.data.ClingFlag = FindClingType(c.body, c.data.towardContact); },
			delegate(FighterContext c, ref _Output output)
			{
				var fctAwayFromContact = Vector2.Dot(c.input.control, c.data.towardContact);
				if (fctAwayFromContact >= 0.2f)
				{
					output = EStateOutput.TriggerExit;
					return;
				}

				c.body.groundFrictionMultiplier *= c.stats.ClingFrictionMult;
				var forward = c.input.control;
				var control = Vector2.Dot(c.input.control, forward);
				var vel = Vector2.Dot(c.body.velocity, forward);
				if (vel < c.stats.ScaleSpeed)
				{
					Contact[] contacts = c.body.GetContactsInDirection(c.data.clingAngle);
					c.body.acceleration += forward * (c.stats.ScaleAcceleration + AidFromFriction(c.body, forward, contacts));
				}
			},
			ClingExit,
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.InContact, ESCBool.True }
			});

		public static _Single ClingTurnaround = new(
			nameof(ClingTurnaround),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var fctBack = Vector2.Dot(c.data.ForwardContactVector, c.input.control);
				var success = !(c.data.ClingFlag == EClingFlag.None || c.data.ClingFlag == EClingFlag.WallHug) && fctBack < -0.7f;
				flags.NoClearCling = success;
				return success;
			},
			delegate (FighterContext c) { c.data.ClingFlag = FindClingType(c.body, -c.data.towardContact); c.data.facing = c.data.facing == EFacing.Left ? EFacing.Right : EFacing.Left; },
			delegate { },
			ClingExit,
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Button, ESCButton.None },
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.InContact, ESCBool.True }
			},
			FStateTraits.Turnaround);

		// These are actually the pre jump frames
		public static _Single GroundJump = new(
			nameof(GroundJump),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				return !c.input.stStick.IsActivated()
					|| Vector2.Dot(c.data.upVector, c.input.control) > 0.2f;
			},
			c =>
			{
				c.data.launchVector =
					c.input.stStick.IsActivated() && c.input.framesSinceSnap < 8
					? c.input.control
					: -c.data.towardContact;
			},
			delegate(FighterContext c, ref _Output output)
			{
				if (c.input.stStick.IsActivated() && c.input.snap && Vector2.Dot(-c.data.towardContact, c.input.control) > 0)
					c.data.launchVector = c.input.control;

				if (c.data.stFramesSinceEntry < c.stats.GroundJumpSquatFrames) // Jump Squat Frames
				{
					output = EStateOutput.Hold;
					return;
				}

				// In same dir, should only speed up to a point
				// Parallel, should slow you down in current dir and speed up
				// Against, if the velocity's magnitude is the same as the jump power, it should mostly cut the velocity
				var newVel = ApplyJump(c.data.launchVector, c.stats.GroundJumpPower, c.body.velocity);
				//Debug.Log($"Old {c.body.velocity}. Launch: {c.data.launchVector} New: {newVel}");
				c.body.velocity = newVel;
				output = EStateOutput.TriggerExit;
			},
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.True },
				{ Clauses.Button, ESCButton.Jump }
			},
			FStateTraits.DashInterruptible
			);

		public static _Single AirJump = new(
			nameof(AirJump),
			delegate { return true; },
			c =>
			{
				//Debug.Log(c.input.jump);
				c.data.launchVector =
					c.input.stStick.IsActivated() && c.input.framesSinceSnap < 8
					? c.input.control
					: Vector2.up; // This should be against the direction of gravity
			},
			delegate (FighterContext c, ref _Output output)
			{
				if (c.input.stStick.IsActivated() && c.input.snap)
					c.data.launchVector = c.input.control;


				if (c.data.stFramesSinceEntry < c.stats.AirJumpSquatFrames) // Jump Squat Frames
				{
					output = EStateOutput.Hold;
					return;
				}

				// In same dir, should only speed up to a point
				// Parallel, should slow you down in current dir and speed up
				// Against, if the velocity's magnitude is the same as the jump power, it should mostly cut the velocity
				var newVel = ApplyJump(c.data.launchVector, c.stats.AirJumpPower, c.body.velocity);
				//Debug.Log($"Old {c.body.velocity}. Launch: {c.data.launchVector} New: {newVel}");
				c.body.velocity = newVel;
				output = EStateOutput.TriggerExit;
				Debug.Log("hh");
			},
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.False },
				{ Clauses.Button, ESCButton.Jump }
			},
			FStateTraits.DashInterruptible | FStateTraits.Airborne
			);

		public static _Single LedgeGrab = new(
			nameof(LedgeGrab),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				foreach (var body in c.data.grabbableBodies)
				{
					for (int i = 0; i < Fysics.Body.kVerts; i++)
					{
						var j = (i + 1) % Fysics.Body.kVerts;
						var line1 = body.FlexBox[i] + body.Position;
						var line2 = body.FlexBox[j] + body.Position;
						var normal = (line1 - line2).GetNormal().normalized;

						var closestPoint = Fysics.FysicsManager.ClosestPointOnLine(c.body.Position + c.stats.GrabOffset, line1, line2);
						var toLine = closestPoint - (c.body.Position + c.stats.GrabOffset);

						var dist = FysicsManager.DistanceToLine(c.body.Position + c.stats.GrabOffset, line1, line2);
						if (dist > c.stats.GrabRadius)
							continue;

						var dot = Vector2.Dot(toLine, normal);
						if (dot > 0)
							return true;
					}
				}

				return false;
			},
			c =>
			{
				c.data.grabEdge = -1;

				var smallestDot = float.MaxValue;
				Body grabbedBody = null;
				int edge = -1;
				float pointAlongEdge = 0;
				var normals = new List<Vector2>();

				foreach (var body in c.data.grabbableBodies)
				{
					var myNormals = new List<Vector2>();
					for (int i = 0; i < Body.kVerts; i++)
					{
						var j = (i + 1) % Fysics.Body.kVerts;
						var line1 = body.FlexBox[i] + body.Position;
						var line2 = body.FlexBox[j] + body.Position;

						var closestPoint = FysicsManager.ClosestPointOnLine(c.body.Position + c.stats.GrabOffset, line1, line2);
						var dist = FysicsManager.DistanceToLine(c.body.Position + c.stats.GrabOffset, line1, line2);
						if (dist > c.stats.GrabRadius)
							continue;

						var normal = (line1 - line2).GetNormal().normalized;
						myNormals.Add(normal);

						var toLine = closestPoint - (c.body.Position + c.stats.GrabOffset);
						var dot = Vector2.Dot(toLine, normal);
						if (dot > 0 && dot < smallestDot)
						{
							edge = i;
							grabbedBody = body;
							pointAlongEdge = Vector2.Distance(line1, closestPoint) / Vector2.Distance(line1, line2);
							smallestDot = dot;
						}
					}

					if (grabbedBody == body)
						normals = myNormals;
				}

				c.data.grabBody = grabbedBody;
				c.data.grabEdge = edge;
				c.data.grabPointAlongEdge = pointAlongEdge;

				//Debug.Log(normals.Select(v => v.ToString()).Aggregate((f, t) => f + " " + t));
				bool opposing = false;
				for (var i = 0; i < normals.Count - 1; i++)
				{
					for (var j = i + 1; j < normals.Count; j++)
					{
						var dot = Vector2.Dot(normals[i], normals[j]);
						opposing |= Approximately(dot, -1);
					}

					if (opposing)
						break;
				}

				if (!opposing)
					c.data.getupJumpVectors = normals;
			},
			delegate(FighterContext c, ref StateOutput<FighterContext> output)
			{

			},
			delegate(FighterContext c, in FighterFlags f)
			{
				if (!f.NoClearLedgeGrab)
				{
					c.data.grabEdge = -1;
					c.data.grabBody = null;
				}
			},
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Grounded, ESCBool.False },
				{ Clauses.Button, ESCButton.Grab }
			},
			FStateTraits.Airborne
			);

		public static _Single LedgeHang = new(
			nameof(LedgeHang),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var success = c.data.grabEdge != -1 && c.data.grabBody;
				flags.NoClearLedgeGrab = success;
				return success;
			},
			c => { },
			delegate(FighterContext c, ref StateOutput<FighterContext> output)
			{
				if (c.input.grab == ControlState.Down)
				{
					// Reset should pick from a set of starter state based on context
					output = EStateOutput.Reset;
					return;
				}

				output = EStateOutput.Hold;
				output.requiredTransitionTraits = FStateTraits.LedgeAction;

				var line1 = c.data.grabBody.FlexBox[c.data.grabEdge];
				var line2 = c.data.grabBody.FlexBox[(c.data.grabEdge + 1) % Fysics.Body.kVerts];
				var grabAxleLocal = (line2 - line1) * c.data.grabPointAlongEdge + line1;
				var grabAxleCentre = grabAxleLocal + c.data.grabBody.Position;

				var stableRange = c.stats.GrabRadius;
				var extendedRange = stableRange + c.stats.ExtendedGrabRange;
				var grabCentre = c.body.Position + c.stats.GrabOffset;
				var toGrabPoint = grabAxleCentre - grabCentre;

				if (toGrabPoint.sqrMagnitude < (stableRange * stableRange))
				{
					if (Vector2.Dot(c.body.velocity, toGrabPoint) > 0)
					{
						// We're heading toward the grabPoint so worry about clipping through
						const float kGrabAxleRadius = 0.1f;
						var distToAxle = FysicsManager.DistanceToLine(grabAxleCentre, grabCentre, grabCentre + c.body.velocity.normalized * (c.stats.GrabRadius * 1.2f));
						if (distToAxle < kGrabAxleRadius)
						{ // Zero out movement
							// If your velocity in this direction exceeds the threshold
							// You will forcibly release and have some of the velocity taken
							// This value will be character dependent
							const float kAxleCatchSpeedMax = 10f;
							var speed = c.body.velocity.sqrMagnitude;
							if (speed > (kAxleCatchSpeedMax * kAxleCatchSpeedMax))
							{
								speed = Sqrt(speed) - kAxleCatchSpeedMax;
								speed *= 0.9f; // impact absorption
								// Rather than just going straight to airborne this should
								// put in some sort of tumble state
								output = EStateOutput.TriggerExit;
							}
							else
							{
								speed = 0;
							}

							c.body.velocity = c.body.velocity.normalized * speed;
						}
					}
					else
					{
						// We're heading away from it so worry about leaving the grab sphere
						// Deal with edge riding
						var motion = c.body.velocity * Time.deltaTime;
						var projectedGrabCentre = grabCentre + motion;
						if ((projectedGrabCentre - grabAxleCentre).sqrMagnitude > (stableRange * stableRange))
						{
							var v = grabCentre + motion - grabAxleCentre;
							var snapOnEdge = grabAxleCentre + v.normalized * c.stats.GrabRadius;
							motion = snapOnEdge - grabCentre;

							var edgeNormal = (grabAxleCentre - snapOnEdge).normalized;
							var edgeNormalPerp = edgeNormal.GetNormal();
							var perpendicularSpeed = Vector2.Dot(c.body.velocity, edgeNormalPerp);
							// Would want this value to be a combination of a baseline value and a
							// relationship between the perpendicular and parallel speed
							perpendicularSpeed *= 0.97f;
							var perpVelocity = edgeNormalPerp * perpendicularSpeed;
							c.body.velocity = perpVelocity;
						}
					}
				}
				else if (toGrabPoint.sqrMagnitude < (extendedRange * extendedRange))
				{
					var dir = toGrabPoint.normalized;
					c.body.acceleration += dir * 40;
				}
				else
				{
					// Release
					output = EStateOutput.Reset;
				}

				//if (c.input.light == ControlState.Down)
				//{
				//	c.body.velocity += c.input.control * 2f;
				//}
			},
			delegate(FighterContext c, in FighterFlags f)
			{
				if (!f.NoClearLedgeGrab)
				{
					c.data.grabEdge = -1;
					c.data.grabBody = null;
				}
			},
			new Dictionary<_Clause, byte>
			{
			});

		// In the future, the ledge hang animation will have the model origin
		// be at the hang point so there will be no need to do this lerping
		public static _Single LedgeGetup = new(
			nameof(LedgeGetup),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (! (c.data.grabEdge != -1 && c.data.grabBody))
					return false;

				//var j = (c.data.grabEdge + 1) % Fysics.Body.kVerts;
				//var line1 = c.data.grabBody.FlexBox[c.data.grabEdge];
				//var line2 = c.data.grabBody.FlexBox[j];
				//var normal = (line1 - line2).GetNormal().normalized;

				//// NOTE Set this up so that if it returns false it clears the flags
				//var success = Vector2.Dot(normal, c.input.control) > 0.8f;
				flags.NoClearLedgeGrab = true;
				return true;
			},
			c =>
			{
				var line1 = c.data.grabBody.FlexBox[c.data.grabEdge];
				var line2 = c.data.grabBody.FlexBox[(c.data.grabEdge + 1) % Fysics.Body.kVerts];
				var grabPoint = (line2 - line1) * c.data.grabPointAlongEdge + line1 + c.data.grabBody.Position;
				c.data.getupStartOffset = grabPoint - c.body.FeetPos;
			},
			delegate (FighterContext c, ref _Output output)
			{
				if (c.data.stFramesSinceEntry >= c.stats.LedgeGetupFrames)
				{
					output = EStateOutput.TriggerExit;
					return;
				}

				// This setup doesn't account for the platform changing its position/orientation
				output = EStateOutput.Hold;
				c.body.environmentAcceleration = Vector2.zero;
				c.manager.AddGhostContact(c.body, c.data.grabBody);

				var to = -Vector2.Lerp(c.data.getupStartOffset, Vector2.zero, c.data.stFramesSinceEntry / (float)c.stats.LedgeGetupFrames);
				var from = -Vector2.Lerp(c.data.getupStartOffset, Vector2.zero, (c.data.stFramesSinceEntry - 1) / (float)c.stats.LedgeGetupFrames);

				c.body.velocity = (to - from) / Time.deltaTime;
			},
			delegate (FighterContext c, in FighterFlags f)
			{
				if (!f.NoClearLedgeGrab)
				{
					c.data.grabEdge = -1;
					c.data.grabBody = null;
				}
			},
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Stick, ESCStickInput.None },
				{ Clauses.Button, ESCButton.Jump }
			},
			FStateTraits.LedgeAction
			);

		public static _Single LedgeJump = new(
			nameof(LedgeJump),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				if (c.data.grabEdge == -1 || !c.data.grabBody)
					return false;

				// Check the input points to a viable jump direction
				foreach (var vec in c.data.getupJumpVectors)
				{
					if (Vector2.Dot(vec, c.input.control) < 0)
					{
						return false;
					}
				}
				flags.NoClearLedgeGrab = true;

				return true;
			},
			c =>
			{
				c.data.launchVector = c.input.control;
			},
			delegate(FighterContext c, ref _Output output)
			{
				output = c.data.stFramesSinceEntry < c.stats.LedgeJumpFrames
					? EStateOutput.Hold
					: EStateOutput.TriggerExit;

				c.manager.AddGhostContact(c.body, c.data.grabBody);
				if (c.data.stFramesSinceEntry == c.stats.PreLedgeJumpFrames)
				{
					c.body.velocity = c.stats.LedgeJumpPower * c.data.launchVector;
					c.data.grabEdge = -1;
					c.data.grabBody = null;
				}
			},
			delegate(FighterContext c, in FighterFlags flags) {  },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.Button, ESCButton.Jump }
			},
			FStateTraits.LedgeAction
			);

		public static _Single Dive = new(
			nameof(Dive),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				foreach (var contact in c.body.contacts)
				{
					var d = Vector2.Dot(contact.normal, c.input.control);
					if (d < -0.8f)
						return true;
				}
				return false;
			},
			delegate (FighterContext c)
			{
				c.data.diveGrab = false;
				c.body.DiveIntention = true;

				var allContacts = c.body.contacts.SelectMany(contact => contact.bodies.Keys).Distinct().ToArray();
				bool anyGhosts = false;
				foreach (var contact in allContacts)
				{
					var closestPoint = contact.GetClosestPointOnSurface(c.body.Position);
					var direction = (closestPoint - c.body.Position).normalized;
					if (Vector2.Dot(direction, c.input.control) <= 0.8f)
						continue;

					c.manager.AddGhostContact(c.body, contact);
					anyGhosts = true;
				}

				if (!anyGhosts)
					return;

				const float kRadius = 1;
				var overlaps = FysicsManager.CircleOverlap(c.data.transform.position, kRadius, allContacts);
				foreach (var body in overlaps)
				{
					for (int i = 0; i < Fysics.Body.kVerts; i++)
					{
						var j = (i + 1) % Fysics.Body.kVerts;
						var line1 = body.FlexBox[i] + body.Position;
						var line2 = body.FlexBox[j] + body.Position;
						var normal = (line1 - line2).GetNormal().normalized;

						var dist = Fysics.FysicsManager.DistanceToLine(c.data.transform.position, line1, line2);
						if (dist > kRadius)
							continue;

						var dot = Vector2.Dot(normal, c.input.control);
						if (dot > 0)
						{
							c.data.diveGrab = true;
							c.data.diveThrustDir = c.input.control;
						}
					}
				}
			},
			delegate (FighterContext c, ref _Output output)
			{
				const int kDuration = 6;
				if (!c.data.diveGrab)
					return;

				if (c.input.grab.IsActivated())
					output.flag = EStateOutput.Hold;

				if (kDuration == c.data.stFramesSinceEntry)
				{
					c.body.velocity += c.data.diveThrustDir * c.stats.DiveThrustPower;
					output.flag = EStateOutput.None;
				}
			},
			delegate (FighterContext x, in FighterFlags f)
			{

			},
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.Button, ESCButton.Grab }
			}
			);

		public static _Single PreemptiveDive = new(
			nameof(PreemptiveDive),
			delegate (FighterContext c, ref FighterFlags flags)
			{
				var box = new Vector2[4];
				for (int i = 0; i < 4; i++)
					box[i] = c.body.Position + c.input.control * c.stats.DiveLookAheadDist + c.body.FlexBox[i];

				var skippables = c.body.contacts.SelectMany(contact => contact.bodies.Keys).Distinct().ToList();
				skippables.Add(c.body);
				var overlaps = c.manager.BoxOverlaps(box, skippables.ToArray());
				return overlaps.Count > 0;
			},
			delegate(FighterContext c)
			{
				var box = new Vector2[4];
				for (int i = 0; i < 4; i++)
					box[i] = c.body.Position + c.input.control * c.stats.DiveLookAheadDist + c.body.FlexBox[i];

				var skippables = c.body.contacts.SelectMany(contact => contact.bodies.Keys).Distinct().ToList();
				skippables.Add(c.body);
				var overlaps = c.manager.BoxOverlaps(box, skippables.ToArray());
				c.body.PreemptiveDive = overlaps[0];
			},
			delegate { },
			delegate { },
			new Dictionary<_Clause, byte>
			{
				{ Clauses.Stick, ESCStickInput.Full },
				{ Clauses.InContact, ESCBool.True },
				{ Clauses.Button, ESCButton.Grab }
			}
			);

		static bool sInitialised = false;
		public static void Init()
		{
			if (sInitialised) return;
			sInitialised = true;

			//sLkPreDash[0] = DashForward;
			//sLkDash[0] = DashTurnaround;
			//chainDash[0] = PreDash;
			//chainDash[1] = GroundDash;

			//chainDashTurnaround[0] = DashPivot;
			//chainDashTurnaround[1] = GroundDash;

			//lksDash[0] = DashTurnaround;
		}

		// It would probably be better to do this with bezier curves
		static float GroundLocomotionAcceleration(float velocity)
		{
			float baseLine = 7.0f;
			const float velCutoff = 17.0f;
			if (velocity >= velCutoff)
				velocity = velCutoff;
			return 2.0f * Sin(velocity / 2.4f - 2.0f) +
				   Max(0.4f * Cos(velocity / 2.4f - 0.4f), 0.0f) -
				   velocity / 4.0f + baseLine;
		}

		// The Y vector is treated as the velocity in the jump direction
		// The X vector is treated as the velocity perpendicular to the jump direction
		// If Y velocity exceeds ~1.4x the jump power no force will be applied
		// If Y velocity is negative we apply 
		// A high X velocity will eat into the applied jump force
		// The X velocity will be decreased

		// Need to take into account frictional forces, e.g. if the fricition is too low
		// in the y direction reduce the jump power
		// If too low in the x, reduce the cut to x velocity
		public static Vector2 ApplyJump(Vector2 launchDir, float jumpPower, Vector2 velocity)
		{
			var launchY = launchDir;
			var launchX = launchDir.GetNormal();

			var velocityY = Vector2.Dot(velocity, launchY);
			var velocityX = Vector2.Dot(velocity, launchX);
			var factY = velocityY / jumpPower;
			var factX = velocityX / jumpPower;

			if (factY >= 0)
			{
				factY = -0.5f * factY * factY + 1;
				factY = Max(0, factY);
			}
			else
			{
				var f = Max(-1, factY);
				factY = -0.2f * f * f + 1;
			}

			var x = Min(1.5f, Abs(factX));
			var yMult = 0.32f * Cos(2 * x) + 0.69f;
			factY *= Max(yMult, 0.4f);

			x = Max(0.3f, Abs(factX)) - 0.3f;
			var xMult = Min(x * x, 0.95f);
			//Debug.Log($"{velocityX * launchX} {velocityY * launchY} {factX} {xMult}");
			factX *= xMult;

			var newY = velocityY + (factY * jumpPower);
			var newX = factX * jumpPower;
			return launchY * newY + launchX * newX;
		}

		public static float AidFromFriction(Fysics.Body body, Vector2 direction, params Fysics.Contact[] contacts)
		{
			// Overcome/ignore the resistance friction gives
			// Issue is this will have the same effect on all surfaces
			// The effect should be less effective on slippery surfaces and plateau at a point
			// Unless the fighter can Skate
			Vector2 friction = Vector2.zero;
			foreach (var contact in contacts)
				friction += contact.friction;
			return Abs(Vector2.Dot(friction, direction));
		}
	}
}
