﻿using UnityEngine;

namespace Actor
{
	public class FighterStats : MonoBehaviour
	{
		[Min(1)]
		public float RunSpeed = 1;

		[Range(0.1f, 2.0f)]
		public float RunOutFrictionMult = 0.4f;
		[Min(1)] public float RunOutSpeedMin = 3;

		[Min(1)] public int PreGroundDashFrames = 3;
		[Min(1)] public float GroundDashPower = 14;
		[Min(1)] public int GroundDashDuration = 38;
		[Min(1)] public int GroundDashPivotLength = 6;
		[Min(1)] public int GroundDashPivotFrame = 4;
		[Min(10)] public float GroundDashPivotFrictionMult = 30;

		[Min(1)] public float SkidFrictionMult = 2;

		[Min(1)] public float SlideFrictionMult = 1.6f;

		[Min(5)] public float ClingFrictionMult = 20;
		[Min(1)] public float ScaleSpeed = 3;
		[Min(30)] public float ScaleAcceleration = 65;

		[Min(1)] public int GroundJumpSquatFrames = 6;
		[Min(1)] public int AirJumpSquatFrames = 3;
		[Min(1)] public float GroundJumpPower = 14;
		[Min(1)] public float AirJumpPower = 11;

		public Vector2 GrabOffset = Vector2.up;
		[Min(0.5f)] public float GrabRadius = 1;
		[Min(0.1f)] public float ExtendedGrabRange = 0.6f;

		[Min(10)] public int LedgeGetupFrames = 32;
		[Min(1)] public int PreLedgeJumpFrames = 5;
		[Min(5)] public int LedgeJumpFrames = 22;
		[Min(1)] public float LedgeJumpPower = 8;

		[Min(5)] public float DiveThrustPower = 10;
		[Min(0)] public float DiveLookAheadDist = 0.6f;
	}
}