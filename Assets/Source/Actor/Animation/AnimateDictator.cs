using Actor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using State = StateSystem.IState<Actor.FighterContext, Actor.FighterFlags>;

public class AnimateDictator : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] Fighter _fighter;

    [SerializeField] float _runMin = 2, _runMax = 11;

    readonly int _prm_JumpAngle = Animator.StringToHash("jump_angle");
    readonly int _prm_LedgeGrabAngle = Animator.StringToHash("ledge_grab_angle");
    readonly int _prm_RunProgress = Animator.StringToHash("bl_run");

    readonly int _anm_Stand = Animator.StringToHash("stand");
    readonly int _anm_Aerial = Animator.StringToHash("aerial");
    readonly int _anm_Crouch = Animator.StringToHash("crouch");

    readonly int _anmbl_Run = Animator.StringToHash("bl-run");
    readonly int _anm_RunOut = Animator.StringToHash("run_out");
    readonly int _anm_Skid = Animator.StringToHash("skid");

    readonly int _anm_Dash = Animator.StringToHash("ground_dash");

    readonly int _anmbl_JumpLaunch = Animator.StringToHash("bl-jump_launch");

    Dictionary<State, int> _stateAnimationMap;
    bool _running = false;

    Transform _model;
    float _faceRightEulerY;

    private void Start()
    {
        _fighter.Machine.OnStateChanged += OnStateChanged;
        _model = _animator.transform;
        _faceRightEulerY = _model.localEulerAngles.y;

        _stateAnimationMap = new Dictionary<State, int>
        {
            { States.Stand, _anm_Stand },
            { States.Airborne, _anm_Aerial },
            { States.Cling, _anm_Crouch },
            { States.Run, _anmbl_Run },
            { States.RunOut, _anm_RunOut },
            { States.GroundJump, _anmbl_JumpLaunch },
            { States.AirJump, _anmbl_JumpLaunch },
            { States.Skid, _anm_Skid },
            { States.PreDash, _anm_Dash },
            { States.GroundDash, _anm_Dash },
            { States.DashPivot, _anm_Dash }
        };
    }

    private void Update()
    {
        _model.localEulerAngles = _fighter.Context.data.facing switch
        {
            EFacing.Left => Vector3.up * -_faceRightEulerY,
            _ => Vector3.up * _faceRightEulerY
        };

        {
            var vel = _fighter.Context.body.velocity.magnitude;
            var f = Mathf.Clamp(vel, _runMin, _runMax) - _runMin;
            f /= _runMax - _runMin;
            _animator.SetFloat(_prm_RunProgress, f);
        }

        {
            Vector2 launchVec = _fighter.Context.data.launchVector;
            Vector2 up = _fighter.Context.data.upVector;
            var f = Vector2.Dot(launchVec, up);
            // Something to flip the model if y < 0
            _animator.SetFloat(_prm_JumpAngle, f);
        }
    }

    void OnStateChanged()
    {
        _running = false;
        var current = _fighter.Machine.Current.ActiveState(_fighter.Context);
        if (_stateAnimationMap.ContainsKey(current))
        {
            _animator.Play(_stateAnimationMap[current], 0, 0);
        }
    }
}
