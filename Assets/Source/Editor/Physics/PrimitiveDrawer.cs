using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Fysics
{
	[CustomPropertyDrawer(typeof(Primitive))]
	public class PrimitiveDrawer : PropertyDrawer
	{
		const float kFieldHeight = 16.0f;
		const float kFoldoutHeaderHeight = 18.0f;
		const float kToolBarHeight = 20.0f;

		const float kPadding = 4f;

		const float kFieldStep = kFieldHeight + kPadding;
		const float kFoldoutStep = kFoldoutHeaderHeight + kPadding;

		const float kAfterBoxBuffer = 4f;

		public bool showProperties = true;
		public bool showFrictionDefinition = true;

		int _boxTab = 0;

		float Scale(float x, float min, float max) => (x - min) / (max - min);

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			var prePrefix = position;
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
			position.x = Mathf.Lerp(prePrefix.x, position.x, 0.2f);
			position.width = Mathf.Lerp(prePrefix.width, position.width, 0.2f);
			var indent = EditorGUI.indentLevel;

			var p_fullbox = property.FindPropertyRelative("FullBox");
			var p_rigidbox = property.FindPropertyRelative("RigidBox");

			{
				// Drawing Box
				var size = GetRenderedBoxSize(position.width);
				var rBox = new Rect(position.x, position.y, position.width, size);
				var offset = new Vector3(
					position.x + position.width / 2 - size, 0); 

				Vector2 fullBoxMin = Vector2.one * float.MaxValue,
				        fullBoxMax = Vector2.one * float.MinValue;
				for (var i = 0; i < Primitive.kVerts; i++)
				{
					var pos = p_fullbox.GetArrayElementAtIndex(i).vector2Value;
					if (pos.x < fullBoxMin.x) fullBoxMin.x = pos.x;
					if (pos.y < fullBoxMin.y) fullBoxMin.y = pos.y;
					if (pos.x > fullBoxMax.x) fullBoxMax.x = pos.x;
					if (pos.y > fullBoxMax.y) fullBoxMax.y = pos.y;
				}

				var fullBox = new List<Vector3>();
				var rigidBox = new List<Vector3>();
				for (var i = 0; i < Primitive.kVerts; i++)
				{
					var fullPos = p_fullbox.GetArrayElementAtIndex(i).vector2Value;
					var rigidPos = p_rigidbox.GetArrayElementAtIndex(i).vector2Value;

					var f = new Vector3(
						Scale(fullPos.x, fullBoxMin.x, fullBoxMax.x), Scale(fullPos.y, fullBoxMax.y, fullBoxMin.y)
						);
					var r = new Vector3(
						Scale(rigidPos.x, fullBoxMin.x, fullBoxMax.x), Scale(rigidPos.y, fullBoxMax.y, fullBoxMin.y)
						);

					fullBox.Add(f * size + offset);
					rigidBox.Add(r * size + offset);
				}

				fullBox.Add(fullBox[0]);
				rigidBox.Add(rigidBox[0]);

				GUI.BeginClip(rBox);
				Handles.color = Color.white;
				Handles.DrawAAPolyLine(fullBox.ToArray());
				Handles.color = Color.green;
				Handles.DrawAAPolyLine(rigidBox.ToArray());
				GUI.EndClip();

				position.y += size + kPadding;
				position.height -= size + kPadding;
			}

			{
				var rToolBar = position;
				rToolBar.height = kToolBarHeight;
				_boxTab = GUI.Toolbar(rToolBar, _boxTab, new[] { "Full", "Rigid" });

				position.y += kToolBarHeight + kPadding;
				position.height -= kToolBarHeight + kPadding;

				var fieldWidth = (position.width - kPadding) / 2f;
				var TL = new Rect(position.x, position.y, fieldWidth, kFieldHeight);
				var TR = new Rect(position.x + fieldWidth + kPadding, position.y, fieldWidth, kFieldHeight);

				var BL = new Rect(position.x, position.y + kFieldHeight + kPadding, fieldWidth, kFieldHeight);
				var BR = new Rect(position.x + fieldWidth + kPadding, position.y + kFieldHeight + kPadding, fieldWidth, kFieldHeight);

				SerializedProperty p_box;
				switch (_boxTab)
				{
					case 1:
						p_box = p_rigidbox;
						break;
					default: // 0
						p_box = p_fullbox;
						break;
				}

				var alRight = new GUIStyle(GUI.skin.label);
				alRight.alignment = TextAnchor.MiddleRight;

				EditorGUI.LabelField(TL, p_box.GetArrayElementAtIndex(0).vector2Value.ToString());
				EditorGUI.LabelField(TR, p_box.GetArrayElementAtIndex(1).vector2Value.ToString(), alRight);
				EditorGUI.LabelField(BR, p_box.GetArrayElementAtIndex(2).vector2Value.ToString(), alRight);
				EditorGUI.LabelField(BL, p_box.GetArrayElementAtIndex(3).vector2Value.ToString());

				position.y += kFieldStep * 2;
				position.height -= kFieldStep * 2;
			}

			position.y += kAfterBoxBuffer;
			position.height -= kAfterBoxBuffer;

			var header = new GUIStyle(EditorStyles.foldout);
			header.fontStyle = FontStyle.Bold;
			header.fontSize = (int)(header.fontSize * 1.05f);

			{
				// Properties
				var rect = position;
				rect.height = kFoldoutHeaderHeight;
				showProperties = EditorGUI.Foldout(rect, showProperties, "Properties", header);

				rect.y += kFoldoutStep;
				position.y += kFoldoutStep;
				position.height -= kFoldoutStep;

				if (showProperties)
				{

					var props = new[]
					{
						property.FindPropertyRelative("_restorationFactor"),
						property.FindPropertyRelative("_rigidity"),
						property.FindPropertyRelative("_shockAbsorption")
					};

					foreach (var prop in props)
					{
						EditorGUI.PropertyField(rect, prop);
						rect.y += kFieldStep;
						position.y += kFieldStep;
						position.height -= kFieldStep;
					}
				}
			}

			{
				// Friction Definition
				var rect = position;
				rect.height = kFoldoutHeaderHeight;
				showFrictionDefinition =
					EditorGUI.Foldout(rect, showFrictionDefinition, "Friction Definition", header);

				rect.y += kFoldoutStep;
				position.y += kFoldoutStep;
				position.height -= kFoldoutStep;

				var p_frictionDef = property.FindPropertyRelative("FrictionDef");

				if (showFrictionDefinition)
				{
					rect.height = kFieldHeight;
					for (var i = 0; i < p_frictionDef.arraySize; i++)
					{
						var p_friction = p_frictionDef.GetArrayElementAtIndex(i);
						var rDist = position;
						rDist.width = (rDist.width - kPadding) / 2;
						var rFrict = rDist;
						rFrict.x += rFrict.width + kPadding;

						EditorGUI.LabelField(rDist, "Distance",
											 p_friction.FindPropertyRelative("Distance")
											   .floatValue.ToString());
						EditorGUI.LabelField(rFrict, "Friction",
											 p_friction.FindPropertyRelative("Friction")
											   .floatValue.ToString());

						rect.y += kFieldStep;
						position.y += kFieldStep;
						position.height -= kFieldStep;
					}
				}
			}

			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float kWidth = EditorGUIUtility.currentViewWidth - 10f;

			// Primitive Box
			float height = GetRenderedBoxSize(kWidth) + kPadding;

			// Box Data
			height += kToolBarHeight + kPadding;
			height += kFieldStep * 2;

			height += kAfterBoxBuffer;

			// Properties
			height += kFoldoutStep;
			if (showProperties)
				height += kFieldStep * 3;

			// Friction Definition
			height += kFoldoutStep;
			if (showFrictionDefinition)
			{
				var length = property.FindPropertyRelative("FrictionDef").arraySize;
				height += length * kFieldStep;
			}

			return height;
		}

		float GetRenderedBoxSize(float width) => Mathf.Clamp(width, 30f, 140f);
	}
}
