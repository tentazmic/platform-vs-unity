﻿using UnityEditor;
using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UIElements;

namespace Fysics
{
	[CustomEditor(typeof(Body))]
	public class BodyEditor : Editor
	{
		const float kHandleSize = 0.2f;

		const KeyCode kKeyAddFrictionHandle = KeyCode.A;
		const KeyCode kKeyAddLinkedPrimitive = KeyCode.S;
		const KeyCode kKeyScalePrimitive = KeyCode.D;

		bool _showSpeeds = false;
		bool _showPrimitives = false;

		SerializedProperty p_velocity, p_acceleration, p_environmentAcceleration;

		SerializedProperty p_origin;
		SerializedProperty p_rotation;
		SerializedProperty p_mass;
		SerializedProperty p_groundFriction;
		SerializedProperty p_airFriction;

		SerializedProperty p_primitives;

		public struct PrimitiveData
		{
			public PrimitiveDrawer Drawer;
			public bool Foldout;
			public bool Scaling;
			public Vector2 ScaleStart;
			public Quaternion Rotation;
		}

		List<PrimitiveData> drawers = null;

		private void OnEnable()
		{
			p_velocity = serializedObject.FindProperty("velocity");
			p_acceleration = serializedObject.FindProperty("acceleration");
			p_environmentAcceleration = serializedObject.FindProperty("environmentAcceleration");

			p_origin = serializedObject.FindProperty("_origin");
			p_rotation = serializedObject.FindProperty("rotation");
			p_mass = serializedObject.FindProperty("mass");
			p_groundFriction = serializedObject.FindProperty("groundFriction");
			p_airFriction = serializedObject.FindProperty("airFriction");

			p_primitives = serializedObject.FindProperty("primitives");

			drawers = new List<PrimitiveData>();
			for (var i = 0; i < p_primitives.arraySize; i++)
				drawers.Add(new PrimitiveData{
					Drawer = new PrimitiveDrawer(),
					Foldout = false,
					Scaling = false,
					ScaleStart = Vector2.zero,
					Rotation = Quaternion.identity
				});
		}

		public override void OnInspectorGUI()
		{
			var header = new GUIStyle(GUI.skin.label);
			header.fontStyle = FontStyle.Bold;
			header.fontSize = (int)(header.fontSize * 1.1f);

			serializedObject.Update();

			if (Application.isPlaying)
			{
				_showSpeeds = EditorGUILayout.BeginFoldoutHeaderGroup(_showSpeeds, new GUIContent("Speeds"));
				if (_showSpeeds)
				{
					EditorGUILayout.LabelField("Velocity", p_velocity.vector2Value.ToString());
					EditorGUILayout.LabelField("Acceleration", p_acceleration.vector2Value.ToString());
					EditorGUILayout.LabelField("Environment Acceleration", p_environmentAcceleration.vector2Value.ToString());
				}
			}

			EditorGUILayout.LabelField("Positional", header);
			EditorGUILayout.PropertyField(p_origin, new GUIContent("Origin"));
			EditorGUILayout.Slider(p_rotation, 0, 360, new GUIContent("Rotation"));

			EditorGUILayout.Space();

			EditorGUILayout.LabelField("Properties", header);

			if (p_mass.intValue > Body.kInfiniteMassThreshold)
			{
				EditorGUILayout.BeginHorizontal(); {
					EditorGUILayout.LabelField("Mass");
					if (GUILayout.Button("Disable Infinite Mass"))
						p_mass.intValue = (int)(Body.kInfiniteMassThreshold / 2);
				} EditorGUILayout.EndHorizontal();
			}
			else
			{
				EditorGUILayout.BeginHorizontal(); {

					p_mass.intValue = EditorGUILayout.IntField(new GUIContent("Mass"), p_mass.intValue);
					if (p_mass.intValue < 1) p_mass.intValue = 1;
					if (GUILayout.Button("Inf") || p_mass.intValue > Body.kInfiniteMassThreshold)
						p_mass.intValue = (int)Body.kInfiniteMassThreshold + 1;

				} EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.PropertyField(p_groundFriction, new GUIContent("Ground Friction"));
			EditorGUILayout.PropertyField(p_airFriction, new GUIContent("Drag Coefficient"));

			_showPrimitives = EditorGUILayout.BeginFoldoutHeaderGroup(_showPrimitives, "Primitives");
			if (_showPrimitives)
			{
				EditorGUI.indentLevel++;
				for (var i = 0; i < p_primitives.arraySize; i++)
				{
					var p = p_primitives.GetArrayElementAtIndex(i);

					var label = new GUIContent(i.ToString());
					var foldout = EditorGUILayout.Foldout(drawers[i].Foldout, label);
					if (foldout)
					{
						var height = drawers[i].Drawer.GetPropertyHeight(p, new GUIContent());

						var rect = EditorGUILayout.GetControlRect(false, height);
						rect.x += 14f;
						rect.width -= 14f;
						drawers[i].Drawer.OnGUI(rect, p, new GUIContent());
					}

					var data = drawers[i];
					data.Foldout = foldout;
					drawers[i] = data;
				}
				EditorGUI.indentLevel--;
			}

			if (GUILayout.Button("Recentre Primitives"))
				RecentrePrimitives();

			serializedObject.ApplyModifiedProperties();
		}

		void RecentrePrimitives()
		{
			var vertexAccum = Vector2.zero;

			for (var i = 0; i < p_primitives.arraySize; i++)
			{
				var p_fullbox =
					p_primitives.GetArrayElementAtIndex(i)
					.FindPropertyRelative("FullBox");

				for (var j = 0; j < p_fullbox.arraySize; j++)
					vertexAccum += p_fullbox.GetArrayElementAtIndex(j).vector2Value;
			}

			var nVertexes = p_primitives.arraySize * Primitive.kVerts;
			var midpoint = vertexAccum / nVertexes;

			Undo.RecordObject(serializedObject.targetObject, $"Recentre Boxes");
			for (var i = 0; i < p_primitives.arraySize; i++)
			{
				var p_fullbox =
					p_primitives.GetArrayElementAtIndex(i)
					.FindPropertyRelative("FullBox");
				var p_rigidbox =
					p_primitives.GetArrayElementAtIndex(i)
					.FindPropertyRelative("RigidBox");

				for (var j = 0; j < p_fullbox.arraySize; j++)
				{
					var p_vFull = p_fullbox.GetArrayElementAtIndex(j);
					var p_vRigid = p_rigidbox.GetArrayElementAtIndex(j);
					p_vFull.vector2Value = p_vFull.vector2Value - midpoint;
					p_vRigid.vector2Value = p_vRigid.vector2Value - midpoint;
				}
			}
		}

		private void OnSceneGUI() => SceneGUI((Body)target, drawers);

		public static void SceneGUI(Body target, List<PrimitiveData> drawers)
		{
			var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

			var mouseWorldPos = IntersectionWithPlane(target, ray);
			target.edt_Selected = GetSelectedPrimitive(target, mouseWorldPos);
			if (target.edt_Selected != -1)
				HandlePrimitive(target, target.edt_Selected, mouseWorldPos, drawers);

			CorrectRigidBoxes(target);
		}

		public static void DrawPrimitive(Body body, int iPrim)
		{
			var primitive = body.primitives[iPrim];
			var rigidbox = primitive.RigidBox
				.Select(v => (Vector3)(v + body.Position)).ToList();
			rigidbox.Add(primitive.RigidBox[0] + body.Position);
			Handles.color = Color.black;
			Handles.DrawAAPolyLine(rigidbox.ToArray());

			Vector3 lastPoint = primitive.FullBox[0] + body.Position;
			var largestIndex = 0;

			for (var i = 0; i < primitive.FrictionDef.Length - 1; i++)
			{
				var edge1 = Mathf.FloorToInt(primitive.FrictionDef[i].Distance);
				var edge2 = Mathf.FloorToInt(primitive.FrictionDef[i + 1].Distance);

				Handles.color = FrictionColour(primitive.FrictionDef[i].Friction);

				for (edge1++; edge1 <= edge2; edge1++)
				{
					var point = primitive.FullBox[edge1] + body.Position;
					Handles.DrawLine(lastPoint, point);
					lastPoint = point;

				}
				edge1--;

				var pos1 = body.Position + primitive.FullBox[edge1];
				var pos2 = body.Position + primitive.FullBox[(edge1 + 1) % Body.kVerts];
				var p = Vector2.Lerp(pos1, pos2, primitive.FrictionDef[i + 1].Distance - edge2);
				Handles.DrawLine(lastPoint, p);
				lastPoint = p;
				largestIndex = Math.Max(largestIndex, edge1);
			}

			Handles.color = FrictionColour(primitive.FrictionDef.Last().Friction);
			for (largestIndex++; largestIndex < Body.kVerts; largestIndex++)
			{
				var point = primitive.FullBox[largestIndex] + body.Position;
				Handles.DrawLine(lastPoint, point);
				lastPoint = point;
			}

			Handles.DrawLine(lastPoint, primitive.FullBox[0] + body.Position);

			Handles.color = Color.black;
			for (var i = 0; i < Body.kVerts; i++)
			{
				if (!IsLinkEdge(body, iPrim, i)) continue;

				var next = (i + 1) % Body.kVerts;
				Handles.DrawLine(
					body.Position + primitive.FullBox[i], body.Position + primitive.FullBox[next]);
			}
		}

		static void HandlePrimitive(Body body, int index, Vector3 mouseWorldPos, List<PrimitiveData> drawers)
		{
			var primitive = body.primitives[index];

			var hoverEdge = 0;
			var hoverDist = float.MaxValue;
			for (var i = 0; i < Body.kVerts; i++)
			{
				// Move a Vertex on the Rigid Box
				Handles.color = Color.black;
				{
					EditorGUI.BeginChangeCheck();
					var pos = body.Position + primitive.RigidBox[i];
					var size = HandleUtility.GetHandleSize(pos) * kHandleSize * 0.4f;
					var rigidPos = Handles
						.FreeMoveHandle(pos, Quaternion.identity, size,
										Vector3.zero, Handles.RectangleHandleCap);
					if (EditorGUI.EndChangeCheck())
					{
						Undo.RecordObject(body, $"Move Rigid {i}");
						MoveVertex(body, index, i, false, (Vector2)rigidPos - pos);
					}
				}

				// Move a Vertex on the Full Box
				Handles.color = Color.grey;
				{
					EditorGUI.BeginChangeCheck();
					var pos = body.Position + primitive.FullBox[i];
					var size = HandleUtility.GetHandleSize(pos) * kHandleSize;
					var move = Handles
						.FreeMoveHandle(pos, Quaternion.identity, size,
										Vector3.zero, Handles.CircleHandleCap);
					if (EditorGUI.EndChangeCheck())
					{
						Undo.RecordObject(body, $"Move Full {i}");
						MoveVertex(body, index, i, true, (Vector2)move - pos);
					}

					var labelPos = pos + pos.normalized * 0.2f;
					string s = "";
					switch (i)
					{
						case 0:
							s = "TL";
							break;
						case 1:
							s = "TR";
							break;
						case 2:
							s = "BR";
							break;
						case 3:
							s = "BL";
							break;
					}
					Handles.Label(labelPos, s);
				}

				var plane = FysicsManager.CalcPlaneData(
					primitive.FullBox[i] + body.Position,
					primitive.FullBox[(i + 1) % Body.kVerts] + body.Position);

				// Friction stuff
				var d = FysicsManager.DistanceToLine(mouseWorldPos, plane.pos1, plane.pos2);
				if (d < hoverDist)
				{
					hoverEdge = i;
					hoverDist = d;
				}
			}

			if (hoverDist < 0.3f)
			{
				var plane = FysicsManager.CalcPlaneData(
					primitive.FullBox[hoverEdge] + body.Position,
					primitive.FullBox[(hoverEdge + 1) % Body.kVerts] + body.Position);
				var pos = FysicsManager.ClosestPointOnLine(mouseWorldPos, plane.pos1, plane.pos2);

				var validEdge = !IsLinkEdge(body, index, hoverEdge);
				var linkedPrimOkay =
					GetLinkInfos(body, index, hoverEdge).Count == 0
					&& GetLinkInfos(body, index, (hoverEdge + 1) % Body.kVerts).Count == 0;

				Handles.color = validEdge ? (linkedPrimOkay ? Color.white : Color.yellow) : new Color(0.2f, 0.2f, 0.2f);
				Handles.DrawSolidDisc(pos, Vector3.back, 0.06f);

				if (validEdge && Event.current.keyCode == kKeyAddFrictionHandle)
				{
					var ratio = Vector2.Distance(plane.pos1, pos) / Vector2.Distance(plane.pos1, plane.pos2);
					var value = hoverEdge + ratio;
					var friction = new PrimitiveFriction { Distance = value, Friction = 1 };

					var frictions = primitive.FrictionDef.ToList();
					int insert = 0;
					for (; insert < frictions.Count; insert++)
					{
						if (value == frictions[insert].Distance)
						{
							insert = -1;
							break;
						}

						if (value < frictions[insert].Distance)
							break;
					}

					if (insert >= 0)
					{
						Undo.RecordObject(body, "Add Friction Def");
						frictions.Insert(insert, friction);
						primitive.FrictionDef = frictions.ToArray();
					}
				}
				else if (validEdge
						 && Event.current.keyCode == kKeyAddLinkedPrimitive && linkedPrimOkay)
				{
					Event.current.Use();
					Undo.RecordObject(body, "Add linked Primitive");
					AddLinkedPrimitive(body, index, primitive, hoverEdge, drawers);
				}
			}

			HandleGeneralPosition(body, index, drawers);
			HandlePrimitiveScale(body, index, mouseWorldPos, drawers);

			for (int j = 1; j < primitive.FrictionDef.Length; j++)
			{
				var frict = primitive.FrictionDef[j];
				var edge = Mathf.FloorToInt(frict.Distance);
				var lerp = frict.Distance - edge;
				var plane = FysicsManager.CalcPlaneData(
					body.Position + primitive.FullBox[edge],
					body.Position + primitive.FullBox[(edge + 1) % Body.kVerts]
					);
				var pos = Vector2.Lerp(plane.pos1, plane.pos2, lerp);

				EditorGUI.BeginChangeCheck();
				var newPos = Handles.FreeMoveHandle(pos, Quaternion.identity, 0.1f, Vector3.zero, Handles.RectangleHandleCap);
				if (EditorGUI.EndChangeCheck())
				{
					Undo.RecordObject(body, "Move Friction Def");
					int closest = edge;
					var closestPoint = pos;
					float dist = float.MaxValue;
					for (var i = 0; i < Body.kVerts; i++)
					{
						var pos1 = body.Position + primitive.FullBox[i];
						var pos2 = body.Position + primitive.FullBox[(i + 1) % Body.kVerts];
						var point = FysicsManager.ClosestPointOnLine(newPos, pos1, pos2);
						var d = Vector2.Distance(point, newPos);
						if (d < dist)
						{
							dist = d;
							closest = i;
							closestPoint = point;
						}
					}

					var normlisedPoint = closestPoint - body.Position;
					var newLerp = Vector2.Distance(normlisedPoint, primitive.FullBox[closest])
						/ Vector2.Distance(primitive.FullBox[closest], primitive.FullBox[(closest + 1) % Body.kVerts]);
					primitive.FrictionDef[j].Distance = closest + newLerp;
				}
			}
			Array.Sort(primitive.FrictionDef, (l, r) => l.Distance < r.Distance ? -1 : 1);

			// Move the link position
		}

		static int GetSelectedPrimitive(Body body, Vector3 mouseWorldPos)
		{
			if (Event.current.type != EventType.MouseDown)
				return body.edt_Selected;

			bool Try(int index)
			{
				bool inBox = true;

				for (int edge = 0; edge < Body.kVerts; edge++)
				{
					var edgeEnd = (edge + 1) % Body.kVerts;
					var vertex = body.Position + body.primitives[index].FullBox[edge];
					var plane = FysicsManager.CalcPlaneData(
						vertex, body.Position + body.primitives[index].FullBox[edgeEnd]);

					inBox &= FysicsManager.IsBehindPlane(plane, (Vector2)mouseWorldPos);
					if (Vector2.Distance(vertex, mouseWorldPos) < kHandleSize * HandleUtility.GetHandleSize(vertex))
						return true;
				}

				return inBox;
			}

			if (body.edt_Selected != -1 && Try(body.edt_Selected))
				return body.edt_Selected;

			for (int i = 0; i < body.primitives.Length; i++)
			{
				if (i != body.edt_Selected && Try(i))
					return i;
			}

			return -1;
		}

		static void MoveVertex(Body body, int iPrim, int index, bool fullbox, Vector2 delta)
		{
			var boundPlanes = fullbox ? GetFullBoxBounds(body, iPrim, index) : GetRigidBoxBounds(body, iPrim, index);
			var vertex = fullbox ? body.primitives[iPrim].FullBox[index] : body.primitives[iPrim].RigidBox[index];

			Vector2 BoundDelta(FysicsManager.Plane plane, Vector2 delta)
			{
				var factor = FysicsManager.GetOverlapFactor(plane, delta + vertex);
				if (factor > 0)
					return delta;

				var newDelta = delta - (factor * plane.normal);
				return newDelta;
			}

			Vector2 boundDelta = delta;
			foreach (var plane in boundPlanes)
				boundDelta = BoundDelta(plane, boundDelta);

			var infos = GetLinkInfos(body, iPrim, index);
			if (fullbox)
			{
				body.primitives[iPrim].FullBox[index] += boundDelta;
				foreach (var info in infos)
					body.primitives[info.primOther].FullBox[info.otherVert] += boundDelta;
			}
			else
			{
				body.primitives[iPrim].RigidBox[index] += boundDelta;
				foreach (var info in infos)
					body.primitives[info.primOther].RigidBox[info.otherVert] += boundDelta;
			}
		}

		static void CorrectRigidBoxes(Body body)
		{
			foreach (var link in body.links)
			{
				var primA = body.primitives[link.iA];
				var primB = body.primitives[link.iB];

				var linkEdge1 = primA.FullBox[link.edgeA];
				var linkEdge2 = primB.FullBox[link.edgeB];

				var newPosA =
					FysicsManager.ClosestPointOnLine(primA.RigidBox[link.edgeA], linkEdge1, linkEdge2);
				var newPosB =
					FysicsManager.ClosestPointOnLine(primB.RigidBox[link.edgeB], linkEdge1, linkEdge2);

				MoveVertex(body, link.iA, link.edgeA, false, newPosA - primA.RigidBox[link.edgeA]);
				MoveVertex(body, link.iB, link.edgeB, false, newPosB - primB.RigidBox[link.edgeB]);
			}
		}

		#region Helper Structures

		struct LinkInfo
		{
			public int primOther;
			public int otherVert;
		}

		#endregion

		#region Helper Functions

		static Vector3 IntersectionWithPlane(Body body, Ray ray)
		{
			var t = Vector3.Dot((Vector3)body.Position - ray.origin, Vector3.back);
			return ray.origin + ray.direction * t;
		}
		static bool IsLinkEdge(Body body, int iPrim, int edge)
			=> body.links.Where(
				link => (link.iA == iPrim && link.edgeA == edge)
						|| (link.iB == iPrim && link.edgeB == edge))
				.Any();
		static void HandleGeneralPosition(Body body, int iPrim, List<PrimitiveData> datas)
		{
			var primitive = body.primitives[iPrim];
			var data = datas[iPrim];
			var position = primitive.RigidBox.Aggregate((pos, next) => pos + next) / Body.kVerts;
			position += body.Position;

			var newPosition = (Vector3)position;
			var quaternion = data.Rotation;

			EditorGUI.BeginChangeCheck();
				Handles.TransformHandle(ref newPosition, ref quaternion);
			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(body, "Move Primitive");

				var change = (Vector2)newPosition - position;
				if (change != Vector2.zero)
				{
					for (var i = 0; i < Body.kVerts; i++)
					{
						MoveVertex(body, iPrim, i, true, change);
						MoveVertex(body, iPrim, i, false, change);
					}
				}

				var primCentre = position - body.Position;

				if (quaternion != data.Rotation)
				{
					quaternion.ToAngleAxis(out var newAngle, out var newAxis);
					data.Rotation.ToAngleAxis(out var oldAngle, out var oldAxis);

					if (newAxis == Vector3.back) newAngle = -newAngle;
					if (oldAxis == Vector3.back) oldAngle = -oldAngle;

					var delta = newAngle - oldAngle;
					var rotation = new UniBoost.Math.Matrix2x2(delta * Mathf.Deg2Rad);
					for (var i = 0; i < Body.kVerts; i++)
					{
						var offsetFull = primitive.FullBox[i] - primCentre;
						offsetFull = rotation * offsetFull;
						var newFullPos = primCentre + offsetFull;

						var offsetRigid = primitive.RigidBox[i] - primCentre;
						offsetRigid = rotation * offsetRigid;
						var newRigidPos = primCentre + offsetRigid;
						MoveVertex(body, iPrim, i, true, newFullPos - primitive.FullBox[i]);
						MoveVertex(body, iPrim, i, false, newRigidPos - primitive.RigidBox[i]);
					}

					data.Rotation = quaternion;
				}
			}

			if (Event.current.type == EventType.MouseUp)
				data.Rotation = Quaternion.identity;

			datas[iPrim] = data;
		}

		static void HandlePrimitiveScale(Body body, int iPrim, Vector3 mouseWorldPos, List<PrimitiveData> datas)
		{
			if (Event.current.keyCode != kKeyScalePrimitive)
				return;

			var data = datas[iPrim];
			var primitive = body.primitives[iPrim];

			(List<Vector2>, List<Vector2>) CalculateBox()
			{
				var midpoint = Vector2.zero;
				for (var i = 0; i < Primitive.kVerts; i++)
					midpoint += primitive.FullBox[i];
				midpoint /= Primitive.kVerts;

				var vecStart = data.ScaleStart - midpoint;
				var vecEnd = (Vector2)mouseWorldPos - midpoint;
				//var vecScale = (Vector2)mouseWorldPos - data.ScaleStart;

				var fullbox = new List<Vector2>();
				var rigidbox = new List<Vector2>();
				for (var i = 0; i < Primitive.kVerts; i++)
				{
					var vertex = Vector2.zero;

					var vecScale = Vector2.zero;
					if (Mathf.Sign(vecStart.x) == Mathf.Sign(vecEnd.x))
						vecScale.x = vecEnd.x - vecStart.x;
					else
						vecScale.x = vecEnd.x;

					if (Mathf.Sign(vecStart.y) == Mathf.Sign(vecEnd.y))
						vecScale.y = vecEnd.y - vecStart.y;
					else
						vecScale.y = vecEnd.y;

					vecScale.x = vecScale.x > 0 ? vecScale.x + 1 : 1 / -(vecScale.x - 1);
					vecScale.y = vecScale.y > 0 ? vecScale.y + 1 : 1 / -(vecScale.y - 1);

					var posFullLocal = primitive.FullBox[i] - midpoint;
					var posRigidLocal = primitive.RigidBox[i] - midpoint;
					var postScaleFull = posFullLocal * vecScale;
					var postScaleRigid = posRigidLocal * vecScale;
					fullbox.Add(postScaleFull + midpoint);
					rigidbox.Add(postScaleRigid + midpoint);
				}

				return (fullbox, rigidbox);
			}

			if (Event.current.type == EventType.KeyDown)
			{
				if (!data.Scaling)
				{
					data.Scaling = true;
					data.ScaleStart = mouseWorldPos;
					datas[iPrim] = data;
					return;
				}

				var (projectedBox, _) = CalculateBox();
				projectedBox.Add(projectedBox[0]);

				// Why isn't this drawing?
				Handles.BeginGUI();
				Handles.color = Color.blue;
				Handles.DrawAAPolyLine(projectedBox.Select(v2 => (Vector3)v2).ToArray());
				Handles.EndGUI();
				return;
			}

			if (Event.current.type != EventType.KeyUp)
				return;

			Undo.RecordObject(body, "Scale");
			var (fullbox, rigidbox) = CalculateBox();
			for (var i = 0; i < Primitive.kVerts; i++)
			{
				Vector2 change;
				change = fullbox[i] - primitive.FullBox[i];
				MoveVertex(body, iPrim, i, true, change);
				change = rigidbox[i] - primitive.RigidBox[i];
				MoveVertex(body, iPrim, i, false, change);
			}
			data.Scaling = false;
			datas[iPrim] = data;
		}

		static List<FysicsManager.Plane> GetFullBoxBounds(Body body, int iPrim, int index)
		{
			var infos = GetLinkInfos(body, iPrim, index);
			var planes = new List<FysicsManager.Plane>();

			switch (infos.Count)
			{
				case 0:
					{
						var primitive = body.primitives[iPrim];
						var a = primitive.RigidBox[index];
						var b = primitive.RigidBox[(index + 1) % Body.kVerts];
						var c = primitive.RigidBox[(index + 3) % Body.kVerts];

						var plane1 = FysicsManager.CalcPlaneData(a, b);
						var plane2 = FysicsManager.CalcPlaneData(c, a);
						planes.Add(plane1);
						planes.Add(plane2);
					}
					break;
				case 1:
					{
						// Use the edge that is 1 less than the other
						int[] additionals;
						int edge;
						if ((index + 3) % Body.kVerts == infos[0].otherVert)
						{
							edge = infos[0].otherVert;
							additionals = new[] { (edge + 3) % Body.kVerts, (edge + 1) % Body.kVerts };
						}
						else
						{
							edge = index;
							additionals = new[] { (edge + 1) % Body.kVerts, (edge + 3) % Body.kVerts };
						}

						var prims = new[] { body.primitives[iPrim], body.primitives[infos[0].primOther] };
						for (int i = 0; i < prims.Length; i++)
						{
							var p = prims[i];
							var a = p.RigidBox[edge];
							var b = p.RigidBox[(edge + 1) % Body.kVerts];
							var plane1 = FysicsManager.CalcPlaneData(a, b);
							planes.Add(plane1);

							var c = p.RigidBox[(additionals[i] + 1) % Body.kVerts];
							var d = p.RigidBox[additionals[i]];
							var plane2 = FysicsManager.CalcPlaneData(c, d);
							planes.Add(plane2);
						}
					}
					break;
				case 2:
					{

					}
					break;
			}

			return planes;
		}

		static List<FysicsManager.Plane> GetRigidBoxBounds(Body body, int iPrim, int index)
		{
			var infos = GetLinkInfos(body, iPrim, index);
			var planes = new List<FysicsManager.Plane>();

			void GetPlanesOfOppositeCorner(int prim, int vertex)
			{
				var primitive = body.primitives[prim];
				var a = primitive.RigidBox[(vertex + 3) % Body.kVerts];
				var b = primitive.RigidBox[(vertex + 2) % Body.kVerts];
				var c = primitive.RigidBox[(vertex + 1) % Body.kVerts];

				var plane1 = FysicsManager.CalcPlaneData(a, b);
				var plane2 = FysicsManager.CalcPlaneData(b, c);
				planes.Add(plane1);
				planes.Add(plane2);
			}

			GetPlanesOfOppositeCorner(iPrim, index);
			foreach (var info in infos)
				GetPlanesOfOppositeCorner(info.primOther, info.otherVert);

			//switch (infos.Count)
			//         {
			//	case 0:
			//		// Clamped by the edges that form the corner on the opposite end of the box
			//		GetPlanesOfOppositeCorner(iPrim, index);
			//		break;
			//	case 1:
			//		GetPlanesOfOppositeCorner(iPrim, index);
			//		GetPlanesOfOppositeCorner(infos[0].primOther, infos[0].otherVert);
			//		break;
			//	default:

			//		break;
			//         }

			return planes;
		}

		static List<LinkInfo> GetLinkInfos(Body body, int iPrim, int vertex)
		{
			var infos = new List<LinkInfo>();

			void InspectEdge(int myEdge, int iOther, int edge)
			{
				var info = new LinkInfo { primOther = iOther };

				if (vertex == myEdge)
					info.otherVert = (edge + 1) % Body.kVerts;
				else if ((vertex + 3) % Body.kVerts == myEdge)
					info.otherVert = edge;
				else
					return;

				infos.Add(info);
			}

			foreach (var link in body.links)
			{
				if (iPrim == link.iA)
					InspectEdge(link.edgeA, link.iB, link.edgeB);
				else if (iPrim == link.iB)
					InspectEdge(link.edgeB, link.iA, link.edgeA);
			}

			return infos;
		}

		static void AddLinkedPrimitive(Body body, int iPrim, Primitive primitive, int edge, List<PrimitiveData> drawers)
		{
			var newPrim = new Primitive();

			int index(int i) { return i % 4; }

			var reflect1 = primitive.FullBox[edge];
			var reflect2 = primitive.FullBox[index(edge + 1)];

			Vector2 Reflect(Vector2 point)
			{
				var mirror = FysicsManager.ClosestPointOnLine(point, reflect1, reflect2);
				var toMirror = point - mirror;
				return mirror - toMirror;
			}

			void CalcBoxes(Vector2[] newBox, Vector2[] fromBox)
			{
				newBox[index(edge + 3)] = Reflect(fromBox[edge]);
				newBox[index(edge + 2)] = Reflect(fromBox[index(edge + 1)]);
				newBox[index(edge)] = Reflect(fromBox[index(edge + 3)]);
				newBox[index(edge + 1)] = Reflect(fromBox[index(edge + 2)]);
			}

			CalcBoxes(newPrim.FullBox, primitive.FullBox);
			CalcBoxes(newPrim.RigidBox, primitive.RigidBox);

			var combineA = (primitive.RigidBox[edge] + newPrim.RigidBox[index(edge + 3)]) / 2f;
			primitive.RigidBox[edge] = newPrim.RigidBox[index(edge + 3)] = combineA;
			var combineB = (primitive.RigidBox[index(edge + 1)] + newPrim.RigidBox[index(edge + 2)]) / 2f;
			primitive.RigidBox[index(edge + 1)] = newPrim.RigidBox[index(edge + 2)] = combineB;


			var prims = body.primitives.ToList();
			prims.Add(newPrim);
			body.primitives = prims.ToArray();

			drawers.Add(new PrimitiveData{
				Drawer = new PrimitiveDrawer(),
				Foldout = false,
				Scaling = false,
				ScaleStart = Vector2.zero,
				Rotation = Quaternion.identity
			});

			var links = body.links.ToList();
			links.Add(new PrimitiveLink { iA = iPrim, iB = prims.Count - 1, edgeA = edge, edgeB = index(edge + 2) });
			body.links = links.ToArray();
		}

		static Color FrictionColour(float friction)
		{
			if (friction < 5) return Color.Lerp(Color.cyan, Color.green, friction / 5f);
			return Color.Lerp(Color.green, Color.red, (friction - 5f) / 5f);
		}

		#endregion

		Vector2 GetPrimitiveCentre(Body body, Primitive p)
			=> body.Position + p.RigidBox.Aggregate((pos, next) => pos + next) / Body.kVerts;
	}
}