﻿using System.Collections.Generic;
using UnityEngine;

namespace StateSystem
{
	public class StateClause<TDataContainer>
	{
		public delegate byte FnClause(TDataContainer fs);

		public readonly string Name;
		public readonly int LeafCount;
		public readonly FnClause Evaluate;
		public readonly Dictionary<StateClause<TDataContainer>, byte> ParentRequirements;

		public StateClause(string name, int outputCount, FnClause evaluate)
		{
			Name = name;
			LeafCount = outputCount;
			Evaluate = evaluate;
			ParentRequirements = new Dictionary<StateClause<TDataContainer>, byte>();
		}

		public override string ToString() => Name;
    }

	public static class ESCBool
	{
		public static byte False = 0;
		public static byte True = 1;
	}

	public static class ESCButton
	{
		public const byte None = 0;
		public const byte Jump = 1;
		public const byte Light = 2;
		public const byte Medium = 3;
		public const byte Heavy = 4;
		public const byte Grab = 5;
		public const byte Guard = 6;
		public const byte Super = 7;
	}

	public static class ESCStickInput
	{
		// For the real one going to need a way of indexing into a data structure
		// using flags
		// My idea is that you order the conditions in descending order of priority
		// and each condition has its own leaf node to follow, and if none of the states
		// in that subtree can be entered you come back up and try out the tree
		// of the next most significant flag
		public const byte None = 0;
		public const byte Low = 1;
		public const byte Full = 2;
		public const byte Snap = 3;
	}

}