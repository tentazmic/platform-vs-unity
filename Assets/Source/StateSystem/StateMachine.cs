﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace StateSystem
{
	public interface IStateTreeNode<TDataContainer, Flags>
	{
		STNLeaf<TDataContainer, Flags> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses);
		string Print(int level, TDataContainer state);
	}

	public class STNBranch<TDataContainer, Flags> : IStateTreeNode<TDataContainer, Flags>
	{
		public readonly StateClause<TDataContainer> Clause;
		public readonly IStateTreeNode<TDataContainer, Flags>[] Children;

		public STNBranch(StateClause<TDataContainer> clause, params IStateTreeNode<TDataContainer, Flags>[] children)
		{
			Clause = clause;
			Children = children;
		}

		public STNBranch(StateClause<TDataContainer> clause)
		{
			Clause = clause;
			Children = new IStateTreeNode<TDataContainer, Flags>[clause.LeafCount];
		}

		public STNLeaf<TDataContainer, Flags> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses)
		{
			var index = Clause.Evaluate(fs);
			coveredClauses.Add(Clause);
			return Children[index] == null ? STNLeaf<TDataContainer, Flags>.Default : Children[index].Search(fs, coveredClauses);
		}

		public string Print(int level, TDataContainer state)
		{
			var s = " " + Clause.Name + "\n";
			level++;
			for (var i = 0; i < Children.Length; i++)
			{
				if (Children[i] == null) continue;
				for (int j = 0; j < level; j++) s += " |";
				s += " " + i + Children[i].Print(level, state);
			}

			return s;
		}

		public override string ToString() => Clause.ToString();
	}

	public class STNLeaf<TDataContainer, Flags> : IStateTreeNode<TDataContainer, Flags>
	{
		public static STNLeaf<TDataContainer, Flags> Default = new();
		public readonly List<IState<TDataContainer, Flags>> States;

		public STNLeaf(params IState<TDataContainer, Flags>[] states)
		{
			States = states.ToList();
		}

		public STNLeaf<TDataContainer, Flags> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses)
		{
			return this;
		}

		public override string ToString()
		{
			var s = States.Aggregate(" leaf:", (current, state) => current + " " + state.ToString() + ",");
			s += "\n";
			return s;
		}

		public string Print(int level, TDataContainer fs) => ToString();
	}

	public class StateMachine<Context, Flags> where Flags : new()
	{
		public readonly IStateTreeNode<Context, Flags> TreeRoot;
		HashSet<StateClause<Context>> _coveredClauses = new();
		IState<Context, Flags> _current;
		IState<Context, Flags> _startingState;

		Action _onStateChanged;

		public IState<Context, Flags> Current => _current;
		public STNLeaf<Context, Flags> PreviousLeaf;

		public event Action OnStateChanged
		{
			add => _onStateChanged += value;
			remove => _onStateChanged -= value;
		}

		public StateMachine(IState<Context, Flags> startingState, IStateTreeNode<Context, Flags> treeRoot)
		{
			TreeRoot = treeRoot;
			_current = startingState;
			_startingState = startingState;
		}

		public void Update(Context data)
		{
			var output = new StateOutput<Context>();
			_current.Update(data, ref output);

			if (output == EStateOutput.Reset)
			{
				Transition(data, _startingState);
				return;
			}

			if (FindNewState(data, output))
				return;

			if (output == EStateOutput.TriggerExit)
				Transition(data, _startingState);
		}

		public void TryEntry(Context data, IEnumerable<IState<Context, Flags>> states)
		{
			_coveredClauses.Clear();
			foreach (var state in states)
			{
				Flags flags = new ();
				if (state.CanEnter(data, ref flags, _coveredClauses))
				{
					Transition(data, state, in flags);
					break;
				}
			}
		}

		public void Reset(Context data)
		{
			Transition(data, _startingState);
		}

		public bool FindNewState(Context data, StateOutput<Context> output)
		{
			_coveredClauses.Clear();
			var leaf = TreeRoot.Search(data, _coveredClauses);

			if (output == EStateOutput.LocalHold && leaf == PreviousLeaf)
				return false;

			FStateTraits traits = output.requiredTransitionTraits;
			if (traits == FStateTraits.None && output == EStateOutput.Hold)
				return false;
			if (output != EStateOutput.LocalHold && output != EStateOutput.Hold)
				traits = FStateTraits.None;

			foreach (var state in leaf.States)
			{
				if (traits != FStateTraits.None && (state.Traits & traits) == FStateTraits.None)
					continue;

				var flags = new Flags();
				if (state.CanEnter(data, ref flags, _coveredClauses))
				{
					if (state != _current)
					{
						PreviousLeaf = leaf;
						Transition(data, state, in flags);
						//Debug.Log($"{state} {leaf.Print(0, data)}");
					}

					return true;
				}
			}

			return false;
		}

		void Transition(Context data, IState<Context, Flags> state) => Transition(data, state, new Flags());

		void Transition(Context data, IState<Context, Flags> state, in Flags flags)
		{
			//Debug.Log(state);
			_current.Exit(data, in flags);
			_current = state;
			_current.Entry(data);
			_onStateChanged?.Invoke();
		}

		public static IStateTreeNode<Context, Flags> CreateTree(StateClause<Context>[] clauses, params IState<Context, Flags>[] states)
		{
			var root = new STNBranch<Context, Flags>(clauses[0]);
			PopulateBranch(root, clauses, 1);
			foreach (var state in states)
				InsertState(root, state, new Dictionary<StateClause<Context>, byte>());

			//foreach (var state in states)
			//	_CreateTree(root, state, clauses, 1);
				//AddBranch(root, state, clauses);

			return root;
		}

		static void PopulateBranch(STNBranch<Context, Flags> current, in StateClause<Context>[] clauses, int next)
		{
			// We find the appropriate branch on CURRENT to add CLAUSES[NEXT] to
			void AddLeaf(int index)
			{
				var leaf = (STNLeaf<Context, Flags>)current.Children[index];
				if (leaf == null)
				{
					leaf = new STNLeaf<Context, Flags>();
					current.Children[index] = leaf;
				}
			}

			if (next == clauses.Length)
			{
				// Add a Leaf
				for (int i = 0; i < current.Children.Length; i++)
					AddLeaf(i);
				return;
			}

			var child = clauses[next];

			var branches = new List<byte>();
			if (child.ParentRequirements.ContainsKey(current.Clause))
				branches.Add(child.ParentRequirements[current.Clause]);
			else
			{
				for (byte i = 0; i < current.Clause.LeafCount; i++)
					branches.Add(i);
			}

			foreach (var index in branches)
			{
				var branch = (STNBranch<Context, Flags>)current.Children[index];
				if (branch == null)
				{
					branch = new STNBranch<Context, Flags>(child);
					current.Children[index] = branch;
				}

				PopulateBranch(branch, clauses, next + 1);
			}

			if (branches.Count > 1) return;
			
			// Parent Requirement found
			for (int i = 0; i < current.Clause.LeafCount; i++)
			{
				if (i == branches[0]) continue;

				AddLeaf(i);
			}
		}

		static void InsertState(IStateTreeNode<Context, Flags> node, IState<Context, Flags> state, Dictionary<StateClause<Context>, byte> history)
		{
			if (node is STNLeaf<Context, Flags>)
			{
				bool CheckClauses(IDictionary<StateClause<Context>, byte> conditions)
				{
					foreach (var (clause, value) in conditions)
					{
						if (history.ContainsKey(clause))
						{
							if (history[clause] != value)
								return false;
						}

						bool success = CheckClauses(clause.ParentRequirements);
						if (!success && value != 0)
							return false;
					}

					return true;
				}

				if (CheckClauses(state.Conditions))
					((STNLeaf<Context, Flags>)node).States.Add(state);
				return;
			}

			var branch = (STNBranch<Context, Flags>)node;
			if (state.Conditions.ContainsKey(branch.Clause))
			{
				var hist = new Dictionary<StateClause<Context>, byte>(history);
				hist.Add(branch.Clause, state.Conditions[branch.Clause]);
				InsertState(branch.Children[state.Conditions[branch.Clause]], state, hist);
				return;
			}

			for (byte i = 0; i < branch.Children.Length; i++)
			{
				var hist = new Dictionary<StateClause<Context>, byte>(history);
				hist.Add(branch.Clause, i);
				InsertState(branch.Children[i], state, hist);
			}
		}

		static void _CreateTree(STNBranch<Context, Flags> current, IState<Context, Flags> state, in StateClause<Context>[] clauses, int nextClause)
		{
			// We find the appropriate branch to attach this node to

			if (nextClause == clauses.Length)
			{
				// This is a leaf
				void AddToLeaf(int index)
				{
					var leaf = (STNLeaf<Context, Flags>)current.Children[index];
					if (leaf == null)
					{
						leaf = new STNLeaf<Context, Flags>();
						current.Children[index] = leaf;
					}
					leaf.States.Add(state);
				}

				if (state.Conditions.ContainsKey(current.Clause))
					AddToLeaf(state.Conditions[current.Clause]);
				else
				{
					for (var i = 0; i < current.Clause.LeafCount; i++)
						AddToLeaf(i);
				}

				return;
			}

			// If PARENT is a condition of STATE, only explore the branch where the evaluation matches
			// If PARENT is an ancestral condition of STATE,
			//   only explore the branch where the evaluation matches if STATE requires a truthy value
			//   from CURRENT, otherwise explore all
			bool CheckAncestralConditions(KeyValuePair<StateClause<Context>, byte> cond, out byte branch)
			{
				if (cond.Key == current.Clause)
				{
					branch = cond.Value;
					return true;
				}

				foreach (var parentCond in cond.Key.ParentRequirements)
				{
					var b = CheckAncestralConditions(parentCond, out branch);
					if (b) return b;
				}

				branch = 0;
				return false;
			}

			var branches = new List<byte>();
			if (state.Conditions.ContainsKey(current.Clause))
			{
				branches.Add(state.Conditions[current.Clause]);
			}
			else
			{
				//bool success = false;
				//foreach (var cond in state.Conditions)
	//            {
				//	success = CheckAncestralConditions(cond, out var branch);
				//	if (success)
	//                {
				//		branches.Add(branch);
				//		break;
	//                }
	//            }

				//if (!success)
					for (byte i = 0; i < current.Clause.LeafCount; i++)
						branches.Add(i);
			}

			Debug.Log($"{current} {current.Clause}");
			foreach (var index in branches)
			{
				var branch = (STNBranch<Context, Flags>)current.Children[index];
				if (branch == null)
				{
					branch = new STNBranch<Context, Flags>(clauses[nextClause]);
					current.Children[index] = branch;
				}
				_CreateTree(branch, state, clauses, nextClause + 1);
			}
		}


		static void AddBranch(STNBranch<Context, Flags> parent, in IState<Context, Flags> state, in StateClause<Context>[] usedClauses)
		{
			var outputCount = (byte)parent.Clause.LeafCount;
			StateClause<Context> next = null;
			for (var i = 0; i < usedClauses.Length - 1; i++)
			{
				if (usedClauses[i] == parent.Clause)
				{
					next = usedClauses[i + 1];
					break;
				}
			}

			var expectation = byte.MaxValue;
			foreach (var condition in state.Conditions)
				if (condition.Key == parent.Clause)
				{
					expectation = condition.Value;
					break;
				}

			byte[] branchIndexes;
			if (expectation == byte.MaxValue)
			{
				branchIndexes = new byte[outputCount];
				for (var i = 0; i < outputCount; i++)
					branchIndexes[i] = (byte)i;
			}
			else
			{
				branchIndexes = new[] { expectation };
			}

			foreach (var index in branchIndexes)
			{
				if (next != null)
				{
					var branch = (STNBranch<Context, Flags>)parent.Children[index];
					if (branch == null)
					{
						branch = new STNBranch<Context, Flags>(next);
						parent.Children[index] = branch;
					}
					AddBranch(branch, state, usedClauses);
				}
				else
				{
					var leaf = (STNLeaf<Context, Flags>)parent.Children[index];
					if (leaf == null)
					{
						leaf = new STNLeaf<Context, Flags>();
						parent.Children[index] = leaf;
					}
					leaf.States.Add(state);
				}
			}
		}
	}
}