using System.Collections.Generic;
using System.Linq;

namespace StateSystem
{
	[System.Flags]
	public enum FStateTraits
	{
		None, DashInterruptible, Airborne, Turnaround, LedgeAction
	}

	public enum EStateOutput
	{
		None,
		Hold, // Skip finding a new state
		LocalHold, // Only skip finding a new state if we arrive at the same leaf
		ThroughLinks,
		TriggerExit,
		Reset
	}

	public struct StateOutput<TDataContainer>
	{
		public EStateOutput flag;
		public FStateTraits requiredTransitionTraits;

		public static implicit operator StateOutput<TDataContainer>(EStateOutput value)
		{
			StateOutput<TDataContainer> output = new ();
			output.flag = value;
			return output;
		}

		public static bool operator ==(StateOutput<TDataContainer> s, EStateOutput flag)
			=> s.flag == flag;
		public static bool operator !=(StateOutput<TDataContainer> s, EStateOutput flag)
			=> !(s == flag);

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
		
	// TODO: Constant conditions
	// Conditions that if not true, forces exit

	public abstract class IState<Context, Flags>
	{
		public delegate bool FnTryEntry(Context c, ref Flags f);
		public delegate void FnEntry(Context c);
		public delegate void FnUpdate(Context c, ref StateOutput<Context> output);
		public delegate void FnExit(Context c, in Flags f);

		//public abstract IEnumerable<IState<Context>> Links { get; }
		public abstract FStateTraits Traits { get; }
		public abstract IDictionary<StateClause<Context>, byte> Conditions { get; }

		public abstract IState<Context, Flags> ActiveState(Context c);
		public abstract bool CanEnter(Context c, ref Flags f, ISet<StateClause<Context>> coveredClauses);
		public abstract void Entry(Context c);
		public abstract void Update(Context c, ref StateOutput<Context> output);
		public abstract void Exit(Context c, in Flags flags);
	}

	public class StSingle<Context, Flags> : IState<Context, Flags>
	{
		string _name;

		readonly FnTryEntry _tryEntry;
		readonly FnEntry _entry;
		readonly FnUpdate _update;
		readonly FnExit _exit;

		public override IDictionary<StateClause<Context>, byte> Conditions { get; }
		public override FStateTraits Traits { get; }
		//public override IEnumerable<IState<Context>> Links { get; }

		public StSingle(string name, FnTryEntry tryEntry, FnEntry entry, FnUpdate update, FnExit exit, Dictionary<StateClause<Context>, byte> conditions, FStateTraits traits = FStateTraits.None)
		{
			_name = name;
			Conditions = conditions;
			Traits = traits;
			_tryEntry = tryEntry;
			_entry = entry;
			_update = update;
			_exit = exit;
		}

		public override IState<Context, Flags> ActiveState(Context c) => this;

		public override void Entry(Context c) => _entry(c);
		public override void Update(Context c, ref StateOutput<Context> output) => _update(c, ref output);
		public override void Exit(Context c, in Flags flags) => _exit(c, in flags);

		public override bool CanEnter(Context c, ref Flags f, ISet<StateClause<Context>> coveredClauses)
		{
			var union = Conditions.Keys.Except(coveredClauses);
			if (union.Any(clause => clause.Evaluate(c) != Conditions[clause]))
			{
				return false;
			}

			return _tryEntry(c, ref f);
		}

		public override string ToString() => _name;
	}
}
