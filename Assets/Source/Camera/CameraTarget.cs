using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Camera
{
    public class CameraTarget : MonoBehaviour
    {
        [SerializeField] Transform _viewCentre;

        public Transform ViewPoint { get; private set; }

        private void Awake()
        {
            ViewPoint = _viewCentre ? _viewCentre : transform;
        }

        private void OnEnable()
        {
            var cam = FindObjectOfType<MultiCamera>();
            if (cam)
            {
                cam.AddTarget(this);
            }
        }

        private void OnDisable()
        {
            var cam = FindObjectOfType<MultiCamera>();
            if (cam)
            {
                cam.RemoveTarget(this);
            }
        }
    }
}
