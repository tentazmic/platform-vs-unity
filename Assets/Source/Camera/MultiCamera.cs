using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Camera
{
    public class MultiCamera : MonoBehaviour
    {
        List<CameraTarget> _targets = new();

        public void AddTarget(CameraTarget target)
        {
            if (_targets.Contains(target)) return;

            _targets.Add(target);
        }

        public void RemoveTarget(CameraTarget target)
        {
            if (!_targets.Contains(target)) return;

            _targets.Remove(target);
        }

        private void LateUpdate()
        {
            var pos = _targets[0].ViewPoint.position;
            pos.z = transform.position.z;
            transform.position = pos;
        }
    }
}
