﻿using System.Collections;
using UnityEngine;
using Fysics;

namespace Environment
{
    [RequireComponent(typeof(Body))]
	public class FloatingPlatform : MonoBehaviour
	{
        [SerializeField, Min(0)] float _speedThreshold = 5f;

        Body _body;

        private void Start()
        {
            _body = GetComponent<Body>();
        }

        private void Update()
        {
            var mag = _body.velocity.magnitude;
            if (mag < _speedThreshold)
            {
                _body.velocity = Vector2.zero;
            }
        }
    }
}