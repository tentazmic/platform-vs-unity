﻿using UnityEngine;

namespace Code.Combat
{
	[System.Serializable]
	public struct DamageOrder
	{
		public DamageOrder(int damage, Vector2 knockback, int baseHitStun, float hitStunScaling = 1.0f)
		{
			this.damage = damage;
			this.knockback = knockback;
			this.baseHitStun = baseHitStun;
			this.hitStunScaling = hitStunScaling;
		}

		public int damage;
		public Vector2 knockback;
		public int baseHitStun;
		public float hitStunScaling;
	}
	
	public enum HitStunType { Free, Light, Medium, Heavy }
}