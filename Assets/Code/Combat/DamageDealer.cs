using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.Combat;
using Code.Core.Fighter;
using PlatformVS.Core.VSEntity;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] FighterState fighter;

    bool _lastAttackHasHit = false;
    
    void Start()
    {
        foreach (var hitBox in fighter.HitBoxes)
        {
            hitBox.OnHitHurtBox += OnHitHurtBox;
        }

        fighter.OnNewAttack += () => { _lastAttackHasHit = false; };
    }

    void OnHitHurtBox(HitBox hit, HurtBox hurt)
    {
        var hitBoxData = fighter.CurrentAttackState.hitBoxes.LastOrDefault(hbData => hbData.idHitBox == hit.ID);
        if (_lastAttackHasHit || hitBoxData.size == Vector2.zero || hit.owner == hurt.owner)
            return;

        var order = hitBoxData.order;
        if (fighter.facingDirection == Facing.Left)
            order.knockback.x = -order.knockback.x;
        
        _lastAttackHasHit = true;
        hurt.TakeDamage(order, hit.transform.position);
        fighter.totalMeter += hitBoxData.meterGain;
    }
}
