﻿using System;
using System.Collections.Generic;
using Code.Core.Fighter;
using StateSystem;
using UniBoost.Input;
using UnityEngine;

namespace Code.Combat
{
	[Serializable]
	public struct HitBoxData
	{
		public int idHitBox;
		public int meterGain;
		public DamageOrder order;
		public Vector2 size;
	}

	public class AttackState : MonoBehaviour
	{
		public string attackAnimation;
		public InputFrame input;
		public bool inAir;
		// public DamageOrder[] hitboxes;
		public HitBoxData[] hitBoxes;

		public StSingle<FighterState> GetState()
		{
			var conditions = new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.Grounded, inAir ? ESCBool.False : ESCBool.True }
			};
			
			if (input.super == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Super);
			if (input.shield == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Guard);
			if (input.grab == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Grab);
			if (input.heavy == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Heavy);
			if (input.medium == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Medium);
			if (input.light == ControlState.Down)
				conditions.Add(FighterClauses.Button, ESCButton.Light);

			return new StSingle<FighterState>(
				attackAnimation,
				_ => true,
				fs =>
				{
					fs.CurrentAttackState = this;
					for (var i = 0; i < Mathf.Min(hitBoxes.Length, fs.HitBoxes.Length); i++)
					{
						fs.HitBoxes[i].ID = hitBoxes[i].idHitBox;
					}
					
					fs.OnNewAttack?.Invoke();
				},
				delegate(FighterState fs, ref EStateOutput output)
				{
					output = fs.AttackAnimationJustFinished 
						? EStateOutput.TriggerExit 
						: EStateOutput.Hold;
					Debug.Log(output);
				},
				delegate(FighterState fs, ref EStateOutput output)
				{
					fs.CurrentAttackState = null;
					foreach (var box in fs.HitBoxes)
						box.Active = false;
				},
				conditions
			);
		}
	}
}