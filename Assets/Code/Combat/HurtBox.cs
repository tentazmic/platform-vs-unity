using System;
using System.Collections;
using System.Collections.Generic;
using Code.Combat;
using Code.Core.Fighter;
using UnityEngine;

[RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
public class HurtBox : MonoBehaviour
{
    Action<DamageOrder, Vector2> _onTakeDamage;
    BoxCollider _collider;

    [NonSerialized] public Fighter owner;
    
    public event Action<DamageOrder, Vector2> OnTakeDamage
    {
        add => _onTakeDamage += value;
        remove => _onTakeDamage -= value;
    }

    void Awake()
    {
        _collider = GetComponent<BoxCollider>();
    }

    void Reset()
    {
        GetComponent<BoxCollider>().isTrigger = true;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    public void TakeDamage(DamageOrder order, Vector2 hitboxPosition)
    {
        _onTakeDamage.Invoke(order, hitboxPosition);
    }

    void OnDrawGizmos()
    {
        if (!_collider) _collider = GetComponent<BoxCollider>();

        Gizmos.color = new Color(0.0f, 1.0f, 0.0f, 1.0f);
        Gizmos.DrawWireCube(_collider.center + transform.position, _collider.size);
        
        Gizmos.color = new Color(.2f, 1.0f, 0.2f, 0.4f);
        Gizmos.DrawCube(_collider.center + transform.position, _collider.size);
    }
}
