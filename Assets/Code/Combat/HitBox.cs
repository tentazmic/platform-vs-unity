using System;
using System.Collections;
using System.Collections.Generic;
using Code.Combat;
using Code.Core.Fighter;
using UnityEngine;

[RequireComponent(typeof(BoxCollider), typeof(Rigidbody))]
public class HitBox : MonoBehaviour
{
    Action<HitBox, HurtBox> _onHitHurtBox;
    BoxCollider _collider;
    
    public event Action<HitBox, HurtBox> OnHitHurtBox
    {
        add => _onHitHurtBox += value;
        remove => _onHitHurtBox -= value;
    }

    [SerializeField] bool defaultActive;
    [NonSerialized] public bool Active;
    [NonSerialized] public int ID;
    [NonSerialized] public Fighter owner;

    void Awake()
    {
        _collider = GetComponent<BoxCollider>();
        Active = defaultActive;
    }

    void Reset()
    {
        GetComponent<BoxCollider>().isTrigger = true;
        GetComponent<Rigidbody>().isKinematic = true;
        Active = defaultActive;
    }

    void OnTriggerEnter(Collider other)
    {
        if (!Active) return;
        
        var hurtbox = other.GetComponent<HurtBox>();
        if (hurtbox)
            _onHitHurtBox?.Invoke(this, hurtbox);
    }

    void OnDrawGizmos()
    {
        if (!Active) return;
        if (!_collider) _collider = GetComponent<BoxCollider>();

        Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        var position = _collider.center + transform.position;
        Gizmos.DrawWireCube(position, _collider.size);
        
        Gizmos.color = new Color(1.0f, 0.2f, 0.2f, 0.4f);
        Gizmos.DrawCube(position, _collider.size);
    }
}
