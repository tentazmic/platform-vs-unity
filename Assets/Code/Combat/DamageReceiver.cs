using Code.Combat;
using Code.Core.Fighter;
using UnityEngine;

public class DamageReceiver : MonoBehaviour
{
	[SerializeField] FighterState state;
	[SerializeField] Transform guardCenter;

	[SerializeField] Vector2 kbDirection = Vector2.down;
	
	HurtBox[] _hurtBoxes;

	void Start()
	{
		_hurtBoxes = GetComponentsInChildren<HurtBox>();
		foreach (var hurtBox in _hurtBoxes)
			hurtBox.OnTakeDamage += guardCenter ? OnDamageTakenWithGuard : OnDamageTaken;
	}

	int _frameTick = 0;

	void Update() => Tick();

	private int roll = 0;
	public void Tick()
	{
		if (state.blockStun > 0) state.blockStun--;
		
		_frameTick = ++_frameTick % 120;

		if (state.HitStun > 0)
		{
			if (state.HSType == HitStunType.Light)
			{
				if (_frameTick % 40 == 0) state.AddHitStun(-1);
			}
			else
			{
				HitStunType type = state.HSType;
				state.AddHitStun(-1);
				state.HSType = state.HSType == HitStunType.Free ? HitStunType.Free : type;
			}
		}

		if (state.GreyHealth != state.Health)
		{
			roll++;
			if (roll > 15)
			{
				roll = 0;
				state.GreyHealth += (int)Mathf.Sign(state.Health - state.GreyHealth);
			}
		}

		if (state.Health < 1 || state.totalMeter < 1)
		{
			state.Die = true;
			enabled = false;
		}

		// if (state.HSType == HitStunType.Heavy || state.HSType == HitStunType.Medium)
		// {
		// 	// Debug.Log(state.previousVelocity);
		// 	for (var i = 0; i < state.NumContacts; i++)
		// 	{
		// 		var contact = state.Contacts[i];
		// 		var dot = Vector2.Dot(-state.previousVelocity, contact.Normal);
		// 		if (dot > 0)
		// 		{
		// 			if (dot > 0.5f)
		// 			{
		// 				state.velocity = Vector2.Reflect(state.previousVelocity, contact.Normal);
		// 				state.velocity *= contact.Terrain.Elasticity;
		// 			}
		//
		// 			break;
		// 		}
		// 	}
		// }
	}

	void OnDamageTaken(DamageOrder order, Vector2 hitboxPosition)
	{
		state.velocity += order.knockback;
		state.Health -= order.damage;
		state.GreyHealth -= (int) (order.damage * 0.5f);

		// Linear scale, will want it to be exponential
		var damaged = (1.0f - state.Health / (float)state.baseHealth) * 2.9f;
		var hitStun = order.baseHitStun + damaged * order.hitStunScaling;
		state.AddHitStun((int) hitStun);
	}

	void OnDamageTakenWithGuard(DamageOrder order, Vector2 hitboxPosition)
	{
		// Debug.Log("k");
		if (state.Guarding)
		{
			if (state.Input.control.magnitude < 0.5f)
			{
				order.damage = (int)(order.damage * 0.6f);
				order.knockback *= 0.4f;
				state.blockStun += (int)(order.baseHitStun * 0.8f);
				order.baseHitStun = 0;
			}
			else
			{
				var hitDirection = (Vector2)(guardCenter.position) - hitboxPosition;
				var guardRate = Vector2.Dot(state.Input.control, -hitDirection);
				Debug.Log(guardRate);

				if (guardRate > 0.21f)
				{
					order.damage = (int) (order.damage * 0.3f);
					order.knockback *= 0.05f;
					state.blockStun += (int)(order.baseHitStun * 0.6f);
					order.baseHitStun = 0;
				}
				else if (guardRate > -0.3f)
				{
					order.damage = (int)(order.damage * 0.5f);
					order.knockback *= 0.3f;
					state.blockStun += (int)(order.baseHitStun * 0.7f);
					order.baseHitStun = 0;
				}
				else
				{
					order.damage = (int)(order.damage * 1.1f);
					state.BreakGuard = true;
				}
			}
		}
		
		OnDamageTaken(order, hitboxPosition);
	}

	public void RingOut()
	{
		state.Health -= (int)(state.baseHealth * 0.4f);
		state.ResetStateMachine = true;
	}

	[ContextMenu("Simulate Knock Back")]
	void SimulateKnockBack()
	{
		var order = new DamageOrder();
		order.damage = 12;
		order.knockback = kbDirection * 10.0f;
		order.baseHitStun = 111;
		order.hitStunScaling = 1.0f;
		
		OnDamageTaken(order, guardCenter.position - (Vector3)(kbDirection * 3.0f));
	}
}
