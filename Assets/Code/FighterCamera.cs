using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro.EditorUtilities;
using UnityEngine;

public class FighterCamera : MonoBehaviour
{
    UnityEngine.Camera _camera;
    Transform[] _trackables;
    Vector2 _midPoint;
    Vector2 _halfSize;
    const float KScreenRatio = 16.0f / 9.0f;
    float _defaultZ;
    float _zoomLevel = 1.0f;
    float _zoomVelocity = 0.0f;

    void Start()
    {
        _camera = GetComponent<UnityEngine.Camera>();
        _defaultZ = transform.position.z;
        _trackables = FindObjectsOfType<Trackable>().Select(t => t.GetComponent<Transform>()).ToArray();
    }

    void LateUpdate()
    {
        _midPoint = Vector2.zero;
        foreach (var trackable in _trackables)
        {
            _midPoint += (Vector2)trackable.position;
        }

        _midPoint /= _trackables.Length;
        var zoomLevel = 1.0f;

        if (_trackables.Length > 1)
        {
            var viewportMid = _camera.WorldToViewportPoint(_midPoint);
            var greatestDist = Vector2.zero;

            foreach (var trackable in _trackables)
            {
                var diff = viewportMid - _camera.WorldToViewportPoint(trackable.position);
                diff.x = Mathf.Abs(diff.x);
                diff.y = Mathf.Abs(diff.y);
                diff.z = 0;

                if (diff.x > _halfSize.x) _halfSize.x = diff.x;
                if (diff.y > _halfSize.y) _halfSize.y = diff.y;
            }

            // float normalisedRatio = (_halfSize.x / _halfSize.y) / KScreenRatio;
            // if (normalisedRatio > 1.0f) // X is larger
            //     _halfSize.y = _halfSize.x / KScreenRatio;
            // else if (normalisedRatio < 1.0f)
            //     _halfSize.x = _halfSize.y * KScreenRatio;
            var dist = Vector2.Distance((Vector2)viewportMid - _halfSize, _halfSize) * 2.0f;
            // Debug.Log(dist);

            // Calculate what z should be
            if (dist > 0.9f)
            {
                zoomLevel += dist - 0.9f;
                zoomLevel *= 1.7f;
            }
            else if (dist < 0.2f)
            {
                zoomLevel -= 0.2f - dist;
                zoomLevel /= 1.7f;
            }

            // if (dist > 0.9f || dist < 0.7f)
            // {
            //     _zoomLevel = Mathf.SmoothDamp(_zoomLevel, _zoomLevel - 0.2f, ref _zoomVelocity, Time.deltaTime);
            // }
        }

        transform.position = new Vector3(_midPoint.x, _midPoint.y, zoomLevel * _defaultZ);
    }
}
