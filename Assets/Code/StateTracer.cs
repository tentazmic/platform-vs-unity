﻿using System.Collections.Generic;
using System.Linq;
using Code.Core.Fighter;
using UnityEngine;

namespace Code
{
    public class StateTracer : MonoBehaviour
    {
        [SerializeField] Fighter _fighter;

        struct StateStats
        {
            public string name;
            public int frameTime;
        }

        IList<StateStats> _track = new List<StateStats>();

        string _current = "";
        int _frameTick = 0;

        void Start()
        {
            _fighter.Machine.OnStateChanged += OnStateChange;

            _frameTick = 0;
            _current = _fighter.Machine.Current.Name(_fighter.state);
        }

        void Update()
        {
            _frameTick++;
        }

        void OnStateChange()
        {
            if (_current.Length != 0)
            {
                StateStats stats;
                stats.name = _current;
                stats.frameTime = _frameTick;
                _track.Add(stats);
            }

            _frameTick = 0;
            _current = _fighter.Machine.Current.Name(_fighter.state);
        }

        void OnDisable()
        {
            OnStateChange();
            Debug.Log(
                _track.Select(stats => $"{stats.name} for {stats.frameTime}\n").Aggregate((from, to) => from + to)
                );
        }
    }
}