using System.Collections.Generic;
using System.Linq;
using Code.Core.Fighter;
using PlatformVS.Core.VSEntity;
using UniBoost.Extensions;
using UniBoost.Input;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.Mathf;

namespace StateSystem
{
	public enum EStateOutput
	{
		None,
		Hold, // Skip finding a new state
		LocalHold, // Only skip finding a new state if we arrive at the same leaf
		TriggerExit,
		Reset
	}

	public delegate bool FnTryEntry(FighterState fs);
	public delegate void FnEntry(FighterState fs);
	public delegate void FnState(FighterState fs, ref EStateOutput output);
	
	public abstract class IState<TDataContainer>
	{
		public delegate bool FnTryEntry(TDataContainer fs);
		public delegate void FnEntry(TDataContainer fs);
		public delegate void FnState(TDataContainer fs, ref EStateOutput output);
		
		public abstract IState<TDataContainer> AnimationState(TDataContainer fs);
		public abstract string Name(TDataContainer fs);
		public virtual IDictionary<StateClause<TDataContainer>, byte> Conditions { get; }
		public abstract bool CanEnter(TDataContainer fs, ISet<StateClause<TDataContainer>> coveredClauses);
		public abstract void Entry(TDataContainer fs);
		public abstract void Update(TDataContainer fs, ref EStateOutput output);
		public abstract void Exit(TDataContainer fs, ref EStateOutput output);
	}

	public class StSingle<DataContainer> : IState<DataContainer>
	{
		string _name;
		
		readonly FnTryEntry _tryEntry;
		readonly FnEntry _entry;
		readonly FnState _update;
		readonly FnState _exit;

		public override IDictionary<StateClause<DataContainer>, byte> Conditions { get; }

		public StSingle(string name, FnTryEntry tryEntry, FnEntry entry, FnState update, FnState exit, Dictionary<StateClause<DataContainer>, byte> conditions)
		{
			_name = name;
			Conditions = conditions;
			_tryEntry = tryEntry;
			_entry = entry;
			_update = update;
			_exit = exit;
		}

		public override IState<DataContainer> AnimationState(DataContainer fs) => this;
		public override string Name(DataContainer fs) => _name;

		public override void Entry(DataContainer fs) => _entry(fs);
		public override void Update(DataContainer fs, ref EStateOutput output) => _update(fs, ref output);
		public override void Exit(DataContainer fs, ref EStateOutput output) => _exit(fs, ref output);

		public override bool CanEnter(DataContainer fs, ISet<StateClause<DataContainer>> coveredClauses)
		{
			var union = Conditions.Keys.Except(coveredClauses);
			if (union.Any(clause => clause.Evaluate(fs) != Conditions[clause]))
			{
				return false;
			}
			
			return _tryEntry(fs);
		}
	}

	public class StSequence<TDataContainer> : IState<TDataContainer>
	{
		string _name;
		
		readonly IState<TDataContainer>[] _states;
		readonly Dictionary<TDataContainer, uint> _indexes = new ();

		public override IDictionary<StateClause<TDataContainer>, byte> Conditions => _states[0].Conditions;

		public StSequence(string name = "", params IState<TDataContainer>[] states)
		{
			_states = states;
			if (name.Equals(""))
				_name = "State Sequence";
			else
				_name = name;
		}

		public override IState<TDataContainer> AnimationState(TDataContainer fs) => _states[_indexes[fs]];
		public override string Name(TDataContainer fs) => _name + $": {_states[_indexes[fs]].Name(fs)}";

		public override void Entry(TDataContainer fs)
		{
			if (_indexes.ContainsKey(fs))
				_indexes[fs] = 0;
			else
				_indexes.Add(fs, 0);
			_states[0].Entry(fs);
		}

		public override void Update(TDataContainer fs, ref EStateOutput output)
		{
			var i = _indexes[fs];
			_states[i].Update(fs, ref output);

			if (output == EStateOutput.TriggerExit)
			{
				_states[i].Exit(fs, ref output);
				i++;
				if (i >= _states.Length)
					return;
				
				_states[i].Entry(fs);
				_indexes[fs] = i;
				output = EStateOutput.Hold;
				return;
			}

			var next = Next(fs);
			if (next == null) return;
			
			var y = 0;
			foreach (var (clause, expectation) in next.Conditions)
			{
				if (clause.Evaluate(fs) != expectation)
					return;
			}
			
			if (!next.CanEnter(fs, new HashSet<StateClause<TDataContainer>>(next.Conditions.Keys)))
				return;

			next.Entry(fs);
			i++;
			_states[i].Entry(fs);
			_indexes[fs] = i;
		}

		public override void Exit(TDataContainer fs, ref EStateOutput output) => _states.Last().Exit(fs, ref output);

		public override bool CanEnter(TDataContainer fs, ISet<StateClause<TDataContainer>> coveredClauses)
		{
			if (_indexes.ContainsKey(fs))
				_indexes[fs] = 0;
			else
				_indexes.Add(fs, 0);
			return _states[0].CanEnter(fs, coveredClauses);
		}
			
		IState<TDataContainer> Next(TDataContainer fs)
		{
			var i = _indexes[fs] + 1;
			if (i == _states.Length)
				return null;
			return _states[i];
		}
	}

	public class StSubTree<TDataContainer> : IState<TDataContainer>
	{
		readonly string _name;
		readonly StateMachine<TDataContainer> _machine;
		readonly Dictionary<TDataContainer, IState<TDataContainer>> _currentStates = new ();

		public override IDictionary<StateClause<TDataContainer>, byte> Conditions { get; }
		
		public StSubTree(string name, Dictionary<StateClause<TDataContainer>, byte> conditions, params IState<TDataContainer>[] states)
		{
			_name = name;

			var combinedConditions = new HashSet<KeyValuePair<StateClause<TDataContainer>, byte>>();
			foreach (var t in states)
				combinedConditions.UnionWith(t.Conditions);

			// var clauses = new HashSet<StateClause>(combinedConditions.Select(pair => pair.Key));
			// Debug.Log(combinedConditions.Select(pair => $"{pair.Key.Name}: {pair.Value}").Aggregate((source, accumulate) => accumulate += $"{source}, "));
			Conditions = conditions;
			var machineClauses = new HashSet<StateClause<TDataContainer>>();
			foreach (var (clause, expectation) in combinedConditions)
			{
				if (combinedConditions.Count(pair => pair.Key == clause) == 1)
					Conditions.Add(clause, expectation);
				else
					machineClauses.Add(clause);
			}
			
			_machine = new StateMachine<TDataContainer>(states[0], StateMachine<TDataContainer>.CreateTree(machineClauses.ToArray(), states));
		}

		public override IState<TDataContainer> AnimationState(TDataContainer fs) => _currentStates.ContainsKey(fs) ? _currentStates[fs] : null;

		public override string Name(TDataContainer fs) =>
			_currentStates.ContainsKey(fs) ? $"{_name}: {_currentStates[fs].Name(fs)}" : _name;

		public override bool CanEnter(TDataContainer fs, ISet<StateClause<TDataContainer>> coveredClauses)
		{
			if (_machine.FindNewState(fs, EStateOutput.None))
			{
				if (_currentStates.ContainsKey(fs))
					_currentStates[fs] = _machine.Current;
				else
					_currentStates.Add(fs, _machine.Current);
				return true;
			}

			return false;
		}

		public override void Entry(TDataContainer fs)
		{
			_currentStates[fs].Entry(fs);
		}

		public override void Update(TDataContainer fs, ref EStateOutput output)
		{
			var state = _currentStates[fs];
			state.Update(fs, ref output);

			if (output == EStateOutput.Hold || output == EStateOutput.Reset) return;
			

			if (_machine.FindNewState(fs, output))
			{
				_currentStates[fs] = _machine.Current;
			}
			
			if (output == EStateOutput.TriggerExit) 
				output = EStateOutput.None;
		}

		public override void Exit(TDataContainer fs, ref EStateOutput output)
		{
			_currentStates[fs].Exit(fs, ref output);
		}
	}

	public static class AllStates
	{
		public static StSingle<FighterState> Stand = new(
			nameof(Stand),
			_ => true,
			fs => fs.acceleration = Vector2.zero,
			delegate { },
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.None }
			});

		public static StSingle<FighterState> Airborne = new(
			nameof(Airborne),
			_ => true,
			_ => { },
			delegate { },
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Grounded, ESCBool.False }
			});

		// It would probably be better to do this with bezier curves
		static float GroundLocomotionAcceleration(float velocity)
		{
			const float baseLine = 7.0f;
			const float velCutoff = 17.0f;
			if (velocity >= velCutoff)
				velocity = velCutoff;
			return 2.0f * Sin(velocity / 2.4f - 2.0f) + 
			       Max(0.4f * Cos(velocity / 2.4f - 0.4f), 0.0f) -
			       velocity / 4.0f + baseLine;
		}

		public static StSingle<FighterState> GMoveProgression = new(
			nameof(GMoveProgression),
			_ => true,
			_ => { },
			delegate(FighterState fs, ref EStateOutput _)
			{
				// if the stick is below a certain threshold stop accelerating
				var acceleration = GroundLocomotionAcceleration(Abs(fs.velocity.x)) * fs.Input.control.x;
				fs.velocity.x += acceleration * Time.deltaTime;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.PerpToContact }
			}
		);

		public static StSingle<FighterState> Walk = new(
			nameof(Walk),
			fs => fs.RunState == FighterState.StRun.None,
			_ => { },
			(FighterState fs, ref EStateOutput _) => fs.acceleration.x += 5.6f * fs.Input.control.x,
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.PerpToContact }
			}
		);

		public static StSingle<FighterState> Run = new(
			nameof(Run),
			fs => fs.RunState == FighterState.StRun.None,
			fs => fs.RunState = FighterState.StRun.Run,
			(FighterState fs, ref EStateOutput _) => fs.acceleration.x += 8.2f * fs.Input.control.x,
			((FighterState fs, ref EStateOutput output) => fs.RunState = FighterState.StRun.None),
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.DoubleSnap_PerpToContact }
			}
		);

		public static StSingle<FighterState> Sprint = new(
			nameof(Sprint),
			fs => Abs(fs.velocity.x) > 12.0f,
			fs => fs.RunState = FighterState.StRun.Run,
			(FighterState fs, ref EStateOutput _) => fs.acceleration.x += 10.8f * fs.Input.control.x,
			((FighterState fs, ref EStateOutput output) => fs.RunState = FighterState.StRun.None),
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.PerpToContact }
			}
		);

		public static StSingle<FighterState> Turnaround = new(
			nameof(Turnaround),
			_ => true,
			fs => fs.facingDirection = fs.facingDirection == Facing.Left ? Facing.Right : Facing.Left,
			delegate { },
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Stick, ESCStickInput.Back }
			}
		);
		
		// static bool ClingEntry

		public static StSingle<FighterState> Cling = new(
			nameof(Cling),
			fs => fs.NumContacts != 0,
			delegate {  }, 
			(FighterState fs, ref EStateOutput output) =>
			{
				fs.FrictionMultiplier = 2.0f;
				if (fs.Input.stStick == ControlState.Up || fs.Input.stStick == ControlState.Released)
					output = EStateOutput.Reset;
			},
			delegate {  },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Stick, ESCStickInput.ToContact }
			}
		);

		public static StSingle<FighterState> Scale = new(
			nameof(Scale),
			fs => fs.NumContacts != 0,
			delegate { },
			(FighterState fs, ref EStateOutput output) =>
			{
				var theta = Vector2.Angle(fs.AwayFromTerrain, Vector2.up) * Deg2Rad;
				var input = fs.Input.control.RotateVector(theta);
				if (Abs(input.x) < 0.1f)
				{
					output = EStateOutput.Reset;
					return;
				}
				var acceleration = new Vector2((Clamp(Abs(input.x), 0.0f, 0.7f) + 0.3f) * 5.0f * Sign(input.x), 0.0f);
				acceleration = acceleration.RotateVector(-theta);
				fs.acceleration += acceleration;
				switch (fs.Input.stStick)
				{
					case ControlState.Down:
					case ControlState.Held:
						fs.FrameMarker = 0;
						break;
					case ControlState.Up:
					case ControlState.Released:
						fs.FrameMarker++;
						output = EStateOutput.Hold;
						break;
				}

				if (fs.FrameMarker >= 3)
					output = EStateOutput.Reset;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				// { FighterClauses.Stick, ESCStickInput.PerpToContact }
			}
		);

		public static StSingle<FighterState> ClingTurnaround = new(
			nameof(ClingTurnaround),
			fs => fs.NumContacts != 0,
			fs => fs.facingDirection = fs.facingDirection == Facing.Left ? Facing.Right : Facing.Left,
			delegate { },
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Stick, ESCStickInput.Back }
			}
		);

		public static StSingle<FighterState> Dash = new(
			nameof(Dash),
			fs => Abs(fs.Input.control.y) < 0.2f,
			fs =>
			{
				const float kDashPower = 14.0f;
				var parallelFactor = Vector2.Dot(fs.velocity, fs.Input.control) * fs.velocity.magnitude;
				var power = CalculateJumpSpeed(kDashPower, parallelFactor);
				Debug.Log($"Dash {fs.Input.control * power} {power} {parallelFactor} {fs.velocity}:{fs.velocity.magnitude}");
				fs.velocity += fs.Input.control * power;
			},
			delegate(FighterState fs, ref EStateOutput output)
			{
				if (fs.StateFrames < 12)
					output = EStateOutput.Hold;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.Jump },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.PerpToContact }
			}
		);

		public static StSingle<FighterState> Skid = new(
			nameof(Skid),
			fs =>
			{
				return Abs(fs.velocity.x) > 5.4f;
			},
			delegate { },
			delegate(FighterState fs, ref EStateOutput output)
			{
				fs.FrictionMultiplier *= 2.0f;
				if (Approximately(fs.Input.control.x, 0.0f) && Abs(fs.velocity.x) > 1.5f)
					output = EStateOutput.LocalHold;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.Back }
			}
		);

		public static StSingle<FighterState> RunTurnaround = new(
			nameof(RunTurnaround),
			fs => Abs(fs.velocity.x) > 3.0f,
			fs =>
			{
				fs.facingDirection = fs.facingDirection == Facing.Left ? Facing.Right : Facing.Left;
				fs.velocity.x *= -1.4f;
			},
			delegate(FighterState fs, ref EStateOutput output)
			{
				if (fs.StateFrames > 7)
				{
					output = EStateOutput.Hold;
					fs.FrictionMultiplier *= 1.5f;
				}
				else
				{
					output = EStateOutput.TriggerExit;
					fs.velocity.x = (int)fs.facingDirection * 0.6f * 8.0f;
				}
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.Back }
			}
		);

		public static StSingle<FighterState> Slide = new(
			nameof(Slide),
			fs => Abs(fs.velocity.x) > 5.0f,
			delegate { },
			delegate(FighterState fs, ref EStateOutput output)
			{
				fs.FrictionMultiplier *= 1.4f;
				if (Abs(fs.velocity.x) < 4.0f || fs.Input.control.y > 0.5f)
					output = EStateOutput.TriggerExit;
				else
				{
					output = EStateOutput.LocalHold;
				}
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.None },
				{ FighterClauses.Grounded, ESCBool.True },
				{ FighterClauses.Stick, ESCStickInput.ToContact }
			}
		);

		public static StSingle<FighterState> GroundJump = new(
			nameof(GroundJump),
			fs =>
			{
				return !fs.Input.stStick.IsActivated() || fs.Input.control.y > 0.1f;
			},
			fs =>
			{
				fs.JumpDirection = fs.Input.stStick.IsActivated() && fs.FramesSinceStickSnap < 8
					? fs.Input.control
					: Vector2.up;
			},
			delegate(FighterState fs, ref EStateOutput output)
			{
				if (fs.StateFrames > 5)
				{
					const float kJumpPower = 14.0f;
					var parallelFactor = Vector2.Dot(fs.velocity, fs.JumpDirection) * fs.velocity.magnitude;
					var power = CalculateJumpSpeed(kJumpPower, parallelFactor);
					Debug.Log($"{fs.JumpDirection * power} : {fs.FramesSinceStickSnap - 5}");
					fs.velocity += fs.JumpDirection * power;
				}
				else
					output = EStateOutput.Hold;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.Jump },
				{ FighterClauses.Grounded, ESCBool.True }
			}
		);

		public static StSingle<FighterState> AirJump = new(
			nameof(AirJump),
			_ => true,
			fs =>
			{
				if (fs.HaveEnoughMeter(new MeterOrder(100, 0.2f)))
				{
					fs.ConsumeMeter(new MeterOrder(100, 0.2f));
					fs.JumpDirection = fs.FramesSinceStickSnap < 8 ? fs.Input.control : Vector2.up;
				}
				else
				{
					fs.JumpDirection = Vector2.zero;
				}
			},
			delegate(FighterState fs, ref EStateOutput output)
			{
				if (fs.JumpDirection == Vector2.zero)
				{
					output = EStateOutput.Reset;
					return;
				}
				
				if (fs.StateFrames > 5)
				{
				    Debug.Log(fs.JumpDirection);
					fs.velocity += fs.JumpDirection * 10.0f;
				}
				else
					output = EStateOutput.Hold;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.Jump },
				{ FighterClauses.Grounded, ESCBool.False }
			}
		);

		public static StSingle<FighterState> Guard = new(
			nameof(Guard),
			_ => true,
			fs => fs.Guarding = true,
			delegate(FighterState fs, ref EStateOutput output)
			{
				if (fs.BreakGuard)
				{
					output = EStateOutput.TriggerExit;
					return;
				}

				output = fs.Input.shield == ControlState.Held || fs.blockStun > 0 ? EStateOutput.Hold : output;
			},
			delegate(FighterState fs, ref EStateOutput output)
			{
				fs.Guarding = false;
				fs.blockStun = 0;
			},
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.False },
				{ FighterClauses.Button, ESCButton.Guard }
			}
		);

		public static StSingle<FighterState> HitStun = new(
			nameof(HitStun),
			_ => true,
			_ => { },
			delegate(FighterState fs, ref EStateOutput output)
			{
				output = fs.HitStun == 0 ? EStateOutput.Reset : EStateOutput.Hold;
			},
			delegate { },
			new Dictionary<StateClause<FighterState>, byte>
			{
				{ FighterClauses.HitStun, ESCBool.True }
			}
		);

		public static StSubTree<FighterState> STr_ClingScale = new (
			nameof(STr_ClingScale),
			new Dictionary<StateClause<FighterState>, byte> { { FighterClauses.Stick, ESCStickInput.ToContact } },
			Cling, Scale, ClingTurnaround
		);
			
		static int FindContactPointedToByStick(Vector2 stick, Contact[] contacts, int count)
		{
			for (var i = 0; i < count; i++)
			{
				var dot = Vector2.Dot(stick, -contacts[i].Normal);
				if (dot > 0.4f)
					return i;
			}

			return -1;
		}
		
		// x = velocity in parallel with the jump vector
		// j = jump power 
		
		// if x < -j,  zeroing slope      -(x / j) + j
		// if x < 0,   flipping curve     (|x^3| / j^3) + j
		// if x >= 0,  additive curve     sqrt(j) * sqrt(j - x)
		// if x > j,   0                  0
		static float CalculateJumpSpeed(float jumpPower, in float parallelVelocity)
		{
			if (parallelVelocity < -jumpPower)
				return -(parallelVelocity / jumpPower + jumpPower);
			if (parallelVelocity < 0)
				return Abs(Pow(parallelVelocity, 3) / Pow(jumpPower, 3)) + jumpPower;
			if (parallelVelocity > jumpPower)
				return 0;
			// x >= 0 && x <= jumpPower
			return Sqrt(jumpPower) * Sqrt(jumpPower - parallelVelocity);
		}
	}
}
