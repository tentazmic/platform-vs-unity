﻿using Code.Combat;
using Code.Core.Fighter;
using PlatformVS.Core.VSEntity;
using UniBoost.Extensions;
using UniBoost.Input;
using UnityEngine;

namespace StateSystem
{
    public class StateClause<TDataContainer>
    {
        public delegate byte FnClause(TDataContainer fs);
        
        public readonly string Name;
        public readonly int LeafCount;
        public readonly FnClause Evaluate;

        public StateClause(string name, int outputCount, FnClause evaluate)
        {
            Name = name;
            LeafCount = outputCount;
            Evaluate = evaluate;
        }
    }

    public static class ESCBool
    {
        public static byte False = 0;
        public static byte True = 1;
    }

    public static class ESCButton
    {
        public const byte None = 0;
        public const byte Jump = 1;
        public const byte Light = 2;
        public const byte Medium = 3;
        public const byte Heavy = 4;
        public const byte Grab = 5;
        public const byte Guard = 6;
        public const byte Super = 7;
    }

    public static class ESCStickInput
    {
        public const byte None = 0;
        public const byte Any = 1;
        public const byte Back = 2;
        public const byte ToContact = 3;
        public const byte PerpToContact = 4;
        public const byte DoubleSnap = 5;

        // For the real one going to need a way of indexing into a data structure
        // using flags
        // My idea is that you order the conditions in descending order of priority
        // and each condition has its own leaf node to follow, and if none of the states
        // in that subtree can be entered you come back up and try out the tree
        // of the next most significant flag
        public const byte DoubleSnap_PerpToContact = 6;
    }
    
    public static class FighterClauses
    {
        public static readonly StateClause<FighterState> Button = new(nameof(Button), 8,fs =>
        {
            if (fs.Input.super == ControlState.Down)
                return ESCButton.Super;
            if (fs.Input.shield == ControlState.Down)
                return ESCButton.Guard;
            if (fs.Input.grab == ControlState.Down)
                return ESCButton.Grab;
            if (fs.Input.heavy == ControlState.Down)
                return ESCButton.Heavy;
            if (fs.Input.medium == ControlState.Down)
                return ESCButton.Medium;
            if (fs.Input.light == ControlState.Down)
                return ESCButton.Light;
            if (fs.Input.jump == ControlState.Down)
                return ESCButton.Jump;

            return ESCButton.None;
        });

        public static readonly StateClause<FighterState> Stick = new(nameof(Stick), 7, fs =>
        {
            if (!fs.Input.stStick.IsActivated())
                return ESCStickInput.None;

            if (!Mathf.Approximately(fs.Input.control.x, 0.0f) && fs.facingDirection != EFacing.Evaluate(fs.Input.control.x))
                return ESCStickInput.Back;

            if (IsPointingToContact(fs.Input.control, fs.Contacts, fs.NumContacts, 0.4f))
                return ESCStickInput.ToContact;

            if (IsPerpendicularToContact(fs.Input.control, fs.AwayFromTerrain))
            {
                if (fs.Input.snapCount > 1)
                    return ESCStickInput.DoubleSnap_PerpToContact;
                return ESCStickInput.PerpToContact;
            }
            
            if (fs.Input.snapCount > 1)
                return ESCStickInput.DoubleSnap_PerpToContact;
            
            return ESCStickInput.Any;
        });

        public static readonly StateClause<FighterState> HitStun = new(nameof(HitStun), 2, fs => fs.HSType == HitStunType.Medium || fs.HSType == HitStunType.Heavy ? ESCBool.True : ESCBool.False);

        public static readonly StateClause<FighterState> Grounded = new(nameof(Grounded), 2, fs => (byte)(fs.IsGrounded ? ESCBool.True : ESCBool.False));

        public static readonly StateClause<FighterState> ToContact = new(nameof(ToContact), 2,
            fs => FindContactPointedToByStick(fs.Input.control, fs.Contacts, fs.NumContacts) != 0 
                ? ESCBool.True 
                : ESCBool.False);
        
        public static readonly StateClause<FighterState>[] All =
        {
            HitStun, Button, Grounded, Stick
        };
        
        static byte FindContactPointedToByStick(Vector2 stick, Contact[] contacts, int count)
        {
            for (var i = 0; i < count; i++)
            {
                var dot = Vector2.Dot(stick, -contacts[i].Normal);
                if (dot > 0.4f)
                    return (byte) (i + 1);
            }

            return 0;
        }

        static bool IsPointingToContact(Vector2 stick, Contact[] contacts, int count, float leniance)
        {
            for (var i = 0; i < count; i++)
            {
                var dot = Vector2.Dot(stick, -contacts[i].Normal);
                if (dot > leniance)
                    return true;
            }
            
            return false;
        }

        static bool IsPerpendicularToContact(Vector2 stick, Vector2 terrainNormal)
        {
            // Not quite sure why I did so much here
            var theta = Vector2.Angle(terrainNormal, Vector2.up) * Mathf.Deg2Rad;
            var input = stick.RotateVector(theta);
            return Mathf.Abs(input.x) > 0.1f;
        }
    }
}
