﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StateSystem
{
	public interface IStateTreeNode<TDataContainer>
	{
		STNLeaf<TDataContainer> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses);
		string Print(int level, TDataContainer state);
	}

	public class STNBranch<TDataContainer> : IStateTreeNode<TDataContainer>
	{
		public readonly StateClause<TDataContainer> Clause;
		public readonly IStateTreeNode<TDataContainer>[] Children;

		public STNBranch(StateClause<TDataContainer> clause, params IStateTreeNode<TDataContainer>[] children)
		{
			Clause = clause;
			Children = children;
		}

		public STNBranch(StateClause<TDataContainer> clause)
		{
			Clause = clause;
			Children = new IStateTreeNode<TDataContainer>[clause.LeafCount];
		}
		
		public STNLeaf<TDataContainer> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses)
		{
			var index = Clause.Evaluate(fs);
			coveredClauses.Add(Clause);
			return Children[index] == null ? STNLeaf<TDataContainer>.Default : Children[index].Search(fs, coveredClauses);
		}

		public string Print(int level, TDataContainer state)
		{
			var s = " " + Clause.Name + "\n";
			level++;
			for (var i = 0; i < Children.Length; i++)
			{
				if (Children[i] == null) continue;
				for (int j = 0; j < level; j++) s += " |";
				s += " " + i + Children[i].Print(level, state);
			}

			return s;
		}
	}
	
	public class STNLeaf<TDataContainer> : IStateTreeNode<TDataContainer>
	{
		public static STNLeaf<TDataContainer> Default = new();
		public readonly List<IState<TDataContainer>> States;

		public STNLeaf(params IState<TDataContainer>[] states)
		{
			States = states.ToList();
		}

		public STNLeaf<TDataContainer> Search(TDataContainer fs, HashSet<StateClause<TDataContainer>> coveredClauses)
		{
			return this;
		}

		public string Print(int level, TDataContainer fs)
		{
			var s = States.Aggregate(" leaf:", (current, state) => current + (" " + state.Name(fs) + ","));
			s += "\n";
			return s;
		}
	}

	public class StateMachine<TDataContainer>
	{
		public readonly IStateTreeNode<TDataContainer> TreeRoot;
		HashSet<StateClause<TDataContainer>> _coveredClauses = new ();
		IState<TDataContainer> _current;
		IState<TDataContainer> _startingState;

		Action _onStateChanged;

		public IState<TDataContainer> Current => _current;
		public STNLeaf<TDataContainer> PreviousLeaf;
		
		public event Action OnStateChanged
		{
			add => _onStateChanged += value;
			remove => _onStateChanged -= value;
		}

		public StateMachine(IState<TDataContainer> startingState, IStateTreeNode<TDataContainer> treeRoot)
		{
			TreeRoot = treeRoot;
			_current = startingState;
			_startingState = startingState;
		}

		public void Update(TDataContainer fs)
		{
			var output = EStateOutput.None;
			_current.Update(fs, ref output);
			
			if (output != EStateOutput.Hold)
				FindNewState(fs, output);
			
			if (output == EStateOutput.TriggerExit || output == EStateOutput.Reset)
				Transition(fs, _startingState);
		}

		public void Reset(TDataContainer fs)
		{
			Transition(fs, _startingState);
		}

		public bool FindNewState(TDataContainer fs, EStateOutput output)
		{
			_coveredClauses.Clear();
			var leaf = TreeRoot.Search(fs, _coveredClauses);
			if (output == EStateOutput.LocalHold && leaf == PreviousLeaf)
				return false;
			
			PreviousLeaf = leaf;

			foreach (var state in leaf.States)
				if (state.CanEnter(fs, _coveredClauses))
				{
					if (state != _current)
						Transition(fs, state);
					return true;
				}

			return false;
		}

		void Transition(TDataContainer fs, IState<TDataContainer> state)
		{
			var output = EStateOutput.None;
			_current.Exit(fs, ref output);
			_current = state;
			_current.Entry(fs);
			_onStateChanged?.Invoke();
		}

		public static IStateTreeNode<TDataContainer> CreateTree(StateClause<TDataContainer>[] clausesInUse, params IState<TDataContainer>[] states)
		{
			var root = new STNBranch<TDataContainer>(clausesInUse[0]);
			foreach (var state in states)
				AddBranch(root, state, clausesInUse);

			return root;
		}

		static void AddBranch(STNBranch<TDataContainer> parent, in IState<TDataContainer> state, in StateClause<TDataContainer>[] usedClauses)
		{
			var outputCount = (byte)parent.Clause.LeafCount;
			StateClause<TDataContainer> next = null;
			for (var i = 0; i < usedClauses.Length - 1; i++)
			{
				if (usedClauses[i] == parent.Clause)
				{
					next = usedClauses[i + 1];
					break;
				}
			}

			var expectation = byte.MaxValue;
			foreach (var condition in state.Conditions)
				if (condition.Key == parent.Clause)
				{
					expectation = condition.Value;
					break;
				}

			byte[] branchIndexes;
			if (expectation == byte.MaxValue)
			{
				branchIndexes = new byte[outputCount];
				for (var i = 0; i < outputCount; i++)
					branchIndexes[i] = (byte)i;
			}
			else
			{
				branchIndexes = new[] { expectation };
			}

			foreach (var index in branchIndexes)
			{
				if (next != null)
				{
					var branch = (STNBranch<TDataContainer>)parent.Children[index];
					if (branch == null)
					{
						branch = new STNBranch<TDataContainer>(next);
						parent.Children[index] = branch;
					}
					AddBranch(branch, state, usedClauses);
				}
				else
				{
					var leaf = (STNLeaf<TDataContainer>)parent.Children[index];
					if (leaf == null)
					{
						leaf = new STNLeaf<TDataContainer>();
						parent.Children[index] = leaf;
					}
					leaf.States.Add(state);
				}
			}
		}
	}
}
