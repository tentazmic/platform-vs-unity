﻿using System;
using UnityEngine;

namespace Code.Core
{
    public class KillBox : MonoBehaviour
    {
        void Start()
        {
            foreach (var plane in GetComponentsInChildren<CollisionEventForwarder>())
                plane.OnTriggerEnterEvent += OnTriggerEnter;
        }

        private void OnTriggerEnter(Collider other)
        {
            var receiver = other.GetComponent<DamageReceiver>();
            if (receiver)
                receiver.RingOut();
        }
    }
}