using System;
using System.Linq;
using Code.Core.Fighter;
using UniBoost.Extensions;
using UnityEngine;
using Terrain = Code.Core.Fighter.Terrain;

public class PhysicsBody : MonoBehaviour
{
    public FighterState state;
    
    readonly RaycastHit[] _hits = new RaycastHit[6];
    readonly Collider[] _colliders = new Collider[6];

    void Awake()
    {
        state.CurrentFluid = new Fluid();
    }

    public void PreStep()
    {
        // Causing direction jumps to fail
        if (!Mathf.Approximately(state.acceleration.x + state.environmentAcceleration.x, 0.0f) &&
            Mathf.Approximately(state.acceleration.x, -state.environmentAcceleration.x))
        {
            // Had a phenomenon where the environmental and regular acceleration would
            // constantly flip, causing the player to have small velocity
            // this is a fix. but may cause issues in the future
            // e.g. Terminal velocity
            state.acceleration.x = state.environmentAcceleration.x = 0.0f;
            state.velocity.x = 0.0f;
        }

        // Debug.Log(state.acceleration);
        state.acceleration = Vector2.zero;
        state.FrictionMultiplier = 1.0f;

        state.previousVelocity = state.velocity;
    }

    public void PostStep()
    {
        state.acceleration += state.environmentAcceleration;
		state.velocity += state.acceleration * Time.deltaTime;
		var motion = state.velocity * Time.deltaTime;
		var flags = state.controller.Move(motion);
		state.NumContacts = 0;
		
		var center = state.transform.position + state.controller.center;
		var radius = state.controller.radius;
		var height = state.controller.height / 2;
		var point1 = center + Vector3.up * (height - radius);
		var point2 = center + Vector3.down * (height - radius);

		state.environmentAcceleration = state.gravity;

		var contacts = new Contact[6];
		Array.Copy(state.Contacts, contacts, 6);
		state.AwayFromTerrain = Vector2.zero;

		void ContactResolution(Vector3 castDirection)
		{
			var count = Physics.CapsuleCastNonAlloc(point1, point2, radius, castDirection, _hits, 0.1f, UniBoost.Physics.PhysicsCollisionMatrix.GetMaskFromLayer(gameObject.layer), QueryTriggerInteraction.Ignore);

			for (var i = 0; i < count; i++)
			{
				var current = _hits[i];
				if (current.collider == state.controller) continue;

				var contact = new Contact(current.collider, new Terrain(), current.normal);
				var stage = current.collider.GetComponent<Stage>();
				if (stage)
				{
					stage.ApplyMomentum(state.mass * state.velocity);
				}
				state.Contacts[state.NumContacts++] = contact;
				state.AwayFromTerrain += contact.Normal;
				// Should only be in the ground check but it should be fine
				state.IsGrounded = state.IsGrounded || Vector3.Dot(current.normal, Vector3.up) > 0.9f;

				// Calculate the acceleration perpendicular to the normal of the surface
				var theta = Vector3.Angle(contact.Normal, -castDirection) * Mathf.Deg2Rad;
				var rotatedAcceleration = state.acceleration.RotateVector(theta);
				var rotatedVelocity = state.velocity.RotateVector(theta);

				// Will want something about shock absorption here
				var bounceFactor = state.CalculateBounceFactor(-rotatedVelocity.y, contact.Terrain);
				
				if (Mathf.Approximately(rotatedVelocity.x, 0.0f))
				{
					// static friction
					var coefficient =
						CalculateCoefficientOfStatic(state.friction * state.FrictionMultiplier, contact.Terrain.Friction);
					// Should be a frictional force, so multiplied by the mass but we're going to remove the mass later
					// so no point in adding it
					var frictionalAccelerationMax = coefficient * rotatedAcceleration.y;
					var diff = Mathf.Abs(rotatedAcceleration.x * state.mass) - Mathf.Abs(frictionalAccelerationMax);

					if (diff > 0.0f)
						rotatedAcceleration.x = Mathf.Sign(rotatedAcceleration.x) * diff;
					else
						rotatedAcceleration.x = 0.0f;

					// Setting the acceleration that runs counter surface normal
					// rotatedAcceleration.y = 0.0f;
					// rotatedVelocity.y = 0.0f;
					// Debug.Log("static");
				}
				else
				{
					// dynamic friction
					var coefficient =
						CalculateCoefficientOfKinetic(state.friction * state.FrictionMultiplier, contact.Terrain.Friction);
					var frictionalAcceleration = coefficient * rotatedAcceleration.y;
					
					rotatedAcceleration.x = -Mathf.Sign(rotatedVelocity.x) * Mathf.Abs(frictionalAcceleration);
					if (Mathf.Abs(rotatedAcceleration.x) < 0.2f) rotatedAcceleration.x = 0.0f;

					// rotatedAcceleration.y = 0.0f;
					// rotatedVelocity.y = 0.0f;
				}
				rotatedAcceleration.y = 0.0f;

				const float kBounceFactorForElasticCollisionThreshold = 0.19f;
				if (bounceFactor > kBounceFactorForElasticCollisionThreshold)
				{
					rotatedVelocity.y *= -bounceFactor;
				}
				else
				{
					// Inelastic collision, two bodies get stuck together
					rotatedVelocity.y = 0; // for now
				}

				state.environmentAcceleration += rotatedAcceleration.RotateVector(-theta);
				state.velocity = rotatedVelocity.RotateVector(-theta);
			}
		}

		if ((flags & CollisionFlags.Below) == CollisionFlags.Below)
		{
			state.IsGrounded = true;
			ContactResolution(Vector3.down);
		}
		else
		{
			// Debug.Log("Not Below");
			state.IsGrounded = false;
		}
		if ((flags & CollisionFlags.Above) == CollisionFlags.Above)
			ContactResolution(Vector3.up);
		if ((flags & CollisionFlags.Sides) == CollisionFlags.Sides)
		{
			ContactResolution(motion.x > 0.0f ? Vector3.right : Vector3.left);
		}
		
		state.AwayFromTerrain.Normalize();
		
		// Apply Drag
		// if (ApplyFluidAccel)
		{
			var scalars = 0.5f * state.CurrentFluid.MassDensity * /* Drag Coefficient */1.0f;
			
			var relativeVelocity = state.CurrentFluid.GetVelocityAtPosition(state.transform.position) - state.velocity;
			var relVelSign = new Vector2(Mathf.Sign(relativeVelocity.x), Mathf.Sign(relativeVelocity.y));
			relativeVelocity *= relativeVelocity * relVelSign;
			var fluidDrag = scalars * state.controller.height * 1.4f * relativeVelocity;
			var fluidAccel = fluidDrag / state.mass;
			_fluidAccel = fluidAccel;
			
			state.environmentAcceleration += fluidAccel;
		}
		
		for (var i = state.NumContacts; i < 6; i++)
			state.Contacts[i] = default;
		
		Physics.OverlapCapsuleNonAlloc(point1, point2, radius + 0.1f, _colliders);
		foreach (var col in _colliders)
		{
			if (state.NumContacts == state.Contacts.Length) break;
			if (col == state.controller) continue;
			if (contacts.All(c => c.Collider != col)) continue;
			if (state.Contacts.Any(c => c.Collider == col)) continue;

			var contact = contacts.First(c => c.Collider == col);
			state.Contacts[state.NumContacts++] = contact;
		}

		for (var i = state.NumContacts; i < 6; i++)
			state.Contacts[i] = default;
    }

    // Update is called once per frame
    void Update()
    {
        PreStep();
        PostStep();
    }
    
    // Values need to be between 0 and 1
    // Values are way too small right now
		
    float CalculateCoefficientOfStatic(float friction1, float friction2)
    {
	    return friction1 * friction2 * 1.5f;
    }
    float CalculateCoefficientOfKinetic(float friction1, float friction2)
    {
	    return friction1 * friction2 * 1.1f;
    }

    Vector2 _fluidAccel;
}
