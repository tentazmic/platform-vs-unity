﻿namespace Code.Core.Fighter
{
	public record MeterOrder(int Cost, float Factor)
	{
		public int Cost { get; } = Cost;
		public float Factor { get; } = Factor;
	}
}