﻿using System.Collections.Generic;
using System.Linq;
using Code.Combat;
using PlatformVS.Input;
using UnityEngine;
using SS = global::StateSystem;
using Sts = global::StateSystem.AllStates;

namespace Code.Core.Fighter
{
	public class Fighter : MonoBehaviour
	{
		public FighterState state;

		[SerializeField] GameObject attackStatesObject;

		StandardInput _input;
		public StateSystem.StateMachine<FighterState> Machine { get; private set; }

		int _snapTimer = 0, _snapCount = 0;

		void Awake()
		{
			var states = new List<SS.IState<FighterState>>
			{
				// Sts.Dash, Sts.GroundJump, Sts.AirJump, Sts.Guard,
				// Sts.HitStun, Sts.Slide, Sts.Skid, Sts.Sprint, Sts.Run, Sts.Walk,
				// Sts.RunTurnaround, Sts.Turnaround, Sts.STr_ClingScale,
				Sts.GMoveProgression, Sts.Turnaround,
				Sts.Stand, Sts.Airborne
			};

			state.AttackStates = attackStatesObject.GetComponents<AttackState>();
			states.AddRange(state.AttackStates.Select(attackState => attackState.GetState()));

			Machine = new SS.StateMachine<FighterState>(Sts.Airborne, SS.StateMachine<FighterState>.CreateTree(SS.FighterClauses.All, states.ToArray()));
			Machine.OnStateChanged += () => state.StateFrames = 0;
			Debug.Log(Machine.TreeRoot.Print(0, state));
			_input = new StandardInput(StartCoroutine);
			state.HitBoxes = GetComponentsInChildren<HitBox>();
			foreach (var hb in state.HitBoxes) hb.owner = this;
			foreach (var hb in GetComponentsInChildren<HurtBox>()) hb.owner = this;
		}

		int _frameTick = 0;

		void Update()
		{
			if (_input.Control.snap)
				state.FramesSinceStickSnap = 0;
			else
				state.FramesSinceStickSnap++;

			if (_snapTimer > 0) _snapTimer--;
			else _snapCount = 0;
			if (_input.Control.snap)
			{
				if (_snapTimer > 0)
					_snapCount++;
				_snapTimer = 8;
			}

			state.Input = new InputFrame
			{
				control = _input.Control.Value * (_input.Modifier ? 0.5f : 1.0f),
				stStick = _input.Control.State,
				snapCount = _snapCount,
				jump = _input.Jump.State,
				light = _input.Light.State,
				medium = _input.Medium.State,
				heavy = _input.Heavy.State,
				super = _input.Super.State,
				grab = _input.Grab.State,
				shield = _input.Shield.State
			};

			state.StateFrames++;
			Machine.Update(state);

			state.FramesSinceFactorChange++;
			if (state.meterFactor > 1.0f)
			{
				var factor = Mathf.Exp(state.FramesSinceFactorChange / 10000.0f) / 1300.0f;
				state.meterFactor -= factor;
			}

			if (state.baseMeter != state.totalMeter)
			{
				var factor = (int) (Mathf.Pow(state.FramesSinceFactorChange, 2) / 50000) + 1;
				if (state.totalMeter > state.baseMeter)
				{
					// state.totalMeter -= factor;
					if (state.totalMeter < state.baseMeter) state.totalMeter = state.baseMeter;
				}
				else
				{
					state.totalMeter += factor;
					if (state.totalMeter > state.baseMeter) state.totalMeter = state.baseMeter;
				}
			}

			if (state.ResetStateMachine)
			{
				state.ResetStateMachine = false;
				Machine.Reset(state);

				state.transform.position = Vector3.zero;
				state.acceleration = Vector2.zero;
				state.environmentAcceleration = Vector2.zero;
				state.velocity = Vector2.zero;
				state.previousVelocity = Vector2.zero;
			}

			Debug.Log(state.Die);
			if (state.Die)
			{
				state.ResetStateMachine = false;
				Machine.Reset(state);

				state.transform.position = Vector3.one * 5.0f;
				state.acceleration = Vector2.zero;
				state.environmentAcceleration = Vector2.zero;
				state.velocity = Vector2.zero;
				state.previousVelocity = Vector2.zero;

				this.enabled = false;
			}

			// _frameTick = ++_frameTick % 120;
			//
			// if (state.HitStun > 0)
			// {
			// 	if (state.HSType == HitStunType.Light)
			// 	{
			// 		if (_frameTick % 40 == 0) state.AddHitStun(-1);
			// 	}
			// 	else
			// 	{
			// 		HitStunType type = state.HSType;
			// 		state.AddHitStun(-1);
			// 		state.HSType = state.HSType == HitStunType.Free ? HitStunType.Free : type;
			// 	}
			// }
		}

		void OnGUI()
		{
			GUI.Label(new Rect(10.0f, 10.0f, 200.0f, 20.0f), Machine.Current.Name(state));
			GUI.Label(new Rect(10.0f, 20.0f, 200.0f, 20.0f), $"Velocity {state.velocity}");
			GUI.Label(new Rect(10.0f, 30.0f, 200.0f, 20.0f), $"Input {state.Input.control} {state.Input.stStick}");
			
			GUI.Label(new Rect(220.0f, 10.0f, 200.0f, 20.0f), $"HitStun {state.HitStun:000} Type {state.HSType.ToString()}");
		}
	}
}
