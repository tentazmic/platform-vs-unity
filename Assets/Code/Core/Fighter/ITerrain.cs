﻿using System;
using UnityEngine;

namespace Code.Core.Fighter
{
	/// <summary>
	/// Template for all physical objects
	/// </summary>
	public interface ITerrain
	{
		// event Action OnTerrainDataChanged;
		
		float Friction { get; }
		float Elasticity { get; }
		float Stability { get; }
		float RegrowRate { get; }

		// Vector2 GetAccelerationAtContact(Vector2 position, Vector2 normal);
		// Vector2 GetVelocityAtPosition(Vector2 position);
	}
}