using System;
using Code.Combat;
using PlatformVS.Core.VSEntity;
using UnityEditor.UIElements;
using UnityEngine;

namespace Code.Core.Fighter
{
    public class FighterState : MonoBehaviour
    {
        public new Transform transform;
        public CharacterController controller;

        public Vector2 gravity = Vector2.down * 9.81f;
        [Range(0, 10001)] public uint mass = 100;
        [Range(0, 1)] public float friction = 0.2f;

        public float bounceVelocityThreshold = 11f;
        [Range(0, 10)] public float bounciness = 1f;
        
        public int baseHealth = 1000;
        [NonSerialized] public int Health;
        [NonSerialized] public int GreyHealth;

        // Should be specific to the contact
        [NonSerialized] public float FrictionMultiplier = 1.0f;

        public Vector2 previousVelocity;
        public Vector2 velocity;
        public Vector2 acceleration;
        public Vector2 environmentAcceleration;
        public Facing facingDirection;

        public IFluid CurrentFluid;

        public Contact[] Contacts = new Contact[6];
        public int NumContacts = 0;

        [NonSerialized]
        public bool IsGrounded;

        // [NonSerialized]
        public InputFrame Input;

        public Action OnNewAttack;

        public enum StRun
        {
            None,
            Run
        };

        [NonSerialized] public StRun RunState = StRun.None;

        public int baseMeter = 1000;
        public int totalMeter = 1000;
        public float meterFactor = 1.0f;
        [NonSerialized] public int FramesSinceFactorChange;

        [SerializeField] int mediumHitStunThreshold = 110;
        [SerializeField] int heavyHitStunThreshold = 3 * 60;
        public int HitStun;
        [NonSerialized] public HitBox[] HitBoxes;

        [NonSerialized] public HitStunType HSType = HitStunType.Free;

        // TODO Replace with outVector, something calculated by the physics
        // to determine what direction is up while standing
        [NonSerialized]
        public Vector2 AwayFromTerrain;

        [NonSerialized] public int StateFrames;
        [NonSerialized] public int FrameMarker;

        [NonSerialized] public int FramesSinceStickSnap;

        [NonSerialized] public Vector2 JumpDirection;

        [NonSerialized] public AttackState[] AttackStates;
        [NonSerialized] public AttackState CurrentAttackState;
        [NonSerialized] public bool AttackAnimationJustFinished;
        
        [NonSerialized] public bool ResetStateMachine;

        [NonSerialized] public bool Die;

        public bool Guarding;
        [NonSerialized] public bool BreakGuard;
        public int blockStun;

        public void ConsumeMeter(MeterOrder order)
        {
            var cost = (int) (order.Cost * meterFactor);
            totalMeter -= cost;
            meterFactor += order.Factor;
            FramesSinceFactorChange = 0;
        }

        public bool HaveEnoughMeter(MeterOrder order)
        {
            var cost = (int) (order.Cost * meterFactor);
            Debug.Log($"{cost} {totalMeter}");
            return totalMeter >= cost;
        }

        public void AddHitStun(int hitStun)
        {
            HitStun += hitStun;

            if (HitStun > heavyHitStunThreshold) HSType = HitStunType.Heavy;
            else if (HitStun > mediumHitStunThreshold) HSType = HitStunType.Medium;
            else if (HitStun > 0) HSType = HitStunType.Light;
            else HSType = HitStunType.Free;
        }

        public float CalculateBounceFactor(float approachSpeed, ITerrain terrain)
        {
            const float kBounceFactorCeiling = 1.1f;
            const float kMaxBounceFactorPlateauReachingSpeed = -0.5f;
            const float kPlateauReachingSpeed = 2.0f / 30.0f;
            var maxBounceFactor = terrain.Elasticity * bounciness;
            // maxBounceFactor = Mathf.Log(0.9f * maxBounceFactor + 1);
            maxBounceFactor =
                kBounceFactorCeiling * (1 - Mathf.Exp(kMaxBounceFactorPlateauReachingSpeed * maxBounceFactor));
            var bounceFactor = maxBounceFactor * (1 - Mathf.Exp(-kPlateauReachingSpeed * approachSpeed));

            return bounceFactor;
            // In future the velocity threshold should be calculated from the terrain's elasticity
            // and the character's bounciness
            if (bounceVelocityThreshold > approachSpeed)
                return -1f;

            approachSpeed -= bounceVelocityThreshold;
            approachSpeed /= 50.0f;
            var tmp = terrain.Elasticity * (1 - Mathf.Exp(-bounciness * approachSpeed));
            Debug.Log($"Bounce Factor: {tmp}");
            return tmp;
        }

        void Awake()
        {
            Health = baseHealth;
            GreyHealth = baseHealth;
        }
    }
}
