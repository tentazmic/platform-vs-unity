using System;
using System.Collections;
using System.Collections.Generic;
using Code.Core.Fighter;
using PlatformVS.Core.VSEntity;
using StateSystem;
using UnityEngine;

public class FighterAnimation : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] FighterState fighterState;

    readonly int _anm_Stand = Animator.StringToHash("idle");
    readonly int _anm_Walk = Animator.StringToHash("walk");
    readonly int _anm_Aerial = Animator.StringToHash("aerial");
    readonly int _anm_JumpSquat = Animator.StringToHash("jumpsquat");

    readonly int _anm_Cling = Animator.StringToHash("crouch");
    readonly int _anm_Scale = Animator.StringToHash("crawl");

    readonly int _anm_Dash = Animator.StringToHash("initial_dash");
    readonly int _anm_Sprint = Animator.StringToHash("sprint");
    readonly int _anm_Skid = Animator.StringToHash("skid");
    readonly int _anm_Slide = Animator.StringToHash("slide");

    readonly int _anm_GroundLocomotion = Animator.StringToHash("groundlocomotion");
    readonly int _anm_GroundLocoRatio = Animator.StringToHash("groundloco_ratio");
    bool _inGroundState = false;

    delegate int AnimIDGetter();
    Dictionary<StateSystem.IState<FighterState>, AnimIDGetter> _stateAnimationMap;

    global::StateSystem.StateMachine<FighterState> _machine;
    bool _rising = false;
    Transform _characterTransform;
    FighterState _fighterState;

    // Start is called before the first frame update
    void Start()
    {
        _fighterState = GetComponent<FighterState>();
        _machine = GetComponent<Fighter>().Machine;
        _machine.OnStateChanged += OnStateChanged;

        _characterTransform = animator.transform;
        _stateAnimationMap = new Dictionary<IState<FighterState>, AnimIDGetter>
        {
            { StateSystem.AllStates.Stand, () => _anm_Stand },
            { StateSystem.AllStates.Airborne, () => _anm_Aerial },
            { StateSystem.AllStates.GMoveProgression, () =>
                {
                    _inGroundState = true;
                    return _anm_GroundLocomotion;
                }
            },
            // { AllStates.Jump, _anm_JumpSquat },
            { StateSystem.AllStates.Cling, () => _anm_Cling },
            { StateSystem.AllStates.Scale, () => _anm_Scale },
            { StateSystem.AllStates.Dash, () => _anm_Dash },
            { AllStates.Sprint, () => _anm_Sprint },
            { StateSystem.AllStates.Skid, () => _anm_Skid },
            { StateSystem.AllStates.Slide, () => _anm_Slide }
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (_machine.Current == StateSystem.AllStates.Airborne)
        {
            animator.Play(_anm_JumpSquat, 0);
            // if (!_rising && fighterState.velocity.y > 1.0f)
            // {
            //     _rising = true;
            //     armatureAnimator.Play(_anm_jlaunch, 0);
            // }
            // else if (_rising && fighterState.velocity.y < 1.0f)
            // {
            //     _rising = false;
            //     armatureAnimator.Play(_anm_Aerial, 0);
            // }
        }

        _characterTransform.right = fighterState.facingDirection switch
        {
            Facing.Left => Vector3.right,
            Facing.Right => Vector3.left,
            _ => _characterTransform.right
        };

        if (_inGroundState)
        {
            var vel = Mathf.Abs(fighterState.velocity.x);
            var f = Mathf.Clamp(vel, 4.0f, 11.5f) - 4.0f ;
            f /= (11.5f - 4.0f);
            animator.SetFloat(_anm_GroundLocoRatio, f);
        }
    }

    // int _frame = 0;

    void LateUpdate()
    {
        if (!fighterState.CurrentAttackState)
        {
            return;
        }

        fighterState.AttackAnimationJustFinished = animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1.0f;
        
        foreach (var hitbox in fighterState.HitBoxes)
        {
            var t = hitbox.transform;
            hitbox.Active = Mathf.Sign(t.position.y) > 0;
        }
    }

    void OnStateChanged()
    {
        _inGroundState = false;
        if (_stateAnimationMap.ContainsKey(_machine.Current))
            animator.Play(_stateAnimationMap[_machine.Current.AnimationState(_fighterState)](), 0);

        if (fighterState.CurrentAttackState)
            animator.Play(fighterState.CurrentAttackState.attackAnimation, 0);
    }
}
