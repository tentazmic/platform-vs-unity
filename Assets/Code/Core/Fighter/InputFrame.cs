﻿using UniBoost.Input;
using UnityEngine;

namespace Code.Core.Fighter
{
	[System.Serializable]
	public struct InputFrame
	{
		public Vector2 control;
		public ControlState stStick;
		public int snapCount;
		public ControlState jump;
		public ControlState light;
		public ControlState medium;
		public ControlState heavy;
		public ControlState super;
		public ControlState grab;
		public ControlState shield;

		public bool EquivalentPrimaryAttackButtons(in InputFrame other)
		{
			return light == other.light && medium == other.medium && heavy == other.heavy;
		}
	}
}