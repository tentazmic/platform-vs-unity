﻿using System;
using UnityEngine;

namespace Code.Core.Fighter
{
	public readonly struct Contact
	{
		public readonly Collider Collider;
		public readonly ITerrain Terrain;
		public readonly Vector2 Normal;

		public Contact(Collider collider, ITerrain terrain, Vector2 normal)
		{
			Collider = collider;
			Terrain = terrain;
			Normal = normal;
		}
	}
}