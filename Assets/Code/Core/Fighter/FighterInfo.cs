using System.Collections;
using System.Collections.Generic;
using Code.Core.Fighter;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FighterInfo : MonoBehaviour
{
    [SerializeField] FighterState fighterState;
    [SerializeField] DamageReceiver damageReceiver;

    [SerializeField] TMP_Text uiDamage;
    [SerializeField] TMP_Text uiHitStun;
    [SerializeField] Slider uiMeter;
    [SerializeField] Slider uiMeterFactor;

    void Update()
    {
        // uiDamage.text = $"{fighterState.damage:00}";
        uiHitStun.text = $"{fighterState.HitStun:000}";
        uiMeter.value = ((fighterState.totalMeter % fighterState.baseMeter) / (float)fighterState.baseMeter);
        uiMeterFactor.value = fighterState.meterFactor / 5.0f;
    }
}
