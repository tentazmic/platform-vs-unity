using System.Collections;
using System.Collections.Generic;
using Code.Core.Fighter;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FighterHUD : MonoBehaviour
{
    [SerializeField] FighterState fighter;
    
    [SerializeField] TMP_Text uiHitStun;

    [SerializeField] Slider uiHealth, uiGreyHealth;

    [SerializeField] TMP_Text uiBarCount;
    [SerializeField] Slider uiMeter, uiMeterConsumptionRate;
    
    
    void Update()
    {
        uiHitStun.text = $"{fighter.HitStun:000}";

        uiHealth.value = fighter.Health / (float)fighter.baseHealth;
        uiGreyHealth.value = fighter.GreyHealth / (float)fighter.baseHealth;

        uiBarCount.text = $"{(fighter.totalMeter / (float)fighter.baseMeter):00}";
        uiMeter.value = fighter.totalMeter % fighter.baseMeter / (float)fighter.baseMeter;
        uiMeterConsumptionRate.value = fighter.meterFactor / 5.0f;
    }
}
