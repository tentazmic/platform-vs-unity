﻿using UnityEngine;

namespace Code.Core.Fighter
{
	/// Template for all fluids, both liquid and gas
	public interface IFluid
	{
		/// How hard it is to move through through the fluid
		float MassDensity { get; }

		/// How much the fluid can be displaced
		float Elasticity { get; }

		/// How hard it is to break into the fluid
		float SurfaceTension { get; }

		/// Gets the direction and magnitude the fluid flows in at the provided position
		Vector2 GetVelocityAtPosition(Vector3 position);
	}

	public class Fluid : IFluid
	{
		public float MassDensity => 1.0f;
		public float Elasticity => 1.0f;
		public float SurfaceTension => 1.0f;
		
		public Vector2 GetVelocityAtPosition(Vector3 position) => Vector2.zero;
	}
}
