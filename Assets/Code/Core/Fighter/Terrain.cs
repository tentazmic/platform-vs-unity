﻿using System;
using UnityEngine;

namespace Code.Core.Fighter
{
	[Serializable]
	public class Terrain : ITerrain
	{
		[SerializeField, Range(0, 1)] float friction = 0.6f;
		[SerializeField, Range(0, 1)] float elasticity = 0.5f;
		[SerializeField, Range(0, 1)] float stability = 0.5f;
		[SerializeField] float regrowRate = 1;

		// public event Action OnTerrainDataChanged;
		
		public float Friction => friction;
		public float Elasticity => elasticity;
		public float Stability => stability;
		public float RegrowRate => regrowRate;
		public Vector2 GetVelocityAtPosition(Vector2 position) => Vector2.zero;
	}
}