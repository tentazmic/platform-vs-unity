﻿using System;
using UnityEngine;

namespace Code.Core
{
    [RequireComponent(typeof(Collider))]
    public class CollisionEventForwarder : MonoBehaviour
    {
        private Action<Collider> _onTriggerEnter;
        private Action<Collider> _onTriggerStay;
        private Action<Collider> _onTriggerExit;
        
        public event Action<Collider> OnTriggerEnterEvent
        {
            add => _onTriggerEnter += value;
            remove => _onTriggerEnter += value;
        }
        
        public event Action<Collider> OnTriggerStayEvent
        {
            add => _onTriggerEnter += value;
            remove => _onTriggerEnter += value;
        }
        
        public event Action<Collider> OnTriggerExitEvent
        {
            add => _onTriggerEnter += value;
            remove => _onTriggerEnter += value;
        }

        private void OnTriggerEnter(Collider other)
        {
            _onTriggerEnter?.Invoke(other);
        }

        private void OnTriggerStay(Collider other)
        {
            _onTriggerStay?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            _onTriggerExit?.Invoke(other);
        }
    }
}