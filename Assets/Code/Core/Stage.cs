using System;
using System.Collections.Generic;
using StateSystem;
using UniBoost.Extensions;
using UnityEngine;
using ClStage = StateSystem.StateClause<Stage>;
using StStage = StateSystem.StSingle<Stage>;

public static class StageClauses
{
    public static ClStage NoVelocity = new ClStage(nameof(NoVelocity), 2,
        fs => fs.Velocity == Vector3.zero ? ESCBool.True : ESCBool.False);

    public static ClStage ExceedLaunchTime = new ClStage(nameof(ExceedLaunchTime), 2,
        fs => fs.DisplacementTime > 42 ? ESCBool.True : ESCBool.False);

    public static ClStage Launch = new(nameof(Launch), 2, fs => fs.LaunchTriggered ? ESCBool.True : ESCBool.False);

    public static ClStage IsAtRest = new(nameof(IsAtRest), 2,
        fs => fs.RestPosition == fs.transform.position ? ESCBool.True : ESCBool.False);

    public static ClStage[] All = { IsAtRest, Launch, NoVelocity, ExceedLaunchTime };
}

public static class StageStates
{
    public static StStage Rest = new(
        nameof(Rest),
        _ => true,
        _ => { },
        delegate(Stage fs, ref EStateOutput output)
        {
            
        },
        delegate { },
        new Dictionary<ClStage, byte>
        {
            { StageClauses.IsAtRest, ESCBool.True }
        }
    );
    
    public static StStage Launch = new(
        nameof(Launch),
        _ => true,
        fs => fs.LaunchTriggered = false,
        delegate(Stage fs, ref EStateOutput output)
        {
            fs.DisplacementTime++;
        },
        delegate { },
        new Dictionary<ClStage, byte>
        {
            { StageClauses.Launch, 1 }
        }
    );

    public static StStage Dampening = new(
        nameof(Dampening),
        _ => true,
        _ => { },
        delegate(Stage fs, ref EStateOutput output)
        {
            var reverse = -fs.Velocity.normalized * fs.returnAcceleration;
            var velChange = reverse * Time.deltaTime;
            if (velChange.magnitude > fs.Velocity.magnitude)
                fs.Velocity = Vector2.zero;
            else
                fs.Velocity += velChange;
        },
        delegate { },
        new Dictionary<ClStage, byte>
        {
            { StageClauses.ExceedLaunchTime, ESCBool.True }
        }
    );

    public static StStage Return = new(
        nameof(Return),
        _ => true,
        fs => { fs.DisplacementTime = 0;},
        delegate(Stage fs, ref EStateOutput output)
        {
            var offset = fs.RestPosition - fs.transform.position;
            if (offset.magnitude > 1.4f)
                fs.Velocity += fs.returnAcceleration * Time.deltaTime * offset.normalized;
            else if (offset.magnitude < 0.05f)
            {
                fs.Velocity = Vector3.zero;
                fs.transform.position = fs.RestPosition;
            }
            else
                fs.Velocity = fs.Velocity.AccelerateTo(fs.returnAcceleration / 74.0f * offset.normalized, Vector2.one * fs.returnAcceleration);
        },
        delegate { },
        new Dictionary<ClStage, byte>
        {
            { StageClauses.ExceedLaunchTime, ESCBool.True },
            { StageClauses.NoVelocity, ESCBool.True }
        }
    );
}

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class Stage : MonoBehaviour
{
    [Range(0, 60.0f)]
    public float returnAcceleration = 18.0f;

    public float stubbornness = 10.0f;

    [NonSerialized] public Vector3 RestPosition;
    
    // [NonSerialized]
    public Vector3 Velocity = Vector3.zero;

    // [NonSerialized] 
    public int DisplacementTime = 0;

    // [NonSerialized] 
    public bool LaunchTriggered = false;

    StateMachine<Stage> _machine;
    Rigidbody _rigidbody;

    void Awake()
    {
        Velocity = Vector3.zero;
        _rigidbody = GetComponent<Rigidbody>();
        RestPosition = transform.position;
        _machine = new StateMachine<Stage>(StageStates.Rest, StateMachine<Stage>.CreateTree(StageClauses.All, StageStates.Rest, StageStates.Launch,  StageStates.Return, StageStates.Dampening));
        // ApplyMomentum(new Vector3(0, -500.0f));
    }
    
    void FixedUpdate()
    {
        _machine.Update(this);
        _rigidbody.MovePosition(_rigidbody.position + Velocity * Time.deltaTime);
        Debug.Log(_machine.Current.Name(this));
    }

    public void ApplyMomentum(Vector3 momentum)
    {
        var vel = momentum / _rigidbody.mass;
        if (vel.magnitude < stubbornness)
            return;
        
        Velocity += vel;
        LaunchTriggered = true;
        _machine.Reset(this);
    }
}
