﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UniBoost.Extensions;

struct BodyRelation
{
	public bool InContact;
}

public class PhySqManager : MonoBehaviour
{
	public PhySqBody[] bodies;


	// Defines the initial approach ratio of the two bodies when they last met
	// A value of 1 means an approach vector (0, 1) or (0, -1)
	// A value of 0 means an approach vector (1, 0) or (-1, 0)
	readonly IDictionary<(PhySqBody, PhySqBody), BodyRelation> _relations =
		new Dictionary<(PhySqBody, PhySqBody), BodyRelation>();

	void Awake()
	{
		if (bodies.Length == 0)
			bodies = FindObjectsOfType<PhySqBody>(); 
		_relations.Add((bodies[0], bodies[1]), new BodyRelation{ InContact = false });
	}

	/*
	 * Upon contact with another body, the rigidity makes adjustments to the characters position
	 * saying, "You should be here" eating up some of the bodies speed in the adjustment

	/*
	 * FlexResolution
	 * 
	 * Find the overlaps of each point, and shift them back until it no longer overlaps
	 * Apply a force on the body using the compression value
	 */

	public class PointContact
	{
		public bool active = true;
		public Vector2 awayNormal;
		public float factor = float.MaxValue;
	}

	public static float ProjectAlongVector(Vector2 vector, Vector2 point) => Vector2.Dot(vector, point);

	public static Vector3[] GetFlexBox(PhySqBody body) => body.FlexBox;

	public struct PlaneData
    {
		public Vector2 pos1, pos2;
		public Vector2 normal;
		public float zero;
    }

	public static PlaneData CalcPlaneData(Vector2 pos1, Vector2 pos2)
    {
		Vector2 plane = pos1 - pos2;
		PlaneData data;
		data.pos1 = pos1;
		data.pos2 = pos2;
		data.normal = plane.GetNormal().normalized;
		data.zero = ProjectAlongVector(data.normal, pos1);
		return data;
	}

	public static float GetPlaneOverlapFactor(Vector3 point, PlaneData data)
    {
		var fct = ProjectAlongVector(data.normal, point);
		return fct - data.zero;
    }

	public static void FindBoxOverlaps(PhySqBody left, PhySqBody right, PointContact[] contactsL, PointContact[] contactsR, Func<PhySqBody, Vector3[]> getBox = null)
	{
		Vector3[] lBox, rBox;
		if (getBox == null)
		{
			lBox = GetFlexBox(left);
			rBox = GetFlexBox(right);
		}
		else
		{
			lBox = getBox(left);
			rBox = getBox(right);
		}

		var posL = left.midPoint.position + left.physicsMotion;
		var posR = right.midPoint.position + right.physicsMotion;
		var rToL = ((Vector2)(left.midPoint.position - right.midPoint.position)).normalized;


		void Check(PhySqBody body, Vector3 myPos, Vector2 selfToOther, Vector3[] box, int index, in PlaneData data, ref PointContact[] contacts, bool log = false)
		{
			var flexPoint = myPos + box[index];
			var fct = ProjectAlongVector(data.normal, flexPoint);
			var factor = fct - data.zero;
			contacts[index].active &= factor < 0;
			if (!contacts[index].active || -factor > contacts[index].factor) return;

			var castVector = body.state.velocity.normalized;
			
			if (Vector2.Dot(data.normal, castVector) > 0)
				castVector = -castVector;

			var leftData = CalcPlaneData(data.pos1, data.pos1 - castVector);
			var rightData = CalcPlaneData(data.pos2, data.pos2 + castVector);
			//if (log) Debug.Log($"{body.name} {index} {castVector} {data.normal} L {leftData.normal} R {rightData.normal}");

			if (!(GetPlaneOverlapFactor(flexPoint, leftData) < 0 && GetPlaneOverlapFactor(flexPoint, rightData) < 0))
				return;

			//if (index > 1) Debug.Log($"{body.name} {body.state.velocity.normalized} {selfToOther} {data.normal} {Vector2.Dot(body.state.velocity.normalized, selfToOther)} L {leftData.normal} R {rightData.normal}");
			contacts[index].factor = -factor;
			contacts[index].awayNormal = data.normal;
		}

		//void OldCheck(PhySqBody body, int iCheck, Vector3[] box, Vector2 planeNormal, float zero, ref PointContact[] contacts)
		//{
		//	var flexPoint = box[iCheck] + body.midPoint.position + body.physicsMotion;
		//	var fct = ProjectAlongVector(planeNormal, flexPoint);
		//	var factor = fct - zero;
		//	contacts[iCheck].active &= factor < 0;
		//	if (contacts[iCheck].active && factor < 0 && -factor < contacts[iCheck].factor)
		//	{
		//		contacts[iCheck].factor = -factor;
		//		contacts[iCheck].awayNormal = planeNormal;
		//	}
		//}

		for (int iAgainst = 0; iAgainst < 4; iAgainst++)
		{
			Vector2 lPlane1 = lBox[iAgainst] + posL, lPlane2 = lBox[(iAgainst + 1) % 4] + posL;
			var planeDataL = CalcPlaneData(lPlane1, lPlane2);
			Vector2 rPlane1 = rBox[iAgainst] + posR, rPlane2 = rBox[(iAgainst + 1) % 4] + posR;
			var planeDataR = CalcPlaneData(rPlane1, rPlane2);
			//Vector2 flexPlaneL = lBox[iAgainst] - lBox[(iAgainst + 1) % 4];
			//var flexNormalL = flexPlaneL.GetNormal().normalized;
			//var flexZeroPointL = ProjectAlongVector(flexNormalL, posL + lBox[iAgainst]);
			//Vector2 flexPlaneR = rBox[iAgainst] - rBox[(iAgainst + 1) % 4];
			//var flexNormalR = flexPlaneR.GetNormal().normalized;
			//var flexZeroPointR = ProjectAlongVector(flexNormalR, posR + rBox[iAgainst]);

			for (var iCheck = 0; iCheck < 4; iCheck++)
			{
				Check(left, posL, -rToL, lBox, iCheck, planeDataR, ref contactsL, true);
				Check(right, posR, rToL, rBox, iCheck, planeDataL, ref contactsR);
				//Check(left, iCheck, lBox, flexNormalR, flexZeroPointR, ref contactsL);
				//Check(right, iCheck, rBox, flexNormalL, flexZeroPointL, ref contactsR);
			}
		}
	}

	public static void FindClosestEdge(PhySqBody left, PhySqBody right, PointContact[] contactsL, PointContact[] contactsR, List<int> indexesL, List<int> indexesR, Func<PhySqBody, Vector3[]> getBox = null)
	{
		var posL = left.midPoint.position + left.physicsMotion;
		var posR = right.midPoint.position + right.physicsMotion;
		Vector3[] lBox, rBox;
		if (getBox == null)
		{
			lBox = GetFlexBox(left);
			rBox = GetFlexBox(right);
		}
		else
		{
			lBox = getBox(left);
			rBox = getBox(right);
		}


		void Check(PhySqBody body, int iCheck, Vector3[] box, PlaneData planeData, ref PointContact[] contacts)
		{
			var flexPoint = box[iCheck] + body.midPoint.position + body.physicsMotion;
			var fct = ProjectAlongVector(planeData.normal, flexPoint);
			Debug.Log($"{body.name} {fct} {planeData.normal} {planeData.zero}");
			var factor = Mathf.Abs(fct - planeData.zero);
			if (factor < contacts[iCheck].factor)
			{
				//Debug.Log($"{body.name}: {factor} {planeData.normal}");
				contacts[iCheck].factor = factor;
				contacts[iCheck].awayNormal = planeData.normal;
			}
		}

		for (int iAgainst = 0; iAgainst < 4; iAgainst++)
		{
			//Vector2 flexPlaneL = lBox[iAgainst] - lBox[(iAgainst + 1) % 4];
			//var flexNormalL = flexPlaneL.GetNormal().normalized;
			//var flexZeroPointL = ProjectAlongVector(flexNormalL, posL + lBox[iAgainst]);
			//Vector2 flexPlaneR = rBox[iAgainst] - rBox[(iAgainst + 1) % 4];
			//var flexNormalR = flexPlaneR.GetNormal().normalized;
			//var flexZeroPointR = ProjectAlongVector(flexNormalR, posR + rBox[iAgainst]);
			var planeDataL = CalcPlaneData(lBox[iAgainst] + posL, lBox[(iAgainst + 1) % 4] + posL);
			//planeDataL.zero = Math.Abs(planeDataL.zero);
			var planeDataR = CalcPlaneData(rBox[iAgainst] + posR, rBox[(iAgainst + 1) % 4] + posR);
			//planeDataR.zero = Math.Abs(planeDataR.zero);

			for (var iCheck = 0; iCheck < 4; iCheck++)
			{
				if (indexesL.Contains(iCheck))
					Check(left, iCheck, lBox, planeDataR, ref contactsL);
				if (indexesR.Contains(iCheck))
					Check(right, iCheck, rBox, planeDataL, ref contactsR);
			}
		}
	}

	public static void RigidityAdjustment(PhySqBody left, PhySqBody right)
	{
		var contactsL = new[] { new PointContact(), new PointContact(), new PointContact(), new PointContact() };
		var contactsR = new[] { new PointContact(), new PointContact(), new PointContact(), new PointContact() };
		FindBoxOverlaps(left, right, contactsL, contactsR);

		const float kInfiniteMassThreshold = 10000.0f;
		var rigidityTotal = left.rigidity + right.rigidity;
		var massTotal = left.state.mass + right.state.mass;
		var infiniteMassRating = Convert.ToInt32(left.state.mass > kInfiniteMassThreshold) + Convert.ToInt32(right.state.mass > kInfiniteMassThreshold);

		PointContact l = new PointContact(), r = new PointContact();
		for (int i = 0; i < 4; i++)
		{
			l.awayNormal += contactsL[i].awayNormal;
			if (contactsL[i].active && contactsL[i].factor < l.factor)
				l.factor = contactsL[i].factor;
			r.awayNormal += contactsR[i].awayNormal;
			if (contactsR[i].active && contactsR[i].factor < r.factor)
				r.factor = contactsR[i].factor;
		}

		if (l.factor > 1000.0f && r.factor > 1000.0f)
			return;
		if (l.factor > 1000.0f)
			l.factor = -1f;
		if (r.factor > 1000.0f)
			r.factor = -1f;

		if (l.factor > r.factor)
		{
			r.factor = l.factor;
			r.awayNormal = -l.awayNormal;
		}
		else
		{
			l.factor = r.factor;
			l.awayNormal = -r.awayNormal;
		}

		//Debug.Log($"{l.factor} {l.awayNormal}");

		void Resolve(PhySqBody body, PointContact contact)
		{
			// The more massive you are, the less you move
			// The more rigid you are, the less you move
			// Unless scaled, this alone will move both body's so they are touching each other
			// The amount you move is scaled by a set value/something to do with your velocity?

			// It seems as though this part is responsible for preserving momentum
			body.adjustmentVector += (Vector3)contact.awayNormal;
			var ratRigid = 1.0f - (body.rigidity / rigidityTotal);
			float ratMass;
			if (infiniteMassRating == 1)
            {
				if (body.state.mass < kInfiniteMassThreshold)
				{
					ratMass = 1;
					ratRigid = 1;
				}
				else
				{
					ratMass = 0;
					ratRigid = 0;
				}
            }
			else
				ratMass = 1.0f - ((float)body.state.mass / massTotal);
			const float kShiftScale = 0.4f;
			var mag = kShiftScale * contact.factor * (ratRigid + ratMass) / 2.0f;
			body.adjustmentMagnitude = Mathf.Max(mag, body.adjustmentMagnitude);
		}

		Resolve(left, l);
		Resolve(right, r);
	}

	// We find the most appropriate edge to snap the points to, which is the closest edge of the point on the framestart
	// box
	public static void FlexResolution(PhySqBody left, PhySqBody right)
	{
		var contactsL = new[] { new PointContact(), new PointContact(), new PointContact(), new PointContact() };
		var contactsR = new[] { new PointContact(), new PointContact(), new PointContact(), new PointContact() };
		FindBoxOverlaps(left, right, contactsL, contactsR);

		List<int> indexesL = new(), indexesR = new();
		for (int i = 0; i < 4; i++)
		{
			if (contactsR[i].active) indexesR.Add(i);
			if (contactsL[i].active) indexesL.Add(i);
		}
		if (indexesL.Count() + indexesR.Count() == 0) return;

		// Find the closest plane of the indexes in contact at frame start, then move the
		FindClosestEdge(left, right, contactsL, contactsR, indexesL, indexesR);

		void Resolve(PhySqBody body, PhySqBody other, int index, in PointContact contact)
		{
			body.FlexBox[index] += (Vector3)contact.awayNormal.normalized * contact.factor;
			if (body.ShockAbsorptionFactors[index] < 0)
				body.ShockAbsorptionFactors[index] = other.shockAbsorption;
			else
			{
				body.ShockAbsorptionFactors[index] += other.shockAbsorption;
				//body.ShockAbsorptionFactors[index] /= 2.0f;
			}
		}

		for (int i = 0; i < 4; i++)
		{
			right.FlexContacts[i] |= contactsR[i].active;
			left.FlexContacts[i] |= contactsL[i].active;
			if (contactsR[i].active)
				Resolve(right, left, i, contactsR[i]);
			if (contactsL[i].active)
				Resolve(left, right, i, contactsL[i]);
		}
	}

	public static void BodyResolution(PhySqBody body)
    {
		// We do a check for each flex point to goes outside its bounds into the rigid
		// Move the rigid box (add to the physics motion) so it is no longer inside the bounds
		// The resultant velocity is calculated using the new rigid motion

		// Shock absorption occurs by the combination of the compression the point and the shock absorption
		// Shock absorption reduces the velocity to 0
		// Collision reflection occurs when a flex point cannot continue to compress in the
		// direction it wants to
		// The velocity is reduced a bit and then reflected along the collision plane

		Vector2 GetAdjustmentVector(int index, int prev, int next)
        {
			Vector2 planeN = body.RigidBox[index] - body.RigidBox[next];
			var normalN = planeN.GetNormal().normalized;
			var zeroN = ProjectAlongVector(normalN, body.RigidBox[index]);
			Vector2 planeP = body.RigidBox[prev] - body.RigidBox[index];
			var normalP = planeP.GetNormal().normalized;
			var zeroP = ProjectAlongVector(normalP, body.RigidBox[index]);

			var factorN = ProjectAlongVector(normalN, body.FlexBox[index]) - zeroN;
			var factorP = ProjectAlongVector(normalP, body.FlexBox[index]) - zeroP;
			//Debug.Log($"{normalN} {factorN} : {normalP} {factorP}");

			if (factorN < factorP)
				return factorN * normalN;
			return factorP * normalP;
        }

		var mag = 0f;
		var rigidResolutionVector = Vector2.zero;
		var velocityAbsorption = Vector2.zero;
		int l = body.RigidBox.Length;
		for (
			int curr = 0, prev = body.RigidBox.Length - 1, next = 1; curr < body.RigidBox.Length;
			curr++, prev = ++prev % l, next = ++next % l)
        {
			Vector2
			  badDir1 = (body.RigidBox[prev] - body.RigidBox[curr]).normalized,
			  badDir2 = (body.RigidBox[next] - body.RigidBox[curr]).normalized;

			Vector2 flexVec = body.FlexBox[curr] - body.RigidBox[curr];
			var normalised = flexVec.normalized;
			var dot1 = Vector2.Dot(badDir1, normalised);
			var dot2 = Vector2.Dot(badDir2, normalised);

			// The rigid box overlaps with the flex box
			if (dot1 > 0 || dot2 > 0)
            {
				var vec = GetAdjustmentVector(curr, prev, next);
				mag = Mathf.Max(mag, vec.sqrMagnitude);
				Debug.Log($"{body.name}, {curr}: {vec}");
				rigidResolutionVector += vec.normalized;

				// Our velocity moves us away so passedzero
				continue;
            }

			if (body.ShockAbsorptionFactors[curr] > 0)
			{
				var rigidDiff = (Vector2)(body.RigidBox[curr] - body.FullBox[curr]);
			var compressionVector = (Vector2)(body.FlexBox[curr] - body.FullBox[curr]);
				//float compressionFactor = compressionVector.sqrMagnitude / ((Vector2)(body.RigidBox[curr] - body.FullBox[curr])).sqrMagnitude;
				//if (body.IsInFlexible)
				//	compressionFactor = 1;
				//else
				//	compressionFactor = compressionVector.sqrMagnitude / ((Vector2)(body.RigidBox[curr] - body.FullBox[curr])).sqrMagnitude;
				var compressionFactor = compressionVector.sqrMagnitude / rigidDiff.sqrMagnitude;
				var factor = (body.shockAbsorption + body.ShockAbsorptionFactors[curr]);
				factor *= compressionFactor;
				velocityAbsorption += factor * compressionVector.normalized;
			}
		}

		body.state.previousVelocity = body.state.velocity;
		body.state.velocity = body.physicsMotion / Time.deltaTime;
		body.state.transform.position += body.physicsMotion;
		body.state.acceleration += body.state.environmentAcceleration;
		if (rigidResolutionVector != Vector2.zero)
        {
			//Debug.Log(rigidResolutionVector);
			rigidResolutionVector = rigidResolutionVector.normalized;
			mag = Mathf.Sqrt(mag);
			//Debug.Log($"{body.name} {rigidResolutionVector} {mag / Time.deltaTime}");
			body.physicsMotion += (Vector3)(rigidResolutionVector * mag);
			var rawResolutionVelocity = rigidResolutionVector * (mag / Time.deltaTime);

			var theta = Vector2.Angle(rigidResolutionVector, Vector2.up) * Mathf.Deg2Rad;
			var rotResolution = rawResolutionVelocity.RotateVector(theta);
			var rotVelocity = body.state.velocity.RotateVector(theta);
			rotVelocity.y = rotResolution.y;
			body.state.velocity = rotVelocity.RotateVector(-theta);

			//body.state.velocity = (rigidResolutionVector * mag) / Time.deltaTime;
        }
		else
        {
			
			body.state.velocity += velocityAbsorption;
			body.state.velocity += body.state.acceleration * Time.deltaTime;
        }

		body.physicsMotion = body.state.velocity * Time.deltaTime;

		body.state.environmentAcceleration = body.state.gravity;
		body.state.acceleration = Vector2.zero;
		body.state.FrictionMultiplier = 1.0f;
		body.state.IsGrounded = body.FlexContacts[2] || body.FlexContacts[3];
    }

	public static void RigidityPass(PhySqBody[] bodies)
	{
		void ApplyAdjustments(PhySqBody body)
		{
			// Adjustment due to rigidity
			var vec = body.adjustmentVector.normalized * body.adjustmentMagnitude;
			body.physicsMotion += vec;
		}

		var firstRun = true;
		for (var i = 0; i < bodies.Length - 1; i++)
		{
			if (firstRun)
				bodies[i].ResetColState();

			for (var j = i + 1; j < bodies.Length; j++)
			{
				if (firstRun)
					bodies[j].ResetColState();

				RigidityAdjustment(bodies[i], bodies[j]);

				if (j == bodies.Length - 1)
					ApplyAdjustments(bodies[j]);
			}
			ApplyAdjustments(bodies[i]);
			firstRun = false;
		}
	}

	public static void FlexPass(PhySqBody[] bodies, bool callResolve = true)
	{
		for (var i = 0; i < bodies.Length - 1; i++)
		{
			for (var j = i + 1; j < bodies.Length; j++)
			{
				FlexResolution(bodies[i], bodies[j]);

				if (callResolve && j == bodies.Length - 1)
					BodyResolution(bodies[j]);
				//{
				//	bodies[j].PhysicsResolve();
				//}
			}
			if (callResolve)
				BodyResolution(bodies[i]);
			//{
			//	bodies[i].PhysicsResolve();
			//}
		}
	}

	public static void ResolutionPass(PhySqBody[] bodies)
    {
		foreach (var body in bodies)
        {
			BodyResolution(body);
        }
    }

	void Update()
	{
		RigidityPass(bodies);
		FlexPass(bodies);
	}
}