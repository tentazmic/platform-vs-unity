using System.Collections;
using System.Collections.Generic;
using PlatformVS.Input;
using UnityEngine;

public class PhySqControl : MonoBehaviour
{
    PhySqBody _body;
    StandardInput _input;
    
    // Start is called before the first frame update
    void Start()
    {
        _body = GetComponent<PhySqBody>();
        _input = new StandardInput(StartCoroutine);
    }

    // Update is called once per frame
    void Update()
    {
        _body.state.velocity = _input.Control.Value * 2;
    }
}
