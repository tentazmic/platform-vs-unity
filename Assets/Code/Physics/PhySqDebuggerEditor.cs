using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(PhySqDebugger))]
public class PhySqDebuggerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var myTarget = (PhySqDebugger)target;

        if (Application.isPlaying && !myTarget.useSystemUpdate && GUILayout.Button("Step")) myTarget.Step();
    }
}
