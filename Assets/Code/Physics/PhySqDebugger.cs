#define RIGIDITY
#define FLEX
#define D_PROJ
#define D_RIGIDITY
#define D_FLEX

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PhySqDebugger : MonoBehaviour
{
	class BodyPositions
	{
		public Vector3[] frameStart = new Vector3[1];
		public Vector3 frameStartPos;
		public Vector3[] projectedPosition = new Vector3[1];
		public Vector3 projectedPos;
		public Vector3[] postRigidity = new Vector3[1];
		public Vector3 postRigidityPos;
		public Vector3[] postFlex = new Vector3[1];
		public Vector3 postRigid;
	}

	[SerializeField] PhySqManager manager;
	public bool useSystemUpdate = false;

	[SerializeField] bool drawRigid = true;
	[SerializeField] bool drawFrameStart = true;
	[SerializeField] bool drawProjected = true;
	[SerializeField] bool drawPostRigidity = true;
	[SerializeField] bool drawPostFlex = true;
	[SerializeField] bool drawPostRigid = true;

	IDictionary<PhySqBody, BodyPositions> _mapBodyPos = new Dictionary<PhySqBody, BodyPositions>();

	private void Start()
	{
		manager.enabled = false;
		Debug.Log("ok");
		foreach (var body in manager.bodies)
		{
			_mapBodyPos.Add(body, new BodyPositions { });
			body.CalculateBoxes();
			body.dbgDraw = false;
			body.dbgMove = false;
        }
	}

	public void Step()
    {
		foreach (var body in manager.bodies)
		{
			//PhySqManager.BodyResolution(body);
			body.physicsMotion = body.state.velocity * Time.deltaTime;
			body.ResetColState();

			var poses = new BodyPositions { };
			poses.frameStart = new Vector3[body.FlexBox.Length];
			poses.projectedPosition = new Vector3[body.FlexBox.Length];
			poses.frameStartPos = body.midPoint.position;
			poses.projectedPos = body.midPoint.position + body.physicsMotion;
			for (int i = 0; i < body.FlexBox.Length; i++)
			{
				poses.frameStart[i] = body.FlexBox[i] + body.midPoint.position;
				poses.projectedPosition[i] = poses.frameStart[i] + body.physicsMotion;
			}
			_mapBodyPos[body] = poses;
		}

#if RIGIDITY
		PhySqManager.RigidityPass(manager.bodies);

		foreach (var body in manager.bodies)
		{
			var poses = _mapBodyPos[body];
			poses.postRigidity = new Vector3[body.FlexBox.Length];
			poses.postRigidityPos = poses.frameStartPos + body.physicsMotion;
			for (int i = 0; i < body.FlexBox.Length; i++)
				poses.postRigidity[i] = poses.frameStart[i] + body.physicsMotion;
		}
#endif

#if FLEX
		PhySqManager.FlexPass(manager.bodies, false);

		foreach (var body in manager.bodies)
		{
			var poses = _mapBodyPos[body];
			poses.postFlex = new Vector3[body.FlexBox.Length];
			var pos = body.midPoint.position;
			for (int i = 0; i < body.FlexBox.Length; i++)
				poses.postFlex[i] = pos + body.FlexBox[i] + body.physicsMotion;
		}

		PhySqManager.ResolutionPass(manager.bodies);

		foreach (var body in manager.bodies)
		{
			//_mapBodyPos[body].postRigid = body.midPoint.position + body.physicsMotion;
			_mapBodyPos[body].postRigid = body.midPoint.position;
        }
#endif
	}

	private void Update()
	{
		if (Time.deltaTime == 0)
			return;

		if (useSystemUpdate) Step();
	}

	void DrawRigid(PhySqBody body, Vector3 pos, int from, int to, Color col)
    {
		if (!drawRigid) return;

		Gizmos.color = col;
		Gizmos.DrawLine(pos + body.RigidBox[from], pos + body.RigidBox[to]);
	}

	private void OnDrawGizmos()
	{
		if (!Application.isPlaying)
			return;

		foreach (var (body, poses) in _mapBodyPos)
		{
			if (poses.frameStart.Length < 3)
				continue;

			var len = poses.frameStart.Length;
			for (int i = 0, j = (i + 1) % len; i < len; i++, j = (++j) % len)
			{
				if (drawFrameStart)
                {
					Gizmos.color = Color.black;
					Gizmos.DrawLine(poses.frameStart[i], poses.frameStart[j]);
					DrawRigid(body, poses.frameStartPos, i, j, new Color(0.5f, 0.5f, 0.5f));
                }

#if D_PROJ
				if (drawProjected)
                {
					Gizmos.color = Color.blue;
					Gizmos.DrawLine(poses.projectedPosition[i], poses.projectedPosition[j]);
					DrawRigid(body, poses.projectedPos, i, j, new Color(0.5f, 0.5f, 1.0f));
                }
#endif
#if D_RIGIDITY
				if (drawPostRigidity)
                {
					Gizmos.color = Color.yellow;
					Gizmos.DrawLine(poses.postRigidity[i], poses.postRigidity[j]);
					DrawRigid(body, poses.postRigidityPos, i, j, new Color(0.2f, 0.55f, 0.55f));
                }
#endif
#if D_FLEX
				if (drawPostFlex)
                {
					Gizmos.color = Color.green;
					Gizmos.DrawLine(poses.postFlex[i], poses.postFlex[j]);
                }

				if (drawPostRigid)
				{
					Gizmos.color = new Color(1.0f, 0.4f, 1.0f);
					Gizmos.DrawLine(poses.postRigid + body.FullBox[i], poses.postRigid + body.FullBox[j]);
					DrawRigid(body, poses.postRigid, i, j, new Color(0.6f, 0.2f, 0.65f));
				}
#endif
			}

#if D_FLEX
			if (drawPostFlex)
				for (int i = 0; i < 4; i++)
				{
					Gizmos.color = body.FlexContacts[i] ? Color.red : Color.green;
					Gizmos.DrawWireSphere(poses.postFlex[i], .06f);
				}
#endif
		}
	}
}
