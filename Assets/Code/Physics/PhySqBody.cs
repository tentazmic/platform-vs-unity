using System.Collections.Generic;
using Code.Core.Fighter;
using UnityEngine;
using System;

public class PhySqBody : MonoBehaviour
{
	public FighterState state;

	public Transform midPoint;
	[SerializeField] Vector2 size = new (1.4f, 2f);
	[Range(0,   1)] public float flexionRatio = 0.4f; // Defines the size of the rigidbox
	[Range(0, 100)] public float rigidity = 5.0f; // How hard the flex box tries to go back to normal and prevent it from deforming
	[Range(0,  10)] public float shockAbsorption = 4f; // How much of the force the body absorbs on collision

	// Top left, top right, bottom right, bottom left
	public readonly Vector3[] RigidBox = new Vector3[4];
	public readonly Vector3[] FlexBox = new Vector3[4];
	public readonly Vector3[] FullBox = new Vector3[4];

	public readonly float[] ShockAbsorptionFactors = { -1.0f, -1.0f, -1.0f, -1.0f };

	// With a bitset this will all fit into one char
	public readonly bool[] RigidContacts = new bool[4]; // LMAO bool array
	public readonly bool[] FlexContacts = new bool[4];

	// For use by the manager, to move the body around due to rigidity and rigidbox overlaps
	[NonSerialized] public Vector3 adjustmentVector;
	[NonSerialized] public float adjustmentMagnitude;

	[NonSerialized] public Vector3 physicsMotion;

	[NonSerialized] public bool dbgDraw = true;
	[NonSerialized] public bool dbgMove = true;

	public bool IsInFlexible => Mathf.Approximately(flexionRatio, 1.0f);

	void Awake()
	{
		state.CurrentFluid = new Fluid();
	}

	void Start()
	{
		CalculateBoxes();
	}

	//public void PhysicsResolve()
	//{
	//	// Shock absorption occurs by the combination of the compression the point and the shock absorption
	//	// Shock absorption reduces the velocity to 0
	//	// Collision reflection occurs when a flex point cannot continue to compress in the
	//	// direction it wants to
	//	// The velocity is reduced a bit and then reflected along the collision plane
	//	var flexionVelocity = Vector2.zero;
	//	for (var i = 0; i < FlexBox.Length; i++)
	//	{
	//		if (ShockAbsorptionFactors[i] < 0) continue;

	//		// Shock absorption
	//		var rigidDiff = (Vector2)(RigidBox[i] - FullBox[i]);
	//		var compressionVector = (Vector2)(FlexBox[i] - FullBox[i]);
	//		var compressionFactor = compressionVector.sqrMagnitude / rigidDiff.sqrMagnitude;
	//		var factor = (shockAbsorption + ShockAbsorptionFactors[i]);
	//		factor *= compressionFactor;
	//		flexionVelocity += factor * compressionVector.normalized;

	//		// Rigidity

	//	}

	//	//state.velocity += flexionVelocity;

	//	if (dbgMove)
	//		state.transform.position += physicsMotion;
	//	state.acceleration += state.environmentAcceleration;
	//	state.velocity += state.acceleration * Time.deltaTime;
	//	physicsMotion = state.velocity * Time.deltaTime;
	//	//Debug.Log(Time.deltaTime);
	//	//physicsMotion = state.velocity / 60.0f; // Temp

	//	state.environmentAcceleration = state.gravity;
	//	state.IsGrounded = FlexContacts[2] || FlexContacts[3];
	//}

	public void ResetColState()
	{
		adjustmentVector = Vector3.zero;
		adjustmentMagnitude = 0;
		for (var i = 0; i < 4; i++)
		{
			FlexBox[i] = FullBox[i];
			RigidContacts[i] = false;
			FlexContacts[i] = false;
		}
	}
	
	public void CalculateBoxes()
	{
		for (var i = 0; i < FlexBox.Length; i++)
		{
			Vector3 diff;
			if (i == 0) diff = new Vector3(-size.x, size.y);
			else if (i == 1) diff = size;
			else if (i == 2) diff = new Vector3(size.x, -size.y);
			else diff = -size;

			RigidBox[i] = diff * flexionRatio;
			FullBox[i] = diff;
			FlexBox[i] = diff;
		}
	}

	void OnDrawGizmos()
	{
		if (!dbgDraw)
			return;

		if (!Application.isPlaying)
			CalculateBoxes();
		var pos = Vector3.zero;
		if (midPoint)
		{
			pos = midPoint.position;
			pos.z = 0;
		}

		for (var i = 0; i < FlexBox.Length; i++)
		{
			Gizmos.color = Color.black;
			if (i < 4)
				Gizmos.DrawLine(FullBox[i] + pos, FullBox[(i + 1) % 4] + pos);

			Gizmos.color = FlexContacts[i] && FlexContacts[(i + 1) % 4] ? Color.red : Color.cyan;
			if (i < 4)
				Gizmos.DrawLine(FlexBox[i] + pos, FlexBox[(i + 1) % 4] + pos);

			Gizmos.color = RigidContacts[i] && RigidContacts[(i + 1) % 4] ? Color.magenta : Color.gray;
			if (i < RigidBox.Length)
				Gizmos.DrawLine(RigidBox[i] + pos, RigidBox[(i + 1) % RigidBox.Length] + pos);
		}

		for (int i = 0; i < 4; i++)
		{
			Gizmos.color = FlexContacts[i] ? Color.red : Color.cyan;
			Gizmos.DrawWireSphere(FlexBox[i] + pos, .08f);
			
			Gizmos.color = RigidContacts[i] ? Color.magenta : Color.gray;;
			Gizmos.DrawSphere(RigidBox[i] + pos, 0.1f);
		}
	}
}
