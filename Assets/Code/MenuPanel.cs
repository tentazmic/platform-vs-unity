using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UniBoost.Extensions;
using UnityEngine;
using UnityEngine.UIElements;
using static UnityEngine.RectTransformUtility;

public class MenuPanel : MonoBehaviour
{
    Action<MenuController.MenuType> _onSelect;
    [SerializeField] MenuController.MenuType type;

    MenuCursor _cursor;
    BoxCollider2D _collider;
    
    public event Action<MenuController.MenuType> OnSelect
    {
        add => _onSelect += value;
        remove => _onSelect -= value;
    }

    void Start()
    {
        // The system iterating over everything will hand this
        _cursor = FindObjectOfType<MenuCursor>();
        _collider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if (_cursor.SelectActive && _collider.OverlapPoint(_cursor.Position))
            _onSelect.Invoke(type);
    }
}
