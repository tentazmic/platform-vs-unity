using System;
using System.Collections;
using System.Collections.Generic;
using PlatformVS.Input;
using UnityEngine;

public class MenuCursor : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5.0f;

    Transform _transform;
    StandardInput _input;

    public Vector2 Position => _transform.position;
    public bool SelectActive { get; private set; } = false;

    void Awake()
    {
        _transform = GetComponent<Transform>();
        _input = new StandardInput(StartCoroutine);
    }

    void Update()
    {
        var vec = _input.Control.Value;
        _transform.position += (Vector3)(vec * (moveSpeed * Time.deltaTime));

        SelectActive = _input.Jump.Down;
    }
}
