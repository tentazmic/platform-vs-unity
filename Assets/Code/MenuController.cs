using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public enum MenuType
    {
        ToOffline, ToOnline, ToSettings
    }

    Animator _animator;

    IDictionary<MenuType, int> _typeAnimMap = new Dictionary<MenuType, int>
    {
        { MenuType.ToOffline, Animator.StringToHash("MainMenu_to_offline") }
    };

    void Awake()
    {
        _animator = GetComponent<Animator>();
        foreach (var panel in FindObjectsOfType<MenuPanel>())
            panel.OnSelect += OnPanelSelect;
    }

    void OnPanelSelect(MenuType type)
    {
        _animator.Play(_typeAnimMap[type], 0);
    }
}
