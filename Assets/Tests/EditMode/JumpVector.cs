using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using Actor;

public class JumpVectorTests : MonoBehaviour
{
    [Test]
    public void T0_Neutral()
    {
        var vel = States.ApplyJump(Vector2.up, 10f, Vector2.zero);
        Assert.AreEqual(Vector2.up * 10.0f, vel);
    }
}
