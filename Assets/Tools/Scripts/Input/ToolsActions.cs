//------------------------------------------------------------------------------
// <auto-generated>
//     This code was auto-generated by com.unity.inputsystem:InputActionCodeGenerator
//     version 1.3.0
//     from Assets/Tools/ToolsActions.inputactions
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace PlatformVS.Tools.Input
{
    public partial class @ToolsActions : IInputActionCollection2, IDisposable
    {
        public InputActionAsset asset { get; }
        public @ToolsActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""ToolsActions"",
    ""maps"": [
        {
            ""name"": ""ActorDebugger"",
            ""id"": ""649fcb2b-836d-4d3b-9df1-55090b1df213"",
            ""actions"": [
                {
                    ""name"": ""FindAllActors"",
                    ""type"": ""Button"",
                    ""id"": ""7ec31935-8fc9-43f7-985c-f3f46b778d2e"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""ToggleDisplay"",
                    ""type"": ""Button"",
                    ""id"": ""80fa651f-4eee-450e-80b9-91c1eae72d55"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                },
                {
                    ""name"": ""ActorsIterate"",
                    ""type"": ""Button"",
                    ""id"": ""5f769c46-0503-41d8-a987-34ec52721d9a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press"",
                    ""initialStateCheck"": false
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""c4a76461-2c81-4964-bdd3-503bf8b17a9e"",
                    ""path"": ""<Keyboard>/f1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""FindAllActors"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""44004f3e-871c-43c5-854f-b3b4368092dc"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""ToggleDisplay"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""18a673be-1932-4934-a9fc-9287dfd0da8f"",
                    ""path"": ""<Keyboard>/tab"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""ActorsIterate"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // ActorDebugger
            m_ActorDebugger = asset.FindActionMap("ActorDebugger", throwIfNotFound: true);
            m_ActorDebugger_FindAllActors = m_ActorDebugger.FindAction("FindAllActors", throwIfNotFound: true);
            m_ActorDebugger_ToggleDisplay = m_ActorDebugger.FindAction("ToggleDisplay", throwIfNotFound: true);
            m_ActorDebugger_ActorsIterate = m_ActorDebugger.FindAction("ActorsIterate", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }
        public IEnumerable<InputBinding> bindings => asset.bindings;

        public InputAction FindAction(string actionNameOrId, bool throwIfNotFound = false)
        {
            return asset.FindAction(actionNameOrId, throwIfNotFound);
        }
        public int FindBinding(InputBinding bindingMask, out InputAction action)
        {
            return asset.FindBinding(bindingMask, out action);
        }

        // ActorDebugger
        private readonly InputActionMap m_ActorDebugger;
        private IActorDebuggerActions m_ActorDebuggerActionsCallbackInterface;
        private readonly InputAction m_ActorDebugger_FindAllActors;
        private readonly InputAction m_ActorDebugger_ToggleDisplay;
        private readonly InputAction m_ActorDebugger_ActorsIterate;
        public struct ActorDebuggerActions
        {
            private @ToolsActions m_Wrapper;
            public ActorDebuggerActions(@ToolsActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @FindAllActors => m_Wrapper.m_ActorDebugger_FindAllActors;
            public InputAction @ToggleDisplay => m_Wrapper.m_ActorDebugger_ToggleDisplay;
            public InputAction @ActorsIterate => m_Wrapper.m_ActorDebugger_ActorsIterate;
            public InputActionMap Get() { return m_Wrapper.m_ActorDebugger; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(ActorDebuggerActions set) { return set.Get(); }
            public void SetCallbacks(IActorDebuggerActions instance)
            {
                if (m_Wrapper.m_ActorDebuggerActionsCallbackInterface != null)
                {
                    @FindAllActors.started -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnFindAllActors;
                    @FindAllActors.performed -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnFindAllActors;
                    @FindAllActors.canceled -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnFindAllActors;
                    @ToggleDisplay.started -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnToggleDisplay;
                    @ToggleDisplay.performed -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnToggleDisplay;
                    @ToggleDisplay.canceled -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnToggleDisplay;
                    @ActorsIterate.started -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnActorsIterate;
                    @ActorsIterate.performed -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnActorsIterate;
                    @ActorsIterate.canceled -= m_Wrapper.m_ActorDebuggerActionsCallbackInterface.OnActorsIterate;
                }
                m_Wrapper.m_ActorDebuggerActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @FindAllActors.started += instance.OnFindAllActors;
                    @FindAllActors.performed += instance.OnFindAllActors;
                    @FindAllActors.canceled += instance.OnFindAllActors;
                    @ToggleDisplay.started += instance.OnToggleDisplay;
                    @ToggleDisplay.performed += instance.OnToggleDisplay;
                    @ToggleDisplay.canceled += instance.OnToggleDisplay;
                    @ActorsIterate.started += instance.OnActorsIterate;
                    @ActorsIterate.performed += instance.OnActorsIterate;
                    @ActorsIterate.canceled += instance.OnActorsIterate;
                }
            }
        }
        public ActorDebuggerActions @ActorDebugger => new ActorDebuggerActions(this);
        private int m_KeyboardSchemeIndex = -1;
        public InputControlScheme KeyboardScheme
        {
            get
            {
                if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
                return asset.controlSchemes[m_KeyboardSchemeIndex];
            }
        }
        public interface IActorDebuggerActions
        {
            void OnFindAllActors(InputAction.CallbackContext context);
            void OnToggleDisplay(InputAction.CallbackContext context);
            void OnActorsIterate(InputAction.CallbackContext context);
        }
    }
}
