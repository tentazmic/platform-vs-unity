# PlatformVS

This project houses the Unity prototype of my Platform VS game (working title).
Not meant to be developed into a formal game.

Until I create the first proper release of this game this prototype project will
never truly be done, thus it will go on hiatuses when I have reached certain
milestones.

I've come back to this project after a break to work on a small game using the game engine.
I had planned to continue working on this project until I got to the previously stated goals
however I can see all the flaws in this codebase, they've gotten even harder for me to
ignore and I'm fairly certain I would need to rewrite some of it to get to those goals.
If I am going to rewrite these systems, I would much rather that be in the final engine.

I do feel a bit bad for not properly creating the planned vertical slice, but this project
has already done the job of a prototype:

- I've iterated through several implementations of a state macine
- I've created a success version of a custom physics solution
- It's informed me of how I should go about implementing the various fundamental systems in the proper project

Me being me, the one thing I haven't been able to properly experiment with relate to art and
sound.

I'll return back to this project to experiment with visuals, sound and isolated systems
when I feel the need.
